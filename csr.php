<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Elite Yoga</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="icon"  href="images/favicon.png">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<link href="fonts/fonts.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" href="css/animate.css">
<link href="css/style.css" type="text/css" rel="stylesheet" />
<link href="css/hover.css" type="text/css" rel="stylesheet" />
<link href="slider/jquery.bxslider.css" rel="stylesheet" type="text/css" />
<link href="css/responsive.css" type="text/css" rel="stylesheet" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="js/parallax.js"></script>
<script type="text/javascript" src="js/wow.min.js"></script>
<script>
    wow = new WOW();
    wow.init();   
  </script>
<script type="text/javascript" src="slider/jquery.bxslider.min.js"></script>
<script src="js/common.js"></script>

</head>

<body>
<?php include_once('header.php');?>
<div class="middle_page_part">
	<div class="site">
        <h2>Corporate Social Responsibility</h2>
        <p>Elite Yoga's core strength is to make a difference and help others achieve a healthy mind, body and soul.</p>
        
        <div class="csr_content_top">
        	<p class="wow fadeIn">Elite Yoga Retreats are extremely passionate about making a difference for the greater good. The very essence of the brand is to help those individuals who maybe going through a difficult time or battling a certain illness.</p>
            <p class="wow fadeIn">Fighting cancer is the toughest battle one can face, the physical and emotional challenges it can bring are enormous.</p> 
            <div class="about_content_inner wow slideInDown"> However, wide spread studies have shown that whilst there is a lack of scientific evidence that it can cure cancer, it can most definitely improve physical symptoms such as fatigue and pain. </div>
        </div>
        
		<div class="csr_part">
        	<div class="csr_banner_left csr_banner slideInLeft wow">
            	<img src="images/csr01.jpg" />
            </div>
            <div class="csr_banner_middle csr_banner slideInUp wow">
            	<img src="images/csr02.jpg" />
            </div>
            <div class="csr_banner_right csr_banner slideInRight wow">
                <img src="images/csr03.jpg" />
            </div>
        </div>
        
        <div class="csr_content_bottom">
        	<div class="tailor_package_text wow slideInDown">
            	<label>Yoga along with meditation can also assist<br/>in tackling anxiety and depression, which is a common reaction that many cancer patients are faced with.</label>
            </div>
        	<p class="wow fadeIn">The grueling treatment that suffers have to undergo can cause mood swings and withdrawal from social gatherings. Yoga can help as it releases endorphins and these help improve your mood. A regular yoga practice, no matter how gentle the movements, allows the body to release endorphins so you can instantly experience a positive boost in mood as highlighted in a recent report by huffpost <a href="http://www.huffingtonpost.com/lorna-borenstein-/4-ways-cancer-patients-ca_b_6094964.html" target="_blank">healthy living</a>).</p>
            <p class="wow fadeIn">Having a yoga group for cancer suffers can help provide a support mechanism for those that may find it difficult to speak to their families or friends about how they are feeling. Engaging with those that are going through a similar experience helps alleviate the impacts of cancer. After all a 'problem shared is a problem halved.'</p> 
            <div class="tailor_package_text wow slideInDown">
                <p>We are proud to announce that in the coming months, <br/>we will be partnering with cancer charities to help those battling the disease find strength through yoga and escape to luxurious surroundings.</p>
            	<span class="slideInRight wow"><a href="contactus">Please watch this space<br/>and feel free to contact us for more info.</a></span>
            </div>
        </div>
    </div>
</div>

<?php include_once('our_partners.php');?>
<?php include_once('footer.php');?>

</body>
</html>
