<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Elite Yoga</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="icon"  href="images/favicon.png">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<link href="fonts/fonts.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" href="css/animate.css">
<link href="css/style.css" type="text/css" rel="stylesheet" />
<link href="css/hover.css" type="text/css" rel="stylesheet" />
<link href="slider/jquery.bxslider.css" rel="stylesheet" type="text/css" />
<link href="css/responsive.css" type="text/css" rel="stylesheet" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="js/parallax.js"></script>
<script type="text/javascript" src="js/wow.min.js"></script>
<script>
    wow = new WOW();
    wow.init();   
  </script>
<script type="text/javascript" src="slider/jquery.bxslider.min.js"></script>
<script src="js/common.js"></script>

</head>

<body>
<?php include_once('header.php');?>
<div class="middle_page_part">
	<div class="site">
        <h2>About Elite Yoga</h2>
        <p>Yoga is no longer confined to a yoga studio, Elite yoga is revolutionizing the way yoga is performed<br/>by allowing you to experience the world.</p>
    
    <div class="about_left">
        <div class="about_img wow slideInLeft">
            <img src="images/aboutus.jpg" />
            <div class="border_div_about">
            </div>
        </div> 
        <div class="testmonial_text wow fadeInUp">
            <span class="left_qu"></span>Escape the winter blues to the wonders of UAE's and Bali's finest 5 star hotels!<span class="right_qu"></span>
        </div>
    </div>
    
    <div class="about_content wow slideInRight">
    	<p>Elite Yoga Retreats was set up in 2015 to cater for a niche that wanted good quality yoga in 5 star surroundings.</p>
<p>What differentiates us from all the other retreats on offer is that our focus is on quality and the overall yoga experience for the customer.</p>
<div class="about_content_inner wow slideInRight">
	    Whether you have done yoga before or not, this retreat caters for those that want to combine a luxurious holiday with good quality yoga.
</div>
<p>Our extensive research on existing retreats on offer has revealed that whilst in some cases the yoga has been great, the accommodation has been mediocre or the food or facilities on offer has lacked excitement, bringing down the whole experience or vice versa.</p>
<p>You want to feel amazing after a yoga retreat and we have found the winning formula: Pick up from the airport in style, stunning location with amazing facilities, scrumptious food and a yoga teacher you can connect with, who doesn't only work on your physical fitness but removes some of the minds hidden demons, making you feel lighter and fabulous.</p>
    </div>  
    <div class="border_div_brown wow slideInLeft about_border"></div>
   <!-- <h2 class=" wow slideInRight">Founder's Note</h2>
    <p>In the sanctuary of a yoga studio, a retreat or a tranquil spot in the<br />countryside, It's easier.</p>
    <div class="about_ceo_img  wow slideInRight">
    	<img src="images/aboutus_ceo.jpg" />
        <div class="border_div_about">
        </div>
    </div>
    <div class="ceo_left  wow slideInLeft">
    	<p>Phasellus ultrices nulla quis nibh. Quisque a lectus. Donec consectetuer ligula vulputate sem tristique cursus. Nam nulla quam, gravida non, commodo a, sodales sit amet, nisi. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan 
porttitor, facilisis luctus, metus.</p>
<p>Phasellus ultrices nulla quis nibh. Quisque a lectus. Donec consectetuer ligula vulputate sem tristique cursus. Nam nulla quam, gravida non, commodo a, sodales sit amet, nisi.
Pellentesque fermentum dolor. Aliquam quam lectus, facilisis auctor, ultrices ut, 
elementum vulputate, nunc.</p>
<p>Sed adipiscing ornare risus. Morbi est est, blandit sit amet, sagittis vel, euismod vel, velit. Pellentesque egestas sem. Suspendisse commodo 
ullamcorper magna.</p>
<p>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, 
accumsan porttitor, facilisis luctus, metus.</p>
<p>Pellentesque fermentum dolor. Aliquam quam lectus, facilisis auctor, ultrices ut, 
elementum vulputate, nunc.</p>
<p><label class="ceo_label">Gagandeep Gill</label>
Founder and CEO</p>
<img src="images/ceo_sign.jpg" />
    </div>-->
    
    </div> 
      
</div>

<?php include_once('our_partners.php');?>
<?php include_once('footer.php');?>

</body>
</html>
