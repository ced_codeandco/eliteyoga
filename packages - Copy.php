<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>Elite Yoga</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="images/favicon.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link href="fonts/fonts.css" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/remodal.css">
    <link rel="stylesheet" href="css/remodal-default-theme.css">
    <link rel="stylesheet" href="css/slider-pro.min.css">
    <link href="css/style.css" type="text/css" rel="stylesheet" />
    <link href="css/hover.css" type="text/css" rel="stylesheet" />
    <link href="slider/jquery.bxslider.css" rel="stylesheet" type="text/css" />
    <link href="css/responsive.css" type="text/css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script type="text/javascript" src="js/wow.min.js"></script>
    <script src="js/parallax.js"></script>
    <script src="js/remodal.min.js"></script>
    <script src="js/jquery.sliderPro.min.js"></script>
    <script src="js/custom-packages.js"></script>
    <script>
    wow = new WOW();
    wow.init();
    </script>
    <script type="text/javascript" src="slider/jquery.bxslider.min.js"></script>
    <script src="js/common.js"></script>
</head>

<body>
    <?php include_once('header.php');?>
    <section class="main-section ">
        <header>
            <h2>Packages</h2>
            <p>In the sanctuary of a yoga studio, a retreat or a tranquil spot in the
                countryside, It's easier.</p>
        </header>
        <div class="container resort-packages">
            <div class="row">
                <h2>Waldorf Astoria, <span>Palm Jumeriah, Dubai</span></h2>
                <div class="col-md-9">
                    <!-- property images slider HTML  -->
                    <div class="property-images slider-pro">
                        <div class="sp-slides">
                            <div class="sp-slide">
                                <img src="images/slider/w-astoria/2.jpg" alt="property image">
                            </div>
                            <div class="sp-slide">
                                <img src="images/slider/w-astoria/2.jpg" alt="property image">
                            </div>
                            <div class="sp-slide">
                                <img src="images/slider/w-astoria/2.jpg" alt="property image">
                            </div>
                        </div>
                        <div class="sp-thumbnails">
                            <img class="sp-thumbnail" src="images/slider/w-astoria/thumb-1.jpg" alt="property image thumb">
                            <img class="sp-thumbnail" src="images/slider/w-astoria/thumb-2.jpg" alt="property image thumb">
                            <img class="sp-thumbnail" src="images/slider/w-astoria/thumb-3.jpg" alt="property image thumb">
                        </div>
                    </div>
                    <!-- End of property slider -->
                    
                </div>
                <div class="col-md-3">
                    <div class="package-writeup">
                        <h4>Premier Yoga</h4>
                        <p>Includes pick up in a lexus or prado <em>(Transfers)</em>, breakfast, lunch and dinner as well as tea, and water provided for in your room. 8 yoga sessions with the amazing Derya Pasinler’and exclusive Elite Yoga mat with extras</p>
                        <em>(package excludes flights)</em>
                        <a href="" class="btn btn-shareit"><i class="fa fa-share-alt"></i>Share This</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                	<ul class="logo_packages">
                    	<li><img src="images/packages/world_travel.png" /><label>Middle East's Leading Luxury Resort <br />Dubai's Leading Hotel Suite - Royal Suite</label></li>
                        <li class="mena_logo"><img src="images/packages/mena.jpg" /><label>Best 5-star New Resort</label></li>
                        <li><img src="images/packages/world_travel2014.png" /><label>World's Leading New Resort<br />Middle East's Leading New Resort</label></li>
                        <li><img src="images/packages/business.png" /><label>Best Luxury Hotel</label></li>
                    </ul>
                </div>
             </div>    
            <!-- End row -->
            <div class="row">
                <div class="col-md-12">
                    <!-- Pricing table -->
                    <table class="pricing-table">
                        <thead>
                            <tr>
                                <th>Dates of retreats (5 days Sun to Thurs)</th>
                                <th>1 Person</th>
                                <th>2 Persons</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>16th Oct to 20th Oct 2016</td>
                                <td>AED9450 / £1890 / $2576</td>
                                <td>AED6750 / £1350 / $1840</td>
                                <td><a href="" class="btn btn-booknow">BOOK NOW</a></td>
                            </tr>
                            <tr>
                                <td>23rd Oct to 27th Oct 2016</td>
                                <td>AED9450 / £1890 / $2576</td>
                                <td>AED6750 / £1350 / $1840</td>
                                <td><a href="" class="btn btn-booknow">BOOK NOW</a></td>
                            </tr>
                            <tr>
                                <td>30th Oct to 3rd Oct  2016</td>
                                <td>AED9450 / £1890 / $2576</td>
                                <td>AED6750 / £1350 / $1840</td>
                                <td><a href="" class="btn btn-booknow">BOOK NOW</a></td>
                            </tr>
                            <tr class="show-more">
                                <td>6th Nov to 10th Nov  2016</td>
                                <td>AED9450 / £1890 / $2576</td>
                                <td>AED6750 / £1350 / $1840</td>
                                <td><a href="" class="btn btn-booknow">BOOK NOW</a></td>
                            </tr>
                            <tr class="show-more">
                                <td>13th Nov to 17th Nov 2016</td>
                                <td>AED9450 / £1890 / $2576</td>
                                <td>AED6750 / £1350 / $1840</td>
                                <td><a href="" class="btn btn-booknow">BOOK NOW</a></td>
                            </tr>
                            <tr class="show-more">
                                <td>15th Jan to 19th Jan  2016</td>
                                <td>AED9450 / £1890 / $2576</td>
                                <td>AED6750 / £1350 / $1840</td>
                                <td><a href="" class="btn btn-booknow">BOOK NOW</a></td>
                            </tr>
                            
                            
                            <tr class="show-more">
                                <td>22nd Jan to 26th Jan 2016</td>
                                <td>AED9450 / £1890 / $2576</td>
                                <td>AED6750 / £1350 / $1840</td>
                                <td><a href="" class="btn btn-booknow">BOOK NOW</a></td>
                            </tr>
                            <tr class="show-more">
                                <td>12th Feb to 16th Feb 2016</td>
                                <td>AED9450 / £1890 / $2576</td>
                                <td>AED6750 / £1350 / $1840</td>
                                <td><a href="" class="btn btn-booknow">BOOK NOW</a></td>
                            </tr>
                            <tr class="show-more">
                                <td>19th Feb to 23rd Feb 2016</td>
                                <td>AED9450 / £1890 / $2576</td>
                                <td>AED6750 / £1350 / $1840</td>
                                <td><a href="" class="btn btn-booknow">BOOK NOW</a></td>
                            </tr>
                            <tr class="show-more">
                                <td>19th Mar to 23rd Mar 2016</td>
                                <td>AED9450 / £1890 / $2576</td>
                                <td>AED6750 / £1350 / $1840</td>
                                <td><a href="" class="btn btn-booknow">BOOK NOW</a></td>
                            </tr>
                            <tr class="show-more">
                                <td>9th Apr to 13th Apr 2016</td>
                                <td>AED9450 / £1890 / $2576</td>
                                <td>AED6750 / £1350 / $1840</td>
                                <td><a href="" class="btn btn-booknow">BOOK NOW</a></td>
                            </tr>
                        </tbody>
                    </table>
                    <!-- Pricing table -->
                </div>
            </div>
            <div class="arrow-down-wrapper">
                <a href="" class="btn-arrow-down">
                    <i class="fa fa-chevron-down"></i>
                </a>
            </div>
            <div class="terms-conditions">
                <small>I agree to the Terms &amp; Conditions. <a href="#modal">Click here to view.</a></small>
            </div>
        </div>
        <div class="container resort-packages">
            <div class="row">
                <h2>Four Seasons Restort, <span>Jumeriah, Dubai</span></h2>
                <div class="col-md-9">
                    <!-- property images slider HTML  -->
                    <div class="property-images slider-pro">
                        <div class="sp-slides">
                            <div class="sp-slide">
                                <img src="images/slider/four-s/2.jpg" alt="property image">
                            </div>
                            <div class="sp-slide">
                                <img src="images/slider/four-s/2.jpg" alt="property image">
                            </div>
                            <div class="sp-slide">
                                <img src="images/slider/four-s/2.jpg" alt="property image">
                            </div>
                        </div>
                        <div class="sp-thumbnails">
                            <img class="sp-thumbnail" src="images/slider/four-s/thumb-1.jpg" alt="property image thumb">
                            <img class="sp-thumbnail" src="images/slider/four-s/thumb-2.jpg" alt="property image thumb">
                            <img class="sp-thumbnail" src="images/slider/four-s/thumb-3.jpg" alt="property image thumb">
                        </div>
                    </div>
                    <!-- End of property slider -->
                </div>
                <div class="col-md-3">
                    <div class="package-writeup">
                        <h4>Superior Yoga</h4>
                        <p>Includes pick up <em>(Transfers),</em>, breakfast and dinner as well as tea, water and fruit provided for in your room. Complimentary wifi service, 8 yoga sessions with the amazing ‘Derya Pasinler’ and exclusive Elite Yoga mat with extras</p>
                        <em>(package excludes flights)</em>
                        <a href="" class="btn btn-shareit"><i class="fa fa-share-alt"></i>Share This</a>
                    </div>
                </div>
            </div>
            <!-- End row -->
            <div class="row">
                <div class="col-md-12">
                    <!-- Pricing table -->
                    <table class="pricing-table">
                        <thead>
                            <tr>
                                <th>Dates of retreats (5 days Sun to Thurs)</th>
                                <th>1 Person</th>
                                <th>2 Persons</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>09th Oct to 13th Oct 2016</td>
                                <td>AED13350 / £2670 / $3637</td>
                                <td>AED9000 / £1800 / $2452</td>
                                <td><a href="" class="btn btn-booknow">BOOK NOW</a></td>
                            </tr>
                            <tr>
                                <td>08th Jan to 12th Jan 2017</td>
                                <td>AED13950 / £2790 / $3801</td>
                                <td>AED9300 / £1860 / $2534</td>
                                <td><a href="" class="btn btn-booknow">BOOK NOW</a></td>
                            </tr>
                            <tr>
                                <td>29th Jan to 02nd Feb 2017</td>
                                <td>AED13950 / £2790 / $3801</td>
                                <td>AED9300 / £1860 / $2534</td>
                                <td><a href="" class="btn btn-booknow">BOOK NOW</a></td>
                            </tr>
                            <tr class="show-more">
                                <td>05th Feb to 09th Feb 2017</td>
                                <td>AED13950 / £2790 / $3801</td>
                                <td>AED9300 / £1860 / $2534</td>
                                <td><a href="" class="btn btn-booknow">BOOK NOW</a></td>
                            </tr>
                            <tr class="show-more">
                                <td>26th Feb to 02nd Mar 2017</td>
                                <td>AED13950 / £2790 / $3801</td>
                                <td>AED9300 / £1860 / $2534</td>
                                <td><a href="" class="btn btn-booknow">BOOK NOW</a></td>
                            </tr>
                            <tr class="show-more">
                                <td>05th Mar to 09th Mar 2017</td>
                                <td>AED13550 / £2670 / $3637</td>
                                <td>AED9000 / £1800 / $2452</td>
                                <td><a href="" class="btn btn-booknow">BOOK NOW</a></td>
                            </tr>
                            
                            <tr class="show-more">
                                <td>12th Mar to 16th Mar 2017</td>
                                <td>AED13950 / £2790 / $3801</td>
                                <td>AED9300 / £1860 / $2534</td>
                                <td><a href="" class="btn btn-booknow">BOOK NOW</a></td>
                            </tr>
                            <tr class="show-more">
                                <td>02nd Apr to 06th Apr 2017</td>
                                <td>AED13950 / £2790 / $3801</td>
                                <td>AED9300 / £1860 / $2534</td>
                                <td><a href="" class="btn btn-booknow">BOOK NOW</a></td>
                            </tr>
                        </tbody>
                    </table>
                    <!-- Pricing table -->
                </div>
            </div>
            <div class="arrow-down-wrapper">
                <a href="" class="btn-arrow-down">
                    <i class="fa fa-chevron-down"></i>
                </a>
            </div>
            <div class="terms-conditions">
                <small>I agree to the Terms &amp; Conditions. <a href="#modal">Click here to view.</a></small>
            </div>
        </div>
        <div class="container resort-packages">
            <div class="row">
                <h2>Sayan Four Seasons Resort, <span>Bali</span></h2>
                <div class="col-md-9">
                    <!-- property images slider HTML  -->
                    <div class="property-images slider-pro">
                        <div class="sp-slides">
                            <div class="sp-slide">
                                <img src="images/slider/sayan-f/2.jpg" alt="property image">
                            </div>
                            <div class="sp-slide">
                                <img src="images/slider/sayan-f/2.jpg" alt="property image">
                            </div>
                            <div class="sp-slide">
                                <img src="images/slider/sayan-f/2.jpg" alt="property image">
                            </div>
                        </div>
                        <div class="sp-thumbnails">
                            <img class="sp-thumbnail" src="images/slider/sayan-f/thumb-1.jpg" alt="property image thumb">
                            <img class="sp-thumbnail" src="images/slider/sayan-f/thumb-2.jpg" alt="property image thumb">
                            <img class="sp-thumbnail" src="images/slider/sayan-f/thumb-3.jpg" alt="property image thumb">
                        </div>
                    </div>
                    <!-- End of property slider -->
                </div>
                <div class="col-md-3">
                    <div class="package-writeup">
                        <h4>Elite Yoga</h4>
                        <p>Sayan Four Seasons Resort, Bali includes pick up <em> (Transfers)</em>, breakfast, 3 course dinner as well as tea, water and fruit provided for in your room. Complimentary wifi service, 15 yoga sessions and an exclusive Batik Yoga mat</p>
                        <em>(package excludes flights)</em>
                        <a href="" class="btn btn-shareit"><i class="fa fa-share-alt"></i>Share This</a>
                    </div>
                </div>
            </div>
            <!-- End row -->
            <div class="row">
                <div class="col-md-12">
                    <!-- Pricing table -->
                    <table class="pricing-table">
                        <thead>
                            <tr>
                                <th>Dates of retreats <em>(5 days Sun to Thurs)</em></th>
                                <th>Package $</th>
                                <th>2 Person $</th>
                                <th>Package for 3-4 max</th>
                                <th>Resident Yogi Master</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>01st May to 05th May 2016</td>
                                <td>4209.2</td>
                                <td>2760.6</td>
                                <td rowspan="9">
                                    Referal will be sent to <br />
                                    elite yoga and we will <br />
                                    manual price this
                                </td>
                                <td>Keiko Onishi <em>(Iyengar)</em></td>
                                <td><a href="" class="btn btn-booknow">BOOK NOW</a></td>
                            </tr>
                            <tr>
                                <td>09th May to 13th May 2016</td>
                                <td>2790</td>
                                <td>1860</td>
                                <td>Keiko Onishi <em>(Iyengar)</em></td>
                                <td><a href="" class="btn btn-booknow">BOOK NOW</a></td>
                            </tr>
                            <tr>
                                <td>01st June to 5th June 2016</td>
                                <td>2790</td>
                                <td>1860</td>
                                <td>Steven Bracken <em>(Scaravelli Inspired Yoga)</em></td>
                                <td><a href="" class="btn btn-booknow">BOOK NOW</a></td>
                            </tr>
                            <tr class="show-more">
                                <td>09th June to 13th June 2016</td>
                                <td>2790</td>
                                <td>1860</td>
                                <td>Steven Bracken <em>(Scaravelli Inspired Yoga)</em></td>
                                <td><a href="" class="btn btn-booknow">BOOK NOW</a></td>
                            </tr>
                            <tr class="show-more">
                                <td>05th Sep to 09th Sep 2016</td>
                                <td>2790</td>
                                <td>1860</td>
                                <td>Niki Dalhammer <em>(Kundalini)</em></td>
                                <td><a href="" class="btn btn-booknow">BOOK NOW</a></td>
                            </tr>
                            <tr class="show-more">
                                <td>12th Sep to 16th Sep 2016</td>
                                <td>2790</td>
                                <td>1860</td>
                                <td>Niki Dalhammer <em>(Kundalini)</em></td>
                                <td><a href="" class="btn btn-booknow">BOOK NOW</a></td>
                            </tr>
                            
                            <tr class="show-more">
                                <td>03rd Oct to 07th Oct 2016</td>
                                <td>2790</td>
                                <td>1860</td>
                                <td>Matt Jullian <em>(Vinyasa)</em></td>
                                <td><a href="" class="btn btn-booknow">BOOK NOW</a></td>
                            </tr>
                            <tr class="show-more">
                                <td>05th Dec to 09th Dec 2016 </td>
                                <td>2790</td>
                                <td>1860</td>
                                <td>Matt Jullian <em>(Vinyasa)</em></td>
                                <td><a href="" class="btn btn-booknow">BOOK NOW</a></td>
                            </tr>
                            <tr class="show-more">
                                <td>12th Dec to 16th Dec 2016  </td>
                                <td>2790</td>
                                <td>1860</td>
                                <td>Faye Riches <em>(Chakra Yoga/Vinyasa)</em></td>
                                <td><a href="" class="btn btn-booknow">BOOK NOW</a></td>
                            </tr>
                        </tbody>
                    </table>
                   <!--  Pricing table -->
                </div>
            </div>
            <div class="arrow-down-wrapper">
                <a href="" class="btn-arrow-down">
                    <i class="fa fa-chevron-down"></i>
                </a>
            </div>
            <div class="terms-conditions">
                <small>I agree to the Terms &amp; Conditions. <a href="#modal">Click here to view.</a></small>
            </div>
        </div>
    </section>
    <?php include_once('our_partners.php');?>
    <?php include_once('footer.php');?>


    <!-- Modal HTML below -->
    <div class="remodal tandc-modal" data-remodal-id="modal">
      <button data-remodal-action="close" class="remodal-close"></button>

      <h2>Terms and Conditions</h2>  

<h4>NON REFUNDABLE</h4>
<p>Once booking has been made and accepted by <span>elite yoga retreats</span> the packages are non refundable however you can change your booking to an alternative date as listed on the website subject to availability and may incur additional costs, by emailingour customer services at, <a href="mailto:bookings@eliteyoga.co">bookings@eliteyoga.co</a> or by calling us on +971 4338 5616 or +971 567120725.</p>
<h4>PRICES</h4>
<p>The price of the travel services will be as quoted on the website from time to time, except in cases of obvious error. prices are liable to change at any time, but changes will not affect bookings already accepted. despite <span>elite yoga retreats</span> best efforts, some of the travel services listed on the website may be incorrectly priced. <span>elite yoga retreats</span> expressly reserves the right to correct any pricing errors on our website and/or on pending reservations made under an incorrect price. in such event, if available, we will offer you the opportunity to keep your pending reservation at the correct price or we will cancel your reservation without penalty. <span>elite yoga retreats</span> is under no obligation to provide travel services to you at an incorrect (lower) price, even after you have been sent confirmation of your booking.</p>
<h4>SUPPLIERS</h4>
<p>The carriers, instructors, hotels and other suppliers providing travel or other services for <span>elite yoga retreatsare</span> independent contractors and not agents or employees of <span>elite yoga retreats</span> or its affiliates. <span>elite yoga retreats</span> are not liable for the acts, errors, omissions, representations, warranties, breaches or negligence of any such suppliers or for any personal injuries, death, property damage, or other damages or expenses resulting therefrom.</p>
<p><span>Elite yoga retreats</span> have no liability and will make no refund in the event of any delay, cancellation, overbooking, strike, force majeure or other causes beyond their direct control, and they have no responsibility for any additional expense, omissions, delays, re-routing or acts of any government or authority.</p>
<p>In no event shall <span>elite yoga retreats</span>, its affiliates, and/or their respective suppliers be liable for any direct, indirect, punitive, incidental, special, or consequential damages arising out of, or in any way connected with, the use of this website or with the delay or inability to use this website, or for any information, software, products, and services obtained through this website, or otherwise arising out of the use of this website, whether based on contract, tort, strict liability, or otherwise, even if <span>elite yoga retreats</span>, its affiliates, and/or their respective suppliers have been advised of the possibility of damages.</p>
<p>because some states/jurisdictions do not allow the exclusion or limitation of liability for consequential or incidental damages, the above limitation may not apply to you.</p>
<h4>LINKS TO THIRD-PARTY SITES</h4>
<p>This website may contain hyperlinks to websites operated by parties other than <span>elite yoga retreats</span>. such hyperlinks are provided for your reference only. <span>elite yoga retreats</span> does not control such websites and is not responsible for their contents or your use of them.</p>
<p><span>Elite yoga retreats</span> inclusion of hyperlinks to such websites does not imply any endorsement of the material on such websites or any association with their operators.</p>
<h4>INDEMNIFICATION</h4>
<p>You agree to defend and indemnify <span>elite yoga retreats</span>, its affiliates, and/or their respective suppliers and any of their officers, directors, employees and agents from and against any claims, causes of action, demands, recoveries, losses, damages, fines, penalties or other costs or expenses of any kind or nature including but not limited to reasonable legal and accounting fees, brought by you or on your behalf in excess of the liability described above; or by third parties as a result of your breach of this agreement or the documents referenced herein.</p>
<h4>NO UNLAWFUL OR PROHIBITED USE</h4>
<p>As a condition of your use of this website, you warrant that you will not use this website for any purpose that is unlawful or prohibited by these terms, conditions, and notices</p>
<h4>SOFTWARE AVAILABLE ON THIS WEBSITE</h4>
<p>Any software that is made available to download from this website ("software") is the copyrighted work of <span>elite yoga retreats</span>, its affiliates, and/or their suppliers. your use of the software is governed by the terms of the end user license agreement, if any, which accompanies, or is included with, the software ("license agreement"). you may not install or use any software that is accompanied by or includes a license agreement unless you first agree to the license agreement terms. for any software not accompanied by a license agreement, <span>elite yoga retreats</span> here by grants to you, the user, a personal, nontransferable license to use the software for viewing and otherwise using this website in accordance with these terms and conditions and for no other purpose. please note that all software, including, without limitation, all html code and active x controls contained on this website, is owned by <span>elite yoga retreats</span>, its affiliates, and/or their suppliers, and is protected by copyright laws and international treaty provisions. any reproduction or redistribution of the software is expressly prohibited by law, and may result in severe civil and criminal penalties. violators will be prosecuted to the maximum extent possible.</p>
<p>Without limiting the foregoing, copying or reproduction of the software to any other server or location for further reproduction or redistribution is expressly prohibited. the software is warranteed, if at all, only according to the terms of the license agreement. you acknowledge that the software and any accompanying documentation and/or technical information are subject to applicable export control laws and regulations of the uae. you agree not to export or re-export the software, directly or indirectly, to any countries that are subject to uae export restrictions.</p>
<p><input type="checkbox" id="accept" class="checkbox" /><label for="accept">Yes you accept the terms and conditions.</label></p>
<p><input type="checkbox" id="noaccept" class="checkbox" /><label for="noaccept">No</label></p>
    


      <br>
      <!-- <button data-remodal-action="cancel" class="remodal-cancel btn btn-shareit">Cancel</button> -->
    </div>

    <!-- End modal HTML -->
</body>

</html>
