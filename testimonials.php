<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>Elite Yoga</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="images/favicon.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link href="fonts/fonts.css" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" href="css/animate.css">
    <link href="css/style.css" type="text/css" rel="stylesheet" />
    <link href="css/hover.css" type="text/css" rel="stylesheet" />
    <link href="slider/jquery.bxslider.css" rel="stylesheet" type="text/css" />
    <link href="css/responsive.css" type="text/css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="js/parallax.js"></script>
    <script src="js/jquery.waypoints.min.js"></script>
    <script src="js/custom-testimonials.js"></script>
    <script type="text/javascript" src="js/wow.min.js"></script>
    <script>
    wow = new WOW();
    wow.init();
    </script>
    <script type="text/javascript" src="slider/jquery.bxslider.min.js"></script>
    <script src="js/common.js"></script>
</head>

<body>
    <?php include_once('header.php');?>
    <section class="main-section">
        <header>
            <h2>Testimonials</h2>
            <p>Don't just take it from us, let our customers do the talking!</p>
        </header>

        <div class="container testimonials-wrapper">
            

                <div class="row">
                    <div class="col-md-6">
                        
                        
                        <blockquote data-animation-name="fadeInLeft">
                            <p>Elite Yoga Retreats are definitely a class above the rest. I have been on many yoga retreats but this was on another level. The hotel where we stayed was absolutely magnificent and the food was amazing. I felt a million dollars afterwards and can't wait to book another retreat in 2016. Every intricate detail had been addressed to ensure my 5-day yoga retreat was nothing short of pure luxury and bliss. I absolutely loved the home- made granola, which even had koji berries in it. It tasted divine with the yogurt and the fresh juices were lush. Can't wait to come back to the Four Season Dubai!<span class="close-quote"></span>
                        </p>
                            <footer>
                                <cite>Zoe Ball, </cite>
                                <p>A yoga retreat enthusiast</p>
                            </footer>
                        </blockquote>



                        <blockquote data-animation-name="fadeInLeft">
                            <p>I had an absolutely amazing time. I work extremely long hours in the city as an Investment banker and wanted to have a relaxing break with some yoga built in. It was the perfect combination and the whole experience was 5 star and nothing less. The Waldorf was breathtaking, after the yoga I would soak in some much needed vitamin D by the pool and enjoy the skyline of Dubai with it's iconic structures in the background from the Burj Al Arab to the Burj Khalifa.</p>

                        <p>The spa facilities were outstanding at the Waldorf, they even had an herbal steam room, which was invigorating and made my skin glow.</p>

                        <p>I actually feel like a new person, this was exactly the escape I needed from the hustle and bustle of the City of London.</p>
                        <p>The group of Yogi's on the retreat were a super bunch and the energy we created whilst performing the postures on the beautiful landscaped areas of the resort was phenomenal.</p>

                        <p>Thanks again Elite Yoga Retreats - you did it again!<span class="close-quote"></span></p>
                            <footer>
                                <cite>Tom Davidson,  </cite>
                                <p>A rejuvenated City Slicker</p>

                            </footer>
                        </blockquote>

                    </div>
                    <div class="col-md-6">
                        

                        <blockquote data-animation-name="fadeInRight">
                            <p>I had been going through a rough patch and just recently broke up with my long-term partner, so I decided to go on one of the Elite Yoga Retreats. It was magnificent; the yoga sessions undertaken by Derya not only improved my physical fitness but also helped with my mind. It enabled me to let go of some of the anger I had built up inside and free myself. She was great even though I was a newbie to Yoga.</p>

                            <p>The energy and aura she gave out was captivating, I can't wait to come back again next year.</p>
                            
                            <p>The Palm Dubai was the perfect venue; it offered breath-taking views of Dubai and doing yoga on the Arabian Sea with the dusty purple sunset in the background was truly magical.</p>

                            <p>The food was nothing short of impeccable; the breakfast buffet was extravagant and lavish with so many choices from fresh juices, fruit, and healthy options to a full American or English.</p>
                            <p>For the evening meal there was a different theme each night to tantalize our taste buds. The buffet style ensured all our needs were catered for even though I am a vegetarian.<span class="close-quote"></span></p>
                            <footer>
                                <cite>Angela Smith, </cite>
                                <p>A more positive inspiring yogi!</p>

                            </footer>
                        </blockquote>

                    </div>
                </div>


        </div>


    </section>

    <?php include_once('our_partners.php');?>
    <?php include_once('footer.php');?>

</body>

</html>
