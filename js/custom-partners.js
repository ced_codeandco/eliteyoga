$(function () {
	

	/*var waypoint = new Waypoint({
	  element: document.getElementById('number-offset'),
	  handler: function(direction) {
	    notify('25px from top')
	  },
	  offset: 25
	})*/


	function addWaypoints( elem ) {

		var waypoint = new Waypoint({
		  element: elem,
		  handler: function() {
		  	var anim_nm = elem.attr('data-animation-name');
		  	var anim_delay = +(elem.attr('data-animation-delay') || 0);
		  	if (anim_delay) {
			  	elem.css('animation-delay' , anim_delay + 's').addClass('animated ' + anim_nm );
		  	};
		  	elem.addClass('animated ' + anim_nm );
		  },
		  offset: '75%'
		});
	}

	$('.partners-testimonials [data-animation-name="fadeInUp"]').css('opacity' , '0' );

	$('.partners-testimonials [data-animation-name="fadeInUp"]').each(function(){
		addWaypoints($(this));
	});


});