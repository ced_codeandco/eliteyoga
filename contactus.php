<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Elite Yoga</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="icon"  href="images/favicon.png">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<link href="fonts/fonts.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" href="css/animate.css">
<link href="css/style.css" type="text/css" rel="stylesheet" />
<link href="css/hover.css" type="text/css" rel="stylesheet" />
<link href="slider/jquery.bxslider.css" rel="stylesheet" type="text/css" />
<link href="css/responsive.css" type="text/css" rel="stylesheet" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="js/parallax.js"></script>
<script type="text/javascript" src="js/wow.min.js"></script>
<script>
    wow = new WOW();
    wow.init();   
  </script>
<script type="text/javascript" src="slider/jquery.bxslider.min.js"></script>
<script src="js/common.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function(){		
			
			
			jQuery('.map_on').click(function(){			
				jQuery(this).hide();
			});
			
			});
			 
</script>

</head>

<body>
<?php include_once('header.php');?>
<div class="middle_page_part">
	<div class="site">
        <h2>Contact Elite Yoga</h2>
        <p>We are here to answer any questions you may have about our Elite Yoga experiences.
Reach out to us and we will respond as soon as we can.</p>
    </div>
    <div class="map_contact">
    <div class="map_on"></div>
    <div class="site">
    	<div class="contact_address wow fadeInUp">
        	<p><img src="images/icon/icon5.jpg" /><span><label>Email</label><a href="mailto:info@eliteyoga.co ">info@eliteyoga.co </a></span></p>
            <p><img src="images/icon/icon6.jpg" /><span><label>Telephone</label>+971 567120725 / +971 43385616</span></p>
            <p><img src="images/icon/icon7.jpg" /><span><label>Address</label>2302, 48 BURJGATE TOWER, DOWNTOWN,<br/>DUBAI, UAE P.O. BOX 9831</span></p>
        	
        </div>
    </div>
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3610.07474593629!2d55.26710371500965!3d25.200701683894607!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f427de4802ecd%3A0x48e2476e0ea1f8c0!2s48+Burjgate!5e0!3m2!1sen!2sae!4v1459063238346" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div> 
    
    <div class="site">
    <h2 class="getintouch">Get In Touch</h2>
    <p>We are very approachable and would love to speak to you. Feel free to call, send us an email, Tweet us or simply fill in the details below with your enquiry and we will get back to you within 24 hours.</p>
    <form class="contact_form">
    	<div class="left_part_contactform slideInLeft wow">
        	<input type="text" placeholder="Full Name" class="input_text full_name" />
            <input type="text" placeholder="Telephone" class="input_text telephone" />
            <input type="text" placeholder="Email Address" class="input_text" />
            <input type="text" placeholder="Subject" class="input_text" />            
        </div>
        <div class="right_part_contactform slideInRight wow">
        	<textarea placeholder="Message"></textarea>
        </div>
        <div class="submit_div">
        	<input type="submit" class="submit_contact" value="SEND" />
        </div>
    
    </form>
    <div class="testmonial_text wow fadeInUp margin_bottom0">
            	<span class="left_qu"></span>We offer 5-day retreats, which we feel is more than enough time to rejuvenate and enjoy the charms of Dubai or Bali. <span class="right_qu"></span>
            </div>
      </div>       
      
</div>

<?php include_once('our_partners.php');?>
<?php include_once('footer.php');?>

</body>
</html>
