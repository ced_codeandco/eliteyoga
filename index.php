<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Elite Yoga</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="icon"  href="images/favicon.png">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<link href="fonts/fonts.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" href="css/remodal.css">
<link rel="stylesheet" href="css/remodal-default-theme.css">
<link rel="stylesheet" href="css/animate.css">
<link href="css/style.css" type="text/css" rel="stylesheet" />
<link href="css/hover.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css"  />
<meta name="keywords" content="Rejuvenated,Fresh,Young,Sexy,Gorgeous,Feel fantastic,Stress free,Beautiful location,First class service,End to end luxury,Amazing facilities,Tantalizing food,Experience the Middle East,Exquisite" />
<link href="slider/jquery.bxslider.css" rel="stylesheet" type="text/css" />
<link href="css/responsive.css" type="text/css" rel="stylesheet" />
</head>

<body class="index">
<?php include_once('header.php');?>
<div id="slider_home">
	<div class="site">
    	<ul class="bxslider">
        	<li><img src="images/slider.jpg" /><div class="desc_slider_home"><label>DO YOUR PRACTICE AND ALL IS COMING</label></div></li> 
            <li><img src="images/slider1.jpg" /><div class="desc_slider_home"><label>DO YOUR PRACTICE AND ALL IS COMING</label></div></li>
            <li><img src="images/slider2.jpg" /><div class="desc_slider_home"><label>DO YOUR PRACTICE AND ALL IS COMING</label></div></li>           
        </ul>
    </div>
</div>
<div id="middle_part">
	<div class="site">
    	<div class="home_feature_post" id="left_side_box">
        	<img src="images/arrowright.png" class="desktop_view"/>
        	<div class="bali-link home_feature_post_content wow slideInLeft desktop_view text-right">
            	<h2><a href="#bali" data-target="#bali_box">New to yoga or not,<br/>Elite Yoga is taking the whole yoga experience to new heights</a></h2>
                <p>Whether you have done yoga before or not, this retreat caters for those that want to combine a luxurious holiday with good quality yoga.</p>
            </div>
            <div class="home_feature_post_img  wow slideInLeft" id="dubai_box" data-target="#right_side_box">
            	<a href="#dubai"><img src="images/dubai.jpg" />
                <div class="border_post_name">
                	<div class="border_left_side"></div>
                	<h2>Dubai</h2>
                    <div class="border_right_side"></div>
                </div></a>
            </div>
            <div class="home_feature_post_content wow slideInLeft mobile_view">
            	<h2><a href="#bali">New to yoga or not,<br/>Elite Yoga is taking the whole yoga experience to new heights</a></h2>
                <p>Whether you have done yoga before or not, this retreat caters for those that want to combine a luxurious holiday with good quality yoga.</p>
            </div>
        </div>
        <div class="home_feature_post right_side" id="right_side_box">
        	<img src="images/arrowleft.png" class="desktop_view"/>
        	<div class="home_feature_post_img wow slideInRight" id="bali_box" data-target="#left_side_box">
            <a href="#bali">
            	<img src="images/bali.jpg" />
                <div class="border_post_name bali">
                	<div class="border_left_side"></div>
                	<h2>Bali</h2>
                    <div class="border_right_side"></div>
                </div>
            </a> 
            </div>
        	<div class="dubai-link home_feature_post_content  wow slideInRight">
            	<h2><a href="#dubai" data-target="#dubai_box">How Yoga<br />Can Change Your Life</a></h2>
                <p>We offer 5-day retreats, which we feel is more than enough time to rejuvenate and enjoy the charms of Dubai or Bali.</p>
            </div>            
        </div>
        
        <div class="home_about_content">
        	<img src="images/about.png" class="wow fadeInUp" />
            <div class="home_about_content_inner">
            	<h2>About Elite Yoga</h2>
                <p>Elite yoga is a yoga retreat specialist that provide quality yoga by ensuring all our instructors are fully qualified in the most luxurious of 
                settings, so that you can escape the winter blues to the wonders of UAE's and Bali's finest 5 star hotels,with guaranteed sunshine from May 2016 
                right through to April 2017.</p>
                <p>Have you ever felt you need a holiday after a holiday? Feeling stressed or tired after working long hours in the city? Have you recently broken 
                up from a long- term relationship or did you lose a loved one and want to find yourself again? Do you want to feel and look like a million dollars in 
                a beautiful setting with the most exquisite food?</p>
                <p>Well look no further, Elite Yoga is a yoga retreat service like no other. We make it simple, we provide the most luxurious of hotels, with the most 
                tantalizing healthy food and door-to-door service, where our exclusive pick up service will collect you and drop you to Dubai or Bali airport without any hassle.</p>
                <p><a class="more-link" href="<?php echo (defined('ROOT_URL_BASE')) ? ROOT_URL_BASE.'../' : '';?>aboutus"><i class="fa fa-caret-right"></i> More About Elite Yoga</a></p>
            </div>
            <div class="testmonial_text">
            	<span class="left_qu"></span>We make it simple, we provide the most luxurious of hotels, with the most tantalizing healthy food and door-to-door service.<span class="right_qu"></span>
            </div>
        </div>
    </div>
</div>
<div class="retreats_div">
    <div class="site">
        <h2>Let's have a peak at what to expect<br/>and what a day of a yogi retreater will <br/>look like in Dubai</h2>
        <ul>
            <li>Soak up on some vitamin D after your morning session (7.30am-8.30am) of hatha yoga or enjoy the wonderful facilities from the spa including the most amazing massage, swimming pool or gym.<br/>
            <span>(Additional costs apply for massages and facial treatments, please check with the hotel for price lists. All use of facilities at the hotel are complimentary including but not limited to steam rooms, gym, Jacuzzi and Sauna.)</span></li>
            <li>Set your taste buds alive by indulging in the scrumptious 5 star breakfast buffet, with amazing fresh juices or smoothies that will make your skin glow, freshly prepared granola, 
            exotic fruit and your very own chef making your eggs just how you like them.</li>
            <li>For those that are avoiding dairy, soya and almond milk options will be a more appealing substitute.<br/>
            <span>(Available at the Four Seasons, Dubai)</span></li>
            <li>But it doesn't stop there, your final session of the day (5.30pm) will see you enjoy the sounds of the Arabian sea and be mesmerized by the beautiful dusty Arabian sunset that will make you 
            feel like princess jasmine, whilst performing the art of yoga outside on a private beach in the most amazing of settings, the Waldorf Astoria Palm Jumeirah or the Jumeirah Beach Four Seasons.</li>
            <li>In the evening (7.30pm) put on your killer outfit to enjoy an exquisite buffet made with the finest ingredients and by the Hotel's very own 5 star chef making fresh, 
            nutritious, feel good food that will make you look and feel vibrant, gorgeous and sexy. You have the added bonus of enjoying the beautiful scenery outside or the chic 
            décor inside. </i>
        </ul>
    </div>
</div>
<div id="goldline"></div>

<?php include_once('our_partners.php');?>
<?php include_once('footer.php');?>

<!-- Popup -->
<div class="remodal homepage" data-remodal-id="bali">
    <button data-remodal-action="close" class="remodal-close"></button>
    <h2>Elite Yoga in Bali, Indonesia</h2>  
    <p>New to yoga or not, Elite Yoga is taking the whole yoga experience to new heights</p>
    <img src="images/bali.jpg" />
    <p>Elite yoga are delighted to be partnered with the Sayan Four Seasons Bali, to bring you a yoga experience that is truly out of this world. Lose your senses in paradise and soak in the tranquility in the most extraordinary of surroundings.</p>
    <p>The luxurious Sayan Four Seasons Bali is located beside the Ayung River and in the midst of nature's true beauty. This is a yoga experience like no other, you have the flexibility to choose when you want to do your 2 yoga sessions with a yogi Master in Residence (as per attached schedule) and we are also throwing in a complimentary session everyday of the retreat either another yoga session or mediation session. </p>
    <p>You could pick two morning classes or Anti Gravity Yoga Power Yoga or Life Talk Series from our list attached and the complimentary daily yoga / meditation session.</p>  
    <p>Indulge in the finest of breakfast (from 6.30am to 11am ) and 3-course dinner in the evening from 6pm to 10pm in one of the elegant restaurants at the resort (Ayung Terrace Restaurant).</p>
    <p>During the day take a swim in the heavenly swimming pool or enjoy a relaxing Balinese massage (separate charges apply for the massage).</p> 
    <p>If you are looking for a stunning yoga break that relaxes your mind and body surrounded by natural beauty in the hillside, with the river flowing nearby this is the retreat for you!</p>
    <p class="full-size"><img src="images/bali-sched.jpg"/></p>
</div>

<div class="remodal homepage" data-remodal-id="dubai">
    <button data-remodal-action="close" class="remodal-close"></button>
    <h2>Elite Yoga in Dubai, UAE</h2>  
    <p>How Yoga Can Change Your Life</p>
    <img src="images/dubai.jpg" />
    <p><strong>So what makes Dubai so exciting?</strong></p>
    <p>Dubai's year long sunshine and lavish 5 and 7 star resorts (Burj Al Arab the first ever 7 star hotel) make it the ideal destination for a relaxing yoga break.</p> 
    <p>But the fun doesn't stop there; it has some of the biggest malls (Dubai Mall), largest Aquariums (located at the Atlantis the Palm and Dubai Mall), first ever indoor ski resort (located in Mall of Emirates), first ever man made island (The Palm Jumeirah) tallest building in the world (Burj Khalifia) and virtually 0% crime, making it one the safest holiday destinations.<br/><br/></p>
    <h3>Some useful tips when travelling to Dubai:</h3>
    <ul>
    	<li><strong>The Dirham.</strong> (Pronounced dir-ham) is the official currency of the UAE. The prefix is written as AED or Dh. The dirham is index linked to the dollar and the official exchange rate is Dh3.671 = US$ 1.00. Foreign banks have branches in the UAE and ATMs are readily available in all urban centres.</li>
        <li><strong>Clothing.</strong> Respect for local culture and customs is highly desirable: bikinis, swimsuits, shorts and revealing tops should be confined to beach resorts. Men should not be bare-chested away from the beach and women are advised not to wear short skirts and to keep their shoulders covered. </li>
        <li><strong>Health and Hygiene.</strong> As with all travel, health insurance is a must to cover all eventualities. However, a successful government immunisation programme, the provision of adequate clean water and high standards of cleanliness and food hygiene in hotels and restaurants virtually guarantees you an illness-free visit. No special immunisations are required. Nevertheless, it would be wise to check beforehand if you are travelling from a health-risk area.<br/><br/>
	There are very few mosquitoes in the towns and cities and, since it is not 	considered to be a risk, malaria tablets are not prescribed for travel to the UAE. It is likely, however, that mosquitoes will find you if you are camping near the mountains or exploring wadis or date groves in the evening and it is always safer to avoid being bitten.<br/><br/>
	Tap water, produced by desalination, is normally safe to drink. Nevertheless you may prefer the taste of bottled water</li>
	<li><strong>Tipping.</strong>Tipping is not compulsory, but is common practice. Gratuities to staff at hotels are at your discretion. Most restaurants add service charges to the bill (Abu Dhabi 16 per cent; Sharjah 15 per cent; Dubai 10 per cent).</li>
    <li><strong>Photography.</strong> Ask permission before photographing people in general. Avoid photographing Muslim women and do not photograph airports, docks, telecommunications equipment, government buildings, military and industrial installations.</li>
    <li><strong>Time.</strong> The UAE is four hours ahead of GMT. The time does not change during the summer.</li>
    <li><strong>Electricity.</strong> Domestic supply is 220 volts. Sockets suitable for three-pin 13 amp plugs of British standard design are the norm. Appliances purchased in the UAE will generally have two-pin plugs attached.</li>
    <li><strong>Telephone and internet.</strong> The landline network, operated by the main national telecommunication organisations ETISALAT and Du, is superb: local calls are free and direct dialling is available to over 150 countries. The international dial code for UAE is +971</li>
<li><strong>Visa for tourists:</strong> Entry requirements for visitors to the United Arab Emirates are rather complicated and subject to regular change; therefore, you are advised to check the current regulations before planning your trip. Residents of the Gulf States are issued visas for up to three months on arrival free of charge, while citizens of most western nations including British Citizens, Australia, New Zealand, the United States, Canada and most western European countries are issued visas for one month on arrival, also without charge. Visitors from non-exempt countries that do not fall into the above categories must apply for a visa at their nearest embassy prior to departure.<br/><br/>
  Visitors should be aware that citizens of Israel would not be granted entry 	to the United Arab Emirates, while any other visitor who has evidence of 	travel to Israel in their passport will also be prevented from entering the country.</li></ul>
  
  <p class="hilite">We are honored to have partnered with some of the finest 5 star resorts (The Four Seasons, Jumeirah and The Waldorf Astoria, Palm Jumeirah) in Dubai.</p> 
  <p>These resorts endured Elite Yoga's rigorous selection process based on their quality of accommodation, food and service to bring you some of the finest yoga retreat packages on offer. </p>
  
  <h3>The Four Seasons Dubai</h3>
  <p>Set on a pristine, natural beach, Four Seasons Resort Dubai at Jumeirah Beach blends Arabic design with cool, contemporary style – creating an atmosphere of light, sophisticated luxury. They have 237 ultra-spacious Dubai hotel accommodations – including 49 suites – offering views of the Arabian Gulf on one side and Burj Khalifa and the downtown skyline on the other.</p>
  <p>This 5-star Dubai hotel is home to 10 restaurants and lounges, one of the city's most lavish spas, three swimming pools and fun family activities. With a warm, comfortable attitude and legendary personalized service, Four Seasons is redefining the Dubai luxury hotel experience.</p>

<h3>The Waldorf Astoria, The Palm Jumeirah</h3>
<p>The Waldorf Astoria Dubai Palm Jumeirah is a remarkable destination. A pristine white pearl surrounded by the azure waters of the Arabian Gulf, it has a generous private beach with soft sands, palatial public spaces and luxurious guest rooms and suites of elegant, timeless design.</p>  

<p>Located 35 minutes from Dubai International Airport, the tranquil island resort is well within reach of the vibrant city scene. Lounge by one of two temperature-controlled swimming pools or stroll along 200 meters of private beach. The resort's unparalleled conference and event facilities and 24-hour Business Center provide an ideal venue for important events of all sizes.</p>

<p>Stylish dining and entertainment venues include 3-starred Michelin cuisine at Social by Heinz Beck and Southeast Asian flavors at LAO. Sample bespoke beverages and small bites at Serafina Bar or enjoy Afternoon Tea at Peacock Alley. 

<p>Waldorf Astoria has set the precedent for luxury in Dubai and has won numerous awards for its service and facilities:</p>
    <div class="awwards">
        <img src="images/packages/world_travel.png">
        <p>- Middle East's Leading Luxury Resort<br>
        - Dubai's Leading Hotel Suite (Royal Suite)</p>
    </div>
    <div class="awwards mena_logo">
        <img src="images/packages/mena.jpg">
        <p>- Best 5 Star New Resort</p>
    </div>
    <div class="awwards">
        <img src="images/packages/world_travel2014.png">
        <p>- World's Leading New Resort<br>
        - Middle East's Leading New Resort</p>
    </div>
    <div class="awwards mena_logo">
        <img src="images/packages/business.png">
        <p>- Best Luxury Hotel</p>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript" src="js/wow.min.js"></script>
<script>
    wow = new WOW();
    wow.init();   
</script>
<script type="text/javascript" src="slider/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="js/parallax.js"></script>
<script src="js/common.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function(){		
		jQuery('.bxslider').bxSlider({
		controls: false,
		mode:'fade',
		auto: true,
		speed:1000
		});	
	});
</script>
<script src="js/remodal.min.js"></script>
</body>
</html>
