jQuery(document).ready(function(){
    $('.hidden-video-fields').each(function(){
        var url = $(this).val();
        var wrap = $(this).parent();
        var convertedLink = convertVideo(url);
        if (convertedLink == '') {
            convertedLink = url;
        }
        $(wrap).html(convertedLink);
    })

	jQuery('.mobile_menu').click(function(){

		jQuery('ul.nav').slideToggle(500);

	});	

	jQuery('input:file').change(function (){

		var fileName = $(this).val();

		fileParts = fileName.split("\\");		  

		if (typeof fileParts[2] != 'undefined') {

			fileName = fileParts[2];

		}

		jQuery(this).parent().find('.placeme').text(fileName);

	});

	

	$('#menuclick #nav-icon3').click(function(){

		if($('ul.nav').hasClass('menushow')){

			$('ul.nav').removeClass('menushow');

			$('.header_main').removeClass('containershow');

			$('.slider_main').removeClass('containershow');

			$('#contact_us').removeClass('containershow');

			$('.footer_copy').removeClass('containershow');

			$('.network_main').removeClass('containershow');

			$('.content_main').removeClass('containershow');

			$('#latest_video').removeClass('containershow');

			$('.network_message').removeClass('containershow');

			$('#released_projects').removeClass('containershow');

			$('.video_message').removeClass('containershow');

			$('.studios_main').removeClass('containershow');

			$('#upcoming_projects').removeClass('containershow');

			

			$('#menuclick #nav-icon3').removeClass('open');

		}else{

			$('ul.nav').addClass('menushow');

			$('.header_main').addClass('containershow');

			$('.slider_main').addClass('containershow');

			$('#contact_us').addClass('containershow');

			$('.footer_copy').addClass('containershow');	

			$('.network_main').addClass('containershow');	

			$('.content_main').addClass('containershow');

			$('#latest_video').addClass('containershow');

			$('.network_message').addClass('containershow');

			$('#released_projects').addClass('containershow');

			$('.video_message').addClass('containershow');

			$('.studios_main').addClass('containershow');

			$('#upcoming_projects').addClass('containershow');

			$('#menuclick #nav-icon3').addClass('open');		

		}

		//$('#mainNav').slideToggle(500);

	});

	if($(window).width()<=961){

	$('.nav li a').on('click',function(){

		$('ul.nav').removeClass('menushow');

		$('.header_main').removeClass('containershow');

		$('.slider_main').removeClass('containershow');

		$('#contact_us').removeClass('containershow');

		$('.footer_copy').removeClass('containershow');

		$('.network_main').removeClass('containershow');

		$('.content_main').removeClass('containershow');

		$('#latest_video').removeClass('containershow');

		$('.network_message').removeClass('containershow');

		$('#released_projects').removeClass('containershow');

		$('.video_message').removeClass('containershow');

		

		$('#menuclick #nav-icon3').removeClass('open');

	});

	}

    $('.category_portfolio_ul li a').click(function(){

        var rel = $(this).attr('rel');

        $('.hide_projects_cate').addClass('active_div_p');

        $('#'+rel).removeClass('active_div_p');

        $('.category_portfolio_ul li').removeClass('active_li_p');

        $(this).parent().addClass('active_li_p');



    });

    $(".pop_up_content_main").mCustomScrollbar({

        live:true,

        theme:"inset-dark"

    });

    $('.close_div').click(function(){

        $('.pop_up').addClass('active_div_p');

    });

    $('.open_popup').click(function(){

        var rel = $(this).attr('rel');
        var id = $(this).attr('data-id');
        var type = $(this).attr('data-type');

        $('.pop_up').addClass('active_div_p');
        $('.pop_up').find('.pop_up_content_main').html('<div class="sk-circle loading "> <div class="sk-circle1 sk-child"></div> <div class="sk-circle2 sk-child"></div> <div class="sk-circle3 sk-child"></div> <div class="sk-circle4 sk-child"></div> <div class="sk-circle5 sk-child"></div> <div class="sk-circle6 sk-child"></div> <div class="sk-circle7 sk-child"></div> <div class="sk-circle8 sk-child"></div> <div class="sk-circle9 sk-child"></div> <div class="sk-circle10 sk-child"></div> <div class="sk-circle11 sk-child"></div> <div class="sk-circle12 sk-child"></div> </div>');
        jQuery.ajax({
            type: "POST",
            url: 'cms/get_popup/'+type+'/'+id,
            cache: false,
            success: function(data) {
                $('.pop_up').find('.pop_up_content_main').replaceWith($(data));
                $('.hidden-video-fields').each(function(){
                    var url = $(this).val();
                    var wrap = $(this).parent();
                    var convertedLink = convertVideo(url, '100%');
                    if (convertedLink == '') {
                        convertedLink = url;
                    }
                    $(wrap).html(convertedLink);
                })
            },
        });

        $('#'+rel).removeClass('active_div_p');



    });
	
	$('.nav li a').on('click',function(){
	});
	
	$('.nav li.has-dropdown').hover(function() {
		if($(this).hasClass('dropdown_active')!=true){
			$('.dropdown_active').find('ul').addClass('hid');
			$('.dropdown_active').find('a.active').removeClass('active');
			$('.dropdown_active').find('span.drop_menu').addClass('hid');
		}
		else {
			$('.dropdown_active').find('ul').removeClass('hid');
			$('.dropdown_active > a').addClass('active');
			$('.dropdown_active').find('span.drop_menu').removeClass('hid');
		}
	  }, function() {
		if($(this).hasClass()!='dropdown_active'){
			$('.dropdown_active').find('ul').removeClass('hid');
			$('.dropdown_active > a').addClass('active');
			$('.dropdown_active').find('span.drop_menu').removeClass('hid');
		}
	  }
	);
	$(window).scroll(function(){
		var window_top = $(window).scrollTop();
		if (window_top > 0) {
			$('#backtop').fadeIn('slow');
		} else {
			$('#backtop').fadeOut();
		}
	});
	$('#backtop > span').click(function(){
		var target = $(this).attr('data-target');
		$('html,body').animate({scrollTop: 0},500);
	});

});



