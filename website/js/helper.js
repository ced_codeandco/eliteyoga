/**
 * Created by USER on 11/8/2015.
 */
function convertVideo(raw_url, width, height){
    //console.log(raw_url);
    //var pattern1 = /(?:http?s?:\/\/)?(?:www\.)?(?:vimeo\.com)\/?(.+)/g;
    var pattern1 = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
    var pattern2 = /(?:http?s?:\/\/)?(?:www\.)?(?:vimeo\.com)\/?(.+)/g;
    var pattern3 = /^http:\/\/(www\.)?vimeo\.com\/(clip\:)?(\d+).*$/;
    var playerString = '';
    if (typeof width == 'undefined') {
        var width = '740';
    }
    if (typeof height == 'undefined') {
        var height = '400';
    }
    var match = raw_url.match(pattern1);
    if (match && match[2].length == 11) {
        var playerString = '<iframe width="'+ width +'" height="'+ height +'" src="http://www.youtube.com/embed/' +match[2]+ '" frameborder="0" allowfullscreen></iframe>';
        console.log(playerString);
    } else if(pattern2.test(raw_url)){
        var vimeoUrl = raw_url.match(/^http:\/\/(www\.)?vimeo\.com\/?(\d+).*$/);
        console.log(vimeoUrl);
        console.log(raw_url);
        var replacement = '<iframe width="'+ width +'" height="'+ height +'" src="//player.vimeo.com/video/$1" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';

        var playerString = raw_url.replace(pattern2, replacement);
        //var playerString = '<iframe width="420" height="345" src="//player.vimeo.com/video/$1" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';

    }

    return playerString;
}
jQuery(document).ready(function(){

    if($(window).width()>=961){

        $('.released_projects_ul').bxSlider({

            minSlides: 1,

            maxSlides: 3,

            slideWidth: 365,

            slideMargin: 38

        });

    }else{

        $('.released_projects_ul').bxSlider();

    }

    $('.upcoming_projects_left_ul').bxSlider({

        pagerCustom: '.upcoming_projects_right_ul'

    });

    $('.category_portfolio_ul li a').click(function(){

        var rel = $(this).attr('rel');

        $('.hide_projects_cate').addClass('active_div_p');

        $('#'+rel).removeClass('active_div_p');

        $('.category_portfolio_ul li').removeClass('active_li_p');

        $(this).parent().addClass('active_li_p');



    });

    $(".pop_up_content_main").mCustomScrollbar({

        live:true,

        theme:"inset-dark"

    });

    $('.close_div').click(function(){

        $('.pop_up').addClass('active_div_p');

    });
    /*
    $('.play_icon').click(function(){
        var rel = $(this).attr('rel');

        $('.pop_up').addClass('active_div_p');

        $('#'+rel).removeClass('active_div_p');
    });*/

});
