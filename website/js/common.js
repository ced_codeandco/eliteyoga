/**
 * Created by USER on 9/10/2015.
 */
function submitSubscription() {
    if ($('#subscription-email').val() != '') {
        $('.subscr-control').hide();$('.loader-wrap').show();
        var email = $('#subscription-email').val();
        jQuery.ajax({
            type: "POST",
            url: $('#rootUrlLink').val() + 'subscribe',
            dataType: 'json',
            data: {email:email},
            cache: false,
            success: function(data) {
                $('.subscr-control').show();$('.loader-wrap').hide();
                var status = data.status;
                var msg = data.message;
                if(status*1 == 1)
                {
                    $('#subsc-response').html(msg).removeClass('alert-danger').addClass('alert-success').show();
                }
                else
                {
                    $('#subsc-response').html(msg).removeClass('alert-success').addClass('alert-danger').show();
                }
            },
            complete: function(){
                $('.subscr-control').show();$('.loader-wrap').hide();
            },
            error: function(){
                $('#subCategoryContainer').html('Something went wrong!! Please try later');
                $('.subscr-control').show();$('.loader-wrap').hide();
            }
        });
    }
    //$('.subscr-control').show();$('.loader-wrap').hide();

    return false;
}
$(document).ready(function() {
    $('.fb-share').click(function (e) {
        window.open($(this).attr('href'), 'Share on facebook', 'height=450, width=550, top=' + ($(window).height() / 2 - 275) + ', left=' + ($(window).width() / 2 - 225) + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
        return false;
    });
    $('.tw-share').click(function (e) {
        window.open($(this).attr('href'), 'Share on twitter', 'height=450, width=550, top=' + ($(window).height() / 2 - 275) + ', left=' + ($(window).width() / 2 - 225) + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
        return false;
    });
	$('div#nav-icon3').click(function(){
		$(this).toggleClass('open');
		$('.main_top_menu').toggleClass('slidein');
		$('body').toggleClass('moveright');
	});
	$('div#nav-icon4').click(function(){
		$(this).toggleClass('open');
		$('#footer_links').slideToggle();
		$('html,body').animate({scrollTop: $('#footer_links').offset().top},'slow');
	});	
	$('div#nav-icon5').click(function(){
		$(this).toggleClass('open');
		$('.directory-link-top').slideToggle();
		$('html,body').animate({scrollTop: $('.directory-link-top').offset().top},'slow');
	});

})
/*$('#twitterbtn-link,#facebookbtn-link').click(function(event) {
    alert("innnn");
    var width  = 575,
        height = 400,
        left   = ($(window).width()  - width)  / 2,
        top    = ($(window).height() - height) / 2,
        url    = this.href,
        opts   = 'status=1' +
            ',width='  + width  +
            ',height=' + height +
            ',top='    + top    +
            ',left='   + left;

    window.open(url, 'twitter', opts);

    return false;
});*/