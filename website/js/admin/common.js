/**
 * Created by USER on 8/4/2015.
 */
function loadAdminSection(obj) {
    if (typeof $(obj).attr('bas_url') != 'undefined') {
        window.location = $(obj).attr('bas_url') + $(obj).val();
    } else {
        window.location = $(obj).val();
    }
}
function add_more_bullets(obj) {
    var last_input = $('input[name="bullet_description[]"]').last();
    var new_input = $(last_input).clone();

    $(new_input).val('');
    $(new_input).insertAfter($(last_input));
    $(new_input).focus();
}