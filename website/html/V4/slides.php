<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <!-- Place favicon.ico in the root directory -->
    <link rel="stylesheet" href="css/slider-pro.test.css">
    <link rel="stylesheet" href="css/style.css">

    <style type="text/css">
		
		body {
			background: #000;
		}

		/*.sp-thumbnails-container {
			position: absolute;
			z-index: 9999;
			bottom: 30px;
			left: 50%;
			-webkit-transform: translateX(-50%);
			-ms-transform: translateX(-50%);
			-o-transform: translateX(-50%);
			transform: translateX(-50%);
		}*/
		
    </style>
</head>

<body>
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <div class="property-images slider-pro">
        <div class="sp-slides">
            <div class="sp-slide">
                <img src="images/slider/four-s/2.jpg" alt="property image">
            </div>
            <div class="sp-slide">
                <img src="images/slider/four-s/2.jpg" alt="property image">
            </div>
            <div class="sp-slide">
                <img src="images/slider/four-s/2.jpg" alt="property image">
            </div>
            <div class="sp-slide">
                <img src="images/slider/four-s/2.jpg" alt="property image">
            </div>
            <div class="sp-slide">
                <img src="images/slider/four-s/2.jpg" alt="property image">
            </div>
            <div class="sp-slide">
                <img src="images/slider/four-s/2.jpg" alt="property image">
            </div>
        </div>
        <div class="sp-thumbnails">
            <img class="sp-thumbnail" src="images/slider/four-s/thumb-1.jpg" alt="property image">
            <img class="sp-thumbnail" src="images/slider/four-s/thumb-2.jpg" alt="property image">
            <img class="sp-thumbnail" src="images/slider/four-s/thumb-3.jpg" alt="property image">
            <img class="sp-thumbnail" src="images/slider/four-s/thumb-1.jpg" alt="property image">
            <img class="sp-thumbnail" src="images/slider/four-s/thumb-2.jpg" alt="property image">
            <img class="sp-thumbnail" src="images/slider/four-s/thumb-1.jpg" alt="property image">
            <img class="sp-thumbnail" src="images/slider/four-s/thumb-2.jpg" alt="property image">
            <img class="sp-thumbnail" src="images/slider/four-s/thumb-3.jpg" alt="property image">
        </div>
    </div>
    <script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="js/jquery.sliderPro.min.js"></script>
    <script type="text/javascript">
    $(function() {

        $('.property-images').sliderPro({
            width: 600,
            height: 350,
            fade: false,
            arrows: true,
            buttons: false,
            fullScreen: true,
            shuffle: true,
            smallSize: 500,
            mediumSize: 1000,
            largeSize: 3000,
            thumbnailArrows: true,
            autoplay: false,
            thumbnailWidth: 85,
            thumbnailHeight: 55,
        });

    });
    </script>
</body>

</html>
