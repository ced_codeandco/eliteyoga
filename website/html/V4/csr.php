<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Elite Yoga</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="icon"  href="images/favicon.png">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<link href="fonts/fonts.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" href="css/animate.css">
<link href="css/style.css" type="text/css" rel="stylesheet" />
<link href="css/hover.css" type="text/css" rel="stylesheet" />
<link href="slider/jquery.bxslider.css" rel="stylesheet" type="text/css" />
<link href="css/responsive.css" type="text/css" rel="stylesheet" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="js/parallax.js"></script>
<script type="text/javascript" src="js/wow.min.js"></script>
<script>
    wow = new WOW();
    wow.init();   
  </script>
<script type="text/javascript" src="slider/jquery.bxslider.min.js"></script>
<script src="js/common.js"></script>

</head>

<body>
<?php include_once('header.php');?>
<div class="middle_page_part">
	<div class="site">
        <h2>Corporate Social Responsibility</h2>
        <p>There have been wide spread studies that show alternative treatments as a way for cancer suffers to deal with their condition.<br/>
        Potentially partnering with cancer research and giving opportunity for cancer suffers to experience a yoga retreat.</p>
		<div class="csr_part">
        	<div class="csr_banner_left csr_banner slideInLeft wow">
            	<img src="images/csr01.jpg" />
            </div>
            <div class="csr_banner_middle csr_banner slideInUp wow">
            	<img src="images/csr02.jpg" />
            </div>
            <div class="csr_banner_right csr_banner slideInRight wow">
                <img src="images/csr03.jpg" />
            </div>
        </div>
    </div>
</div>

<?php include_once('our_partners.php');?>
<?php include_once('footer.php');?>

</body>
</html>
