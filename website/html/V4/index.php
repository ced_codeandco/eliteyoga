<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Elite Yoga</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="icon"  href="images/favicon.png">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<link href="fonts/fonts.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" href="css/animate.css">
<link href="css/style.css" type="text/css" rel="stylesheet" />
<link href="css/hover.css" type="text/css" rel="stylesheet" />
<meta name="keywords" content="Rejuvenated,Fresh,Young,Sexy,Gorgeous,Feel fantastic,Stress free,Beautiful location,First class service,End to end luxury,Amazing facilities,Tantalizing food,Experience the Middle East,Exquisite" />
<link href="slider/jquery.bxslider.css" rel="stylesheet" type="text/css" />
<link href="css/responsive.css" type="text/css" rel="stylesheet" />
</head>

<body class="index">
<?php include_once('header.php');?>
<div id="slider_home">
	<div class="site">
    	<ul class="bxslider">
        	<li><img src="images/slider.jpg" /><div class="desc_slider_home"><label>DO YOUR PRACTICE AND ALL IS COMING</label></div></li> 
            <li><img src="images/slider1.jpg" /><div class="desc_slider_home"><label>DO YOUR PRACTICE AND ALL IS COMING</label></div></li>
            <li><img src="images/slider2.jpg" /><div class="desc_slider_home"><label>DO YOUR PRACTICE AND ALL IS COMING</label></div></li>           
        </ul>
    </div>
</div>
<div id="middle_part">
	<div class="site">
    	<div class="home_feature_post">
        	<div class="home_feature_post_content wow slideInLeft desktop_view">
            	<h2><a href="" data-target="#bali_box">Taking Yoga<br />Beyond The Mat Training</a></h2>
                <p>Whether you have done yoga before or not, this retreat caters for those that want to combine a luxurious holiday with good quality yoga.</p>
            </div>
            <div class="home_feature_post_img  wow slideInLeft" id="dubai_box">
            	<img src="images/dubai.jpg" />
                <div class="border_post_name">
                	<div class="border_left_side"></div>
                	<h2>Dubai</h2>
                    <div class="border_right_side"></div>
                </div>
            </div>
            <div class="home_feature_post_content wow slideInLeft mobile_view">
            	<h2><a href="">Taking Yoga<br />Beyond The Mat Training</a></h2>
                <p>Whether you have done yoga before or not, this retreat caters for those that want to combine a luxurious holiday with good quality yoga.</p>
            </div>
        </div>
        <div class="home_feature_post right_side">
        	<div class="home_feature_post_img wow slideInRight" id="bali_box">
            <a href="">
            	<img src="images/bali.jpg" />
                <div class="border_post_name bali">
                	<div class="border_left_side"></div>
                	<h2>Bali</h2>
                    <div class="border_right_side"></div>
                </div>
               </a> 
            </div>
        	<div class="home_feature_post_content  wow slideInRight">
            	<h2><a href="" data-target="#dubai_box">How Yoga<br />Can Change Your Life</a></h2>
                <p>We offer 5-day retreats, which we feel is more than enough time to rejuvenate and enjoy the charms of Dubai or Bali.</p>
            </div>            
        </div>
        
        <div class="home_about_content">
        	<img src="images/about.png" class="wow fadeInUp" />
            <div class="home_about_content_inner">
            	<h2>About Elite Yoga</h2>
                <p>Elite yoga is a yoga retreat specialist that provide quality yoga by ensuring all our instructors are fully qualified in the most luxurious of settings, so that you can escape the winter blues to the wonders of UAE's and Bali's finest 5 star hotels,with guaranteed sunshine from May 2016 right through to April 2017.</p>
<p>Have you ever felt you need a holiday after a holiday? Feeling stressed or tired after working long hours in the city? Have you recently broken up from a long- term relationship or did you lose a loved one and want to find yourself again?Do you want to feel and look like a million dollars in a beautiful setting with the most exquisite food?</p>
<p>Well look know further, Elite Yoga is a yoga retreat service like no other. We make it simple, we provide the most luxurious of hotels, with the most tantalizing healthy food and door-to-door service where our exclusive pick up service will collect you and drop you to Dubai or Bali airport without any hassle.</p>

            </div>
            <div class="testmonial_text">
            	<span class="left_qu"></span>We make it simple, we provide the most luxurious of hotels, with the most tantalizing healthy food and door-to-door service.<span class="right_qu"></span>
            </div>
        </div>
    </div>
</div>
<div class="retreats_div">
    <div class="site">
        <h2>Let's have a peak at what to expect<br/>and what a day of a yogi retreater will<br/>look like in Dubai</h2>
        <ul>
            <li>Soak up on some vitamin D after your morning session (7am-8.30am) of hatha yoga or enjoy the wonderful facilities from the spa including the most amazing massage, swimming pool or gym.<br/>
            <span>(Additional costs apply please check with the hotel for price lists)</span></li>
            <li>Set your taste buds alive by indulging in the scrumptious 5 star breakfast buffet, with amazing fresh juices or smoothies that will make your skin glow, freshly prepared granola, 
            exotic fruit and your very own chef making your eggs just how you like them.</li>
            <li>For those that are avoiding dairy, soya and almond milk options will be a more appealing substitute.<br/>
            <span>(Available at the Four Seasons, Dubai)</span></li>
            <li>But it doesn't stop there, your final session of the day (5.30pm) will see you enjoy the sounds of the Arabian sea and be mesmerized by the beautiful dusty Arabian sunset 
            that will make you feel like princess jasmine whilst performing the art of yoga outside on a private beach in the most amazing of settings, the Waldorf Astoria Palm Jumeriah 
            or the Jumeriah Beach Four Seasons.</li>
            <li>In the evening (7.30pm) put on your killer outfit to enjoy an exquisite buffet made with the finest ingredients and by the Hotel's very own 5 star chef making fresh, 
            nutritious, feel good food that will make you look and feel vibrant, gorgeous and sexy. You have the added bonus of enjoying the beautiful scenery outside or the chic 
            décor inside. </i>
        </ul>
    </div>
</div>
<div id="goldline"></div>
<?php include_once('our_partners.php');?>
<?php include_once('footer.php');?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript" src="js/wow.min.js"></script>
<script>
    wow = new WOW();
    wow.init();   
</script>
<script type="text/javascript" src="slider/jquery.bxslider.min.js"></script>
<script src="js/common.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function(){		
		jQuery('.bxslider').bxSlider({
		controls: false,
		mode:'fade',
		auto: true,
		speed:1000/*,
		onSliderLoad: function () {
			$('.bx-controls-direction').hide();
			$('.bx-wrapper').hover(
				function () {  $(this).find('.bx-controls-direction').fadeIn(300); },
				function () {  $(this).find('.bx-controls-direction').fadeOut(300); }
			);
		}*/
		});	
	});
</script>
</body>
</html>
