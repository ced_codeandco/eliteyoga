<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>Elite Yoga</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="images/favicon.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link href="fonts/fonts.css" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" href="css/animate.css">
    <link href="css/style.css" type="text/css" rel="stylesheet" />
    <link href="css/hover.css" type="text/css" rel="stylesheet" />
    <link href="slider/jquery.bxslider.css" rel="stylesheet" type="text/css" />
    <link href="css/responsive.css" type="text/css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="js/parallax.js"></script>
    <script src="js/jquery.waypoints.min.js"></script>
    <script type="text/javascript" src="js/wow.min.js"></script>
    <script type="text/javascript" src="js/custom-partners.js"></script>
    <script>
    wow = new WOW();
    wow.init();
    </script>
    <script type="text/javascript" src="slider/jquery.bxslider.min.js"></script>
    <script src="js/common.js"></script>
</head>

<body>
    <?php include_once('header.php');?>
    <section class="main-section">
        <header>
            <h2>Partners</h2>
            <!-- Keep the below line break -->
            <p>In the sanctuary of a yoga studio, a retreat or a tranquil spot in the 
            countryside, It's easier.</p>
        </header>
        <div class="container partners-testimonials">
            <div class="row">
                <div class="col-md-6">
                    <article class="instructor-article animated fadeInLeft">
                        <figure>
                            <span class="partner-lines">
                                <img src="images/partner-derya.jpg" alt="yoga instructor" />
                            </span>
                            <figcaption>
                                <h4>About <br/>
                                Mrs Derya Varol <br/> 
                                Pasinler</h4>
                                <p>Yoga Instructor</p>
                            </figcaption>
                        </figure>
                        <div class="instructor-writeup">
                            <p>Derya Pasinler is a certified Sivananda Yoga instructor and is accredited by the Sivananda Yoga Vedanta Center in Kerala, India &amp; Sivananda ashram Yoga Farm in Grass Valley, CA, USA. </p>
                            <p>She holds 500 hours teacher training certificate and teaches the principle of Hatha Yoga and Sivananda Yoga. She’s a specialist in leading group classes and works with private clients building personalized yoga programs. </p>
                            <p>As well as this she is a member of REP’s in the UAE. REP’s is an independent professional body, which recognizes the qualifications and expertise of fitness professionals in the UAE. By being a member she meet’s the health and fitness industry’s agreed UAE Fitness Occupational Standards.
                            </p>
                            <p>Deryastarted practicing yoga in 2000 and then decided to share her gift with others. </p>
                            <p>During her yoga practice she uses a combination of Hatha Yoga, Sivananda Yoga, JivamuktiYoga andAnusara Yoga as well as specific breathing techniques which includeujayi breath, kapalabathi, alternate nostril breathing and bandhas.</p>
                            <p>Derya incorporates the words of Swami Vishnudevananda into her teaching philosophy….
                            </p>
                        </div>
                    </article>
                    <blockquote class="highlight-quote" data-animation-name="fadeInUp">
                        Health is wealth, peace of mind is happiness, Yoga shows the way
                    </blockquote>
                    <article class="testimonial-list" data-animation-name="fadeInUp">
                        <blockquote>
                            <p>I have started practicing yoga with Derya, about 3 years ago and from the moment I met her, her energy enlightened me. Derya is an inspirational and a very attentive yoga instructor with an innate ability to read her students minds. She makes adjustments kindly and accurately and communicates </p>
                            <p>She is always learning and expanding her knowledge and horizons on the spiritual and physical aspects of yoga and what a difference this enthusiasm for learning and sharing makes on her classes.</p>
                            <p>After every lesson with Derya, I feel lighter, brighter and stronger both physically and spiritually (inside and out) <span class="close-quote"></span></p>
                            <cite>Balkizsumerler</cite>
                        </blockquote>
                        <hr/ data-animation-name="fadeInUp" data-animation-delay="0.3">
                            <blockquote data-animation-name="fadeInUp">
                                <p>Yoga in the mornings with Derya is such a peaceful yet energizing way to start the day; once it’s done I feel stronger, more relaxed, and more upright! I felt very comfortable from the first session, as she makes sure that all abilities are catered for. She has a lovely and supportive attitude – talk to her about any aches and pains you have, and she’ll have some helpful advice.<span class="close-quote"></span></p>
                                <cite>Hannah</cite>
                            </blockquote>
                            <hr/ data-animation-name="fadeInUp" data-animation-delay="0.3">
                                <blockquote data-animation-name="fadeInUp">
                                    <p>I have been a student of Derya at her Yoga class for the past 6 months. This was my first introduction to yoga. Derya is an excellent instructor, she has made me aware of muscles which I did not know existed. She rapidly gasped my limits and helped me push myself beyond that. She has guided me one-on-one through difficult moves, breathing practices and even when stretchıng. Through her guidance I came to love yoga and cannot wait each week to get to her sessions. Without any doubt, I would recommend Derya as a yoga instructor to anyone, regardless of their level.<span class="close-quote"></span></p>
                                    <cite>Erhan</cite>
                                </blockquote>
                    </article>
                </div>
                <div class="col-md-6">
                    <article class="instructor-article animated fadeInRight">
                        <figure>
                            <span class="partner-lines">
                                <img src="images/partner-damla.jpg" alt="yoga instructor" />
                            </span>
                            <figcaption>
                                <h4>About <br/>
                                    DamlaSuer</h4>
                                <p>Yoga Instructor</p>
                            </figcaption>
                        </figure>
                        <div class="instructor-writeup">
                            <p>(Yoga Alliance ID: 142843) started her yoga practice as a student of a YogaWorks certified teacher in 2009, when she was 35 years old. Regarding this as ‘a better late than never’ type of an encounter; she kept on practicing and studying about Yoga since then. </p>
                            <p>She completed her RYT200hrs training in Dubai in February 2014 with Dr. Sanjay Sharma from TattvaYogaShala in Rishikesh. Dr. Sanjay is a doctorate graduate of Kaivalyadhama in Pune; which was founded by Swami Kuvalayananda in 1924 as one of the oldest Yoga Institute in the world.</p>
                            <p>She completed her RYT300hrs training in Dubai in February 2015 with Dr. Sanjay Sharma and Sunil Sharma from TattvaYogaShala in Rishikesh. Deryastarted practicing yoga in 2000 and then decided to share her gift with others.</p>
                            <p>She also holds a 100hrs Yin Yoga teacher training certificate by Jade Wood and Emily Baxter; a 90hrs Yoga Anatomy Principles certificate by Leslie Kaminoff; and a GroovyKids Yoga Teacher training certificate by GrevilleHenwood.</p>
                            <p>She currently is studying in Kaivalyadhama’s Online 100hrs Yoga Teacher's Training Course (kdham.org); as she believes in life long learning.</p>
                            <p>Damla regards Yoga as a science that one proves in her own laboratory i.e. her own body; and a holistic comprehensive theory, which addresses all aspects of a human life (social, personal, physical, physiological, mental, psychological, intellectual and spiritual) as laid out in Patanjali’s 8 limb Yoga system.</p>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </section>
    <?php include_once('footer.php');?>
</body>

</html>
