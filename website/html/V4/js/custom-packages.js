$(function () {

      $('.property-images').sliderPro({
          width: '100%',
          height: 502,
          imageScaleMode : 'cover',
          fade: false,
          arrows: true,
          buttons: false,
          fullScreen: true,
          shuffle: true,
          smallSize: 500,
          mediumSize: 1000,
          largeSize: 3000,
          thumbnailArrows: true,
          autoplay: false,
          thumbnailWidth: 85,
          thumbnailHeight: 55,
          fadeArrows:false,
          breakpoints: {
                1200 : {
                  height: 300  
                },
                500: {
                  height: 200
                },
                448: {
                  height: 180
                }
              }
      });

      $('.pricing-table .show-more').slideUp();

      $('.resort-packages .btn-arrow-down').on('click' , function(){
          $(this).parent().siblings('.row').find('.show-more').fadeToggle();
          // $('.pricing-table .show-more').slideDown();
          $(this).find('i').toggleClass('fa-chevron-down fa-chevron-up')
          return false;
      });


      
});