$(window).scroll(function(){
	if  ($(window).width() >=961){	 
		if  ($(window).scrollTop() >=60){$("#header").addClass("nav-fixed");} 
		else {$("#header").removeClass("nav-fixed");}
	}
}); 

$(document).ready(function(){ 
	$('.testmonial_ul').bxSlider({
		controls: true,mode:'fade',//vertical
		auto: true,speed:2000
	});	
	if  ($(window).width() <=1200 && $(window).width() >=1000){
		var maxslidesilder = 4;
		var maxslidewidth = 219;	
	}else if  ($(window).width() <=961 && $(window).width() >=750){
		var maxslidesilder = 3;
		var maxslidewidth = 219;	
	}else if  ($(window).width() <=750 && $(window).width() >=500){
		var maxslidesilder = 2;	
		var maxslidewidth = 219;
	}else if  ($(window).width() <=500){
		var maxslidesilder = 2;	
		var maxslidewidth = 125;	
	}else{
		var maxslidesilder = 5;	
		var maxslidewidth = 219;
	}
	$('.Our_Partners_ul').bxSlider({
		controls: true,
		minSlides: 1,
		maxSlides: maxslidesilder,
		slideWidth: maxslidewidth,
		slideMargin: 17
	});

	if ($(window).width() <=961){$("#header").addClass("nav-fixed"); }

	$('#menuclick #nav-icon3').click(function(){
		if($('ul.nav').hasClass('menushow')){
			$('ul.nav').removeClass('menushow');
			$('#header').removeClass('showleft');
			$('.middle_page_part').removeClass('containershow');			
			$('.field_activities').removeClass('containershow');
			$('.footer_main').removeClass('containershow');
			$('.Our_Partners').removeClass('containershow');			
			$('#slider_home').removeClass('containershow');
			$('#middle_part').removeClass('containershow');			
			$('#menuclick #nav-icon3').removeClass('open');
		}else{
			$('ul.nav').addClass('menushow');
			$('#header').addClass('showleft');
			$('.middle_page_part').addClass('containershow');			
			$('.field_activities').addClass('containershow');	
			$('.footer_main').addClass('containershow');	
			$('.Our_Partners').addClass('containershow');			
			$('#slider_home').addClass('containershow');
			$('#middle_part').addClass('containershow');			
			$('#menuclick #nav-icon3').addClass('open');		
		}
	});
	  
	$('.home_feature_post_content h2 a').hover(
		function(){
			var target = $(this).attr('data-target');
			$(target).addClass('hovered');
		},
		function(){
			var target = $(this).attr('data-target');
			$(target).removeClass('hovered');
		}
	);
});	