$(function () {
	

		function addWaypoints( elem ) {

			var waypoint = new Waypoint({
			  element: elem,
			  handler: function() {
			  	var anim_nm = elem.attr('data-animation-name');
			  	var anim_delay = +(elem.attr('data-animation-delay') || 0);
			  	if (anim_delay) {
				  	elem.css('animation-delay' , anim_delay + 's').addClass('animated ' + anim_nm );
			  	};
			  	elem.addClass('animated ' + anim_nm );
			  },
			  offset: '75%'
			});
		}

		$('.testimonials-wrapper [data-animation-name="fadeInLeft"]').css('opacity' , '0' );
		$('.testimonials-wrapper [data-animation-name="fadeInRight"]').css('opacity' , '0' );

		$('.testimonials-wrapper [data-animation-name="fadeInLeft"]').each(function(){
			addWaypoints($(this));
		});

		$('.testimonials-wrapper [data-animation-name="fadeInRight"]').each(function(){
			addWaypoints($(this));
		});

});