<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Elite Yoga</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="icon"  href="images/favicon.png">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<link href="fonts/fonts.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" href="css/animate.css">
<link href="css/style.css" type="text/css" rel="stylesheet" />
<link href="css/hover.css" type="text/css" rel="stylesheet" />
<link href="slider/jquery.bxslider.css" rel="stylesheet" type="text/css" />
<link href="css/responsive.css" type="text/css" rel="stylesheet" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="js/parallax.js"></script>
<script type="text/javascript" src="js/wow.min.js"></script>
<script>
    wow = new WOW();
    wow.init();   
  </script>
<script type="text/javascript" src="slider/jquery.bxslider.min.js"></script>
<script src="js/common.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function(){		
			
			
			jQuery('.map_on').click(function(){			
				jQuery(this).hide();
			});
			
			});
			 
</script>

</head>

<body>
<?php include_once('header.php');?>
<div class="middle_page_part">
	<div class="site">
        <h2>Contact Elite Yoga</h2>
        <p>We are here to answer any questions you may have about our Elite Yoga experiences. <br />
Reach out to us and we'll respond as soon as we can.</p>
    </div>
    <div class="map_contact">
    <div class="map_on"></div>
    <div class="site">
    	<div class="contact_address wow fadeInUp">
        	<p><img src="images/icon/icon5.jpg" /><span><label>Email</label><a href="mailto:info@elite-yoga.com">info@elite-yoga.com</a></span></p>
            <p><img src="images/icon/icon6.jpg" /><span><label>Telephone</label>+971 55 8956298</span></p>
            <p><img src="images/icon/icon7.jpg" /><span><label>Address</label>Dubai Media City,<br />P O Box 73802, Dubai, United Arab Emirates</span></p>
        	
        </div>
    </div>
    <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d3613.2593130467817!2d55.149468315320355!3d25.093081942073795!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1s73802%2C+Dubai+Media+City%2C+Dubai!5e0!3m2!1sen!2s!4v1452950583009" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe></div> 
    
    <div class="site">
    <h2 class="getintouch">Get In Touch</h2>
    <p>We're very approachable and would lve to speak to you. Feel free to call,<br /> send us an email, Tweet us or simply complete the enquiry</p>
    <form class="contact_form">
    	<div class="left_part_contactform slideInLeft wow">
        	<input type="text" placeholder="Full Name" class="input_text full_name" />
            <input type="text" placeholder="Telephone" class="input_text telephone" />
            <input type="text" placeholder="Email Address" class="input_text" />
            <input type="text" placeholder="Subject" class="input_text" />            
        </div>
        <div class="right_part_contactform slideInRight wow">
        	<textarea placeholder="Message"></textarea>
        </div>
        <div class="submit_div">
        	<input type="submit" class="submit_contact" value="SEND" />
        </div>
    
    </form>
    <div class="testmonial_text wow fadeInUp margin_bottom0">
            	<span class="left_qu"></span>We offer 5-day retreats, which we feel is more than enough time to rejuvenate and enjoy the charms of Dubai or Bali. <span class="right_qu"></span>
            </div>
      </div>       
      
</div>

<?php include_once('our_partners.php');?>
<?php include_once('footer.php');?>

</body>
</html>
