<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . 'libraries/Public_controller.php';

class Cms extends Public_controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public $headerData;
    public $contentData;
    public $footerData;

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('cms_model', 'generic_category_model', 'retreats_model'));
    }

    public function index($page_slug)
    {

        $this->load->library('session');
        $wrapperType = $this->uri->segment(3);
        $this->headerData['title'] = 'Homepage';
        $succ_msg = $this->session->flashdata('flash_success');
        $err_msg = $this->session->flashdata('flash_error');
        if (isset($succ_msg) && $succ_msg != '') {
            $this->contentData['successMsg'] = $this->session->flashdata('flash_success');
        }
        if (isset($err_msg) && $err_msg != '') {
            $this->contentData['errMsg'] = $this->session->flashdata('flash_error');
        }
        if ($cmsData = $this->cms_model->getPageDetails($page_slug)) {

            $this->contentData['cmsData'] = $cmsData;
            $this->footerData['cmsData'] = $cmsData;
            $this->contentData['show404'] = false;
            $this->headerData['pageId'] = $cmsData->id;//die();
            if ($cmsData->id == CMS_CONTENT_PAGE_ID) {
                $this->contentData['categoryList'] = $this->generic_category_model->getAllRecords('*', "is_active = '1' AND is_deleted='0' ", 'ORDER BY category_order ASC');
                if (!empty($this->contentData['categoryList']) && is_array($this->contentData['categoryList'])) {
                    foreach ($this->contentData['categoryList'] as $categ) {
                        $this->contentData['portfolioList'][$categ->id] = $this->portfolio_model->getAllRecords('*', "is_active = '1' AND is_deleted='0' AND  parent_id = $categ->id ", 'ORDER BY sort_order ASC');
                    }
                }
                $latestVideo = $this->portfolio_model->getAllRecords('*', "is_active = '1' AND is_deleted='0' ", 'ORDER BY created_date_time DESC', 'LIMIT 1');
                $this->contentData['latestVideo'] = !empty($latestVideo[0]) ? $latestVideo[0] : '';
            } else if ($cmsData->id == CMS_NETWORK_PAGE_ID) {
                $this->add_video($cmsData);
            } else if ($cmsData->id == CMS_STUDIOS_PAGE_ID) {
                $this->contentData['released_projects'] = $this->released_projects_model->getAllRecords('*', "is_active = '1' AND is_deleted='0'", 'ORDER BY sort_order ASC');
                $totalCountResult = $this->upcoming_projects_model->getAllRecords('count(*) as resultCount', "is_active = '1' AND is_deleted='0'");
                $totalCount = !empty($totalCountResult[0]->resultCount) ? $totalCountResult[0]->resultCount : 0;
                $this->load->library('pagination');
                $config = getPaginationConfig();
                $config['total_rows'] = $totalCount;
                $config['per_page'] = 4;
                $this->pagination->initialize($config);
                $this->contentData['pagination'] = $this->pagination->create_links();
                $this->contentData['upcoming_projects'] = $this->upcoming_projects_model->getAllRecords('*', "is_active = '1' AND is_deleted='0'", 'ORDER BY sort_order ASC', 'LIMIT 4');
            }
        } else {
            $this->contentData['show404'] = true;
        }

        if (!empty($cmsData)) {
            //$this->headerData['seo'] = $this->classified_model->populateSeoTags($cmsData);
        }

        $this->generateRobotText();
        $this->load->view('templates/header', $this->headerData);
        $this->load->view('templates/navigation', $this->headerData);
        if ($this->contentData['show404'] == false && !empty($cmsData->id)) {
            if ($cmsData->id == CMS_CONTENT_PAGE_ID) {
                $this->load->view('cms_page_content', $this->contentData);
            } else if ($cmsData->id == CMS_NETWORK_PAGE_ID) {
                $this->load->view('cms_page_network', $this->contentData);
            } else if ($cmsData->id == CMS_STUDIOS_PAGE_ID) {
                $this->load->view('cms_page_studios', $this->contentData);
            }
        } else {
            $this->load->view('cms_404');
        }
        $this->load->view('templates/footer_contact_us', $this->footerData);
        $this->load->view('templates/footer', $this->footerData);

    }

    function add_video($cmsData)
    {
        $this->load->library('form_validation');
        $this->contentData['succMsg'] = $this->session->flashdata('flash_success');
        $this->contentData['errMsg'] = $this->session->flashdata('flash_error');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
        $this->form_validation->set_rules('name', 'Name', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|email|required');
        $this->form_validation->set_rules('school', 'School', 'trim');
        $this->form_validation->set_rules('video_link', 'Video Link', 'trim|required');

        $this->form_validation->set_rules('age', 'confirm age', 'trim|required');
        $this->form_validation->set_rules('ownership', 'confirm ownership', 'trim|required');
        $this->form_validation->set_rules('terms', 'agree to terms and conditions', 'trim|required');

        if ($this->form_validation->run() == TRUE)
        {
            $this->load->model('video_model');
            $data = $this->input->post();
            if (isset($data['video_submit'])) unset($data['video_submit']);
            $data['created_ip'] = $this->input->ip_address();
            $data['created_date_time'] = date('Y-m-d H:i:s');
            $this->video_model->addDetails($data);
            $this->session->set_flashdata('flash_success', 'Your video submitted successfully.');
            redirect(ROOT_URL . $cmsData->cms_slug);
        }
        //die("Here");
        //video_submit
    }

    function get_popup($type = '', $id = '') {
        if (!empty($type) && !empty($id)) {
            if ($type == 'portfolio') {
                $this->contentData['dataContent'] = $this->portfolio_model->getDetails($id);
            } else {
                $this->contentData['dataContent'] = $this->released_projects_model->getDetails($id);
            }
            $this->load->view('popup_content', $this->contentData);
        }
    }

    function paginate_studios($id = ''){
        $totalCountResult = $this->upcoming_projects_model->getAllRecords('count(*) as resultCount', "is_active = '1' AND is_deleted='0'");
        $totalCount = !empty($totalCountResult[0]->resultCount) ? $totalCountResult[0]->resultCount : 0;
        $this->load->library('pagination');
        $config = getPaginationConfig();
        $config['total_rows'] = $totalCount;
        $config['per_page'] = 4;
        $this->pagination->initialize($config);
        //$this->pagination->cur_page = floor($id/4);
        $this->contentData['pagination'] = $this->pagination->create_links();

        $this->contentData['upcoming_projects'] = $this->upcoming_projects_model->getAllRecords('*', "is_active = '1' AND is_deleted='0'", 'ORDER BY sort_order ASC', "LIMIT $id, 4");
        $this->load->view('upcoming_projects', $this->contentData);
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */