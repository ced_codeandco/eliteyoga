<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . 'libraries/Public_controller.php';

class Home extends Public_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 public $headerData;
	 public $contentData;
	 public $footerData;
	 public function __construct()
	{
		parent::__construct();
	}
	public function index()
	{
        //echo $dddd;
		$this->load->library('session');
		$this->load->model('banner_model');
		$this->headerData['title']= 'Homepage';
		$this->headerData['isHomePage']= 1;
		$succ_msg = $this->session->flashdata('flash_success');
		$err_msg = $this->session->flashdata('flash_error');
		if(isset($succ_msg) && $succ_msg != ''){				
			$this->contentData['successMsg'] = $this->session->flashdata('flash_success');				
		}
		if(isset($err_msg) && $err_msg != ''){				
			$this->contentData['errMsg'] = $this->session->flashdata('flash_error');				
		}

        $homePageContent = $this->cms_model->getDetails(CMS_HOME_PAGE_ID);
        $this->generateRobotText();

        $this->headerData['homePageContent'] = $homePageContent;
        $this->headerData['bannerList'] = $this->banner_model->getAllRecords('*', "is_active = '1' AND is_deleted ='0' ", " ORDER BY sort_order ASC");
        $this->load->view('templates/header', $this->headerData);
        $this->load->view('templates/navigation', $this->headerData);
		$this->load->view('templates/banner', $this->headerData);
		$this->load->view('templates/footer_contact_us', $this->footerData);
        $this->headerData['sections'] = array('page' => 'home', 'right' => 'templates/rightside_bar');
        //$this->load->view('templates/interface', $this->headerData);
        //$this->load->view('templates/footer_nav', $this->headerData);
        $this->load->view('templates/footer', $this->footerData);
	}
    public function packages() {
        $this->load->model(array('cms_model', 'generic_category_model', 'retreats_model', 'generic_category_model', 'banner_model', 'limit_model'));
        $this->contentData['packageList'] = $this->cms_model->getAllRecords('*' , "is_active = '1' AND is_deleted ='0' ",' ORDER BY cms_order ASC', '', true);
		$this->contentData['limitList'] = $this->limit_model->getAllRecords('*');
        $this->load->view('packages', $this->contentData);
    }
    public function payment() {
        $this->load->model(array('cms_model', 'generic_category_model', 'retreats_model', 'generic_category_model', 'banner_model', 'subscription_model'));
        $booking_id = $this->decrypt("key123", $this->uri->segment(2));
		//$params = $this->uri->segment(2);
        //$booking_details = $this->subscription_model->getDetails(explode("&", $params)[1]);
		$booking_details = $this->subscription_model->getDetails(intval($booking_id));
        $this->contentData['package'] = $this->cms_model->getDetails($booking_details->package_id);
        $this->contentData['retreat'] = $this->retreats_model->getDetails($booking_details->retreat_id);

        $this->contentData['booking_details'] = $booking_details;
        if (!empty($booking_details->price_quoted) &&  !empty($booking_details->is_accepted)) {
            $this->contentData['booking_not_approved'] = 0;
        } else {
            $this->contentData['booking_not_approved'] = 1;
        }
        //$this->subscription_model->add_reply($booking_id, $data);
        //$this->contentData['packageList'] = $this->cms_model->getAllRecords('*' , "is_active = '1' AND is_deleted ='0' ",' ORDER BY cms_order ASC', '', true);
        $this->load->view('payment', $this->contentData);
    }
    public function success() {
        $this->load->model(array('cms_model', 'generic_category_model', 'retreats_model', 'generic_category_model', 'banner_model', 'subscription_model'));
        $booking_id = $this->uri->segment(2);
        $booking_details = $this->subscription_model->getDetails($booking_id);

		if ($this->input->get('status') === 'success') {
            //Success
            //Send email to admin
            $transaction_no = $this->input->post('transactionId');
            $amount = $this->input->post('amount');
            //$currency = $this->input->post('cc');
			$reason = $this->input->post('reason');
        
            $package = $this->cms_model->getDetails($booking_details->package_id);
            $retreat = $this->retreats_model->getDetails($booking_details->retreat_id);
            $this->subscription_model->update_payment_status($booking_id, $transaction_no);
            $this->subscription_model->send_payment_success_email($booking_id, $package, $retreat);
            $this->session->set_flashdata('flash_success', 'Payment processed successfully!!<br />We will get back to you once the payment is confirmed by Nexxus Gateway Payment');
            redirect(ROOT_URL.'thankyou/'.$booking_id.'/'.$transaction_no);
        } else {
            //Failed
            //return to failed page
            $this->session->set_flashdata('flash_error', 'Sorry! Payment could not be verified. <br />Please try again later');
            redirect(ROOT_URL.'payment_failed/'.$booking_id);

        }
		
        //if ($this->input->get('tx')) {
        //    //Success
        //    //Send email to admin
        //    $transaction_no = $this->input->get('tx');
        //    $amount = $this->input->get('amt');
        //    $currency = $this->input->get('cc');
        //
        //    $package = $this->cms_model->getDetails($booking_details->package_id);
        //    $retreat = $this->retreats_model->getDetails($booking_details->retreat_id);
        //    $this->subscription_model->update_payment_status($booking_id, $transaction_no, $amount, $currency);
        //    $this->subscription_model->send_payment_success_email($booking_id, $package, $retreat);
        //    $this->session->set_flashdata('flash_success', 'Payment processed successfully!!<br />We will get back to you once the payment is confirmed by PayPal');
        //    redirect(ROOT_URL.'thankyou/'.$booking_id.'/'.$transaction_no);
        //} else {
        //    //Failed
        //    //return to failed page
        //    $this->session->set_flashdata('flash_error', 'Sorry! Payment could not be verified. <br />Please try again later');
        //    redirect(ROOT_URL.'payment_failed/'.$booking_id);
        //}
    }

    public function payment_failed() {
        $this->load->model(array('cms_model', 'generic_category_model', 'retreats_model', 'generic_category_model', 'banner_model', 'subscription_model'));
        $booking_id = $this->uri->segment(2);
        $booking_details = $this->subscription_model->getDetails($booking_id);
        $this->contentData['package'] = $this->cms_model->getDetails($booking_details->package_id);
        $this->contentData['retreat'] = $this->retreats_model->getDetails($booking_details->retreat_id);

        $this->contentData['booking_details'] = $booking_details;
        $this->contentData['payment_status'] = 'failed';
        $err_msg = $this->session->flashdata('flash_error');
        $this->contentData['err_msg'] = !empty($err_msg) ? $err_msg : 'Payment failed! Please try again later.';

        $this->load->view('payment', $this->contentData);
    }

    public function thankyou() {
        $this->load->model(array('cms_model', 'generic_category_model', 'retreats_model', 'generic_category_model', 'banner_model', 'subscription_model'));
        $booking_id = $this->uri->segment(2);
        $booking_details = $this->subscription_model->getDetails($booking_id);
        $this->contentData['package'] = $this->cms_model->getDetails($booking_details->package_id);
        $this->contentData['retreat'] = $this->retreats_model->getDetails($booking_details->retreat_id);

        $this->contentData['booking_details'] = $booking_details;
        $this->contentData['payment_status'] = 'success';
        $succ_msg = $this->session->flashdata('flash_success');
        $this->contentData['succ_msg'] = !empty($succ_msg) ? $succ_msg : 'Transaction processed successfully';

        $this->load->view('payment', $this->contentData);
    }


    function contact_us()
    {
        $this->load->library('form_validation');
        $this->contentData['succMsg'] = $this->session->flashdata('flash_success');
        $this->contentData['errMsg'] = $this->session->flashdata('flash_error');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
        $this->form_validation->set_rules('name', 'Name', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|email|required');
        $this->form_validation->set_rules('comments', 'Message', 'trim|required');	
        $this->form_validation->set_rules('captcha', 'Captcha', 'trim|required');

		$cmsData = $this->cms_model->getPageDetails('contact-mums-like-us');
        $this->contentData['cmsData'] = $cmsData;


        $response = array('status' => 0, 'message' => 'Something went wrong. Please try again later');
        if ($this->form_validation->run() == TRUE)
        {
            if ($this->validate_captcha($this->input->post('captcha'))) {
                $this->load->model('contact_model');
                $data = $this->input->post();
                if (isset($data['captcha'])) unset($data['captcha']);
                $data['created_ip'] = $this->input->ip_address();
                $data['created_date_time'] = date('Y-m-d H:i:s');
                $this->contact_model->addDetails($data);
                $this->sendContactEmail();

                $response['status'] = 1;
                $response['message'] = 'Inquiry sent successfully';
            } else {
                $response['status'] = 0;
                $response['message'] = 'Invalid captcha';
            }
        } else {
            $response['status'] = 0;
            $response['message'] = 'Invalid inputs';
        }
        $this->generateRobotText();
        $verify_robot = $this->footerData['verify_robot'];
        $response['new_captcha'] = 'How much is '.$verify_robot[0].' + '.$verify_robot[1];

        echo json_encode($response);
    }

    function search() {
        $searchCriteria = $this->input->get();

        $search_option = (!empty($searchCriteria['search']) && $searchCriteria['search'] == 1) ? true : false;
        $searchString = "is_active ='1' AND is_deleted='0' ";
        $searchString .= $this->classified_model->build_search_string($searchCriteria, $search_option);

        $result_order = $this->classified_model->build_search_order($searchCriteria);
        $sortString = !empty($result_order) ? "ORDER BY ".$result_order : '';
        $starts_with = !empty($searchCriteria['per_page']) ? $searchCriteria['per_page'] : 0;

        $results = $this->classified_model->getAllRecords('*', $searchString, $sortString, "LIMIT $starts_with, ".SEARCH_RESULTS_PER_PAGE, true);
        $searchResultTotal = !empty($results['total_row_count']) ? $results['total_row_count'] : 0;
        $this->contentData['searchResult'] = $results['result'];

        $paginator = $this->classified_model->build_search_paginator($searchResultTotal, SEARCH_RESULTS_PER_PAGE, $searchCriteria);

        $this->contentData['paginator'] = $paginator;
        $this->contentData['searchResultTotal'] = $searchResultTotal;
        $this->contentData['searchCriteria'] = $searchCriteria;

        $this->load->view('templates/header', $this->headerData);
        $this->load->view('templates/banner', $this->headerData);
        $this->load->view('search', $this->contentData);
        $this->load->view('templates/footer_nav', $this->headerData);
        $this->load->view('templates/footer', $this->footerData);
    }

    function subscribe() {
        $this->load->model('subscription_model');
        //Subscription_model
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<span>', '</span>');
        $this->form_validation->set_rules('package_id', 'Package', 'trim|required');
        $this->form_validation->set_rules('retreat_id', 'Package', 'trim|required');
        $this->form_validation->set_rules('start_date', 'Package', 'trim|required');
        $this->form_validation->set_rules('end_date', 'Package', 'trim|required');
        $this->form_validation->set_rules('price_dollar_selected', 'Package', 'trim|required');
        $this->form_validation->set_rules('price_euro_selected', 'Package', 'trim|required');
        $this->form_validation->set_rules('full_name', 'Full Name', 'trim|required');
        $this->form_validation->set_rules('telephone', 'Phone', 'trim|required');
        $this->form_validation->set_rules('city', 'City', 'trim|required');
        $this->form_validation->set_rules('state', 'State', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|email|required');

        $response = array('status' => 0, 'message' => 'Something went wrong. Please try again later');
        if ($this->form_validation->run() == TRUE)
        {
            $data = array(
                'email' => $this->input->post('email'),
                'package_id' => $this->input->post('package_id'),
                'retreat_id' => $this->input->post('retreat_id'),
                'start_date' => $this->input->post('start_date'),
                'end_date' => $this->input->post('end_date'),
                'price_dollar_selected' => $this->input->post('price_dollar_selected'),
                'price_euro_selected' => $this->input->post('price_euro_selected'),
                'name' => $this->input->post('full_name'),
                'telephone' => $this->input->post('telephone'),
                'street' => $this->input->post('street'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'country' => $this->input->post('country'),
                'comments' => $this->input->post('comments'),
                'created_date_time' => date('Y-m-d H:i:s'),
                'created_ip' => $this->input->ip_address(),
                'is_deleted' => '0',
            );
            $this->subscription_model->addDetails($data);
            $response = array('status' => 1, 'message' => 'Enquiry sent successfully');
        } else {
            $response = array('status' => 0, 'message' => 'Invalid input');
        }

        echo json_encode($response);
        exit;
    }

    function sendContactEmail()
    {
        $admin_subject = "New Inquiry on ".SITE_NAME;
        $user_subject = "Thank you for Inquiry on ".SITE_NAME;

        $admin_header = "Dear Administrator,<br /><br />".$_POST['name']." has submitted contact form with below details on ".SITE_NAME.". <br /><br />";
        $user_header = "Dear ".$_POST['name'].",<br /><br /> Thank you for Inquiry on ".SITE_NAME." with below details. <br /><br />";

        $emailMessage  = "<strong>Detail :</strong> <br /><br />";
        $emailMessage .= "Name   		  : ".$_POST['name']." <br /><br />";

        if(isset($_POST['email']) && $_POST['email'] != '')
            $emailMessage .= "E-mail  	  : ".$_POST['email']." <br /><br />";

        if(isset($_POST['comments']) && $_POST['comments'] != '')
            $emailMessage .= "Comments   	  : ".$_POST['comments']." <br /><br />";

        $emailMessage .= "Thanks <br />".SITE_NAME." Team";

        $this->load->library('emailclass');
        $email = $this->emailclass->send_mail(DEFAULT_EMAIL, $admin_subject, $admin_header.$emailMessage);
        $email = $this->emailclass->send_mail($_POST['email'], $user_subject, $user_header.$emailMessage);
    }
	
	
	function maindirectory() {
        
        $searchString = "tbl_directory_category.is_active ='1' AND tbl_directory_category.is_deleted='0' AND parent_id=0 ";
        //$searchString .= !empty($category_id) ? " AND category_id = '$category_id'" : '';

        $results = $this->directory_category_model->getAllRecords('tbl_directory_category.*', $searchString, "ORDER BY id ASC ");
		
		//print_r($results);
		$subcategory = array();
		$i=0;
		
		foreach($results as $row){
			
		$searchString1 = "tbl_directory_category.is_active ='1' AND tbl_directory_category.is_deleted='0' AND parent_id=".$row->id;
        //$searchString .= !empty($category_id) ? " AND category_id = '$category_id'" : '';

        $results1 = $this->directory_category_model->getAllRecords('tbl_directory_category.*', $searchString1, "ORDER BY id ASC ");
		//print_r( $results1);
		$subcategory[$row->id] = $row;
		$subcategory[$row->id]->sub = $results1;
		$i++;
		}
		
       // $searchResultTotal = !empty($results['total_row_count']) ? $results['total_row_count'] : 0;
        $this->contentData['subcategory'] = $subcategory;
       // $base_url = ROOT_URL.'posts/'.(!empty($sectionSlug) ? $sectionSlug.'/' : '').(!empty($categorySlug) ? $categorySlug.'/' : '').'?';
       // $this->contentData['paginator'] = $this->classified_model->build_search_paginator_category($searchResultTotal, RECORD_PER_PAGE, $base_url);


        $succ_msg = $this->session->flashdata('flash_success');
        $err_msg = $this->session->flashdata('flash_error');
        if(isset($succ_msg) && $succ_msg != ''){
            $this->contentData['successMsg'] = $this->session->flashdata('flash_success');
        }
        if(isset($err_msg) && $err_msg != ''){
            $this->contentData['errMsg'] = $this->session->flashdata('flash_error');
        }

        


        $this->load->view('templates/header', $this->headerData);
        $this->load->view('templates/banner', $this->headerData);
        $this->headerData['sections'] = array('page' => 'maindirectory', 'right' => 'templates/rightside_bar');
        $this->load->view('templates/interface', $this->headerData);
        $this->load->view('templates/footer_nav', $this->headerData);
        $this->load->view('templates/footer', $this->footerData);
    }
	
	function directory($sectionSlug = '', $categorySlug = '') {
        $categoryIdArray = '';
        $searchedSection = !empty($sectionSlug) ? $this->getDirectoryBySlug($sectionSlug) : '';
        $section_id = !empty($searchedSection->id) ? $searchedSection->id : '';
		$section_id_par = !empty($searchedSection->parent_id) ? $searchedSection->parent_id : '';
        define('CURRENT_SECTION_ID_DIR', $section_id);
		define('CATEGORY_ID_DIR', $section_id_par);
        $this->contentData['searchedSection'] = $searchedSection;
        if (!empty($section_id)) { $categoryIdArray[] = $section_id; }

        $searchedCategory = !empty($categorySlug) ? $this->getDirectoryBySlug($categorySlug) : '';
		//print_r($searchedSection);
        $category_id = !empty($searchedCategory->id) ? $searchedCategory->id : '';
        //define('CATEGORY_ID_DIR', $category_id);
        $this->contentData['searchedCategory'] = $searchedCategory;
        if (!empty($category_id)) { $categoryIdArray = '';
		 $categoryIdArray[] = $category_id; }
		
		//print_r($categoryIdArray);
		
		
		


        $searchCriteria = $this->input->get();
        $starts_with = !empty($searchCriteria['per_page']) ? $searchCriteria['per_page'] : 0;
        $searchString = "tbl_classified_directory.is_active ='1' AND tbl_classified_directory.is_deleted='0' ";
        $searchString .= !empty($category_id) ? " AND category_id = '$category_id'" : '';

        $results = $this->classified_directory_model->getAllRecords('tbl_classified_directory.*', $searchString, "ORDER BY sort_order ASC ", "LIMIT $starts_with, ".RECORD_PER_PAGE, true, $categoryIdArray);
        $searchResultTotal = !empty($results['total_row_count']) ? $results['total_row_count'] : 0;
        $this->contentData['searchResult'] = $results['result'];
        $base_url = ROOT_URL.'posts/'.(!empty($sectionSlug) ? $sectionSlug.'/' : '').(!empty($categorySlug) ? $categorySlug.'/' : '').'?';
        $this->contentData['paginator'] = $this->classified_directory_model->build_search_paginator_category($searchResultTotal, RECORD_PER_PAGE, $base_url);


        $succ_msg = $this->session->flashdata('flash_success');
        $err_msg = $this->session->flashdata('flash_error');
        if(isset($succ_msg) && $succ_msg != ''){
            $this->contentData['successMsg'] = $this->session->flashdata('flash_success');
        }
        if(isset($err_msg) && $err_msg != ''){
            $this->contentData['errMsg'] = $this->session->flashdata('flash_error');
        }

        if (!empty($this->headerData['navCategories'])) {
            $this->headerData['seo'] = $this->classified_directory_model->populateSeoTagsForListing($this->headerData['navCategories'], CURRENT_SECTION_ID_DIR, $category_id);
        }
		
		
		
		
		
		
		
		$searchString = "tbl_directory_category.is_active ='1' AND tbl_directory_category.is_deleted='0' AND parent_id=0 ";
        //$searchString .= !empty($category_id) ? " AND category_id = '$category_id'" : '';

        $resultscate = $this->directory_category_model->getAllRecords('tbl_directory_category.*', $searchString, "ORDER BY id ASC ");
		
		//print_r($results);
		$subcategory = array();
		$i=0;
		
		foreach($resultscate as $row){
			
		$searchString1 = "tbl_directory_category.is_active ='1' AND tbl_directory_category.is_deleted='0' AND parent_id=".$row->id;
        //$searchString .= !empty($category_id) ? " AND category_id = '$category_id'" : '';

        $results1 = $this->directory_category_model->getAllRecords('tbl_directory_category.*', $searchString1, "ORDER BY id ASC ");
		//print_r( $results1);
		$subcategory[$row->id] = $row;
		$subcategory[$row->id]->sub = $results1;
		$i++;
		}
		
       // $searchResultTotal = !empty($results['total_row_count']) ? $results['total_row_count'] : 0;
        $this->contentData['subcategory'] = $subcategory;
		
		
		
		
		


        $this->load->view('templates/header', $this->headerData);
        $this->load->view('templates/banner', $this->headerData);
        $this->headerData['sections'] = array('page' => 'directory', 'right' => 'templates/rightside_bar');
        $this->load->view('templates/interface', $this->headerData);
        $this->load->view('templates/footer_nav', $this->headerData);
        $this->load->view('templates/footer', $this->footerData);
    }
	
    function read_directory($url_slug = '') {
        //Get post details
        $this->contentData['postDetails'] = (!empty($url_slug)) ? $this->classified_directory_model->getDetailsBySlug($url_slug) : '';
        $this->headerData['seo'] = $this->classified_directory_model->populateSeoTags($this->contentData['postDetails']);

        //Get section of post
        $section_id = !empty($this->contentData['postDetails']->section_id) ? $this->contentData['postDetails']->section_id : 0;
        $searchedSection = !empty($section_id) ? $this->getCategoryById($section_id) : '';
        define('CURRENT_SECTION_ID_DIR', $section_id);
        $this->contentData['searchedSection'] = $searchedSection;

        //Get category of post
        $category_id = !empty($this->contentData['postDetails']->category_id) ? $this->contentData['postDetails']->category_id : 0;
        $searchedCategory = !empty($category_id) ? $this->getCategoryById($category_id) : '';
        define('CATEGORY_ID_DIR', $category_id);
        $this->contentData['searchedCategory'] = $searchedCategory;

        //Get related posts
        if (!empty($section_id)) {
            $where = " tbl_classified_directory.is_active = '1' AND tbl_classified_directory.is_deleted ='0' ";
            if (!empty($this->contentData['postDetails']->id)) {
                $where .= " AND tbl_classified_directory.id !=" . $this->contentData['postDetails']->id . " ";
            }
            $this->contentData['relatedArticles'] = $this->classified_directory_model->getAllRecords('*', $where, 'ORDER BY number_of_hits DESC', 'LIMIT '.RELATED_ARTICLE_COUNT);
        }

        //Update hits count
        if (!isset($this->isAdminLogin) OR $this->isAdminLogin == false) {
            $this->classified_directory_model->updateHitsCount($this->contentData['postDetails']->id);
        }

        if (!empty($this->contentData['postDetails']->banner_image) && file_exists(DIR_UPLOAD_BANNER.$this->contentData['postDetails']->banner_image)){
            $this->headerData['seo']['og:image'] = DIR_UPLOAD_BANNER_SHOW.$this->contentData['postDetails']->banner_image;
        }

        $this->load->view('templates/header', $this->headerData);
        $this->load->view('templates/banner', $this->headerData);
        $this->headerData['sections'] = array('page' => 'read_directory', 'right' => 'templates/rightside_bar');
        $this->load->view('templates/interface', $this->headerData);
        $this->load->view('templates/footer_nav', $this->headerData);
        $this->load->view('templates/footer', $this->footerData);
    }
	
	
	


	public function no_access(){
		$this->headerData['title']= 'Access Denied';
		$this->load->view('templates/header', $this->headerData);
		$this->load->view('no_access', $this->contentData);
		$this->load->view('templates/footer', $this->footerData);
	}
	
	private function decrypt($key, $string, $action = 'decrypt'){
            $res = '';
            if($action !== 'encrypt'){
                $string = base64_decode($string);
            } 
            for( $i = 0; $i < strlen($string); $i++){
                    $c = ord(substr($string, $i));
                    if($action == 'encrypt'){
                        $c += ord(substr($key, (($i + 1) % strlen($key))));
                        $res .= chr($c & 0xFF);
                    }else{
                        $c -= ord(substr($key, (($i + 1) % strlen($key))));
                        $res .= chr(abs($c) & 0xFF);
                    }
            }
            if($action == 'encrypt'){
                $res = base64_encode($res);
            } 
            return $res;
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */