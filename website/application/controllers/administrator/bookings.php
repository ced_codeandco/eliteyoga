<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . 'libraries/Admin_controller.php';

class Bookings extends Admin_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 public $headerData;
	 public $contentData;
	 public $footerData;
	 public function __construct()
	 {
		parent::__construct();

		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->model(array('admin_model', 'subscription_model'));
		$this->load->library('form_validation');
		$this->load->library('session');
         $this->headerData['adminModuleList'] = $this->admin_model->getModuleList();
         $this->headerData['isAdminLogin'] = $this->admin_model->checkAdminLogin();
         $this->headerData['activeAdminDetails'] = $this->admin_model->activeAdminDetails();
         $this->headerData['noneEditPage'] = array('home');
	}
	function status_inactive(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
			$contactsId =  $this->uri->segment(4);
			if($contactsId == ''){
				redirect(ADMIN_ROOT_URL.'bookings');
			}else{
				$this->subscription_model->changeStatus(0,$contactsId);
				$this->session->set_flashdata('flash_success', 'Contact Requests Status changed successfully');
				redirect(ADMIN_ROOT_URL.'bookings');
			}
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}		
	}
	function status_active(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
			$contactsId =  $this->uri->segment(4);
			if($contactsId == ''){
				redirect(ADMIN_ROOT_URL.'bookings');
			}else{
				$this->subscription_model->changeStatus(1,$contactsId);
				$this->session->set_flashdata('flash_success', 'Contact Requests Status changed successfully');
				redirect(ADMIN_ROOT_URL.'bookings');
			}
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
		
	}
	function delete(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
			$contactsId =  $this->uri->segment(4);
			$this->subscription_model->deleteRecord($contactsId);
			$this->session->set_flashdata('flash_success', 'Contact Requests deleted successfully');
			redirect(ADMIN_ROOT_URL.'bookings');
			
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
	}

	public function index()
	{
		$this->load->library('session');
		$pId = $this->uri->segment(4);
		$parentId =  (isset($pId) && $pId != '') ? $this->uri->segment(4) : 0;
        
		
		if($this->session->userdata('admin_id')==''){
			redirect(ADMIN_ROOT_URL.'login');
		}else{
			
			$this->contentData['contactsList'] = $this->subscription_model->getAllRecords('*' ,'',' ORDER BY id DESC');
			$succ_msg = $this->session->flashdata('flash_success');
			$err_msg = $this->session->flashdata('flash_error');
			if(isset($succ_msg) && $succ_msg != ''){				
				$this->contentData['successMsg'] = $this->session->flashdata('flash_success');				
			}
			if(isset($err_msg) && $err_msg != ''){				
				$this->contentData['errMsg'] = $this->session->flashdata('flash_error');				
			}
			$this->headerData['title']= 'Contact Requests List | Admin Module';
			$this->load->view('admin/templates/header', $this->headerData);
			$this->load->view('admin/bookings_list', $this->contentData);
			$this->load->view('admin/templates/footer', $this->footerData);
		}
	}
    function booking_reply(){
        $this->load->library('emailclass');
        if($this->input->post()){
            $booking_id = $this->input->post('booking_id');
            $data['package_id'] = $this->input->post('package_id');
            $data['retreat_id'] = $this->input->post('retreat_id');
            $data['is_accepted'] = $this->input->post('is_accepted');
            $data['replied_member_id'] = $this->input->post('replied_member_id');
            $data['reply_message'] = $this->input->post('reply_message');
            $data['price_quoted'] = $this->input->post('price_quoted');
            $this->subscription_model->add_reply($booking_id, $data);

            $this->subscription_model->sendBookingReply($booking_id);

            /*$this->inquiry_model->changeReplyStatus($enquiry_id);*/
            echo 'replied';
        }else{
            echo 'error';
        }
        exit;

    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */