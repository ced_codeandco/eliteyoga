<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . 'libraries/Admin_controller.php';

class Generic_category extends Admin_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 public $headerData;
	 public $contentData;
	 public $footerData;
	 public function __construct()
	 {
		parent::__construct();

		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->model(array('admin_model', 'generic_category_model', 'cms_model'));
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->headerData['adminModuleList'] = $this->admin_model->getModuleList();
		$this->headerData['isAdminLogin'] = $this->admin_model->checkAdminLogin();
		$this->headerData['activeAdminDetails'] = $this->admin_model->activeAdminDetails();
		$this->headerData['noneEditPage'] = array('home');
         $this->contentData['package_id'] = !empty($_GET['package']) ? $_GET['package'] : 0;;
		
	}
	function status_inactive(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
			$categoryId =  $this->uri->segment(4);
			if($categoryId == ''){
				redirect(ADMIN_ROOT_URL.'generic_category');
			}else{
				$this->generic_category_model->changeStatus(0,$categoryId);
				$this->session->set_flashdata('flash_success', 'Retreats Options Status changed successfully');
				redirect(ADMIN_ROOT_URL.'generic_category');
			}
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}		
	}
	function status_active(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
			$categoryId =  $this->uri->segment(4);
			if($categoryId == ''){
				redirect(ADMIN_ROOT_URL.'generic_category');
			}else{
				$this->generic_category_model->changeStatus(1,$categoryId);
				$this->session->set_flashdata('flash_success', 'Retreats Options Status changed successfully');
				redirect(ADMIN_ROOT_URL.'generic_category');
			}
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
		
	}
	function delete(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
			$categoryId =  $this->uri->segment(4);			
			$this->generic_category_model->deleteRecord($categoryId);
			$this->session->set_flashdata('flash_success', 'Retreats Options deleted successfully');
			redirect(ADMIN_ROOT_URL.'generic_category');

		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
	}
	
	function add(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
			$categoryId =  $this->uri->segment(4);
			$action = 'Add';
			if($categoryId == ''){
				$action = 'Add';
				$this->contentData['categoryDetails'] = array();
				$classifiedParentId = 0;
				$classifiedId = 0;
			}else{
				$action = 'Edit';
				$categoryDetails = $this->generic_category_model->getDetails($categoryId);
                $this->contentData['package_id'] = $categoryDetails->parent_id;
				$this->contentData['categoryDetails'] = $categoryDetails;
				$classifiedParentId = $categoryDetails->parent_id;
				$classifiedId = $categoryDetails->id;
			}
			$this->load->library('ckeditor');
			$this->load->library('ckfinder');
			$this->ckeditor->basePath = base_url().'assets/ckeditor/';
			
			$this->ckeditor->config['language'] = 'en';
			$this->ckeditor->config['width'] = '700px';
			$this->ckeditor->config['height'] = '200px';
			
			//Add Ckfinder to Ckeditor
			$this->ckfinder->SetupCKEditor($this->ckeditor,'../../assets/ckfinder/'); 

			if($this->input->post()){
				
				$this->load->helper(array('form', 'url'));
				
				$this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>', '</div>');
				$this->form_validation->set_rules('title', 'Title', 'trim|required');
				if(isset($_FILES['category_image']) && $_FILES['category_image']['name']!=''){
					$this->form_validation->set_rules('category_image', 'Image', 'trim|callback_upload_image');
					
				}else{
					$_POST['category_image']	= (isset($_POST['uploaded_file']) && $_POST['uploaded_file'] != '') ? $_POST['uploaded_file'] : '';	
				}
					
				if ($this->form_validation->run() == TRUE)
				{

                    $data = $this->input->post();
                    unset($data['current_parent_id']);
                    unset($data['action']);
                    if($this->input->post('action') == 'Add') {
                        $data['url_slug'] = $this->generic_category_model->generateCategorySlug($this->input->post('title'));
                        //print_r($data); die();
						$insertedId = $this->generic_category_model->addDetails($data);
						
						if($insertedId){
							$this->session->set_flashdata('flash_success', 'Retreats Options Details Added successfully');
							if(isset($_POST['parent_id']) && $_POST['parent_id'] != 0)
								redirect(ADMIN_ROOT_URL.'generic_category?package='.$_POST['parent_id']);
							else
								redirect(ADMIN_ROOT_URL.'generic_category');
						}
					}else{

						$updateStatus = $this->generic_category_model->updateDetails($data, $categoryId);
						if($updateStatus){
							$this->session->set_flashdata('flash_success', 'Retreats Options Details Updated successfully');
							
							if(isset($_POST['parent_id']) && $_POST['parent_id'] != 0)
                                redirect(ADMIN_ROOT_URL.'generic_category?package='.$_POST['parent_id']);
							else
								redirect(ADMIN_ROOT_URL.'generic_category');
						}
					}
				}else{
					$_SESSION = $_POST;
					$classifiedParentId = $_POST['parent_id'];
					$classifiedId =$_POST['id'];
				}
				
			}
			//$this->contentData['parentPageList'] = $this->generic_category_model->getParentCategoryLists('id, title' ,' parent_id=0',' ORDER BY category_order ASC');
			$this->contentData['action'] = $action;
			//$this->contentData['contentSelectData'] = $this->generic_category_model->getAllRecords('*', "is_deleted ='0' AND is_active = '1' AND parent_id=0", 'ORDER BY category_order ASC');
            $this->contentData['packagesList'] = $this->cms_model->getAllRecords('*' ,'',' ORDER BY cms_order ASC', '', true);
			$this->contentData['title']= $action.' Retreats Options';
			$this->headerData['title']= $action.' Retreats Options | Admin Module';
			$this->load->view('admin/templates/header', $this->headerData);
			$this->load->view('admin/add_generic_category', $this->contentData);
			$this->load->view('admin/templates/footer', $this->footerData);
			
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
	}
	function get_parent(){
		$this->generic_category_model->getParentCategoryLists($_GET['id'],$_GET['current_parent_id']);
		exit;
	}
	function order(){
		
		$updateStatus = $this->generic_category_model->changeOrder($_GET['id'],$_GET['category_order'],$_GET['position']);
		$this->session->set_flashdata('flash_success', 'Retreats Options Order Updated successfully');
		if(isset($_GET['parent']) && $_GET['parent'] != 0)
			redirect(ADMIN_ROOT_URL.'generic_category/?package='.$_GET['parent']);
		else
			redirect(ADMIN_ROOT_URL.'generic_category');
								
	}
	function upload_image(){
		$config['file_name'] = date('dmYHis').'_'.$_FILES['category_image']['name'];
		$_POST['category_image'] = $config['file_name'];
		$config['upload_path'] = DIR_UPLOAD_BANNER;
		$config['allowed_types'] = IMAGE_ALLOWED_TYPES;
		$config['max_size']	= MAX_BANNER_IMAGE_SIZE;				
		$this->load->library('upload', $config);		
		if ($this->upload->do_upload('category_image'))
		{
			if($this->input->post('action') == 'Edit') {
					if(isset($_POST['uploaded_file']) && $_POST['uploaded_file']!='' && file_exists(DIR_UPLOAD_BANNER.$_POST['uploaded_file'])){
						unlink(DIR_UPLOAD_BANNER.$_POST['uploaded_file']);
					}
			}
			return TRUE;
		}
		else
		{
			$this->form_validation->set_message('upload_image', $this->upload->display_errors());
			return FALSE;
		}
		
	}
	function email_exist($email){
		$alreadyExist = $this->admin_model->checkEmailExist($email,$_POST['id']);
		if(count($alreadyExist) > 0){
			$this->form_validation->set_message('email_exist', 'The %s is already registered !!!');
			return FALSE;
		}else{
			
			return TRUE;
		}
	}
	public function index()
	{
		$this->load->library('session');
		$pId = $this->uri->segment(4);
		$parentId =  (isset($pId) && $pId != '') ? $this->uri->segment(4) : 0;
		
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
		
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
		
		if($this->session->userdata('admin_id')==''){
			redirect(ADMIN_ROOT_URL.'login');
		}else{

            $where = "is_active = '1' ";
            if (!empty($this->contentData['package_id'])) {
                $where .= " AND parent_id = ".$this->contentData['package_id'];
            }
			$this->contentData['categoryList'] = $this->generic_category_model->getAllRecords('*' , $where,' ORDER BY category_order ASC', '', true);
			$succ_msg = $this->session->flashdata('flash_success');
			$err_msg = $this->session->flashdata('flash_error');
			if(isset($succ_msg) && $succ_msg != ''){				
				$this->contentData['successMsg'] = $this->session->flashdata('flash_success');				
			}
			if(isset($err_msg) && $err_msg != ''){				
				$this->contentData['errMsg'] = $this->session->flashdata('flash_error');				
			}

            //$package_id
            $this->contentData['packagesList'] = $this->cms_model->getAllRecords('*' ,'',' ORDER BY cms_order ASC', '', true);

			$this->contentData['title']= 'Retreats Options List';
			$this->headerData['title']= 'Retreats Options List | Admin Module';
			$this->load->view('admin/templates/header', $this->headerData);
			$this->load->view('admin/generic_category_list', $this->contentData);
			$this->load->view('admin/templates/footer', $this->footerData);
		}
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */