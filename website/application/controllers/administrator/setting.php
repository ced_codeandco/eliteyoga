<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . 'libraries/Admin_controller.php';

class Setting extends Admin_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 public $headerData;
	 public $contentData;
	 public $footerData;
	 public function __construct()
	{
		parent::__construct();

		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->model('admin_model');
		$this->load->model('setting_model');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->headerData['adminModuleList'] = $this->admin_model->getModuleList();
		$this->headerData['isAdminLogin'] = $this->admin_model->checkAdminLogin();
		$this->headerData['activeAdminDetails'] = $this->admin_model->activeAdminDetails();
	}
	function status_inactive(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('5', $this->headerData['activeAdminDetails']->module_access)){
			$settingId =  $this->uri->segment(4);
			if($settingId == ''){
				redirect(ADMIN_ROOT_URL.'setting');
			}else{
				$this->setting_model->changeStatus(0,$settingId);
				$this->session->set_flashdata('flash_success', 'Setting Status changed successfully');
				redirect(ADMIN_ROOT_URL.'setting');
			}
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
		
		
	}
	function status_active(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('5', $this->headerData['activeAdminDetails']->module_access)){
			$settingId =  $this->uri->segment(4);
			if($settingId == ''){
				redirect(ADMIN_ROOT_URL.'setting');
			}else{
				$this->setting_model->changeStatus(1,$settingId);
				$this->session->set_flashdata('flash_success', 'Setting Status changed successfully');
				redirect(ADMIN_ROOT_URL.'setting');
			}
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
		
		
	}
	/*function delete(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('5', $this->headerData['activeAdminDetails']->module_access)){
			$adminId =  $this->uri->segment(4);
			if($adminId == 1){
				$this->session->set_flashdata('flash_error', 'Super Admin can not be deleted');
				redirect(ADMIN_ROOT_URL.'admin');
			}else{
				$this->admin_model->deleteRecord($adminId);
				$this->session->set_flashdata('flash_success', 'Admin deleted successfully');
				redirect(ADMIN_ROOT_URL.'admin');
			}
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
	}*/
	function add(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('5', $this->headerData['activeAdminDetails']->module_access)){
			$settingId =  $this->uri->segment(4);
			$action = 'Add';
			if($settingId == ''){
				$action = 'Add';
				$this->contentData['settingDetails'] = array();
			}else{
				$action = 'Edit';
				$settingDetails = $this->setting_model->getDetails($settingId);
				$this->contentData['settingDetails'] = $settingDetails;
			}
			
			if($this->input->post()){
				
				$this->load->helper(array('form', 'url'));
				$this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>', '</div>');
				$this->form_validation->set_rules('default_title', 'Setting Title', 'trim|required|callback_name_exist');
				$this->form_validation->set_rules('value', 'Setting Value', 'trim|required');
				if ($this->form_validation->run() == TRUE)
				{
					if($this->input->post('action') == 'Add') {
						$insertedId = $this->setting_model->addDetails();
						if($insertedId){
							$this->session->set_flashdata('flash_success', 'Default Setting Details Added successfully');
							redirect(ADMIN_ROOT_URL.'setting');
						}
					}else{
						$updateStatus = $this->setting_model->updateDetails();
						if($updateStatus){
							$this->session->set_flashdata('flash_success', 'Default Setting Details Updated successfully');
                            if (!empty($this->frontendUrl)) {
                                redirect(base64_decode($this->frontendUrl));
                            } else {
                                redirect(ADMIN_ROOT_URL . 'setting');
                            }
						}
					}
				}
				
			}
		
			$this->contentData['action'] = $action;
			$this->headerData['title']= $action.' Setting | Admin Module';
			$this->load->view('admin/templates/header', $this->headerData);
			$this->load->view('admin/add_setting', $this->contentData);
			$this->load->view('admin/templates/footer', $this->footerData);
			
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
		
		
	}
	
	function name_exist($name){
		$alreadyExist = $this->setting_model->checkSettingExist($name,$_POST['id']);
		if(count($alreadyExist) > 0){
			$this->form_validation->set_message('name_exist', 'The %s is already available !!!');
			return FALSE;
		}else{			
			return TRUE;
		}
		
	}
	public function index()
	{
		$this->load->library('session');
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('5', $this->headerData['activeAdminDetails']->module_access)){
		
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
		
		if($this->session->userdata('admin_id')==''){
			redirect(ADMIN_ROOT_URL.'login');
		}else{
			
			$this->contentData['settingList'] = $this->setting_model->getAllRecords('*' ,'',' ORDER BY id ASC');
			$succ_msg = $this->session->flashdata('flash_success');
			$err_msg = $this->session->flashdata('flash_error');
			if(isset($succ_msg) && $succ_msg != ''){				
				$this->contentData['successMsg'] = $this->session->flashdata('flash_success');				
			}
			if(isset($err_msg) && $err_msg != ''){				
				$this->contentData['errMsg'] = $this->session->flashdata('flash_error');				
			}
			$this->headerData['title']= 'Setting List | Admin Module';
			$this->load->view('admin/templates/header', $this->headerData);
			$this->load->view('admin/setting_list', $this->contentData);
			$this->load->view('admin/templates/footer', $this->footerData);
		}
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */