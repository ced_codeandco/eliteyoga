<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . 'libraries/Admin_controller.php';

class Retreats extends Admin_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 public $headerData;
	 public $contentData;
	 public $footerData;
	 public function __construct()
	 {
		parent::__construct();

		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->model(array('admin_model', 'retreats_model', 'cms_model', 'generic_category_model', 'limit_model'));
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->headerData['adminModuleList'] = $this->admin_model->getModuleList();
		$this->headerData['isAdminLogin'] = $this->admin_model->checkAdminLogin();
		$this->headerData['activeAdminDetails'] = $this->admin_model->activeAdminDetails();
		$this->headerData['noneEditPage'] = array('home');
         $this->contentData['package_id'] = !empty($_GET['package']) ? $_GET['package'] : 0;;
		
	}
	function status_inactive(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
			$retreatId =  $this->uri->segment(4);
			if($retreatId == ''){
				redirect(ADMIN_ROOT_URL.'retreats');
			}else{
				$this->retreats_model->changeStatus(0,$retreatId);
				$this->session->set_flashdata('flash_success', 'Retreats Status changed successfully');
				redirect(ADMIN_ROOT_URL.'retreats');
			}
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}		
	}
	function status_active(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
			$retreatId =  $this->uri->segment(4);
			if($retreatId == ''){
				redirect(ADMIN_ROOT_URL.'retreats');
			}else{
				$this->retreats_model->changeStatus(1,$retreatId);
				$this->session->set_flashdata('flash_success', 'Retreats Status changed successfully');
				redirect(ADMIN_ROOT_URL.'retreats');
			}
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
		
	}
	function delete(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
			$retreatId =  $this->uri->segment(4);
			$this->retreats_model->deleteRecord($retreatId);
			$this->session->set_flashdata('flash_success', 'Retreats deleted successfully');
			redirect(ADMIN_ROOT_URL.'retreats');

		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
	}
	
	function add(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
			$retreatId =  $this->uri->segment(4);
			$action = 'Add';
			if($retreatId == ''){
				$action = 'Add';
				$this->contentData['retreatDetails'] = array();
				$classifiedParentId = 0;
				$classifiedId = 0;
			}else{
				$action = 'Edit';
				$retreatDetails = $this->retreats_model->getDetails($retreatId);
                $this->contentData['package_id'] = $retreatDetails->parent_id;
				$this->contentData['limit_id'] = $retreatDetails->limit_id;
				$this->contentData['retreatDetails'] = $retreatDetails;
				$classifiedParentId = $retreatDetails->parent_id;
				$classifiedId = $retreatDetails->id;
			}
			$this->load->library('ckeditor');
			$this->load->library('ckfinder');
			$this->ckeditor->basePath = base_url().'assets/ckeditor/';
			
			$this->ckeditor->config['language'] = 'en';
			$this->ckeditor->config['width'] = '700px';
			$this->ckeditor->config['height'] = '200px';
			
			//Add Ckfinder to Ckeditor
			$this->ckfinder->SetupCKEditor($this->ckeditor,'../../assets/ckfinder/'); 

			if($this->input->post()){
				
				$this->load->helper(array('form', 'url'));
				
				$this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>', '</div>');
				$this->form_validation->set_rules('title', 'Title', 'trim|required');
					
				if ($this->form_validation->run() == TRUE)
				{

                    $data = $this->input->post();
                    unset($data['current_parent_id']);
                    unset($data['action']);
                    if($this->input->post('action') == 'Add') {
                        $retreatData = array(
                            'parent_id' => $data['parent_id'],
                            'title' => $data['title'],
                            'start_date' => $data['start_date'],
                            'end_date' => $data['end_date'],
                            'retreat_details' => serialize(array(
                                'retreat_price' => !empty($data['retreat_price']) ? $data['retreat_price'] : array(),
                                'retreat_value' => !empty($data['retreat_value']) ? $data['retreat_value'] : array()
                            )),
                            'is_active' => $data['is_active'],

                        );
                       /* print_r($data);
                        print_r($retreatData);
                        die();*/
						$insertedId = $this->retreats_model->addDetails($retreatData);
						
						if($insertedId){
							$this->session->set_flashdata('flash_success', 'Retreats Details Added successfully');
							if(isset($_POST['parent_id']) && $_POST['parent_id'] != 0)
								redirect(ADMIN_ROOT_URL.'retreats?package='.$_POST['parent_id']);
							else
								redirect(ADMIN_ROOT_URL.'retreats');
						}
					}else{

						$updateStatus = $this->retreats_model->updateDetails($data, $retreatId);
						if($updateStatus){
							$this->session->set_flashdata('flash_success', 'Retreats Details Updated successfully');
							
							if(isset($_POST['parent_id']) && $_POST['parent_id'] != 0)
                                redirect(ADMIN_ROOT_URL.'retreats?package='.$_POST['parent_id']);
							else
								redirect(ADMIN_ROOT_URL.'retreats');
						}
					}
				}else{
					$_SESSION = $_POST;
					$classifiedParentId = $_POST['parent_id'];
					$classifiedId =$_POST['id'];
				}
				
			}
			$this->contentData['action'] = $action;
			$this->contentData['packagesList'] = $this->cms_model->getAllRecords('*' ,"is_active='1' AND is_deleted='0'",' ORDER BY cms_order ASC', '', true);
            if (!empty($this->contentData['package_id'])) {
                $this->contentData['retreatOptionList'] = $this->generic_category_model->getAllRecords('*', 'parent_id='.$this->contentData['package_id']." AND is_common = '0'", ' ORDER BY category_order ASC', '', true);
                //print_r($this->contentData['retreatOptionList']);
            }
			
			$this->contentData['limitList'] = $this->limit_model->getAllRecords('*');
			
			$this->contentData['title']= $action.' Retreats';
			$this->headerData['title']= $action.' Retreats | Admin Module';
			$this->load->view('admin/templates/header', $this->headerData);
			$this->load->view('admin/add_retreats', $this->contentData);
			$this->load->view('admin/templates/footer', $this->footerData);
			
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
	}
	function get_parent(){
		$this->retreats_model->getParentCategoryLists($_GET['id'],$_GET['current_parent_id']);
		exit;
	}
	function order(){
		
		$updateStatus = $this->retreats_model->changeOrder($_GET['id'],$_GET['category_order'],$_GET['position']);
		$this->session->set_flashdata('flash_success', 'Retreats Order Updated successfully');
		if(isset($_GET['parent']) && $_GET['parent'] != 0)
			redirect(ADMIN_ROOT_URL.'retreats/index/'.$_GET['parent']);
		else
			redirect(ADMIN_ROOT_URL.'retreats');
								
	}
	function upload_image(){
		$config['file_name'] = date('dmYHis').'_'.$_FILES['category_image']['name'];
		$_POST['category_image'] = $config['file_name'];
		$config['upload_path'] = DIR_UPLOAD_BANNER;
		$config['allowed_types'] = IMAGE_ALLOWED_TYPES;
		$config['max_size']	= MAX_BANNER_IMAGE_SIZE;				
		$this->load->library('upload', $config);		
		if ($this->upload->do_upload('category_image'))
		{
			if($this->input->post('action') == 'Edit') {
					if(isset($_POST['uploaded_file']) && $_POST['uploaded_file']!='' && file_exists(DIR_UPLOAD_BANNER.$_POST['uploaded_file'])){
						unlink(DIR_UPLOAD_BANNER.$_POST['uploaded_file']);
					}
			}
			return TRUE;
		}
		else
		{
			$this->form_validation->set_message('upload_image', $this->upload->display_errors());
			return FALSE;
		}
		
	}
	function email_exist($email){
		$alreadyExist = $this->admin_model->checkEmailExist($email,$_POST['id']);
		if(count($alreadyExist) > 0){
			$this->form_validation->set_message('email_exist', 'The %s is already registered !!!');
			return FALSE;
		}else{
			
			return TRUE;
		}
	}
	public function index()
	{
		$this->load->library('session');
		$pId = $this->uri->segment(4);
		$parentId =  (isset($pId) && $pId != '') ? $this->uri->segment(4) : 0;
		
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
		
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
		
		if($this->session->userdata('admin_id')==''){
			redirect(ADMIN_ROOT_URL.'login');
		}else{

            $where = "is_active = '1' ";
            if (!empty($this->contentData['package_id'])) {
                $where .= " AND parent_id = ".$this->contentData['package_id'];
            }
			$this->contentData['categoryList'] = $this->retreats_model->getAllRecords('*' , $where,' ORDER BY category_order ASC', '', true);
			$succ_msg = $this->session->flashdata('flash_success');
			$err_msg = $this->session->flashdata('flash_error');
			if(isset($succ_msg) && $succ_msg != ''){				
				$this->contentData['successMsg'] = $this->session->flashdata('flash_success');				
			}
			if(isset($err_msg) && $err_msg != ''){				
				$this->contentData['errMsg'] = $this->session->flashdata('flash_error');				
			}

            //$package_id
            $this->contentData['packagesList'] = $this->cms_model->getAllRecords('*' ,'',' ORDER BY cms_order ASC', '', true);

			$this->contentData['title']= 'Retreats List';
			$this->headerData['title']= 'Retreats List | Admin Module';
			$this->load->view('admin/templates/header', $this->headerData);
			$this->load->view('admin/retreats_list', $this->contentData);
			$this->load->view('admin/templates/footer', $this->footerData);
		}
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */