<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . 'libraries/Admin_controller.php';
class Admin extends Admin_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 public $headerData;
	 public $contentData;
	 public $footerData;
	 public function __construct()
	{
		parent::__construct();

		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->model('admin_model');
		$this->load->library('form_validation');
		$this->load->library('session');
        $this->headerData['isAdminLogin'] = $this->admin_model->checkAdminLogin();
        $this->headerData['adminModuleList'] = $this->admin_model->getModuleList();
		$this->headerData['isAdminLogin'] = $this->admin_model->checkAdminLogin();
		$this->headerData['activeAdminDetails'] = $this->admin_model->activeAdminDetails();
		
	}
	function status_inactive(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('1', $this->headerData['activeAdminDetails']->module_access)){
			$adminId =  $this->uri->segment(4);
			if($adminId == ''){
				redirect(ADMIN_ROOT_URL.'admin');
			}else{
				$this->admin_model->changeStatus(0,$adminId);
				$this->session->set_flashdata('flash_success', 'Admin Status changed successfully');
				redirect(ADMIN_ROOT_URL.'admin');
			}
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
		
		
	}
	function status_active(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('1', $this->headerData['activeAdminDetails']->module_access)){
			$adminId =  $this->uri->segment(4);
			if($adminId == ''){
				redirect(ADMIN_ROOT_URL.'admin');
			}else{
				$this->admin_model->changeStatus(1,$adminId);
				$this->session->set_flashdata('flash_success', 'Admin Status changed successfully');
				redirect(ADMIN_ROOT_URL.'admin');
			}
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
		
		
	}
	function delete(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('1', $this->headerData['activeAdminDetails']->module_access)){
			$adminId =  $this->uri->segment(4);
			if($adminId == 1){
				$this->session->set_flashdata('flash_error', 'Super Admin can not be deleted');
				redirect(ADMIN_ROOT_URL.'admin');
			}else{
				$this->admin_model->deleteRecord($adminId);
				$this->session->set_flashdata('flash_success', 'Admin deleted successfully');
				redirect(ADMIN_ROOT_URL.'admin');
			}
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
	}
	function change_password(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('1', $this->headerData['activeAdminDetails']->module_access)){
			$adminId =  $this->uri->segment(4);
			
			if($adminId != ''){
				if($this->input->post()){
					
					$this->load->helper(array('form', 'url'));
					$this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>', '</div>');
					$this->form_validation->set_rules('password', 'Password', 'trim|required');
					$this->form_validation->set_rules('retype_password', 'Password again', 'trim|required|callback_check_password');
					if ($this->form_validation->run() == TRUE)
					{
						
						$adminDetails = $this->admin_model->changePassword();
						$this->session->set_flashdata('flash_success', 'Password changed successfully');
						redirect(ADMIN_ROOT_URL.'admin');
					}
				}
				$adminDetails = $this->admin_model->getAdminDetails($adminId);
				$this->contentData['adminDetails'] = $adminDetails;
			}else{
				redirect(ADMIN_ROOT_URL.'admin');
			}
			$this->headerData['title']= 'Change Password | Admin Module';
			$this->load->view('admin/templates/header', $this->headerData);
			$this->load->view('admin/change_admin_password', $this->contentData);
			$this->load->view('admin/templates/footer', $this->footerData);
			
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
	}
	function check_password($password){
		
		if($password != $_POST['retype_password']){
			$this->form_validation->set_message('check_password', 'Password and again password did not match !!!');
			return FALSE;
		}else{
			
			return TRUE;
		}
		
	}
	function add(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('1', $this->headerData['activeAdminDetails']->module_access)){
			$adminId =  $this->uri->segment(4);
			$action = 'Add';
			if($adminId == ''){
				$action = 'Add';
				$this->contentData['adminDetails'] = array();
			}else{
				$action = 'Edit';
				$adminDetails = $this->admin_model->getAdminDetails($adminId);
				$this->contentData['adminDetails'] = $adminDetails;
			}
			
			if($this->input->post()){
				
				$this->load->helper(array('form', 'url'));
				$this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>', '</div>');
				$this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
				$this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_email_exist');
				if($this->input->post('action') == 'Add') {
					$this->form_validation->set_rules('password', 'Password', 'trim|required');
					$this->form_validation->set_rules('retype_password', 'Password again', 'trim|required');
				}
				if ($this->form_validation->run() == TRUE)
				{
					if($this->input->post('action') == 'Add') {
						$_POST['module_access'] 		=  (isset($_POST['module_access']) && count($_POST['module_access']) > 0) ? implode(',',$_POST['module_access']) : '';
						$insertedId = $this->admin_model->addDetails();
						if ($insertedId) {
							$this->session->set_flashdata('flash_success', 'Admin Details Added successfully');
							redirect(ADMIN_ROOT_URL.'admin');
						}
					}else{
						if($this->input->post('id') == 1){
							$_POST['module_access'] = 'FULL';
						} else if ($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' && empty($_POST['module_access'])) {
                            $_POST['module_access'] = 'FULL';
                        } else {
							$_POST['module_access'] =  (isset($_POST['module_access']) && count($_POST['module_access']) > 0) ? implode(',',$_POST['module_access']) : '';	
						}
						$updateStatus = $this->admin_model->updateDetails();
						if($updateStatus){
							$this->session->set_flashdata('flash_success', 'Admin Details Updated successfully');
							redirect(ADMIN_ROOT_URL.'admin');
						}
					}
				}
				
			}
		
			$this->contentData['action'] = $action;
			$this->headerData['title']= $action.' Admin | Admin Module';
			$this->load->view('admin/templates/header', $this->headerData);
			$this->load->view('admin/add_admin', $this->contentData);
			$this->load->view('admin/templates/footer', $this->footerData);
			
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
		
		
	}
	
	function email_exist($email){
		$alreadyExist = $this->admin_model->checkEmailExist($email,$_POST['id']);
		if(count($alreadyExist) > 0){
			$this->form_validation->set_message('email_exist', 'The %s is already registered !!!');
			return FALSE;
		}else{
			
			return TRUE;
		}
		
	}
	public function index()
	{
		$this->load->library('session');
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('1', $this->headerData['activeAdminDetails']->module_access)){
		
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
		
		if($this->session->userdata('admin_id')==''){
			redirect(ADMIN_ROOT_URL.'login');
		}else{
			
			$this->contentData['adminList'] = $this->admin_model->getAllRecords('id, first_name, last_name,email,last_login_date, last_login_ip, is_active,admin_role' ,'',' ORDER BY id ASC');
			$succ_msg = $this->session->flashdata('flash_success');
			$err_msg = $this->session->flashdata('flash_error');
			if(isset($succ_msg) && $succ_msg != ''){				
				$this->contentData['successMsg'] = $this->session->flashdata('flash_success');				
			}
			if(isset($err_msg) && $err_msg != ''){				
				$this->contentData['errMsg'] = $this->session->flashdata('flash_error');				
			}
			$this->headerData['title']= 'Admin List | Admin Module';
			$this->load->view('admin/templates/header', $this->headerData);
			$this->load->view('admin/admin_list', $this->contentData);
			$this->load->view('admin/templates/footer', $this->footerData);
		}
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */