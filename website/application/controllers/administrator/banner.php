<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . 'libraries/Admin_controller.php';
class Banner extends Admin_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 public $headerData;
	 public $contentData;
	 public $footerData;
	 public function __construct()
	 {
		parent::__construct();

		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->model(array('admin_model', 'banner_model', 'cms_model'));
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->headerData['adminModuleList'] = $this->admin_model->getModuleList();
		$this->headerData['isAdminLogin'] = $this->admin_model->checkAdminLogin();
		$this->headerData['activeAdminDetails'] = $this->admin_model->activeAdminDetails();
		$this->headerData['noneEditPage'] = array('home');
        $this->contentData['package_id'] = !empty($_GET['package']) ? $_GET['package'] : 0;;
		
	}
	function status_inactive(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('7', $this->headerData['activeAdminDetails']->module_access)){
			$bannerId =  $this->uri->segment(4);
			if($bannerId == ''){
				redirect(ADMIN_ROOT_URL.'banner');
			}else{
				$this->banner_model->changeStatus(0,$bannerId);
				$this->session->set_flashdata('flash_success', 'Image Status changed successfully');
				redirect(ADMIN_ROOT_URL.'banner');
			}
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}		
	}
	function status_active(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('7', $this->headerData['activeAdminDetails']->module_access)){
			$bannerId =  $this->uri->segment(4);
			if($bannerId == ''){
				redirect(ADMIN_ROOT_URL.'banner');
			}else{
				$this->banner_model->changeStatus(1,$bannerId);
				$this->session->set_flashdata('flash_success', 'Image Status changed successfully');
				redirect(ADMIN_ROOT_URL.'banner');
			}
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
		
	}
	function delete(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('7', $this->headerData['activeAdminDetails']->module_access)){
			$bannerId =  $this->uri->segment(4);
			
				$this->banner_model->deleteRecord($bannerId);
				$this->session->set_flashdata('flash_success', 'Admin deleted successfully');
				redirect(ADMIN_ROOT_URL.'banner');
			
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
	}
	
	function add(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('7', $this->headerData['activeAdminDetails']->module_access)){
			$bannerId =  $this->uri->segment(4);
			$action = 'Add';
			if($bannerId == ''){
				$action = 'Add';
				$this->contentData['bannerDetails'] = array();
			}else{
				$action = 'Edit';
				$imageDetails = $this->banner_model->getDetails($bannerId);
				$this->contentData['imageDetails'] = $imageDetails;
			}
			        
			if($this->input->post()){
				
				$this->load->helper(array('form', 'url'));
				
				$this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>', '</div>');
				$this->form_validation->set_rules('title', 'Title', 'trim|required');
				if(isset($_FILES['image_path']) && $_FILES['image_path']['name']!=''){
					$this->form_validation->set_rules('image_path', 'Image', 'trim|callback_upload_image');
					
				}else{
					$_POST['image_path']	= (isset($_POST['uploaded_file']) && $_POST['uploaded_file'] != '') ? $_POST['uploaded_file'] : '';	
				}
					
				if ($this->form_validation->run() == TRUE)
				{
					if($this->input->post('action') == 'Add') {					
						$insertedId = $this->banner_model->addDetails();
						
						if($insertedId){
							$this->session->set_flashdata('flash_success', 'Image Details Added successfully');
							redirect(ADMIN_ROOT_URL.'banner?package='.$_POST['parent_id']);
						}
					}else{
						
						$updateStatus = $this->banner_model->updateDetails();
						if($updateStatus){
                            $this->session->set_flashdata('flash_success', 'Image Details Updated successfully');
                            redirect(ADMIN_ROOT_URL.'banner?package='.$_POST['parent_id']);
						}
					}
				}else{
					$_SESSION = $_POST;	
				}
				
			}
            $this->contentData['packagesList'] = $this->cms_model->getAllRecords('*' ,"is_active='1' AND is_deleted='0'",' ORDER BY cms_order ASC', '', true);
			$this->contentData['action'] = $action;
			$this->headerData['title']= $action.' Image | Admin Module';
			$this->load->view('admin/templates/header', $this->headerData);
			$this->load->view('admin/add_banner', $this->contentData);
			$this->load->view('admin/templates/footer', $this->footerData);
			
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
	}
	function order(){
		
		$updateStatus = $this->banner_model->changeOrder($this->input->get('id'), $this->input->get('sort_order'),$this->input->get('position'));
		$this->session->set_flashdata('flash_success', 'Image Order Updated successfully');
			redirect(ADMIN_ROOT_URL.'banner');
								
	}
	function upload_image(){
		$config['file_name'] = date('dmYHis').'_'.$_FILES['image_path']['name'];
		$_POST['image_path'] = $config['file_name'];
		$config['upload_path'] = DIR_UPLOAD_BANNER;
		$config['allowed_types'] = IMAGE_ALLOWED_TYPES;
		$config['max_size']	= MAX_BANNER_IMAGE_SIZE;				
		$this->load->library('upload', $config);		
		if ($this->upload->do_upload('image_path'))
		{
			if($this->input->post('action') == 'Edit') {
					if(isset($_POST['uploaded_file']) && $_POST['uploaded_file']!='' && file_exists(DIR_UPLOAD_BANNER.$_POST['uploaded_file'])){
						unlink(DIR_UPLOAD_BANNER.$_POST['uploaded_file']);
					}
			}
			return TRUE;
		}
		else
		{
			$this->form_validation->set_message('upload_image', $this->upload->display_errors());
			return FALSE;
		}
		
	}
	
	public function index()
	{
		$this->load->library('session');

		$parentId =  (isset($pId) && $pId != '') ? $this->uri->segment(4) : 0;
		
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('7', $this->headerData['activeAdminDetails']->module_access)){
		
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
		
		if($this->session->userdata('admin_id')==''){
			redirect(ADMIN_ROOT_URL.'login');
		}else{

            if (!empty($this->contentData['package_id'])) {
                $this->contentData['imageList'] = $this->banner_model->getAllRecords('*', "is_deleted != '1' AND parent_id=".$this->contentData['package_id'], ' ORDER BY sort_order ASC');
            }
			$succ_msg = $this->session->flashdata('flash_success');
			$err_msg = $this->session->flashdata('flash_error');
			if(isset($succ_msg) && $succ_msg != ''){				
				$this->contentData['successMsg'] = $this->session->flashdata('flash_success');				
			}
			if(isset($err_msg) && $err_msg != ''){				
				$this->contentData['errMsg'] = $this->session->flashdata('flash_error');				
			}
            $this->contentData['packagesList'] = $this->cms_model->getAllRecords('*' ,"is_active='1' AND is_deleted='0'",' ORDER BY cms_order ASC', '', true);
			$this->headerData['title']= 'Banner List | Admin Module';
			$this->load->view('admin/templates/header', $this->headerData);
			$this->load->view('admin/banner_list', $this->contentData);
			$this->load->view('admin/templates/footer', $this->footerData);
		}
	}
    public function cropic_save_image(){
        $imagePath = 'temp/';

        $allowedExts = array("gif", "jpeg", "jpg", "png", "GIF", "JPEG", "JPG", "PNG");
        $temp = explode(".", $_FILES["img"]["name"]);
        $extension = end($temp);

//Check write Access to Directory

        if(!is_writable($imagePath)){
            $response = Array(
                "status" => 'error',
                "message" => 'Can`t upload File; no write Access'
            );
            print json_encode($response);
            return;
        }

        if ( in_array($extension, $allowedExts))
        {
            if ($_FILES["img"]["error"] > 0)
            {
                $response = array(
                    "status" => 'error',
                    "message" => 'ERROR Return Code: '. $_FILES["img"]["error"],
                );
            }
            else
            {

                $filename = $_FILES["img"]["tmp_name"];
                list($width, $height) = getimagesize( $filename );
                $uploaded_path_parts = pathinfo($_FILES['img']['name']);
                $clean_name = generate_clean_file_name($_FILES['img']['name']);
                $file_name = $clean_name.'.'.$uploaded_path_parts['extension'];
                $counter = 1;
                while (file_exists($imagePath.$file_name)) {
                    $counter++;
                    $file_name = $clean_name.'_'.$counter.'.'.$uploaded_path_parts['extension'];
                }
                move_uploaded_file($filename,  $imagePath . $file_name);

                $response = array(
                    "status" => 'success',
                    "url" => ROOT_URL_BASE.$imagePath.$file_name,
                    "width" => $width,
                    "height" => $height
                );

            }
        }
        else
        {
            $response = array(
                "status" => 'error',
                "message" => 'something went wrong, most likely file is to large for upload. check upload_max_filesize, post_max_size and memory_limit in you php.ini',
            );
        }

        print json_encode($response);
        exit;
    }

    public function cropic_crop_image() {
        ini_set('post_max_size', 0);
        ini_set('upload_max_filesize', 0);
        ini_set('memory_limit', '-1');
        /*
*	!!! THIS IS JUST AN EXAMPLE !!!, PLEASE USE ImageMagick or some other quality image processing libraries
*/
        $imgUrl = $_POST['imgUrl'];
        // original sizes
        $imgInitW = $_POST['imgInitW'];
        $imgInitH = $_POST['imgInitH'];
        // resized sizes
        $imgW = $_POST['imgW'];
        $imgH = $_POST['imgH'];
        // offsets
        $imgY1 = $_POST['imgY1'];
        $imgX1 = $_POST['imgX1'];
        // crop box
        $cropW = $_POST['cropW'];
        $cropH = $_POST['cropH'];
        // rotation angle
        $angle = $_POST['rotation'];

        $jpeg_quality = 100;

        $output_filename = "banner_image_".date('dmYHis').rand();

        // uncomment line below to save the cropped image in the same location as the original image.
        //$output_filename = dirname($imgUrl). "/Fair_bridge_Image_".rand();

        $what = getimagesize($imgUrl);

        switch(strtolower($what['mime']))
        {
            case 'image/png':
                $img_r = imagecreatefrompng($imgUrl);
                $source_image = imagecreatefrompng($imgUrl);
                $type = '.png';
                break;
            case 'image/jpeg':
                $img_r = imagecreatefromjpeg($imgUrl);
                $source_image = imagecreatefromjpeg($imgUrl);
                error_log("jpg");
                $type = '.jpeg';
                break;
            case 'image/gif':
                $img_r = imagecreatefromgif($imgUrl);
                $source_image = imagecreatefromgif($imgUrl);
                $type = '.gif';
                break;
            default: die('image type not supported');
        }

        $subscript = 0;
        while(file_exists(DIR_UPLOAD_BANNER.$output_filename.$subscript.$type)) {
            $subscript++;
        }
        $output_filename .= $subscript;

        //Check write Access to Directory
        if(!is_writable(dirname(DIR_UPLOAD_BANNER.$output_filename))){
            $response = Array(
                "status" => 'error',
                "message" => 'Can`t write cropped File'
            );
        }else{

            // resize the original image to size of editor
            $resizedImage = imagecreatetruecolor($imgW, $imgH);
            imagecopyresampled($resizedImage, $source_image, 0, 0, 0, 0, $imgW, $imgH, $imgInitW, $imgInitH);
            // rotate the rezized image
            $rotated_image = imagerotate($resizedImage, -$angle, 0);
            // find new width & height of rotated image
            $rotated_width = imagesx($rotated_image);
            $rotated_height = imagesy($rotated_image);
            // diff between rotated & original sizes
            $dx = $rotated_width - $imgW;
            $dy = $rotated_height - $imgH;
            // crop rotated image to fit into original rezized rectangle
            $cropped_rotated_image = imagecreatetruecolor($imgW, $imgH);
            imagecolortransparent($cropped_rotated_image, imagecolorallocate($cropped_rotated_image, 0, 0, 0));
            imagecopyresampled($cropped_rotated_image, $rotated_image, 0, 0, $dx / 2, $dy / 2, $imgW, $imgH, $imgW, $imgH);
            // crop image into selected area
            $final_image = imagecreatetruecolor($cropW, $cropH);
            imagecolortransparent($final_image, imagecolorallocate($final_image, 0, 0, 0));
            imagecopyresampled($final_image, $cropped_rotated_image, 0, 0, $imgX1, $imgY1, $cropW, $cropH, $cropW, $cropH);
            // finally output png image
            //imagepng($final_image, $output_filename.$type, $png_quality);
            imagejpeg($final_image, DIR_UPLOAD_BANNER.$output_filename.$type, $jpeg_quality);
            $response = Array(
                "status" => 'success',
                "url" => DIR_UPLOAD_BANNER_SHOW.$output_filename.$type,
                "file_name" => $output_filename.$type,
            );
        }
        print json_encode($response);
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */