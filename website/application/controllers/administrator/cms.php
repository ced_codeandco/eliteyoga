<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . 'libraries/Admin_controller.php';

class Cms extends Admin_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 public $headerData;
	 public $contentData;
	 public $footerData;
	 public function __construct()
	 {
		parent::__construct();

		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->model('admin_model');
		$this->load->model('cms_model');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->headerData['adminModuleList'] = $this->admin_model->getModuleList();
		$this->headerData['isAdminLogin'] = $this->admin_model->checkAdminLogin();
		$this->headerData['activeAdminDetails'] = $this->admin_model->activeAdminDetails();
		$this->headerData['noneEditPage'] = array('home');
		
	}
	function status_inactive(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('4', $this->headerData['activeAdminDetails']->module_access)){
			$cmsId =  $this->uri->segment(4);
			if($cmsId == ''){
				redirect(ADMIN_ROOT_URL.'cms');
			}else{
				$this->cms_model->changeStatus(0,$cmsId);
				$this->session->set_flashdata('flash_success', 'CMS Status changed successfully');
				redirect(ADMIN_ROOT_URL.'cms');
			}
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}		
	}
	function status_active(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('4', $this->headerData['activeAdminDetails']->module_access)){
			$cmsId =  $this->uri->segment(4);
			if($cmsId == ''){
				redirect(ADMIN_ROOT_URL.'cms');
			}else{
				$this->cms_model->changeStatus(1,$cmsId);
				$this->session->set_flashdata('flash_success', 'CMS Status changed successfully');
				redirect(ADMIN_ROOT_URL.'cms');
			}
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
		
	}
	function delete(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('4', $this->headerData['activeAdminDetails']->module_access)){
			$cmsId =  $this->uri->segment(4);
			
				$this->cms_model->deleteRecord($cmsId);
				$this->session->set_flashdata('flash_success', 'CMS Page deleted successfully');
				redirect(ADMIN_ROOT_URL.'cms');
			
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
	}
	
	function add(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('4', $this->headerData['activeAdminDetails']->module_access)) {
			$cmsId =  $this->uri->segment(4);
			$action = 'Add';
			if($cmsId == ''){
				$action = 'Add';
				$this->contentData['cmsDetails'] = array();
			}else{
				$action = 'Edit';
				$cmsDetails = $this->cms_model->getDetails($cmsId);
				$this->contentData['cmsDetails'] = $cmsDetails;
			}
			$this->load->library('ckeditor');
			$this->load->library('ckfinder');
			$this->ckeditor->basePath = base_url().'assets/ckeditor/';
			
			$this->ckeditor->config['language'] = 'en';
			$this->ckeditor->config['width'] = '1000px';
			$this->ckeditor->config['height'] = '300px';            
			
			//Add Ckfinder to Ckeditor
			$this->ckfinder->SetupCKEditor($this->ckeditor,'../../assets/ckfinder/'); 

			if($this->input->post()){
				
				$this->load->helper(array('form', 'url'));
				
				$this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>', '</div>');
				$this->form_validation->set_rules('title', 'Title', 'trim|required');
				if(isset($_FILES['cms_banner_image']) && $_FILES['cms_banner_image']['name']!=''){
					$this->form_validation->set_rules('cms_banner_image', 'Image', 'trim|callback_upload_image');
					
				}else{
					$_POST['cms_banner_image']	= (isset($_POST['uploaded_file']) && $_POST['uploaded_file'] != '') ? $_POST['uploaded_file'] : '';
				}

					
				if ($this->form_validation->run() == TRUE) {
					if($this->input->post('action') == 'Add') {
						$_POST['description'] = addslashes($_POST['description']);
                        $data = $this->input->post();
                        $data['cms_slug'] = $this->cms_model->generateCMSSlug($this->input->post('title'));
                        if (!empty($data['action'])) unset($data['action']);
                        //print_r($data); die();
						$insertedId = $this->cms_model->addDetails($data);
						
						if($insertedId){
							$this->session->set_flashdata('flash_success', 'CMS Details Added successfully');
							if(isset($_POST['parent_id']) && $_POST['parent_id'] != 0)
								redirect(ADMIN_ROOT_URL.'cms/index/'.$_POST['parent_id']);
							else
								redirect(ADMIN_ROOT_URL.'cms');
						}
					}else{
                        $data = $this->input->post();
                        if (isset($data['action'])) unset($data['action']);
                        if (isset($data['uploaded_file'])) unset($data['uploaded_file']);
                        if (isset($data['uploaded_file1'])) unset($data['uploaded_file1']);
                        if (isset($data['uploaded_file2'])) unset($data['uploaded_file2']);
                        if (isset($data['uploaded_file3'])) unset($data['uploaded_file3']);
                        //print_r($data); //die();
						$updateStatus = $this->cms_model->updateDetails($data);
						if($updateStatus){
							$this->session->set_flashdata('flash_success', 'CMS Details Updated successfully');
                            if (!empty($this->frontendUrl)) {
                                redirect(base64_decode($this->frontendUrl));
                            } else {
                                if (isset($_POST['parent_id']) && $_POST['parent_id'] != 0)
                                    redirect(ADMIN_ROOT_URL . 'cms/index/' . $_POST['parent_id']);
                                else
                                    redirect(ADMIN_ROOT_URL . 'cms');
                            }
						}
					}
				} else {
					$_SESSION = $_POST;	
				}
				
			}
			$this->contentData['parentPageList'] = $this->cms_model->getAllRecords('id, title' ,' parent_id=0',' ORDER BY cms_order ASC');
			$this->contentData['action'] = $action;
			$this->headerData['title']= $action.' CMS | Admin Module';
			$this->load->view('admin/templates/header', $this->headerData);
			$this->load->view('admin/add_cms', $this->contentData);
			$this->load->view('admin/templates/footer', $this->footerData);
			
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
	}
	function order(){
        $id = $this->input->get('id');
        $cms_order = $this->input->get('cms_order');
        $position = $this->input->get('position');
		$updateStatus = $this->cms_model->changeOrder($id, $cms_order, $position);
		$this->session->set_flashdata('flash_success', 'CMS Order Updated successfully');
        $parent = $this->input->get('parent');

		if(!empty($parent) && $parent != 0)
			redirect(ADMIN_ROOT_URL.'cms/index/'.$parent);
		else
			redirect(ADMIN_ROOT_URL.'cms');
								
	}
	function upload_image(){
        $uploaded_path_parts = pathinfo($_FILES['cms_banner_image']['name']);
        $config['file_name'] = date('dmYHis').'.'.$uploaded_path_parts['extension'];
        $counter = 100;
        while (file_exists(DIR_UPLOAD_BANNER.$config['file_name'])) {
            $counter++;
            $config['file_name'] = date('dmYHis').'_'.$counter.'.'.$uploaded_path_parts['extension'];
        }
		$_POST['cms_banner_image'] = $config['file_name'];
		$config['upload_path'] = DIR_UPLOAD_BANNER;
		$config['allowed_types'] = IMAGE_ALLOWED_TYPES;
		$config['max_size']	= MAX_BANNER_IMAGE_SIZE;				
		$this->load->library('upload');
        $this->upload->initialize($config);
		if ($this->upload->do_upload('cms_banner_image'))
		{
			if($this->input->post('action') == 'Edit') {
					if(isset($_POST['uploaded_file']) && $_POST['uploaded_file']!='' && file_exists(DIR_UPLOAD_BANNER.$_POST['uploaded_file'])){
						unlink(DIR_UPLOAD_BANNER.$_POST['uploaded_file']);
					}
			}
			return TRUE;
		}
		else
		{
			$this->form_validation->set_message('upload_image', $this->upload->display_errors());
			return FALSE;
		}
	}

	function upload_logo(){
        $uploaded_path_parts = pathinfo($_FILES['home_logo']['name']);
        $config['file_name'] = date('dmYHis').'.'.$uploaded_path_parts['extension'];
        $counter = 100;
        while (file_exists(DIR_UPLOAD_BANNER.$config['file_name'])) {
            $counter++;
            $config['file_name'] = date('dmYHis').'_'.$counter.'.'.$uploaded_path_parts['extension'];
        }
		$_POST['home_logo'] = $config['file_name'];
		$config['upload_path'] = DIR_UPLOAD_BANNER;
		$config['allowed_types'] = IMAGE_ALLOWED_TYPES;
		$config['max_size']	= MAX_BANNER_IMAGE_SIZE;
		$this->load->library('upload');
        $this->upload->initialize($config);
		if ($this->upload->do_upload('home_logo'))
		{
			if($this->input->post('action') == 'Edit') {
					if(isset($_POST['uploaded_file1']) && $_POST['uploaded_file1']!='' && file_exists(DIR_UPLOAD_BANNER.$_POST['uploaded_file1'])){
						unlink(DIR_UPLOAD_BANNER.$_POST['uploaded_file1']);
					}
			}
			return TRUE;
		}
		else
		{
			$this->form_validation->set_message('upload_logo', $this->upload->display_errors());
			return FALSE;
		}
	}

	function upload_logo_border(){
        $uploaded_path_parts = pathinfo($_FILES['home_logo_border']['name']);
        $config['file_name'] = date('dmYHis').'.'.$uploaded_path_parts['extension'];
        $counter = 100;
        while (file_exists(DIR_UPLOAD_BANNER.$config['file_name'])) {
            $counter++;
            $config['file_name'] = date('dmYHis').'_'.$counter.'.'.$uploaded_path_parts['extension'];
        }
		$_POST['home_logo_border'] = $config['file_name'];
		$config['upload_path'] = DIR_UPLOAD_BANNER;
		$config['allowed_types'] = IMAGE_ALLOWED_TYPES;
		$config['max_size']	= MAX_BANNER_IMAGE_SIZE;
		$this->load->library('upload');
        $this->upload->initialize($config);
		if ($this->upload->do_upload('home_logo_border'))
		{
			if($this->input->post('action') == 'Edit') {
					if(isset($_POST['uploaded_file2']) && $_POST['uploaded_file2']!='' && file_exists(DIR_UPLOAD_BANNER.$_POST['uploaded_file2'])){
						unlink(DIR_UPLOAD_BANNER.$_POST['uploaded_file2']);
					}
			}
			return TRUE;
		}
		else
		{
			$this->form_validation->set_message('upload_logo_border', $this->upload->display_errors());
			return FALSE;
		}
	}

	function upload_main_image(){
        $uploaded_path_parts = pathinfo($_FILES['main_image']['name']);
        $config['file_name'] = date('dmYHis').'.'.$uploaded_path_parts['extension'];
        $counter = 100;
        while (file_exists(DIR_UPLOAD_BANNER.$config['file_name'])) {
            $counter++;
            $config['file_name'] = date('dmYHis').'_'.$counter.'.'.$uploaded_path_parts['extension'];
        }

		$_POST['main_image'] = $config['file_name'];
		$config['upload_path'] = DIR_UPLOAD_BANNER;
		$config['allowed_types'] = IMAGE_ALLOWED_TYPES;
		$config['max_size']	= MAX_BANNER_IMAGE_SIZE;
		$this->load->library('upload');
        $this->upload->initialize($config);
		if ($this->upload->do_upload('main_image'))
		{
			if($this->input->post('action') == 'Edit') {
					if(isset($_POST['uploaded_file3']) && $_POST['uploaded_file3']!='' && file_exists(DIR_UPLOAD_BANNER.$_POST['uploaded_file3'])){
						unlink(DIR_UPLOAD_BANNER.$_POST['uploaded_file3']);
					}
			}
			return TRUE;
		}
		else
		{
			$this->form_validation->set_message('upload_main_image', $this->upload->display_errors());
			return FALSE;
		}
	}

	function email_exist($email){
		$alreadyExist = $this->admin_model->checkEmailExist($email,$_POST['id']);
		if(count($alreadyExist) > 0){
			$this->form_validation->set_message('email_exist', 'The %s is already registered !!!');
			return FALSE;
		}else{
			
			return TRUE;
		}
	}
	public function index()
	{
		$this->load->library('session');
		$pId = $this->uri->segment(4);
		$parentId =  (isset($pId) && $pId != '') ? $this->uri->segment(4) : 0;
		
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('4', $this->headerData['activeAdminDetails']->module_access)){
		
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
		
		if($this->session->userdata('admin_id')==''){
			redirect(ADMIN_ROOT_URL.'login');
		}else{
			
			$this->contentData['cmsList'] = $this->cms_model->getAllRecords('id, title, cms_slug,parent_id,cms_banner_image, is_active,created_date_time,cms_order' ,'parent_id = '.$parentId,' ORDER BY cms_order ASC');
			$succ_msg = $this->session->flashdata('flash_success');
			$err_msg = $this->session->flashdata('flash_error');
			if(isset($succ_msg) && $succ_msg != ''){				
				$this->contentData['successMsg'] = $this->session->flashdata('flash_success');				
			}
			if(isset($err_msg) && $err_msg != ''){				
				$this->contentData['errMsg'] = $this->session->flashdata('flash_error');				
			}
			$this->headerData['title']= 'CMS List | Admin Module';
			$this->load->view('admin/templates/header', $this->headerData);
			$this->load->view('admin/cms_list', $this->contentData);
			$this->load->view('admin/templates/footer', $this->footerData);
		}
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */