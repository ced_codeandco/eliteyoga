<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . 'libraries/Public_controller.php';

class Pay extends Public_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 public $headerData;
	 public $contentData;
	 public $footerData;
	 public function __construct()
	{
		parent::__construct();
	}
	public function index()
	{
        $this->load->library('session');
		$this->load->model('banner_model');
		$this->headerData['title']= 'Homepage';
		$this->headerData['isHomePage']= 1;
		$succ_msg = $this->session->flashdata('flash_success');
		$err_msg = $this->session->flashdata('flash_error');
		if(isset($succ_msg) && $succ_msg != ''){				
			$this->contentData['successMsg'] = $this->session->flashdata('flash_success');				
		}
		if(isset($err_msg) && $err_msg != ''){				
			$this->contentData['errMsg'] = $this->session->flashdata('flash_error');				
		}

        $this->load->view('payment', $this->footerData);
	}


    function sendContactEmail()
    {
        $admin_subject = "New Inquiry on ".SITE_NAME;
        $user_subject = "Thank you for Inquiry on ".SITE_NAME;

        $admin_header = "Dear Administrator,<br /><br />".$_POST['name']." has submitted contact form with below details on ".SITE_NAME.". <br /><br />";
        $user_header = "Dear ".$_POST['name'].",<br /><br /> Thank you for Inquiry on ".SITE_NAME." with below details. <br /><br />";

        $emailMessage  = "<strong>Detail :</strong> <br /><br />";
        $emailMessage .= "Name   		  : ".$_POST['name']." <br /><br />";

        if(isset($_POST['email']) && $_POST['email'] != '')
            $emailMessage .= "E-mail  	  : ".$_POST['email']." <br /><br />";

        if(isset($_POST['comments']) && $_POST['comments'] != '')
            $emailMessage .= "Comments   	  : ".$_POST['comments']." <br /><br />";

        $emailMessage .= "Thanks <br />".SITE_NAME." Team";

        $this->load->library('emailclass');
        $email = $this->emailclass->send_mail(DEFAULT_EMAIL, $admin_subject, $admin_header.$emailMessage);
        $email = $this->emailclass->send_mail($_POST['email'], $user_subject, $user_header.$emailMessage);
    }


	public function no_access(){
		$this->headerData['title']= 'Access Denied';
		$this->load->view('templates/header', $this->headerData);
		$this->load->view('no_access', $this->contentData);
		$this->load->view('templates/footer', $this->footerData);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */