<?php
class Admin_model extends CI_Model {

    var $title          = '';
    var $content        = '';
    var $date           = '';
    public $error       = array();
    public $error_count = 0;
    public $data        = array();

    const FORUM_ADMIN_ROLE = 'administrator';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
    
    function getPage($slug)
    {
		$this->db->where("url like '/$slug'");
        $query = $this->db->get('pages');
		return $query->result();
    }
	
	function fnColumnToField( $i )
	{
		if ( $i == 0 )
			return "id";
		if ( $i == 1 )
			return "name";			
		else if ( $i == 2 )
			return "email";
		
	}
	function checkAdminLogin()
	{
		$user = $this->session->userdata('admin_id');

        if($user == null || $user == '')
		{
			return FALSE;
		}
		else
		{
			$this->db->where('id',$user);
			$query=$this->db->get("tbl_admin");
			if($query->num_rows >0)
			{
				$user_id = $query->row()->id;
				$screen_name = $query->row()->first_name.' '.$query->row()->last_name;
				$data = array(
					'admin_name' => $query->row()->first_name.' '.$query->row()->last_name,
					'admin_id' => $query->row()->id,
					'admin_email' => $query->row()->email,
					'module_access' => $query->row()->module_access,		  
					'is_admin_logged_in' => true
				);
				$this->session->set_userdata($data);
				return TRUE;	
			}else{
				return FALSE;
			}	
	
       }		
	
	}
	function adminLogout(){
		$this->session->sess_destroy();
		return TRUE;
	}
	function adminLogin(){
		$email =  $this->input->post('username');
		$password =  md5($this->input->post('password'));
		
		$this->db->where('email', $email);
		$this->db->where('password', $password);
        $this->db->where('is_active','1');
		$query = $this->db->get('tbl_admin') or die(mysql_error());
		if($query->num_rows >= 1) {
			mysql_query("UPDATE tbl_admin SET last_login_date = NOW(), last_login_ip='".$this->input->ip_address()."' WHERE id= ".$query->row()->id."");
			return $query->row()->id;
		}else{
            return '0';
		}
	}

    /**
     * Function to update password
     * If password token is passed, where condition checks password token and resets the token field aswell
     *
     * @return bool
     */
	function updatePassword($password_token = null){
		$data['password'] = md5($this->input->post('password'));
        if (!empty($password_token)) {
            $data['password_token'] = '';
        }
        if (empty($password_token)) {
            $this->db->where("email", $this->input->post('email'));
        } else {
            $this->db->where("password_token", $password_token);
        }
		$this->db->update('tbl_admin',$data);

        return true;
	}

	function getAdminDetails($adminId){
		
		$this->db->where('id', $adminId);		
		$query = $this->db->get('tbl_admin') or die(mysql_error());
		if($query->num_rows >= 1)
			return $query->row();
		else
            return false;
	}
	function getDetailsFromEmail($email){
		
		$this->db->where('email', $email);		
		$query = $this->db->get('tbl_admin') or die(mysql_error());
		if($query->num_rows >= 1)
			return $query->row();
		else
            return false;
	}
	function getCountryList(){
		$query = $this->db->get('tbl_countries');
		return $query->result();
	}
    function getCountryById($country_id){
        $this->db->where('id',$country_id);
        $query = $this->db->get('tbl_countries');
        return $query->result();
    }
	function getBusinessList(){
		$query = $this->db->get('tbl_business_type');
		return $query->result();
	}
	function getCityList($country_id=0){
		if($country_id != 0){
			$this->db->where('country_id',$country_id);
		}
		$query = $this->db->get('tbl_city');
		return $query->result();
	}
	function getCityListByCountryId($country_id=0){
        $this->db->select('city.*');
        $this->db->from('tbl_cities AS city');
        $this->db->join('tbl_countries AS country', 'city.country_code = country.iso_alpha2');

		if($country_id != 0){
			$this->db->where('country.id',$country_id);
		}
		$query = $this->db->get();
		return $query->result();
	}
	function changePassword(){
		$_POST['password'] = md5($this->input->post('password'));
		$data = array(
			'password' => $this->input->post('password'),
		);
		$this->db->where("id",$this->input->post('id'));
		$this->db->update('tbl_admin',$data);
		
	}
	function changeMyPassword($adminId){
		$_POST['password'] = md5($this->input->post('password'));
		$data = array(
			'password' => $this->input->post('password'),
		);
		$this->db->where("id", $adminId);
		$this->db->update('tbl_admin',$data);

	}
	public function sendEmail($toName, $fromName, $toEmail, $fromEmail, $subject, $content){

	$this->load->library('email');
	$this->email->set_mailtype("html");
	$this->email->set_crlf( "\r\n" );
	$this->email->from($fromEmail,$fromName);
	$this->email->to($toEmail,$toName);
	$this->email->subject($subject);
	$this->email->message($content);
	if($this->email->send())
		return TRUE;
    else
	    return FALSE;
	}
	function addDetails(){
		
		$_POST['password'] = md5($this->input->post('password'));
		$data = array(
			'first_name' => $this->input->post('first_name'),
			'last_name' => $this->input->post('last_name'),
			'email' => $this->input->post('email'),
			'password' => $this->input->post('password'),
			'module_access' => $this->input->post('module_access'),
			'admin_role' => 'Member Admin',
			'is_active' => $this->input->post('is_active'),
			'created_date' =>date('Y-m-d H:i:s')			
		);
		
		$this->db->insert('tbl_admin',$data) or die(mysql_error());
        $user_id = mysql_insert_id();
        $forum_user = array(
            'user_id'  => $user_id,
            'username' => (!empty($data['first_name']) ? $data['first_name'] : ''),
            'role_id' => $this->get_forum_role_id(self::FORUM_ADMIN_ROLE),
        );
        $this->db->insert(TBL_USERS, $forum_user);

		return $user_id;
		
	}
    function get_forum_role_id($role)
    {
        $this->db->select('id');
        $result = $this->db->get_where(TBL_ROLES, array('role' => $role))->result_array();
        $result = $result[0];

        return $result['id'];
    }
	function updateDetails(){
		
		$data = array(
			'first_name' => $this->input->post('first_name'),
			'last_name' => $this->input->post('last_name'),
			'email' => $this->input->post('email'),
			'module_access' => $this->input->post('module_access'),
			'is_active' => $this->input->post('is_active')
		);
		$this->db->where("id",$this->input->post('id'));
		$this->db->update('tbl_admin',$data);

		
		return true;
		
	}
	function updateCurrentDetails(){
		
		$data = array(
			'first_name' => $this->input->post('first_name'),
			'last_name' => $this->input->post('last_name'),
			//  'email' => $this->input->post('email')
		);
		$this->db->where("id",$this->input->post('id'));
		$this->db->update('tbl_admin',$data);

		
		return true;
		
	}
	
	function checkEmailExist($email,$id=0){
		if($email!='' && $id != 0){
			$sql = "SELECT * FROM tbl_admin WHERE email = '".$email."' AND id != $id";
		}else if($email!=''){
			$sql = "SELECT * FROM tbl_admin WHERE email = '".$email."' ";
		}
		$query = $this->db->query($sql);
		return $query->result();

	}
	public function activeAdminDetails(){
		$adminId = $this->session->userdata('admin_id');
		if(isset($adminId) && $adminId > 0){
			$this->db->where('id', $adminId);		
			$query = $this->db->get('tbl_admin') or die(mysql_error());
			if($query->num_rows >= 1){
				$query->row()->module_access = explode(',',$query->row()->module_access);
				return $query->row();
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	function changeStatus($status,$id){
		
		mysql_query("UPDATE tbl_admin SET is_active = '$status' WHERE id= ".$id."");
		return true;
	}
	function deleteRecord($id){
		
		mysql_query("DELETE FROM tbl_admin WHERE id= ".$id);
		return true;
	}
	function getModuleList(){
			
		$query = $this->db->get('tbl_module');

		return $query->result();
	}
	function getAllRecords($all='*',$where='',$orderby='',$limit=''){
		
		$sql ="select $all FROM tbl_admin WHERE 1=1 ";
		if($where!=''){
			$sql .= " AND $where ";
		}
		if($orderby!=''){
			$sql .= " $orderby ";
		}
		if($limit!=''){
			$sql .= " $limit ";
		}
		$query = $this->db->query($sql);
		return $query->result();
	}

    /**
     * Function to generate random string and update the token field of admin table
     *
     * @param $user_id
     *
     * @return string
     */
    function setPasswordToken($user_id)
    {
        $reset_pwd_string = strtolower(base64_encode($user_id . gen_random_string(15)));
        $this->db->where('id', $user_id);
        $this->db->update('tbl_admin', array('password_token' => $reset_pwd_string));

        return $reset_pwd_string;
    }

    /**
     * Function to check if password token is valid
     *
     * @param $password_token
     *
     * @return boolean
     */
    function isValidPasswordToken($password_token)
    {
        $this->db->select('id');
        $this->db->where('password_token', $password_token);
        $query = $this->db->get('tbl_admin');

        return ($query->num_rows > 0);
    }

    /**
     * Function to get aget group
     */
    function getAgeGroupList()
    {
        $query = $this->db->order_by('id')->get('tbl_agegroup');

        return $query->result();
    }

    /**
     * Function to get ConditionList
     */
    function getConditionList()
    {
        $query = $this->db->get('tbl_classified_condition');

        return $query->result();
    }

    /**
     * Function to get aget group
     */
    function getUsageList()
    {
        $query = $this->db->get('tbl_usage');

        return $query->result();
    }

    /**
     * Function to get aget group
     */
    function getSpecializationList()
    {
        $query = $this->db->get('tbl_specialization');

        return $query->result();
    }

    /**
     * ---------------------------------------------------------------------
     * ==========================FORUM CODE=====================BEGINS======
     * ---------------------------------------------------------------------
     */
    // start user function
    public function user_edit()
    {
        $row = $this->input->post('row');
        if ($row['password'] != "" || $row['password2'] != "") {
            if ($row['password'] != $row['password2']) {
                $this->error['password'] = 'Password not match';
            } else if (strlen($row['password']) < 5) {
                $this->error['password'] = 'Password minimum 5 character';
            }
        }

        if (count($this->error) == 0) {

            if ($row['password'] != "" && $row['password2'] != "") {
                $key = $this->config->item('encryption_key');
                $row['password'] = $this->encrypt->encode($row['password'], $key);
            } else {
                unset($row['password']);
            }

            unset($row['password2']);

            $this->db->where('id', $row['id']);
            $this->db->update(TBL_USERS, $row);
        } else {
            $this->error_count = count($this->error);
        }
    }
    // end user function

    // start roles function
    public function role_create()
    {
        $row = $this->input->post('row');

        // check role name
        if (strlen($row['role']) == 0) {
            $this->error['role'] = 'Role name cannot be empty';
        } else {
            $role_check = $this->db->get_where(TBL_ROLES, array('role' => $row['role']));
            if ($role_check->num_rows() > 0) {
                $this->error['role'] = 'Role name "'.$row['role'].'" already in use';
            }
        }

        // check roles
        if (!isset($row['roles'])) {
            $this->error['roles'] = 'Choose minimum 1 role';
        }

        if (count($this->error) == 0) {
            $data = array();
            $data['role'] = $row['role'];
            foreach ($row['roles'] as $key => $value) {
                $data[$key] = 1;
            }
            $this->db->insert(TBL_ROLES, $data);
        } else {
            $this->error_count = count($this->error);
        }
    }

    public function role_edit()
    {
        $row = $this->input->post('row');

        // check role name
        if (strlen($row['role']) == 0) {
            $this->error['role'] = 'Role name cannot be empty';
        } else {
            if ($row['role'] != $row['role_c']) {
                $role_check = $this->db->get_where(TBL_ROLES, array('role' => $row['role']));
                if ($role_check->num_rows() > 0) {
                    $this->error['role'] = 'Role name "'.$row['role'].'" already in use';
                }
            }
        }

        // check roles
        if (!isset($row['roles'])) {
            $this->error['roles'] = 'Choose minimum 1 role';
        }

        if (count($this->error) == 0) {
            unset($row['role_c']);

            // reset row value
            $row_reset = $row['roles'];
            foreach ($row_reset as $key => $value) {
                $row_reset[$key] = 0;
            }

            $this->db->where('id', $row['id']);
            $this->db->update(TBL_ROLES, $row_reset);

            // update role
            $data = array();
            $data['role'] = $row['role'];
            foreach ($row['roles'] as $key => $value) {
                $data[$key] = 1;
            }
            $this->db->where('id', $row['id']);
            $this->db->update(TBL_ROLES, $data);
        } else {
            $this->error_count = count($this->error);
        }
    }

    public function role_get_all()
    {
        $this->db->order_by('role', 'asc');
        $data = $this->db->get(TBL_ROLES)->result_array();
        $data_return = array();
        $loop = 0;
        foreach ($data as $role)
        {
            foreach ($role as $key => $value)
            {
                $data_return[$loop][$key] = $value;
            }
            $loop++;
        }
        return $data_return;
    }
    // end roles function

    // start category function
    public function category_create()
    {
        $row = $this->input->post('row');

        // check name
        $is_exist_name = $this->db->get_where(TBL_CATEGORIES,
            array('name' => $row['name']))->num_rows();
        if ($is_exist_name > 0) {
            $this->error['name'] = 'Name already in use';
        }
        if (strlen($row['name']) == 0) {
            $this->error['name'] = 'Name cannot be empty';
        }

        // check slug
        $is_exist_slug = $this->db->get_where(TBL_CATEGORIES,
            array('slug' => $row['slug']))->num_rows();
        if ($is_exist_slug > 0) {
            $this->error['slug'] = 'Slug already in use';
        }
        if (strlen($row['slug']) == 0) {
            $this->error['slug'] = 'Slug cannot be empty';
        }

        if (count($this->error) == 0) {
            $this->db->insert(TBL_CATEGORIES, $row);
        } else {
            $this->error_count = count($this->error);
        }
    }

    public function category_edit()
    {
        $row = $this->input->post('row');

        // check name
        if (strlen($row['name']) == 0) {
            $this->error['name'] = 'Name cannot be empty';
        } else {
            if ($row['name'] != $row['name_c']) {
                $role_check = $this->db->get_where(TBL_CATEGORIES, array('name' => $row['name']));
                if ($role_check->num_rows() > 0) {
                    $this->error['name'] = 'Role name "'.$row['name'].'" already in use';
                }
            }
        }

        // check slug
        if (strlen($row['slug']) == 0) {
            $this->error['slug'] = 'Slug cannot be empty';
        } else {
            if ($row['slug'] != $row['slug_c']) {
                $role_check = $this->db->get_where(TBL_CATEGORIES, array('slug' => $row['slug']));
                if ($role_check->num_rows() > 0) {
                    $this->error['slug'] = 'Slug "'.$row['slug'].'" already in use';
                }
            }
        }

        if (count($this->error) == 0) {
            unset($row['name_c']);
            unset($row['slug_c']);
            $this->db->where('id', $row['id']);
            $this->db->update(TBL_CATEGORIES, $row);
        } else {
            $this->error_count = count($this->error);
        }
    }

    public function category_get_all($cat_id = 0)
    {
        $this->data = array();
        $this->db->order_by('name', 'asc');
        $query = $this->db->get_where(TBL_CATEGORIES, array('parent_id' => $cat_id, 'is_active' => '1', 'is_deleted' => '0'));
        $counter = 0;
        foreach ($query->result() as $row) {
            $this->data[$counter]['id'] = $row->id;
            $this->data[$counter]['parent_id'] = $row->parent_id;
            $this->data[$counter]['name'] = $row->name;
            $this->data[$counter]['slug'] = $row->slug;
            $this->data[$counter]['real_name'] = $row->name;
            $children = $this->category_get_children($row->id, ' - ', $counter);
            $counter = $counter + $children;
            $counter++;
        }
        return $this->data;
    }

    public function category_get_children($id, $separator, $counter)
    {
        $this->db->order_by('name', 'asc');
        $query = $this->db->get_where(TBL_CATEGORIES, array('parent_id' => $id));
        if ($query->num_rows() == 0)
        {
            return FALSE;
        }
        else
        {
            foreach($query->result() as $row)
            {
                $counter++;
                $this->data[$counter]['id'] = $row->id;
                $this->data[$counter]['parent_id'] = $row->parent_id;
                $this->data[$counter]['name'] = $separator.$row->name;
                $this->data[$counter]['slug'] = $row->slug;
                $this->data[$counter]['real_name'] = $row->name;
                $children = $this->category_get_children($row->id, $separator.' - ', $counter);

                if ($children != FALSE)
                {
                    $counter = $counter + $children;
                }
            }
            return $counter;
        }
    }

    public function category_get_all_parent($id, $counter)
    {
        $row = $this->db->get_where(TBL_CATEGORIES, array('id' => $id))->row_array();
        $this->data[$counter] = $row;
        if (!empty($row['parent_id']) && $row['parent_id'] != 0) {
            $counter++;
            $this->category_get_all_parent($row['parent_id'], $counter);
        }
        return array_reverse($this->data);
    }
    // end category function

    // start thread function
    public function thread_get_all($start = 0, $limit = 20)
    {
        $sql = "SELECT a.*, b.name as cat_name FROM ".TBL_THREADS." a, ".TBL_CATEGORIES." b
                WHERE a. category_id = b.id ORDER BY a.date_add DESC LIMIT ".$start.", ".$limit;
        return $this->db->query($sql)->result();
    }

    public function thread_edit()
    {
        $thread = $this->input->post('row');

        // check title
        if (strlen($thread['title']) == 0) {
            $this->error['title'] = 'Title cannot be empty';
        }

        // check category
        if ($thread['category_id'] == "0") {
            $this->error['category'] = 'Choose category';
        }

        if (count($this->error) == 0) {
            // update thread
            $this->db->where('id', $thread['id']);
            $this->db->update(TBL_THREADS, $thread);
        } else {
            $this->error_count = count($this->error);
        }
    }
    function getCityLookup(){
        $query = $this->db->get('tbl_cities');
        $results = $query->result();
        $return = array();
        foreach ($results as $result) {
            $return[$result->id] = $result;
        }

        return $return;
    }
    // end thread function
}