<?php
class Subscription_model extends CI_Model {

    var $title   = '';
    var $content = '';
    var $date    = '';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
    
    function addDetails($data){
		
		$this->db->insert('tbl_booking_requests',$data) or die(mysql_error());
        //echo $this->db->last_query();
		$id=mysql_insert_id();
		return $id;
		
	}
	
	function updateDetails($data){

		$this->db->where("id",$this->input->post('id'));
		$this->db->update('tbl_booking_requests',$data);
		
		
		return true;
		
	}
	function changeStatus($status,$id){
		mysql_query("UPDATE tbl_booking_requests SET is_active = '$status' WHERE id= ".$id."");
		return true;
	}
	function deleteRecord($id){		
		mysql_query("UPDATE tbl_booking_requests SET is_deleted = '1',is_active = '0' WHERE id= ".$id."");
		return true;
	}
	function getDetails($id){
		
		$this->db->where('id', $id);
		$this->db->where('is_deleted', '0');
		$query = $this->db->get('tbl_booking_requests') or die(mysql_error());
		if($query->num_rows >= 1)
			return $query->row();
		else
            return false;
	}
	function getDetailsFromEmail($email){
		
		$this->db->where('email', $email);
		$this->db->where('is_deleted', '0');
		$query = $this->db->get('tbl_booking_requests') or die(mysql_error());
		if($query->num_rows >= 1)
			return $query->row();
		else
            return false;
	}
	function getAllRecords($all='*',$where='',$orderby='',$limit=''){
		$this->load->model('cms_model');
		$sql ="select $all FROM tbl_booking_requests WHERE 1=1 ";
		if($where!=''){
			$sql .= " AND $where ";
		}
		$sql .= " AND is_deleted='0' ";
		if($orderby!=''){
			$sql .= " $orderby ";
		}
		if($limit!=''){
			$sql .= " $limit ";
		}
		
		$query = $this->db->query($sql);
		$query_data = $query->result();
		//package_name
        if(count($query_data) > 0)
        {
            foreach ($query_data as $key => $value)
            {
                $query_data[$key]->package_details = $this->cms_model->getDetails($value->package_id);
            }

        }

		return $query_data;
	}

    function add_reply($booking_id, $data) {
        if (!empty($booking_id)) {

            $this->db->where("id",$booking_id);
            $this->db->update('tbl_booking_requests',$data);
            //echo $this->db->last_query();
        }
    }

    function update_payment_status($booking_id, $transaction_no) {
		
		if (!empty($booking_id)) {
            $data = array(
                'trans_date_time'   => date('Y-m-d H:i:s'),
                'transaction_no'    => $transaction_no,
                'payment_processed' => '1'
            );

            $this->db->where("id",$booking_id);
            $this->db->update('tbl_booking_requests',$data);
        }
		
        //if (!empty($booking_id)) {
        //    $data = array(
        //        'trans_date_time'   => date('Y-m-d H:i:s'),
        //        'transaction_no'    => $transaction_no,
        //        'trans_amount'      => $amount,
        //        'trans_currency'    => $currency,
        //        'payment_processed' => '1'
        //    );
        //
        //    $this->db->where("id",$booking_id);
        //    $this->db->update('tbl_booking_requests',$data);
        //    //echo $this->db->last_query();
        //}
    }
    function sendBookingReply($booking_id) {
        $booking_details = $this->getDetails($booking_id);

        $user_subject = "Your booking has been approved ".SITE_NAME;
        $message = "Hello $booking_details->name,<br /><br />";
        $message .= nl2br($booking_details->reply_message);
        $message .= "Price of package: ".$booking_details->price_quoted."<br /><br />";
        $message .= "Please click on the link below to make payment:<br />";
        $url = ROOT_URL."payment/".$booking_details->id."/".$booking_details->package_id."/".$booking_details->retreat_id;
        $message .= "<a href='$url'>$url</a><br />";



        $message .= "<br />Thanks <br />".SITE_NAME." Team";

        $this->load->library('emailclass');
        $email = $this->emailclass->send_mail($booking_details->email, $user_subject, $message);
    }
    function send_payment_success_email($booking_id, $package, $retreat) {
        $booking_details = $this->getDetails($booking_id);

        $admin_subject = "New transaction on ".SITE_NAME;
        $user_subject = "Thank you for transaction on ".SITE_NAME;

        $admin_header = "Dear Administrator,<br /><br />".$booking_details->name." has made a transaction of ".'$'.$booking_details->trans_amount."  on ".SITE_NAME.". <br /><br />";
        //$admin_header .= "Please verify the transaction";
        $user_header = "Dear ".$booking_details->name.",<br /><br /> Thank you for your transaction of ".'$'.$booking_details->trans_amount."  on ".SITE_NAME." <br /><br />";
        $user_header .= "We will get back to you once the payment is confirmed.<br /><br />";

        $emailMessage = "<b>Details of transaction:</b><br />";
        $emailMessage .= "Package: $package->title<br />";
        $emailMessage .= "Date: ".date('d M Y ', strtotime($booking_details->start_date))." to ".date('d M Y ', strtotime($booking_details->end_date))."<br />";
        $emailMessage .= "Transaction Code: ".$booking_details->transaction_no."<br />";

        $emailMessage .= "<br />Thanks <br />".SITE_NAME." Team";

        $this->load->library('emailclass');
        $email = $this->emailclass->send_mail(DEFAULT_EMAIL, $admin_subject, $admin_header.$emailMessage);
        $email = $this->emailclass->send_mail($_POST['email'], $user_subject, $user_header.$emailMessage);
    }

}