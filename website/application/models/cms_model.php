<?php
class Cms_model extends CI_Model {

    var $title   = '';
    var $content = '';
    var $date    = '';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
    
    function addDetails($data = array()){
		
		
		$order = $this->getLastOrder();
		$cms_order = $order + 1;
		if (empty($data)) {
            $data = array(
                'title' => $this->input->post('title'),
                'cms_slug' => $this->input->post('cms_slug'),
                'parent_id' => $this->input->post('parent_id'),
                'small_description' => $this->input->post('small_description'),
                'description' => $this->input->post('description'),
                'cms_banner_image' => $this->input->post('cms_banner_image'),
                'cms_order' => $cms_order,
                'meta_title' => $this->input->post('meta_title'),
                'meta_desc' => $this->input->post('meta_desc'),
                'meta_keywords' => $this->input->post('meta_keywords'),
                'on_header' => $this->input->post('on_header'),
                'on_home' => $this->input->post('on_home'),
                'on_footer' => $this->input->post('on_footer'),
                'is_active' => $this->input->post('is_active'),
                'created_by' => $this->session->userdata('admin_id'),
                'updated_by' => $this->session->userdata('admin_id'),
                'created_date_time' => date('Y-m-d H:i:s')
            );
        }
        if (empty($data['created_ip'])) {
            $data['created_ip'] = $this->input->ip_address();
        }
        if (empty($data['cms_order'])) {
            $data['cms_order'] = $cms_order;
        }
        $data['created_by'] = $this->session->userdata('admin_id');
        $data['updated_by'] = $this->session->userdata('admin_id');
        $data['created_date_time'] = date('Y-m-d H:i:s');

		$this->db->insert('tbl_cms',$data) or die(mysql_error()); 	
		$id=mysql_insert_id();
		return $id;
		
	}
	
	function updateDetails($data = array()){
		if(LOG_ENTRY == TRUE){
			//$id = $this->addCMSLog($this->input->post('id'));
		}
        if (empty($data)) {
            $data = array(
                'title' => $this->input->post('title'),
                'parent_id' => $this->input->post('parent_id'),
                'small_description' => $this->input->post('small_description'),
                'description' => $this->input->post('description'),
                'cms_banner_image' => $this->input->post('cms_banner_image'),
                'meta_title' => $this->input->post('meta_title'),
                'meta_desc' => $this->input->post('meta_desc'),
                'meta_keywords' => $this->input->post('meta_keywords'),
                'on_header' => $this->input->post('on_header'),
                'on_footer' => $this->input->post('on_footer'),
                'is_active' => $this->input->post('is_active'),
                'updated_by' => $this->session->userdata('admin_id')
            );
        }

        if (empty($data['updated_ip'])) {
            $data['updated_ip'] = $this->input->ip_address();
        }
        $data['updated_by'] = $this->session->userdata('admin_id');

		$this->db->where("id",$this->input->post('id'));
		$this->db->update('tbl_cms',$data);
		//echo $this->db->last_query(); die();
		
		return true;
		
	}
	function addCMSLog($id){
		$cmsDetails = $this->getDetails($id);
		$data = array(
			'title' => $cmsDetails->title,
			'cms_id' =>  $cmsDetails->id,
			'parent_id' =>  $cmsDetails->parent_id,
			'small_description' => $cmsDetails->small_description,
			'description' =>  $cmsDetails->description,
			'cms_banner_image' =>  $cmsDetails->cms_banner_image,
			'meta_title' => $cmsDetails->meta_title,
			'meta_desc' => $cmsDetails->meta_desc,
			'meta_keywords' => $cmsDetails->meta_keywords,
			'on_header' => $cmsDetails->on_header,
			'on_footer' => $cmsDetails->on_footer,
			'updated_by' => $this->session->userdata('admin_id'),
			'updated_date_time' =>date('Y-m-d H:i:s')			
		);
		
		$this->db->insert('tbl_cms_log',$data) or die(mysql_error()); 	
		return mysql_insert_id();
	}
	function changeStatus($status,$id){
		
		mysql_query("UPDATE tbl_cms SET is_active = '$status' WHERE id= ".$id."");
		return true;
	}
	function deleteRecord($id){
		mysql_query("UPDATE tbl_cms SET is_deleted = '1', deleted_date_time = '".date('Y-m-d H:i:s')."' WHERE id= ".$id."");
		return true;
	}
	function getDetails($id){
		$this->db->where('is_deleted', '0');
		$this->db->where('id', $id);		
		$query = $this->db->get('tbl_cms') or die(mysql_error());
		if($query->num_rows >= 1)
			return $query->row();
		else
            return false;
	}
	function generateCMSSlug($title='cms page'){
		$urltitle=preg_replace('/[^a-z0-9]/i',' ', ltrim(rtrim(strtolower($title))));
		$newurltitle=str_replace(" ","-",$urltitle);
		$queryCount = "SELECT cms_slug from tbl_cms WHERE cms_slug LIKE '".$newurltitle."%'";
		$rqC = mysql_num_rows(mysql_query($queryCount));
		if($rqC != 0){
			$newurltitle = $newurltitle.'-'.$rqC; 
		}
		return $newurltitle;				
	}
	function getAllRecords($all='*',$where='',$orderby='',$limit='', $get_details = false){
		if ($get_details == true) {
            $this->load->model(array('generic_category_model', 'retreats_model', 'generic_category_model', 'banner_model'));
        }
		$sql ="select $all FROM tbl_cms WHERE 1=1 ";
		if($where!=''){
			$sql .= " AND $where ";
		}
		$sql .= " AND is_deleted='0' ";
		if($orderby!=''){
			$sql .= " $orderby ";
		}
		if($limit!=''){
			$sql .= " $limit ";
		}
        //echo $sql;
		$query = $this->db->query($sql);
		$query_data = $query->result();
		if(count($query_data) > 0 && $get_details == true)
		{
			$i = 0;
			foreach ($query->result_array() as $value) 
			{
				
				$query_data[$i]->images = $this->banner_model->getAllRecords($value['id']);
                $query_data[$i]->imageList = $this->banner_model->getAllRecords('*', "is_deleted != '1' AND is_active ='1' AND  parent_id=".$value['id'], ' ORDER BY sort_order ASC');
                $query_data[$i]->retreatOptions = $this->generic_category_model->getAllRecords('*', "is_deleted != '1' AND is_active ='1' AND  parent_id=".$value['id'], ' ORDER BY category_order ASC');
                $query_data[$i]->retreatList = $this->retreats_model->getAllRecords('*', "is_deleted != '1' AND is_active ='1' AND  parent_id=".$value['id'], ' ORDER BY start_date ASC');
				$i++;
			}
		
		}
		
		return $query_data;
	}
	function getCMSPageList($all='*',$where='',$orderby='',$limit=''){
		
		$sql ="select $all FROM tbl_cms WHERE 1=1 ";
		if($where!=''){
			$sql .= " AND $where ";
		}
		$sql .= " AND is_deleted='0' ";
		if($orderby!=''){
			$sql .= " $orderby ";
		}
		if($limit!=''){
			$sql .= " $limit ";
		}
        echo $sql;
		$query = $this->db->query($sql);
		$query_data = $query->result();
		
		if(count($query_data) > 0 )
		{
			$i = 0;
			foreach ($query->result_array() as $value) 
			{
				
				$sql1 ="select $all FROM tbl_cms WHERE parent_id = ".$value['id']." AND is_deleted='0' AND is_active='1'";
				$query = $this->db->query($sql1);
				$query_data[$i]->sub_page_List = $query->result();
				$i++;
			}
		
		}
		
		return $query_data;
	}
	function getSubPageCount($parent_id){
		$sql ="select count(id) as sub_page_count FROM tbl_cms WHERE parent_id = $parent_id AND is_deleted='0' ";
		$query = $this->db->query($sql);
		$result = $query->result();
		return $result[0]->sub_page_count;		
	}
	function getLastOrder(){
		$sql ="select cms_order FROM tbl_cms ORDER BY cms_order desc LIMIT 0,1 "; 
		$query = $this->db->query($sql);
		$result = $query->result();
		return $result[0]->cms_order;	
		
	}
	function changeOrder($id,$cms_order,$position){

		if($id!='' && $cms_order!='' && $position!=''){
		$pageDetails = $this->getDetails($id);	
			if($position=='Dn'){
				$qr="select cms_order,id from tbl_cms where parent_id = $pageDetails->parent_id  AND cms_order > '".$cms_order."' AND is_deleted='0'  order by cms_order asc limit 0,1";
				$query = $this->db->query($qr);
				$result1 = $query->result();
				if($result1 && count($result1)>0){
					$NewId=$result1[0]->id;
					$NewOrder = $result1[0]->cms_order;
					$qry = "UPDATE tbl_cms SET `cms_order`= $cms_order WHERE id =".$NewId;
						mysql_query($qry);

					$qry1 = "UPDATE tbl_cms SET `cms_order`= $NewOrder WHERE id =".$id;
					mysql_query($qry1);
				}
			}
			
			if($position=='Up'){
				$qr="select cms_order,id from tbl_cms where parent_id = $pageDetails->parent_id  AND cms_order < '".$cms_order."' AND is_deleted='0' order by cms_order desc limit 0,1";
				$query = $this->db->query($qr);
				$result1 = $query->result();
				
				if($result1 && count($result1)>0){
					$NewId=$result1[0]->id;
					$NewOrder = $result1[0]->cms_order;
					$qry = "UPDATE tbl_cms SET `cms_order`= $cms_order WHERE id =".$NewId;
					mysql_query($qry);

					$qry1 = "UPDATE tbl_cms SET `cms_order`= $NewOrder WHERE id =".$id;
					mysql_query($qry1);
				}
			}
		}		
	}

    /**
     * Function to get page details with slug
     *
     * @param $cms_slug
     *
     * @return bool
     */
    function getPageDetails($cms_slug){
        $this->db->where('cms_slug', $cms_slug);
        $query = $this->db->get('tbl_cms');
        $query_data = $query->result();

        return ($query_data != false && is_array($query_data)) ? $query_data[0] : false;
    }
}