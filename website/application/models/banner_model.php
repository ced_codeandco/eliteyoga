<?php
class Banner_model extends CI_Model {

    var $title   = '';
    var $content = '';
    var $date    = '';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
    
    function addDetails(){
		
		$data = array(
			'title' => $this->input->post('title'),
			'parent_id' => $this->input->post('parent_id'),
			'advertize_url' => $this->input->post('advertize_url'),
			'image_path' => $this->input->post('image_path'),
			'advertize_type' => $this->input->post('advertize_type'),			
			'is_active' => $this->input->post('is_active'),
			'created_date_time' =>date('Y-m-d H:i:s')			
		);
		if (empty($data['sort_order'])) {
            $order = $this->getLastOrder();
            $data['sort_order'] = $order + 1;
        }
		$this->db->insert('tbl_banner',$data) or die(mysql_error()); 	
		$id=mysql_insert_id();
		return $id;
		
	}
    function changeOrder($id,$sort_order,$position){

        if($id!='' && $sort_order!='' && $position!=''){
            $pageDetails = $this->getDetails($id);
            if($position=='Dn'){
                $qr="select sort_order,id from tbl_banner where  sort_order > '".$sort_order."' AND is_deleted='0'  order by sort_order asc limit 0,1";
                $query = $this->db->query($qr);
                $result1 = $query->result();
                if($result1 && count($result1)>0){
                    $NewId=$result1[0]->id;
                    $NewOrder = $result1[0]->sort_order;
                    $qry = "UPDATE tbl_banner SET `sort_order`= $sort_order WHERE id =".$NewId;
                    mysql_query($qry);

                    $qry1 = "UPDATE tbl_banner SET `sort_order`= $NewOrder WHERE id =".$id;
                    mysql_query($qry1);
                }
            }

            if($position=='Up'){
                $qr="select sort_order,id from tbl_banner where  sort_order < '".$sort_order."' AND is_deleted='0' order by sort_order desc limit 0,1";
                $query = $this->db->query($qr);
                $result1 = $query->result();

                if($result1 && count($result1)>0){
                    $NewId=$result1[0]->id;
                    $NewOrder = $result1[0]->sort_order;
                    $qry = "UPDATE tbl_banner SET `sort_order`= $sort_order WHERE id =".$NewId;
                    mysql_query($qry);

                    $qry1 = "UPDATE tbl_banner SET `sort_order`= $NewOrder WHERE id =".$id;
                    mysql_query($qry1);
                }
            }
        }
    }
    function getLastOrder(){
        $sql ="select sort_order FROM tbl_banner ORDER BY sort_order desc LIMIT 0,1 ";
        $query = $this->db->query($sql);
        $result = $query->result();
        return !empty($result[0]->sort_order) ? $result[0]->sort_order : 0;
    }

	function updateDetails(){
		
		$data = array(
			'title' => $this->input->post('title'),
			'advertize_url' => $this->input->post('advertize_url'),
			'image_path' => $this->input->post('image_path'),
			'advertize_type' => $this->input->post('advertize_type'),
			'is_active' => $this->input->post('is_active'),
			'created_date_time' =>date('Y-m-d H:i:s')
		);
		$this->db->where("id",$this->input->post('id'));
		$this->db->update('tbl_banner',$data);
		
		
		return true;
		
	}
	function changeStatus($status,$id){
		
		mysql_query("UPDATE tbl_banner SET is_active = '$status' WHERE id= ".$id."");
		return true;
	}
	function deleteRecord($id){
		//mysql_query("DELETE FROM tbl_banner WHERE id= ".$id."");
        mysql_query("UPDATE tbl_banner SET is_deleted = '1' WHERE id= ".$id."");
		return true;
	}
	function getDetails($id){
		
		$this->db->where('id', $id);		
		$query = $this->db->get('tbl_banner') or die(mysql_error());
		if($query->num_rows >= 1)
			return $query->row();
		else
            return false;
	}
	function getRandomAdvt($type='SideBar'){
		
		$sql ="select * FROM tbl_banner WHERE 1=1 AND is_active = '1' AND advertize_type = '$type' ORDER BY RAND() LIMIT 0,1";
		$query = $this->db->query($sql);
		$result = $query->result();
		return $result[0];
	}
	function getAllRecords($all='*',$where='',$orderby='',$limit=''){
		
		$sql ="select $all FROM tbl_banner WHERE 1=1 ";
		if($where!=''){
			$sql .= " AND $where ";
		}
		
		if($orderby!=''){
			$sql .= " $orderby ";
		}
		if($limit!=''){
			$sql .= " $limit ";
		}
        //echo $sql;
		$query = $this->db->query($sql);
		$query_data = $query->result();
		
		return $query_data;
	}
	
		
}