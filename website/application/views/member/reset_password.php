<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 5/4/2015
 * Time: 3:29 PM
 */?>
<div class="container">
	<div class="content-row">
        <div class="col-md-12">

            <div class="contact-form">
                <div id="abhi">
                    <?php
                    if(isset($errMsg) && $errMsg != ''){ ?>
                        <div class="alert alert-danger">
                            <?php echo $errMsg;?>
                        </div>
                        <?php unset($errMsg);
                    }
                    if(isset($succMsg) && $succMsg != ''){ ?>
                        <div class="alert alert-success">
                            <?php echo $succMsg;?>
                        </div>
                        <?php unset($succMsg);
                    }
                    echo validation_errors();
                    if (!empty($validToken)) {
                    $attributes = array('name' => 'loginForm', 'id' => 'loginForm', 'enctype' => 'multipart/form-data', 'onsubmit' => 'return validate_admin();');
                    echo form_open(MEMBER_ROOT_URL . 'reset_password/' . $password_token, $attributes); ?>

                    <div class="col-md-6">
                        <label>New Passward*</label>
                        <input type="password"  required="required" name="password" id="password" placeholder="Password">
                    </div>

                    <div class="col-md-6 pull-right">
                        <label>Password Again*</label>
                        <input value="" class="validate[required] inputbg" type="password" required="required" name="retype_password" id="retype_password"/>
                    </div>

                    <div class="clearfix"></div>
                    <input  name="submit" value="Submit" type="submit"  class="submit" style="width:100px; margin-left:0;">
                        <?php echo form_close();
                    } else {?>
                        <a href="<?php echo MEMBER_ROOT_URL?>forgot_password">Forgot Password?</a> <br />
                    <?php }?>
                </div>
            </div>
        </div>

    </div>
</div><!-- container -->