<div class="container create-classified normal-page">
    <div class="content-row">
        <h2><?php echo $action?></h2>

        <div class="col-md-8">
            <div class="contact-form">
                <?php
                if(isset($succMsg) && $succMsg != ''){ ?>
                    <div class="regn-success-wrapper">
                        <div class="alert alert-success">
                            <?php echo $succMsg;?>
                        </div>
                    </div>
                    <?php unset($succMsg);
                }?>
                <div id="abhi">
                    <?php  echo validation_errors();?>
                    <form class="register-form" name="createClassified" method="post" id="createClassified" enctype="multipart/form-data">
                        <input type="hidden" name="created_by" value="<?php echo $user_id;?>" />
                        <input type="hidden" name="classified_country" value="<?php echo DEFAULT_COUNTRY_ID;?>" />


                        <div class="col-md-12">
                            <div class="select_style" id="city_container">
                                <select  name="city_id">
                                    <option value="">Select City</option>
                                    <?php if(!empty($cityList) && is_array($cityList)){
                                        foreach($cityList as $city) {
                                            echo '<option value="'.$city->id.'">'.($city->combined).'</option>';
                                        }
                                    }?>
                                </select>
                                <img class="ajaxLoaderImg hidden" src="/images/loading-1.gif" class="hidden">
                            </div>
                            <p class="group-text">Your item will be displayed across the city selected here.</p>
                        </div>

                        <div class="col-md-6">
                            <input type="text" class="form-control" name="classified_locality" placeholder="Enter  Locality">
                        </div>
                        <div class="col-md-6 pull-right">
                            <input type="text" class="form-control" name="amount" placeholder="Enter Price in AED">
                        </div>
                        <div class="clearfix"></div>

                        <div class="col-md-12">
                            <label class="sub-title">Your product details</label>
                            <input type="text" class="form-control" id="title" name="title" placeholder="Title">
                        </div>

                        <div class="col-md-6">
                            <div class="select_style">
                                <select name="category_id" id="category" class="form-control" style="width:274px;">
                                    <option value="">Select Category</option>
                                    <?php //echo $contentSelectData;
                                    if (!empty($contentSelectData) && is_array($contentSelectData)) {
                                        foreach ($contentSelectData as $categ){
                                            echo '<option value="'.$categ->id.'">'.$categ->title.'</option>';
                                        }
                                    }?>
                                </select>
                            </div>
                            <p class="group-text select-hint">Choose appropriate <span class="blue-text">category</span> from the list</p>
                        </div>

                        <div class="col-md-6 pull-right">
                            <div class="select_style">
                            <select name="age_group_id" id="age_group" class="form-control" style="width:274px;">
                                <option value="">Select Age group</option>
                                <?php if (!empty($ageGroupList) && is_array($ageGroupList)) {
                                    foreach ($ageGroupList as $listItem) {
                                        echo '<option value="'.$listItem->id.'">'.$listItem->title.'</option>';
                                    }
                                }?>
                            </select>
                            </div>
                            <p class="group-text select-hint">Choose appropriate <span class="blue-text">age group</span> from the list</p>
                        </div>

                        <div class="col-md-6">
                            <div class="select_style">
                                <select name="condition_id" id="condition" class="form-control" style="width:274px;">
                                    <option value="">Select Condition</option>
                                    <?php if (!empty($conditionList) && is_array($conditionList)) {
                                        foreach ($conditionList as $listItem) {
                                            echo '<option value="'.$listItem->id.'">'.$listItem->title.'</option>';
                                        }
                                    }?>
                                </select>
                            </div>
                            <p class="group-text select-hint">Choose appropriate <span class="blue-text">condition</span> of your item from the list</p>
                        </div>

                        <div class="col-md-6 pull-right">
                            <div class="select_style">
                                <select name="usage_id" class="form-control" style="width:274px;">
                                    <option value="">Select Usage</option>
                                    <?php if (!empty($usageList) && is_array($usageList)) {
                                        foreach ($usageList as $listItem) {
                                            echo '<option value="'.$listItem->id.'">'.$listItem->title.'</option>';
                                        }
                                    }?>
                                </select>
                            </div>
                            <p class="group-text select-hint">Choose appropriate <span class="blue-text">condition</span> of your item from the list</p>
                        </div>


                        <div class="clearfix"></div>
                        <div class="col-md-12">
                            <textarea class="form-control description-box" rows="3" name="description" placeholder="Description"></textarea>
                        </div>
                        <div class="preview-images-wrap">
                            <?php if (!empty($uploadedImages)) {
                                foreach($uploadedImages as $images) {
                                    if (file_exists(DIR_UPLOAD_CLASSIFIED . $images->classified_image)) { ?>
                                        <div class="preview-images">
                                            <button class="removeImages" imageId="<?php echo $images->id?>" title="Remove this image">&nbsp;</button>
                                            <img src="<?php echo ROOT_URL_BASE ?>assets/timthumb.php?src=<?php echo DIR_UPLOAD_CLASSIFIED_SHOW . $images->classified_image; ?>&q=100&w=100" />
                                        </div><?php
                                    }
                                }?>
                            <?php }?>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group file-uplopader-row">
                            <label class="filebutton sub-title">
                                Upload Photo(Upto 5 Images)
                                <span><input type="file" accept="image/*" size="2" multiple id="myfile" name="classified_images[]"></span>
                            </label>
                            <p class="help-block">Max size: 3072 KB per image (Only jpeg, jpg, png format).</p>
                        </div>
                        <div class="clearfix"></div>

                        <div class="col-md-12">
                            <label class="sub-title">Your contact details</label>
                            <textarea name="classified_address" placeholder="Address"></textarea>
                        </div>
                        <div class="col-md-12">
                            <input type="text" class="form-control" name="classified_contact_no" placeholder="Telephone / Mobile number">
                            <div class="clearfix"></div>
                            <p class="group-text">10 digit mobile no. with +971 or 0</p>
                        </div>

                        <input  value="<?php echo ($editClassifieds) ? 'UPDATE' : 'POST'?>" type="submit"  class="submit" style="margin-left:0;">
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-4 pull-right">
            <!--<p>HelpingMommy.com is an online
                community that helps moms around
                UAE share their's and their infant's
                stuff for FREE!. </p>-->

            <p></p>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>js/jquery.validate.min.js"></script>

<script language="javascript" type="text/javascript">
    function getModelList(brand_name){
        $.ajax({
            type: "POST",
            url: "home/getmodel",
            data: "brand_name="+brand_name,
            success: function(result){
                $('#model_select').html(result);
            },
            complete: function(){

            },
            error: function(){
                $('#detail_inner').html('Error occured. Please try later');
            }
        });
    }
    $(document).ready(function(){
        //$("#createClassified").validate();
        jQuery.validator.addMethod("validPhoneNumber", function(value, element) {
            //$(element).val(value.trim());
            return this.optional(element) || validPhoneNumber(value);
        }, "Invalid phone number");
        $("#createClassified").validate({
            rules: {
                classified_country: {
                    required: true
                },
                city_id:{
                    required: true,
                    maxlength: 200
                },
                classified_locality:{
                    required: true,
                    maxlength: 200
                },
                amount:{
                    required: true,
                    maxlength: 10,
                    number: true,
                },
                title:{
                    required: true,
                    maxlength: 200
                },
                category_id:{
                    required: true,
                },

                age_group_id:{
                    required: true,
                },
                condition_id:{
                    required: true,
                },
                usage_id:{
                    required: true,
                },
                /*small_description:{
                    minlength: 3,
                    maxlength: 200
                },*/
                description:{
                    minlength: 3
                },
                /*amount:{
                    required: true,
                    maxlength: 7,
                    number: true
                },
                manufacture_year:{
                    number: true
                },*/
                classified_address:{
                    maxlength: 200
                },
                classified_contact_no:{
                    required: true,
                    maxlength: 15,
                    validPhoneNumber: true,
                    minlength: 10
                }
            },
            messages: {
                classified_country: {
                    required: 'Please select country of advertisement'
                },
                city_id:{
                    required: 'Please select your city',
                    maxlength: 'Maximum length allowed is 100 characters'
                },
                classified_locality:{
                    required: 'Please enter your locality',
                    maxlength: 'Maximum length allowed is 100 characters'
                },
                amount:{
                    required: 'Please enter the price',
                    number: 'Invalid price',
                },
                category_id:{
                    required: 'Please select a category'
                },
                age_group_id:{
                    required: 'Please select an age group'
                },
                condition_id:{
                    required: 'Please select condition'
                },
                usage_id:{
                    required: 'Please select usage'
                },
                title:{
                    required: 'Please enter a title',
                    maxlength: 'Maximum length allowed is 200'
                },
                /*small_description:{
                    minlength: 'Short description should be at least 3 characters long',
                    maxlength: 'Maximum length allowed is 200'
                },*/
                description:{
                    minlength: 'Description should be at least 3 characters long',
                },
                /*amount:{
                    required: 'Please enter an amount',
                    maxlength: 'Maximum digits allowed is 7',
                    number: 'Please enter a valid amount'
                },
                manufacture_year:{
                    number: 'Manufacture year should be numeric'
                },*/
                classified_address:{
                    maxlength: 'Maximum length allowed is 200'
                },
                classified_contact_no:{
                    required: 'Contact number is required',
                    maxlength: 'Invalid phone number',
                    validPhoneNumber: 'Invalid phone number'
                }
            }
        });
//validPhoneNumber($('input[name="classified_contact_no"]').val());
        $('.removeImages').click(function(){
            var imageId = $(this).attr('imageId');
            if (confirm("Do you really want to remove this image ?")) {
                $('.preview-images-wrap').append('<input type="hidden" name="removeImageId[]" id="removeImageIdHidden" value="' + imageId + '">');
                $(this).parent().remove();
                if ($('.preview-images').length < 5) {
                    $('.file-uplopader-row').show();
                }
            }
            return false;
        })
        if ($('.preview-images').length >= 5) {
            $('.file-uplopader-row').hide();
        }
    })
    <?php
    $classifiedDetails = (array) $classifiedDetails;
    if(!empty($classifiedDetails) && is_array($classifiedDetails)) {?>
    $(document).ready(function(){
        <?php echo build_filldata_javascript($classifiedDetails);?>
    })
    <?php }?>
</script>