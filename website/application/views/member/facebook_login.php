<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 5/10/2015
 * Time: 10:22 AM
 */?>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo ROOT_URL_BASE?>css/facebookLogin.css">
<link rel="stylesheet" href="<?php echo ROOT_URL_BASE?>css/responsive.css">
<div class="facebook-login-wrap">
<input type="hidden" name="ROOT_URL" id="rootUrlLink" value="<?php echo ROOT_URL?>" />
<div class="loginform">
    <div class="facebook" id="facebook"><i class="fa fa-facebook"></i>
        <div class="connect">Connect with Facebook</div>
    </div>
    <div id="loadingIconContainer" class="facebook loadingIcon"  id="loadingIconContainer">
        <img src="<?php echo ROOT_URL_BASE?>images/facebookLoading.gif" />
        <div class="loading">Connecting to Facebook</div>
    </div>
    <div class="mainlogin">
        <div id="or">or</div>
        <h1>Log in with your credentials</h1>
        <?php
        if(!empty($errMsg)){ ?>
            <div class="alert">
            <?php echo $errMsg;?>
            </div><?php
        }
        if(!empty($succMsg)){ ?>
            <div class="alert">
            <?php echo $succMsg;?>
            </div><?php
        }

        $attributes = array('name' => 'loginForm', 'enctype' => 'multipart/form-data');
        echo form_open(ROOT_URL.'login'.(!empty($wrapperType) ? '/'.$wrapperType : ''),$attributes); ?>
        <!--<form action="#">-->
            <input type="email" placeholder="email" required value="<?php echo !empty($_POST['username']) ? $_POST['username'] : '';?>" name="username">
            <input type="password" placeholder="password" value="" required type="password" placeholder="Password" name="password">
            <button type="submit"><i class="fa fa-arrow-right"></i></button>
        </form>
        <div class="note forgot-pwd"><a id="forgot-pwd-link" href="<?php echo MEMBER_ROOT_URL?>forgot_password">Forgot your password?</a></div>
    </div>
</div>
</div>
<script src="<?php echo ROOT_URL_BASE?>js/jquery.1.11.1.min.js"></script>
<script src="<?php echo ROOT_URL_BASE?>js/facebookLogin.js"></script>
<script>
    var parentWindow= $('#cboxContent', window.parent.document).addClass('facebookLogin');
    $('#forgot-pwd-link').click(function () {
        window.top.location = $(this).attr('href');
    })
</script>