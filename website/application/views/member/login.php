<?php
/**
 * Created by PhpStorm.
 * User: Anas
 * Date: 5/3/2015
 * Time: 12:25 PM
 */?>
<?php if(!empty($wrapperType) && $wrapperType == 'ajax') {?>
<link href="<?php echo ROOT_URL_BASE?>css/style.css" type="text/css" rel="stylesheet" title="<?php echo DEFAULT_META_TITLE?>" />
<?php }?>

<?php if(empty($wrapperType) OR $wrapperType != 'ajax') {?>
<div class="container">
    <div class="banner">
<?php }?>
        <div class="register-form-popup <?php if(empty($wrapperType) OR $wrapperType != 'ajax') {?>login-form-wrapped<?php }?> login-form-wrapper-width">
            <div class="contact-form">
                <h3 class="register-heading">Login</h3>
                <div id="abhi">
                    <?php
                    if(!empty($errMsg)){ ?>
                        <div class="alert alert-danger alert-popup">
                            <?php echo $errMsg;?>
                        </div><?php
                    }
                    if(!empty($succMsg)){ ?>
                        <div class="alert alert-success alert-popup">
                            <?php echo $succMsg;?>
                        </div><?php
                    }

                    $attributes = array('name' => 'loginForm', 'id' => 'loginForm', 'enctype' => 'multipart/form-data');
                    echo form_open(ROOT_URL.'login'.(!empty($wrapperType) ? '/'.$wrapperType : ''),$attributes); ?>
                        <div class="col-md-12">
                            <label>User Name:</label>
                            <input value="<?php echo !empty($_POST['username']) ? $_POST['username'] : '';?>" class="validate[required] inputbg" type="text" name="username" id="Name" />
                        </div>
                        <div class="col-md-12">
                            <label>Password:</label>
                            <input type="password"  id="exampleInputPassword1" placeholder="Password" name="password">
                        </div>

                        <div class="clearfix"></div>
                        <div class="col-md-6">
                            <div class="checkbox">
                                <label><!--<input type="checkbox">--></label>
                                <div class=" col-md-form-3 checkbox-aliment" style="width:150px!important;"><!--<label >Remember Me </label>--></div>
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="checkbox">
                                <div class=" col-md-form-3 " style="width:172px!important; margin-top:8px;"><p ><a href="<?php echo MEMBER_ROOT_URL?>forgot_password" target="_parent"  style="font-size: 16px; font-weight:normal;">Forgot password?</a></p></div>
                            </div>

                        </div>
                        <input  name="submit" value="sign in" type="submit"  class="submit create-new-account">
                    </form>
                </div>
            </div>
        </div>
<?php if(empty($wrapperType) OR $wrapperType != 'ajax') {?>
    </div>
</div>
<?php }?>