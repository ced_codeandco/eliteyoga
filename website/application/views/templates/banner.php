<script type="text/javascript">
jQuery(document).ready(function(){

$('.slider_main').parallax({imageSrc: '<?php echo ROOT_URL_BASE?>/images/about_bg.jpg'});
});
</script>
<div class="slider_main">

    <ul class="bxslider">
        <?php
        if(!empty($bannerList) && is_array($bannerList)) {
            foreach ($bannerList as $banner) {
                if (!empty($banner->image_path) && file_exists(DIR_UPLOAD_BANNER.$banner->image_path)) {
                    ?>
                    <li><img src="<?php echo DIR_UPLOAD_BANNER_SHOW.$banner->image_path;?>" alt="<?php echo $banner->title;?>"/></li>
                <?php
                }
            }
        } else {?>
            <li><img src="images/banner.jpg" alt="Engineering" /></li>
        <?php }?>
    </ul>

    <div class="emotion_other_logo">

        <div class="main_outer">
        <?php if (!empty($headerCMSList) && is_array($headerCMSList)) {
            $i = 1;
            foreach ($headerCMSList as $page) {
                if ($page->on_home == 1) { ?>
                    <div
                        class="emotion_other_logo_content <?php echo ($i == 2) ? 'margin_both' : ''; ?> <?php if (!empty($page->theme_color)) {
                            if ($page->theme_color == 'orange') {
                                echo 'emotion_orange';
                            } else if ($page->theme_color == 'violet') {
                                echo 'emotion_pink';
                            }
                        } ?>">
                        <a href="<?php echo $page->cms_slug ?>">
                        <?php if (!empty($page->home_logo_border) && file_exists(DIR_UPLOAD_BANNER . $page->home_logo_border)) { ?>
                            <img class="logo_emotion"
                                 src="<?php echo DIR_UPLOAD_BANNER_SHOW . $page->home_logo_border; ?>"/>
                        <?php } ?>
                        <?php if (!empty($page->home_logo) && file_exists(DIR_UPLOAD_BANNER . $page->home_logo)) { ?>
                            <img src="<?php echo DIR_UPLOAD_BANNER_SHOW . $page->home_logo; ?>"/>
                        <?php } ?>
                        <p><?php echo content_truncate(strip_tags(stripslashes($page->home_page_description)), 70, ' ', '...'); ?></p>

                        <div class="clearfix"></div>

                        <?php /*?><a href="<?php echo $page->cms_slug ?>">read more</a><?php */?>
</a>
                    </div>
                    <?php
                    $i++;
                }
            }
        }?>
        </div>

    </div>

    <div class="about_home">

        <div class="main_outer">

            <?php echo stripslashes($homePageContent->description);?>

        </div>

    </div>

</div>
<?php return;?>
<?php
if (!empty($advertizeTopHeaderObj) && is_array($advertizeTopHeaderObj)) {
    echo '<div id="header-top-banner-wrap" class="top-ad-banner-wrap hidden">';
    echo '<div class="top-ad-banner-inner-wrap">';
    echo (count($advertizeTopHeaderObj)>1) ? '<ul class="bx_slider_ad_top_head">' : '';
    foreach($advertizeTopHeaderObj as $row){?>
        <li>
        <?php echo !empty($row->advertize_url) ? '<a href="' . prep_url($row->advertize_url) . '" target="_blank">' : '';?>
        <img  src="<?php echo DIR_UPLOAD_ADVERTIZE_SHOW.$row->image_path;?>"/>
        <?php echo !empty($row->advertize_url) ? '</a>' : '';?>
        <?php echo showEditBannerpost($row);?>
        </li><?php
    }
    echo (count($advertizeTopHeaderObj)>1) ? '</ul>' : '';
    echo '<a class="ad_close_div" id="remove_top_header_banner" href="javascript:void(0);">X close</a>';
    echo '</div>';
    echo '</div>';
    ?>
    <script>
    $(document).ready(function(){
        setTimeout(function() {
            $('#header-top-banner-wrap').show(function () {
                jQuery('.bx_slider_ad_top_head').bxSlider({
                    auto: true,
                    controls: false,
                    pager: false,
                    randomStart: true,
                    preloadImages: 'all'
                });
            });
        },4000);
        $('#remove_top_header_banner').click(function(){
            $('#header-top-banner-wrap').slideUp(function(){
                $('#header-top-banner-wrap').remove();
            })
        })
    })
    </script><?php
}?>
<div class="header_main <?php echo (!isset($isHomePage) OR $isHomePage == '') ? 'header_main_page' : '';?>">
    <div class="top_line">
        <div class="header_right hidden-lg">
            <div id="nav-icon3">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>

            <div class="social_div">
                <a href="mailto:<?php echo defined('CONTACT_US_EMAIL') ? CONTACT_US_EMAIL : '';?>"><img src="<?php echo ROOT_URL_BASE?>images/email.png" /></a>
                <a href="<?php echo defined('FACEBOOK_LINK') ? prep_url(FACEBOOK_LINK) : '';?>" target="_blank"><img src="<?php echo ROOT_URL_BASE?>images/facebook.png" /></a>
                <a href="<?php echo defined('INSTAGRAM_LINK') ? prep_url(INSTAGRAM_LINK) : '';?>" target="_blank"><img src="<?php echo ROOT_URL_BASE?>images/instragram.png" /></a>
                <?php echo showEditSettings('', 'bottom-30px');?>
            </div>
            <div class="slider_top_header hidden-xs"><?php
                if (!empty($advertizeHeader) && is_array($advertizeHeader)) {
                    echo (count($advertizeHeader) > 1) ? '<ul class="bx_slider_ul">' : '';
                    foreach ($advertizeHeader as $row) {
                        ?>
                        <li>
                        <?php echo !empty($row->advertize_url) ? '<a href="' . prep_url($row->advertize_url) . '" target="_blank">' : '';?>
                        <img src="<?php echo DIR_UPLOAD_ADVERTIZE_SHOW . $row->image_path;?>"/>
                        <?php echo !empty($row->advertize_url) ? '</a>' : '';?>
                        <?php echo showEditBannerpost($row);?>
                        </li><?php
                    }
                    echo (count($advertizeHeader) > 1) ? '</ul>' : '';
                }?>
            </div>
        </div>
    </div>
    <div class="main_outer">

        <a href="<?php echo ROOT_URL;?>" class="logo_a"><img src="<?php echo ROOT_URL_BASE?>images/logo.jpg" /></a>
        <div class="top_header_text">
            <?php echo defined('HOME_TITLE') ? HOME_TITLE : '';?>
            <?php echo showEditSettings('HOME_TITLE');?>
        </div>
        <div class="header_right hidden-xs">
            <div class="social_div">
                <a href="mailto:<?php echo defined('CONTACT_US_EMAIL') ? CONTACT_US_EMAIL : '';?>"><img src="<?php echo ROOT_URL_BASE?>images/email.png" /></a>
                <a href="<?php echo defined('FACEBOOK_LINK') ? prep_url(FACEBOOK_LINK) : '';?>" target="_blank"><img src="<?php echo ROOT_URL_BASE?>images/facebook.png" /></a>
                <a href="<?php echo defined('INSTAGRAM_LINK') ? prep_url(INSTAGRAM_LINK) : '';?>" target="_blank"><img src="<?php echo ROOT_URL_BASE?>images/instragram.png" /></a>
                <?php echo showEditSettings('', 'bottom-30px');?>
            </div>
            <div class="slider_top_header"><?php
                if (!empty($advertizeHeader) && is_array($advertizeHeader)) {
                    echo (count($advertizeHeader) > 1) ? '<ul class="bx_slider_ul">' : '';
                    foreach ($advertizeHeader as $row) {
                        ?>
                        <li>
                        <?php echo !empty($row->advertize_url) ? '<a href="' . prep_url($row->advertize_url) . '" target="_blank">' : '';?>
                        <img src="<?php echo DIR_UPLOAD_ADVERTIZE_SHOW . $row->image_path;?>"/>
                        <?php echo !empty($row->advertize_url) ? '</a>' : '';?>
                        <?php echo showEditBannerpost($row);?>
                        </li><?php
                    }
                    echo (count($advertizeHeader) > 1) ? '</ul>' : '';
                }?>
            </div>
        </div>
        <div class="clearfix"></div>
    </div><?php
    if (isset($isHomePage) && $isHomePage == 1) {
    $inlineStyle = "";
    if (!empty($homePageData->cms_banner_image) && file_exists(DIR_UPLOAD_BANNER.$homePageData->cms_banner_image)){?>
        <?php $inlineStyle = 'style="background:#fff url('.DIR_UPLOAD_BANNER_SHOW.$homePageData->cms_banner_image.') center no-repeat;"';?>
    <?php }?>
    <div class="banner_main" <?php echo $inlineStyle;?>><?php echo showEditCmsPage($homePageData);?></div>
    <?php }?>
    <div class="main_top_menu">
        <div class="main_outer">
        	<div class="search_main hidden-lg">
                <form method="get" action="<?php echo ROOT_URL.'search'?>">
                    <input type="hidden" name="search" value="1">
                    <input type="text" placeholder="search" name="keyword" />
                    <input type="submit" />
                </form>
                <div class="clearfix"></div>
            </div>
            <div class="nav">
                <li></li>
                <?php $this->load->view('templates/navigation_menu', $this->headerData);?>
            </div>
            <div class="search_main hidden-xs">
                <form method="get" action="<?php echo ROOT_URL.'search'?>">
                    <input type="hidden" name="search" value="1">
                    <input type="text" placeholder="search" name="keyword" />
                    <input type="submit" />
                </form>
            </div>
        </div>
    </div>
</div>
