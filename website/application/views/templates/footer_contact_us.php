<div id="contact-us" class="contact_main">

    <div id="map"></div>

    <div class="main_outer">

        <div class="contact_form">

            <p class=""><?php echo strtoupper($contactUs->title);?></p>
            <p><?php echo nl2br(stripslashes($contactUs->small_description));?></p>

            <p><a href="https://www.google.com/maps/place//@<?php echo $contactUs->location_longitude.','.$contactUs->location_latitude;?>,16z/" target="_blank"><?php echo $contactUs->location_longitude.','.$contactUs->location_latitude?></a></p>

            <p><label>t.</label> <?php echo $contactUs->telephone;?><br />

                <label>f.</label> <?php echo $contactUs->fax;?><br />

                <label>e.</label> <?php echo $contactUs->email;?></p>

            <form id="contactform" name="contactform" >

                <input type="text" name="name" placeholder="Your Name"  id="Name" class="contact_input subscr-control" required  />
                <input type="text" name="email" placeholder="Email Address" id="Email" class="contact_input subscr-control" required />
                <textarea name="comments" placeholder="Inquiry" maxlength="150" class="contact_input subscr-control" id="Message" required></textarea>
                <input type="text" placeholder="How much is <?php echo $verify_robot[0].' + '.$verify_robot[1]?>" class="captcha entrybox captcha_contact subscr-control" name="captcha" id="captcha-form" autocomplete="off" required/>
                <input  name="submit" value="SEND" type="submit"  class="submit bg_emotion subscr-control">
                <div class="contact-response hidden" id="subsc-response"></div>
            </form>
            <div class="clearfix"></div>
            <div class="loader-wrap hidden">
            </div>
        </div>

    </div>
<input type="hidden" id="map_latitude" value="<?php echo $contactUs->location_longitude;?>">
<input type="hidden" id="map_longitude" value="<?php echo $contactUs->location_latitude;?>">
</div>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=&sensor=false&extension=.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#contactform').submit(function(){
            $('.subscr-control').hide();$('.loader-wrap').show();
            var email = $('#subscription-email').val();
            jQuery.ajax({
                type: "POST",
                url: $('#rootUrlLink').val() + 'contact_us',
                dataType: 'json',
                data: $('#contactform').serialize(),
                cache: false,
                success: function(data) {
                    $('.subscr-control').show();$('.loader-wrap').hide();
                    var status = data.status;
                    var msg = data.message;
                    var new_captcha = data.new_captcha;
                    if(status*1 == 1)
                    {
                        $('#subsc-response').html(msg).removeClass('alert-danger').addClass('alert-success').show();
                        $('#contactform').find('input[type="text"],input[type="number"],textarea').val('');

                    }
                    else
                    {
                        $('#subsc-response').html(msg).removeClass('alert-success').addClass('alert-danger').show();
                    }
                    $('#captcha-form').val('');
                    $('#captcha-form').attr('placeholder', new_captcha);
                },
                complete: function(){
                    $('.subscr-control').show();$('.loader-wrap').hide();
                },
                error: function(){
                    $('#subCategoryContainer').html('Something went wrong!! Please try later');
                    $('.subscr-control').show();$('.loader-wrap').hide();
                }
            });
            return false;
        })
    })
    jQuery(document).ready(function(){
        var latitude = $('#map_latitude').val();
        var longitude = $('#map_longitude').val();
        // When the window has finished loading create our google map below
        google.maps.event.addDomListener(window, 'load', init);
        /*google.maps.event.addListener(map, 'click', function(event){
            alert("here");
            this.setOptions({scrollwheel:true});
        });*/
        function init() {
            // Basic options for a simple Google Map
            // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
            var mapOptions = {
                // How zoomed in you want the map to start at (always required)
                zoom: 14,
                // The latitude and longitude to center the map (always required)
                //center: new google.maps.LatLng(25.21108,55.281955),
                center: new google.maps.LatLng(latitude,longitude),
                scrollwheel: false,
                // How you would like to style the map.
                // This is where you would paste any style found on Snazzy Maps.
                styles: [{featureType:"landscape",stylers:[{color:"#e3e3e3"},{lightness:45},{visibility:"on"}]},{featureType:"poi",stylers:[{saturation:-100},{lightness:51},{visibility:"simplified"}]},{featureType:"road.highway",stylers:[{saturation:-100},{visibility:"simplified"}]},{featureType:"road.arterial",stylers:[{saturation:-100},{lightness:30},{visibility:"on"}]},{featureType:"road.local",stylers:[{saturation:-100},{lightness:40},{visibility:"on"}]},{featureType:"transit",stylers:[{saturation:-100},{visibility:"simplified"}]},{featureType:"administrative.province",stylers:[{visibility:"off"}]/**/},{featureType:"administrative.locality",stylers:[{visibility:"off"}]},{featureType:"administrative.neighborhood",stylers:[{visibility:"on"}]/**/},{featureType:"water",elementType:"labels",stylers:[{visibility:"on"},{lightness:-25},{saturation:-100}]},{featureType:"water",elementType:"geometry",stylers:[{color:"#000"}]}]
            };
            // Get the HTML DOM element that will contain your map
            // We are using a div with id="map" seen below in the <body>
            var mapElement = document.getElementById('map');
            // Create the Google Map using out element and options defined above
            var map = new google.maps.Map(mapElement, mapOptions);
            var marker = new google.maps.Marker({

                map: map,

                position: map.getCenter(),

                icon: $('#map-cion').val()

            });

            map.addListener('click', function() {
                map.set('scrollwheel', true);
            });
            //var infowindow = new google.maps.InfoWindow();
            //  infowindow.setContent('<b>RTC Facilities Management</b><br/>Blue Bay Tower<br/>PO Box 487044<br />Dubai, UAE');
            // infowindow.open(map, marker);


        }
    })
</script>