<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 4/23/2015
 * Time: 10:37 AM
 */?>
<div class="container marketing">
    <div class="row">
        <div class="col-lg-12">
            <div class="bradcram-box">
                <ul>
                    <li><a href="<?php echo ROOT_URL;?>">Home</a></li>
                    <li><?php echo !empty($pageTitle) ? $pageTitle : '';?></li>
                </ul>
                <h3><?php echo !empty($pageTitle) ? $pageTitle : '';?></h3>
            </div>
        </div>
    </div><!-- /.row -->

<div class="row">
    <div class="col-lg-12" style="margin-top:2px;">
        <div class="tabs">
            <ul class="tabs">
                <?php if(!empty($tabList) && is_array(($tabList))) {
                    $count = 1;
                    foreach ($tabList as $urlstring => $tab) {
                        $tabId = 'memberTab'.$count;?>
                        <li id="<?php echo $tabId;?>" <?php echo ($tab['is_active'] == 1) ? 'class="active"' : '';?>><a href="<?php echo MEMBER_ROOT_URL.$urlstring;?>"><?php echo $tab['title'];?></a></li><?php
                        $count++;
                    }
                }?>
            </ul>
        </div>
    </div>
</div>