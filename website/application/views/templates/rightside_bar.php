<div class="right_area <?php echo (!empty($read_post_page) && $read_post_page ==1) ? 'read_post_sidebar' : '';?>">
    <div class="sidebar-banner">
    <?php //echo !empty($advertizeSideBar) ? $advertizeSideBar : '';?>
    <?php //echo showEditBanner($advertizeSideBarObj);?>
        <?php
        if (!empty($advertizeSideBar) && is_array($advertizeSideBar)) {
            echo (count($advertizeSideBar)>1) ? '<ul class="bx_slider_ul">' : '';
            foreach($advertizeSideBar as $row){?>
                <li>
                    <?php echo !empty($row->advertize_url) ? '<a href="' . prep_url($row->advertize_url) . '" target="_blank">' : '';?>
                    <img  src="<?php echo DIR_UPLOAD_ADVERTIZE_SHOW.$row->image_path;?>"/>
                    <?php echo !empty($row->advertize_url) ? '</a>' : '';?>
                    <?php echo showEditBannerpost($row);?>
                </li><?php
            }
            echo (count($advertizeSideBar)>1) ? '</ul>' : '';
        }?>
    </div>
    <?php //?>
    <?php if (!empty($this->headerData['topTen']) && is_array($this->headerData['topTen'])) {
    $topTen = $this->headerData['topTen'];?>
    <div class="right_area_text">
        <div class="border_div">
            <div class="border_div">
                <div class="text_div1">
                    <h3>Top 10</h3>
                    <ul class="slider_home_right"><?php
                        foreach($topTen as $comp) {?>
                        <li><?php
                            if (!empty($comp->inline_image) && file_exists(DIR_UPLOAD_BANNER.$comp->inline_image)){?>
                                <a href="<?php echo ROOT_URL.'read-post/'.$comp->classified_slug;?>"><img class="featured-post-img" src="<?php echo DIR_UPLOAD_BANNER_SHOW.$comp->inline_image;?>"/></a>
                            <?php }?>

                            <h5><a href="<?php echo ROOT_URL.'read-post/'.$comp->classified_slug;?>"><?php echo !empty($comp->title) ? content_truncate($comp->title, 60, ' ', '...') : '';?></a></h5>
                            <p><?php echo !empty($comp->description) ? content_truncate($comp->title, 60, ' ', '...') : '';?></p>
                            <a href="<?php echo ROOT_URL.'read-post/'.$comp->classified_slug;?>" class="see_more">See More</a>
                            <?php echo showEditPost($comp);?>
                        </li><?php
                        }?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <?php }?>
    <?php if (defined('FACEBOOK_LINK')) {?>
    <div class="right_area_text">
        <div class="border_div">
            <div class="border_div">
                <div class="text_div1" style="width:217px">
                    <h3 class="fb_head"><img src="<?php echo ROOT_URL_BASE?>images/fb.jpg" />FACEBOOK</h3>
                    <div id="fb-root"></div>
                    <script>(function(d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id)) return;
                            js = d.createElement(s); js.id = id;
                            js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.4";
                            fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk'));</script>
                    <div class="fb-page" data-href="<?php echo defined('FACEBOOK_LINK') ? prep_url(FACEBOOK_LINK) : '';?>" data-width="100%" data-height="250" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false" data-show-posts="true">
                        <div class="fb-xfbml-parse-ignore">
                            <blockquote cite="<?php echo defined('FACEBOOK_LINK') ? prep_url(FACEBOOK_LINK) : '';?>"><a href="<?php echo defined('FACEBOOK_LINK') ? prep_url(FACEBOOK_LINK) : '';?>">Facebook</a></blockquote>
                        </div>
                    </div>
                    <?php echo showEditSettings('FACEBOOK_LINK', 'facebook-edit');?>
                </div>
            </div>
        </div>
    </div>
    <?php }?>
    <?php if (!empty($this->headerData['competitons']) && is_array($this->headerData['competitons'])) {
        $competitons = $this->headerData['competitons'];?>
    <div class="right_area_text">
        <div class="border_div">
            <div class="border_div">
                <div class="text_div1">
                    <h3>competitions</h3>
                    <ul class="slider_home_right"><?php
                        foreach($competitons as $comp) {?>
                            <li><?php                            if (!empty($comp->inline_image) && file_exists(DIR_UPLOAD_BANNER.$comp->inline_image)){?>
                                <a href="<?php echo ROOT_URL.'read-post/'.$comp->classified_slug;?>"><img class="featured-post-img" src="<?php echo DIR_UPLOAD_BANNER_SHOW.$comp->inline_image;?>"/></a>
                            <?php }?>



                            <div class="date_div"><?php echo (!empty($comp->target_date) && $comp->target_date != '0000-00-00 00:00:00' ) ? date('dS F Y', strtotime($comp->target_date)) : '';?></div>
                            <h5><a href="<?php echo ROOT_URL.'read-post/'.$comp->classified_slug;?>"><?php echo !empty($comp->title) ? content_truncate($comp->title, 60, ' ', '...') : '';?></a></h5>

                            <p><?php echo !empty($comp->description) ? content_truncate($comp->title, 60, ' ', '...') : '';?></p>
                            <?php echo showEditPost($comp);?>
                            </li><?php
                        }?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <?php }?>
    <div class="right_area_text news_letter">
        <div class="border_div">
            <div class="border_div">
                <div class="text_div1"><?php //print_r($newsletterData)?>
                    <h3 class="news_letter_head"><?php echo !empty($newsletterData->title) ? $newsletterData->title : '';?></h3>
                    <p class="news_letter_p"><?php echo !empty($newsletterData->small_description) ? $newsletterData->small_description : '';?></p>
                    <form method="post" id="subscr-form" onsubmit="return submitSubscription();">
                        <input class="subscr-control" type="email" required="required" placeholder="Email address" id="subscription-email" />
                        <input class="subscr-control" type="submit" value="Sign Up"  />
                        <div class="" id="subsc-response"></div>
                    </form>
                    <div class="loader-wrap hidden">
                        <img src="<?php echo ROOT_URL_BASE;?>images/ajax-loader.gif" />
                    </div>
                    <?php echo showEditCmsPage($newsletterData, 'newsletter-edit');?>
                </div>
            </div>
        </div>
    </div>
</div>