<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="<?php echo ROOT_URL_BASE?>images/favicon.png">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo DEFAULT_META_TITLE;?><?php echo !empty($seo['title']) ? ' : '.$seo['title'] : '';?></title>
    <meta name="keywords" content="<?php echo DEFAULT_META_KEYWORDS;?>
<?php if (!empty($seo['tags'])) { echo ','.$seo['tags']; } else {}?>" />
    <meta name="Description" content="<?php echo DEFAULT_META_DESCRIPTION;?> <?php if (!empty($seo['description'])){ echo ' '.$seo['description']; } else {};?>"/>
    <meta property="og:title" content="<?php echo DEFAULT_META_TITLE;?> <?php echo !empty($seo['title']) ? ' : '.$seo['title'] : '';?>" />
    <meta property="og:description" content="<?php echo DEFAULT_META_DESCRIPTION;?> <?php if (!empty($seo['description'])){ echo ' '.$seo['description']; } else {};?>" />
    <?php if (!empty($seo['og:image'])) {?>
    <meta name="og:image" content="<?php echo $seo['og:image'];?>"/>
    <?php } else {?>
    <meta name="og:image" content="<?php echo ROOT_URL_BASE;?>images/logo.jpg"/>
    <?php
    }?>
    <link href="<?php echo ROOT_URL_BASE?>fonts/fonts.css" type="text/css" rel="stylesheet" />
    <link href="<?php echo ROOT_URL_BASE?>css/style.css" type="text/css" rel="stylesheet" />
    <link href="<?php echo ROOT_URL_BASE?>css/dev.css" type="text/css" rel="stylesheet" />
    <link href="<?php echo ROOT_URL_BASE?>assets/slider/jquery.bxslider.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo ROOT_URL_BASE?>css/responsive.css" type="text/css" rel="stylesheet" title="<?php echo DEFAULT_META_TITLE;?>" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo ROOT_URL_BASE?>assets/slider/jquery.bxslider.min.js"></script>
    <script type="text/javascript" src="<?php echo ROOT_URL_BASE?>js/scripts.js"></script>
    <script src="<?php echo ROOT_URL_BASE?>js/jquery.mCustomScrollbar.concat.min.js"></script>
    <link href="<?php echo ROOT_URL_BASE?>css/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<?php echo ROOT_URL_BASE?>js/helper.js"></script>
    <script type="text/javascript" src="<?php echo ROOT_URL_BASE?>js/parallax.min.js"></script>
    <script type="text/javascript">

        jQuery(document).ready(function(){



            jQuery('.bxslider').bxSlider({

                controls: true,

                mode: 'fade', auto: true,speed:1000,

                onSliderLoad: function () {

                    /*$('.bx-controls-direction').hide();

                    $('.bx-wrapper').hover(

                        function () {  $(this).find('.bx-controls-direction').fadeIn(300); },

                        function () {  $(this).find('.bx-controls-direction').fadeOut(300); }

                    );*/

                }

            });



        });
    </script>
    </head>

    <body id="<?php if (isset($pageId)){ if($pageId == CMS_CONTENT_PAGE_ID){ echo 'content_body'; } else if($pageId == CMS_NETWORK_PAGE_ID){ echo 'network_body'; } else if($pageId == CMS_STUDIOS_PAGE_ID){ echo 'studios_body'; }}?>" >
    <input type="hidden" name="ROOT_URL" id="rootUrlLink" value="<?php echo ROOT_URL?>" />
