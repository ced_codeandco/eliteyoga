<div class="footerclear" ></div>
<div class="footer_div_main">
    <div class="main_outer">
        <a target="_blank" href="<?php echo defined('INSTAGRAM_LINK') ? INSTAGRAM_LINK : '';?>"><img src="<?php echo ROOT_URL_BASE?>images/instragram01.png" class="insta_head" /></a>
        <?php echo showEditSettings('INSTAGRAM_LINK');?>
    </div>

    <!--<img src="<?php echo ROOT_URL_BASE?>images/banner4.jpg" />-->
    <div class="instagram-wrap">
    <script src="http://instansive.com/widget/js/instansive.js"></script>
    <?php echo INSTAGRAM_SCRIPT;?>
    <?php echo showEditSettings('INSTAGRAM_SCRIPT');?>
    </div>
    <div class="footer_nav_div">
        <div class="main_outer" id="footer_links">
            <?php $footerCMSList = $this->footerData['footerCMSList'];
            if (!empty($footerCMSList) && is_array($footerCMSList)) { ?>
                <ul><?php
                foreach ($footerCMSList as $parentCms) {?>
                <?php if($parentCms->id ==CMS_HOME_PAGE_ID){ ?>
                    <li><a class="active_a" href="<?php echo (!empty($parentCms->id) && $parentCms->id ==CMS_HOME_PAGE_ID) ? ROOT_URL : ROOT_URL.'cms/'.$parentCms->cms_slug;?>"><?php echo $parentCms->title;?></a></li>
					
					<?php
				}?>
               <?php if($parentCms->id ==5){ ?> <li><a href="<?php echo ROOT_URL.'about_us';?>">About us</a></li><?php
				}?>
                
                <?php /* if (!empty($parentCms->sub_page_List) && is_array($parentCms->sub_page_List)) {
                        foreach ($parentCms->sub_page_List as $page) {
                            ?>
                            <li><a class="active_a" href="<?php echo ROOT_URL.'cms/'.$page->cms_slug;?>"><?php echo $page->title;?></a></li><?php
                        }
                    }*/
                  //  if (!empty($parentCms->id) && $parentCms->id ==1) {?>
                        <?php
                   // }
                }?>
                <li><a href="<?php echo ROOT_URL;?>meettheteam">Meet the team</a></li>
                <li><a href="<?php echo ROOT_URL;?>contact-us">Contact us</a></li>
                <?php echo showEditCmsPage('', '', '<li class="cms-edit-wrap">', '</li>');?>
                </ul><?php
            }
            if(!empty($navCategories)) {
                $orderedReturn = array();
                foreach ($navCategories as $categ) {
                    if (!empty($categ['main']->category_order)) {
                        $order = $categ['main']->category_order;
                        $orderedReturn[$order] = $categ;
                    }
                }
                ksort($orderedReturn);
                $navCategories = $orderedReturn;
                $navigationMenu = '';
                $currentUrl = current_url();
                $currentUrl = rtrim($currentUrl, '/').'/';

                foreach ($navCategories as $category) {
                    if (!empty($category['main'])) {?>
                        <ul><?php
                        $mainCategory = $category['main'];
                        $mainCategUrl = $mainCategory->url_slug;?>
                        <li><a class="active_a" href="<?php echo ROOT_URL .'posts/'. $mainCategUrl;?>"><?php echo $mainCategory->title;?></a></li><?php
                        if (!empty($category['sub'])) {
                            $tipSubCateg = $category['sub'];
                            if (!empty($tipSubCateg) && is_array($tipSubCateg)) {
                                (!empty($tipSubCateg) && is_array($tipSubCateg)) ? ksort($tipSubCateg) : array();
                                foreach ($tipSubCateg as $subCateg) {?>
                                    <li><a href="<?php echo ROOT_URL . 'posts/'. $mainCategUrl . '/' . $subCateg->url_slug; ?>"><?php echo $subCateg->title; ?></a></li>
                                    <?php
                                }
                            }?><?php
                        }?>
                        </ul>
                        <?php
                    }
                }
                echo showEditCategory('', 'right-15px', '<ul><li class="cms-edit-wrap">', '</li></ul>');
            }?>
            <div class="clearfix hidden-lg"></div>
        </div>
        <div class="main_outer">
            <div class="copy_div"><?php echo defined('FOOTER_CREDITS') ? FOOTER_CREDITS : '';?><?php echo showEditSettings('FOOTER_CREDITS');?><br />
                Designed and Developed by <a target="_blank" href="http://codeandco.ae">Code&Co	</a>
            </div>
            <div id="nav-icon4" class="hidden-lg">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
            <div class="clearfix hidden-lg"></div>
        </div>
    </div>
</div>