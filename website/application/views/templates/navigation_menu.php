<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 7/27/2015
 * Time: 5:29 PM
 */?>
<ul>
    <li class="hidden-xs"><a class="homeNavigation" href="<?php echo ROOT_URL;?>"></a></li>
    <li class="hidden-lg"><a class="" href="<?php echo ROOT_URL;?>">Home</a></li>
    <?php
    if(!empty($navCategories)) {
        $orderedReturn = array();
        foreach ($navCategories as $categ) {
            if (!empty($categ['main']->category_order)) {
                $order = $categ['main']->category_order;
                $orderedReturn[$order] = $categ;
            }
        }
        ksort($orderedReturn);
        $navCategories = $orderedReturn;
        $navigationMenu = '';
        $currentUrl = current_url();
        $currentUrl = rtrim($currentUrl, '/').'/';

        foreach ($navCategories as $category) {
            if (!empty($category['main'])) {?>
                <li><?php
                $mainCategory = $category['main'];
                $mainCategUrl = $mainCategory->url_slug;?>
                <a <?php echo (defined('CURRENT_SECTION_ID') && CURRENT_SECTION_ID == $mainCategory->id) ? 'class="active"' : '';?> href="<?php echo ROOT_URL .'posts/'. $mainCategUrl;?>"><?php echo $mainCategory->title;?></a><?php
                if (!empty($category['sub'])) {
                    $tipSubCateg = $category['sub'];
                    if (!empty($tipSubCateg) && is_array($tipSubCateg)) {
                        (!empty($tipSubCateg) && is_array($tipSubCateg)) ? ksort($tipSubCateg) : array();?>
                        <div <?php echo (defined('CURRENT_SECTION_ID') && CURRENT_SECTION_ID == $mainCategory->id) ? 'class="active_div"' : '';?>>
                            <ul><?php
                                foreach ($tipSubCateg as $subCateg) {?>
                                    <li><a <?php echo (defined('CATEGORY_ID') && CATEGORY_ID == $subCateg->id) ? 'class="active"' : '';?>
                                        href="<?php echo ROOT_URL . 'posts/'. $mainCategUrl . '/' . $subCateg->url_slug; ?>"><?php echo $subCateg->title; ?></a>
                                    </li><?php
                                }?>
                            </ul>
                        </div><?php
                    }?><?php
                }?>
                </li><?php
            }
        }
    }?>
   <li> <a href="<?php echo ROOT_URL .'directory/';?>">Directory</a></li>
    <?php echo showEditCategory('');?>
</ul>
