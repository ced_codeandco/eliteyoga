<div class="header_main">

    <div class="main_outer">

        <div class="logo_div">
            <a href="<?php echo ROOT_URL;?>"><?php
                    if (!empty($pageId) && $pageId == CMS_CONTENT_PAGE_ID) {?>
                        <img src="<?php echo ROOT_URL_BASE;?>images/content/logo.png"/><?php
                    } else if (!empty($pageId) && $pageId == CMS_NETWORK_PAGE_ID) {?>
                        <img src="<?php echo ROOT_URL_BASE;?>images/network/logo.png"/><?php
                    } else if (!empty($pageId) && $pageId == CMS_STUDIOS_PAGE_ID) {?>
                        <img src="<?php echo ROOT_URL_BASE;?>images/studios/logo.png"/><?php
                    } else { ?>
                        <img src="<?php echo ROOT_URL_BASE;?>images/logo.png"/><?php
                    }
                ?>
            </a>
        </div>

        <span class="pull-left" id="menuclick"><div id="nav-icon3"> <span></span> <span></span> <span></span> <span></span> </div></span>

        <div class="menu_nav_div">
            <?php $currentUrl = current_url();
            if (!empty($pageId)) {?>
            <ul class="social_ul">
                <?php if ($pageId == CMS_CONTENT_PAGE_ID) {?>
                    <li><a href="<?php echo FACEBOOK_LINK;?>" target="_blank"><img src="<?php echo ROOT_URL_BASE;?>images/content/facebook.png"></a></li>

                    <li><a href="<?php echo TWITTER_LINK;?>" target="_blank"><img src="<?php echo ROOT_URL_BASE;?>images/content/twitter.png"></a></li>

                    <li><a href="<?php echo INSTAGRAM_LINK;?>" target="_blank"><img src="<?php echo ROOT_URL_BASE;?>images/content/blog.png"></a></li>

                    <li><a href="<?php echo VIMEO_LINK;?>" target="_blank"><img src="<?php echo ROOT_URL_BASE;?>images/content/v.png"></a></li>

                    <li><a href="<?php echo SNAP_CHAT_LINK;?>" target="_blank"><img src="<?php echo ROOT_URL_BASE;?>images/content/alert.png"></a></li>
                <?php } else if ($pageId == CMS_NETWORK_PAGE_ID) {?>
                    <li><a href="<?php echo FACEBOOK_LINK;?>" target="_blank"><img src="<?php echo ROOT_URL_BASE;?>images/network/facebook.png"></a></li>

                    <li><a href="<?php echo TWITTER_LINK;?>" target="_blank"><img src="<?php echo ROOT_URL_BASE;?>images/network/twitter.png"></a></li>

                    <li><a href="<?php echo INSTAGRAM_LINK;?>" target="_blank"><img src="<?php echo ROOT_URL_BASE;?>images/network/blog.png"></a></li>

                    <li><a href="<?php echo VIMEO_LINK;?>" target="_blank"><img src="<?php echo ROOT_URL_BASE;?>images/network/v.png"></a></li>

                    <li><a href="<?php echo SNAP_CHAT_LINK;?>" target="_blank"><img src="<?php echo ROOT_URL_BASE;?>images/network/alert.png"></a></li>
                <?php } else if ($pageId == CMS_STUDIOS_PAGE_ID) {?>
                    <li><a href="<?php echo FACEBOOK_LINK;?>" target="_blank"><img src="<?php echo ROOT_URL_BASE;?>images/studios/facebook.png"></a></li>

                    <li><a href="<?php echo TWITTER_LINK;?>" target="_blank"><img src="<?php echo ROOT_URL_BASE;?>images/studios/twitter.png"></a></li>

                    <li><a href="<?php echo INSTAGRAM_LINK;?>" target="_blank"><img src="<?php echo ROOT_URL_BASE;?>images/studios/blog.png"></a></li>

                    <li><a href="<?php echo VIMEO_LINK;?>" target="_blank"><img src="<?php echo ROOT_URL_BASE;?>images/studios/v.png"></a></li>

                    <li><a href="<?php echo SNAP_CHAT_LINK;?>" target="_blank"><img src="<?php echo ROOT_URL_BASE;?>images/studios/alert.png"></a></li>
                <?php }?>
            </ul>
            <div class="clearfix"></div>
            <?php }?>

            <ul class="nav">

                <li><a href="<?php echo ROOT_URL?>" class="<?php echo empty($pageId) ? 'active' : ''; ?>">Home</a></li>
                <?php if (!empty($headerCMSList) && is_array($headerCMSList)) {
                    foreach ($headerCMSList as $page) {
                        if ($page->id == CMS_CONTACT_PAGE_ID) {
                            echo '<li><a href="#' . $page->cms_slug . '" >' . $page->title . '</a></li>';
                        } else {?>
                            <li class="sub_menu has-dropdown <?php echo  ((!empty($pageId) && $pageId == $page->id) ? 'dropdown_active' : '');?>">
                                <a href="<?php echo ROOT_URL . $page->cms_slug;?>"  <?php echo  ((!empty($pageId) && $pageId == $page->id) ? 'class="active"' : '');?> ><?php echo  $page->title;?></a>
                                <?php 
                                    if ($page->id == CMS_CONTENT_PAGE_ID) {?>
                                        <span class="arrow_span drop_menu"></span>
                                        <ul class="drop_menu submenu_ul">
                                            <li><a href="<?php echo ROOT_URL . $page->cms_slug;?>#contact-us">contact</a></li>
                                            <li><a href="<?php echo ROOT_URL . $page->cms_slug;?>#latest_video">showreel</a>/</li>
                                            <li><a href="<?php echo ROOT_URL . $page->cms_slug;?>#released_projects">Case studies</a>/</li>
                                        </ul>
                                <?php } else if ($page->id == CMS_NETWORK_PAGE_ID) {?>
                                 		<span class="arrow_span drop_menu"></span>
                                        <ul class="drop_menu submenu_ul">
                                            <li><a href="<?php echo ROOT_URL . $page->cms_slug;?>#contact-us">contact</a></li>
                                            <li><a href="<?php echo ROOT_URL . $page->cms_slug;?>#intro">Upload Videos</a>/</li>
                                        </ul>
								<?php } else if ($page->id == CMS_STUDIOS_PAGE_ID) {?>
                                    <span class="arrow_span drop_menu"></span>
                                    <ul class="drop_menu submenu_ul1">
										<li><a href="<?php echo ROOT_URL . $page->cms_slug;?>#contact-us">contact</a></li>
                                        <li><a href="<?php echo ROOT_URL . $page->cms_slug;?>#upcoming_projects">Upcoming</a>/</li>
                                        <li><a href="<?php echo ROOT_URL . $page->cms_slug;?>#released_projects">Released</a>/</li>
                                        <li><a href="<?php echo ROOT_URL . $page->cms_slug;?>#intro">Intro</a>/</li>
                                    </ul>
                                <?php } else ?>
                                <?php //}?>
                            </li>

                <?php
                        }
                    }
                }?>
                <li><a href="<?php echo ROOT_URL;?>#contact-us">Contact</a></li>

            </ul>

        </div>

        <div class="clearfix"></div>

    </div>
    <?php if (!empty($pageId) && $pageId == CMS_CONTENT_PAGE_ID) {
        echo '<input type="hidden" id="map-cion" value="'.ROOT_URL_BASE.'images/content/google-map-cion.png">';
    } else if (!empty($pageId) && $pageId == CMS_NETWORK_PAGE_ID) {
        echo '<input type="hidden" id="map-cion" value="'.ROOT_URL_BASE.'images/network/google-map-cion.png">';
    } else if (!empty($pageId) && $pageId == CMS_STUDIOS_PAGE_ID) {
        echo '<input type="hidden" id="map-cion" value="'.ROOT_URL_BASE.'images/studios/google-map-cion.png">';
    } else {
        echo '<input type="hidden" id="map-cion" value="'.ROOT_URL_BASE.'images/google-map-cion.png">';
    }?>
</div>