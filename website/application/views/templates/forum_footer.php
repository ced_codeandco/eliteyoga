</div></div></div>
<?php if(empty($wrapperType) OR $wrapperType != 'ajax') {?>
<div class="clearfix"></div>
<footer class="footer">
    <div class="container">
        <p class="text-muted">&copy; 2015 MomsUnited. All rights reserved. <?php
        if(!empty($footerCMSList) && is_array($footerCMSList)) {
            $i = 0;
            $count = count($footerCMSList);
            foreach ($footerCMSList as $page) {
                echo '<a href="'.ROOT_URL.'cms/'.$page->cms_slug.'/ajax" class="privacy-policy">'. $page->title .'</a>';
                echo ($i != 1 && $i != $count && $count > 1) ? ' | ' : '';
                $i ++;
            }
        }?>
        </p>
    </div>
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-65046627-1', 'auto');
  ga('send', 'pageview');

</script>
</footer>
</body>
</html>
<?php }?>