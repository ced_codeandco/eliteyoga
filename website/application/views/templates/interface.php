<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 9/10/2015
 * Time: 2:09 PM
 */
?>
<div class="main_outer"><?php
    $contentData = array();
    if(is_array($this->contentData) && is_array($this->headerData)) {
        $contentData = array_merge($this->contentData, $this->headerData);
    } else if(is_array($this->contentData)) {
        $contentData = $this->contentData;
    } else if(is_array($this->headerData)) {
        $contentData = $this->headerData;
    }
    //print_r($contentData); die();
    if (!empty($sections) && !empty($sections['page'])) {
        $this->load->view($sections['page'], $contentData);
    }

    if (!empty($sections) && !empty($sections['right'])) {
        $this->load->view($sections['right'], $this->contentData);
    }?>
</div>