<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 5/4/2015
 * Time: 8:12 PM
 */?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<script>
    window.twttr=(function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],t=window.twttr||{};if(d.getElementById(id))return t;js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);t._e=[];t.ready=function(f){t._e.push(f);};return t;}(document,"script","twitter-wjs"));
</script>
<script type="text/javascript" src="<?php echo ROOT_URL_BASE;?>js/crank-slider.min.js"></script>
<link type="text/css" rel="stylesheet" href="<?php echo ROOT_URL_BASE;?>css/crank-slider.css" />
<div class="container calendar">
    <div class="content-row">
        <div class="city-filter-classifieds">
            <div class="clasfds-title"><h2>Event details</h2></div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
        <?php
        if(isset($errMsg) && $errMsg != ''){
            echo '<div class="alert alert-danger">' . $errMsg. '</div>';
            unset($errMsg);
        }
        if(isset($succMsg) && $succMsg != ''){
            echo '<div class="alert alert-success">' . $succMsg . '</div>';
            unset($succMsg);
        }
        //echo validation_errors();?>
        <div class="event-banner">
                <div class="event-title-banner">
                    <?php //print_r($event['eventDetails']->target_date);?>
                    <div class="event-date-wrap"><?php
                        echo date_create($event['eventDetails']->target_date)->format('M');
                        echo '<br />';						
                        echo "<span class='edate'>".date_create($event['eventDetails']->target_date)->format('d')."</span>";
                        //echo date_create($event['eventDetails']->target_date)->format('Y');//date('', $event['eventDetails']->target_date);?></div>
                    <?php echo '<span class="etitle">'.$event['eventDetails']->title.'</span>';?>
                </div>
            <?php if (!empty($event['eventDetails']->banner_image) && file_exists(DIR_UPLOAD_EVENTS.$event['eventDetails']->banner_image)) {?>
                <img class="banner-image" src="<?php echo DIR_UPLOAD_EVENTS_SHOW.$event['eventDetails']->banner_image;?>" /><?php
            } else {?>
                <img class="banner-image dummy"  src="<?php echo ROOT_URL_BASE;?>images/eventsDummyBanner.png" />
            <?php }?>
        </div>
        <div class="event-host"><label id="hosted-by">Hosted By: </label><?php echo !empty($event['ownerDetails']->first_name) ? $event['ownerDetails']->first_name : ''; echo !empty($event['ownerDetails']->last_name) ? ' '.$event['ownerDetails']->last_name : ''; //print_r($event['ownerDetails']);?></div>
        <div class="event-description"><!--<label>Location:</label>--><?php echo $event['eventDetails']->description;?></div>
        <?php
        if (!empty($event['attendees']) && is_array($event['attendees'])) {
            $attendees = '';
            $maybeAttending = '';
            //$notGoing = '';
            foreach ($event['attendees'] as $item) {
                if (!empty($item->response_status_id)) {
                    if ($item->response_status_id == 1) {
                        //$attendees .= !empty($attendees) ? ', ' : '';
                        $attendees .= '<a href="javascript:void(0)" class="attendees-name-wrap">' . $item->first_name . '</a>';
                    } else if ($item->response_status_id == 2) {
                        //$maybeAttending .= !empty($maybeAttending) ? ', ' : '';
                        $maybeAttending .= '<a href="javascript:void(0)" class="attendees-name-wrap">' . $item->first_name . '</a>';
                    }
                    //echo '<img src="' . ROOT_URL . 'images/' . $item->responseStatusIcon . '" />';
                }
            }
        }?>
        <div class="clearfix"></div>
        <?php if (!empty($attendees)) {?>
        <div class="event-location" id="attendees"><label id="yesattend">Attendees:</label><?php echo $attendees;?></div>
        <div class="clearfix"></div>
        <?php }?>
        <?php if (!empty($maybeAttending)) {?>
        <div class="event-location"><label id="maybeattend">Maybe Attending:</label><?php echo $maybeAttending;?></div>
        <div class="clearfix"></div>
        <?php }?>
        <div class="event-location <?php if (empty($attendees)) {echo "no-attendees"; }?>"><label id="iconloc">Location:</label><?php echo $event['eventDetails']->classified_locality;?></div>
        <div class="clearfix"></div>
        <?php if(!empty($event['eventDetails']->embedd_map)) {?>
        <div class="event-location" id="emap"><iframe src="<?php echo $event['eventDetails']->embedd_map;?>" width="100%" height="500"></iframe></div>
        <?php }?>
        <div >
        <?php //echo $paginator;//print_r($event['attendees']);?>
       </div>

    </div>
</div>
<script src='https://www.google.com/recaptcha/api.js?hl=en'></script>
<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>js/jquery.validate.min.js"></script>
<script>

    $(document).ready(function(){
        $('#termsAndConditions').click(function(){
            //$.colorbox({width:"700", height:"450", html:$('#termsAndConditions-content').html()});
            $('.privacy-policy:first').trigger('click');
            return false;
        })
        $('#privacyPolicy').click(function(){
            $.colorbox({width:"700", height:"450", html:$('#privacyPolicy-content').html()});
            return false;
        })

        jQuery.validator.addMethod("validPhoneNumber", function(value, element) {
            //$(element).val(value.trim());
            return this.optional(element) || validPhoneNumber(value);
        }, "Invalid phone number");
        jQuery.validator.addMethod("validateCaptcha", function(value, element) {
            var v = grecaptcha.getResponse();
            if(v.length == 0)
            {
                return false;
            } else {
                return true;
            }
        }, "Please verify you are not a robot");
        $("#enquiryForm").validate({
            rules: {
                email: {
                    required: true,
                    email: true,
                    maxlength: 200
                },
                name:{
                    required: true,
                    maxlength: 100
                },
                phone:{
                    required: true,
                    maxlength: 15,
                    validPhoneNumber: true,
                    minlength: 10
                },
                comment:{
                    required: true,
                    maxlength: 200
                },
                validateCaptcha:{
                    validateCaptcha: true
                }
            },
            messages: {
                email: {
                    required: 'Please select country of advertisement',
                    email: 'Invalid email id',
                    maxlength: 'Maximum length allowed is 200'
                },
                name:{
                    required: 'Please enter your name',
                    maxlength: 'Maximum length allowed is 100 characters'
                },
                phone:{
                    required: 'Please enter your phone number',
                    maxlength: 'Invalid phone number',
                },
                comment:{
                    required: 'Please enter your comments',
                    maxlength: 200
                },
                validateCaptcha:{
                    validateCaptcha: 'Please verify you are not a robot'
                }
            }
        });

        $('#showPhoneNumber').click(function(){
            console.log($('#showPhoneNumber').parent().find('span').length);
            $(this).find('span').toggle();
        })
        $('#enquiryForm').submit(function(){
            /*var v = grecaptcha.getResponse();
            if(v.length == 0)
            {
                $('#captcha').html("Please verify you are not a robot").show();
                return false;
            } else {
                $('#captcha').hide();
            }*/
        })
    })
</script>