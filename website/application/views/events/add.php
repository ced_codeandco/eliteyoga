<link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_BASE?>css/events-calendar.css" />
<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 7/26/2015
 * Time: 5:54 PM
 */
//print_r($eventsLookUp);
?>
<div class="container event-wrap normal-page add-event">
    <div class="content-row">
        <h2>Add Events</h2><?php
        if(isset($errMsg) && $errMsg != ''){
            echo '<div class="alert alert-danger">' . $errMsg. '</div>';
            unset($errMsg);
        }
        if(isset($succMsg) && $succMsg != ''){
            echo '<div class="alert alert-success">' . $succMsg . '</div>';
            unset($succMsg);
        }
        echo validation_errors();?>
        <div class="col-md-12">

            <p>Add details of your event and send invitation to all members</p>

            <div class="contact-form" id="add-event">
                <div id="abhi">
                    <form id="add_events_form" name="add_events_form" method="post" action="" enctype="multipart/form-data">
                        <div class="col-md-7">
                             <label>Name *</label>
                            <input value="<?php if (!empty($eventDetails['title'])){ echo $eventDetails['title']; }?>" placeholder="ex: Playdate, Get together, Seminar..." type="text" name="title" id="title" required />
                        </div>

                        <label class="comment">Details</label>
                        <textarea name="description" id="description" placeholder="Add more info" rows="4" style="height:80px; font-size:13px;" required ><?php if (!empty($eventDetails['description'])){ echo $eventDetails['description']; }?></textarea>
                        <div class="clearfix"></div>

                        <div class="col-md-6">
                            <label>Where *</label>
                            <input value="<?php if (!empty($eventDetails['classified_locality'])){ echo $eventDetails['classified_locality']; }?>"  placeholder="Add a place" type="text" name="classified_locality" id="classified_locality" required />
                        </div>

                        <div class="col-md-6 marg-left">
                            <label>Embedd Map</label>
                            <input value="<?php if (!empty($eventDetails['embedd_map'])){ echo $eventDetails['embedd_map']; }?>"  placeholder="Embed location map" type="text" name="embedd_map" id="embedd_map"  />
                        </div>
                        
                        <div class="clearfix"></div>

                        <div class="col-md-3">
                            <label>Maximum attendees</label>
                            <input value="<?php if (!empty($eventDetails['max_attendee_count'])){ echo $eventDetails['max_attendee_count']; }?>"  placeholder="Maximum number of attendees" type="text" name="max_attendee_count" id="max_attendee_count"  />
                        </div>

                        <div class="col-md-3">
                            <label>When*</label>
                            <input placeholder="Date" value="<?php if (!empty($eventDetails['target_date'])){ echo date('d F Y', strtotime($eventDetails['target_date'])); } else if (!isset($eventDetails['target_date'])) { echo date('d F Y'); }?>"   type="text" name="target_date" id="datePicker" required />
                        </div>
                        <div class="col-md-3">
                            <label class="hidden-xs">&nbsp;</label><?php
                            $selectedHour = (!empty($eventDetails['target_date'])) ? date('H:i:s', strtotime($eventDetails['target_date'])) : '';?>
                            <input placeholder="Time" type="text" name="event_time" id="eventTime" value="<?php echo $selectedHour;?>" />
                        </div>
                        <div class="clearfix"></div>
                        
                        <div class="col-md-12 checkbox-wrapper">
                        	<div class="pull-left"><input type="checkbox" value="1" name="allow_maybe_status" <?php if (isset($eventDetails['allow_maybe_status'])){ if(!empty($eventDetails['allow_maybe_status']) && $eventDetails['allow_maybe_status'] ==1){ echo 'checked'; } } else if (!isset($_POST['allow_maybe_status'])) { echo 'checked'; }?> >
                            <span class="checkbox-label">Allow attendees to choose Maybe</span></div>
                            
                            <div class="pull-left"><input type="checkbox" name="notify" value="1" <?php if (!empty($eventDetails['notify']) && $eventDetails['notify'] == 1){ echo 'checked="checked"'; }?> >
                            <span class="checkbox-label">Notify all members</span></div>
                        </div>

                        <div class="preview-images-wrap"><?php
                            if (!empty($eventDetails['banner_image']) && file_exists(DIR_UPLOAD_EVENTS . $eventDetails['banner_image'])) { ?>
                                <div class="preview-images">
                                <!--<button class="removeImages" id="removeEventImage" eventId="<?php /*echo $eventDetails['id'];*/?>" title="Remove this image">&nbsp;</button>-->
                                <img src="<?php echo ROOT_URL_BASE ?>assets/timthumb.php?src=<?php echo DIR_UPLOAD_EVENTS_SHOW . $eventDetails['banner_image']; ?>&q=100&w=100" />
                                </div><?php
                            }
                        ?>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group file-uplopader-row">
                            <div class="pull-left" id="filearea_wrap"><label class="filebutton">
                                Upload Image
                                <div class="clearfix"></div>
                                <span>Browse Here<input type="file" accept="image/*" id="banner_image" name="banner_image"></span>
                            </label>
                            <p class="help-block">Max size: 3MB (Only jpeg, jpg, png).</p></div>
                            
                            <input  value="ADD EVENT" type="submit"  class="pull-left submit contact-form">
                        </div>
                        <div class="clearfix"></div>


                        <?php /*?><div class="col-md-12"><?php //print_r($eventDetails['notify']);?>
                            <div class="checkbox">
                                <label><input type="checkbox" name="notify" value="1" <?php if (!empty($eventDetails['notify']) && $eventDetails['notify'] == 1){ echo 'checked="checked"'; }?> ></label>
                            </div>
                            <div class=" col-md-12 event-checkbox-aliment"><label>Notify all members</a></label></div>

                            <label id="notify-error" class="error register-floated" for="notify" style=" display: none;"></label>
                        </div>
                        <div class="clearfix"></div><?php */?>

                       <?php /*?> <div class="col-md-12  pull-left">
                            <input  value="ADD" type="submit"  class="submit contact-form">
                        </div><?php */?>
                    </form>
                </div></div>
        </div>
    </div><?php

    $dateIntervalArray = getTimeIntervalInMinutes(5);
    ?>
</div>
<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>js/jquery.validate.min.js"></script>
<script src="<?php echo ROOT_URL_BASE;?>js/ui/jquery.ui.core.js"></script>
<script src="<?php echo ROOT_URL_BASE;?>js/ui/jquery.ui.widget.js"></script>
<script src="<?php echo ROOT_URL_BASE;?>js/ui/jquery.ui.datepicker.js"></script>
<script src="<?php echo ROOT_URL_BASE;?>js/ui/jquery.ui.autocomplete.js"></script>
<script src="<?php echo ROOT_URL_BASE;?>js/ui/jquery.ui.core.js"></script>
<script src="<?php echo ROOT_URL_BASE;?>js/ui/jquery.ui.widget.js"></script>
<script src="<?php echo ROOT_URL_BASE;?>js/ui/jquery.ui.menu.js"></script>
<script src="<?php echo ROOT_URL_BASE;?>js/ui/jquery.ui.position.js"></script>
<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>js/events.js"></script>
<link rel="stylesheet" href="<?php echo ROOT_URL_BASE;?>css/themes/base/jquery.ui.all.css">
<script>

    $(function() {
        var availableTags = [<?php echo '"'.implode('","', $dateIntervalArray).'"' ?>];
        console.log(availableTags.toString());
        /*$( "#eventTime" ).autocomplete({
            source: availableTags
        });*/
        $( "#eventTime" ).autocomplete({
            source: availableTags,
            change: function (event, ui) {
                //if the value of the textbox does not match a suggestion, clear its value
                if ($(".ui-autocomplete li:textEquals('" + $(this).val() + "')").size() == 0) {
                    $(this).val('');
                }
            }
        }).live('keydown', function (e) {
            var keyCode = e.keyCode || e.which;
            //if TAB or RETURN is pressed and the text in the textbox does not match a suggestion, set the value of the textbox to the text of the first suggestion
            if((keyCode == 9 || keyCode == 13) && ($(".ui-autocomplete li:textEquals('" + $(this).val() + "')").size() == 0)) {
                $(this).val($(".ui-autocomplete li:visible:first").text());
            }
        });
    });
</script>