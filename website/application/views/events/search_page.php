<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 5/3/2015
 * Time: 2:05 PM
 */?>
<div class="container generic-search-wrap-event">
    <div class="content-row">
        <div class="clasfds-title"><h2>Search Results</h2>
        </div>
        <div class="col-md-10">
        	<div class="city-filter-classifieds hidden-lg">
            <span class="sub-title"></span>
            <div class="clearfix"></div>
            </div><br class="hidden-lg"/>
            <div class="contact-form"><?php
                $serializedFilterControls = (!empty($this->headerData['navCategories'][CURRENT_SECTION_ID]['main']->filter_controls) ? $this->headerData['navCategories'][CURRENT_SECTION_ID]['main']->filter_controls : array());
                $filterControls = populateFilterControl(CURRENT_SECTION_ID, current_url(), $serializedFilterControls);
                $serializedSortControls = (!empty($this->headerData['navCategories'][CURRENT_SECTION_ID]['main']->sort_controls) ? $this->headerData['navCategories'][CURRENT_SECTION_ID]['main']->sort_controls : array());
                $sortControls = populateSortControl(CURRENT_SECTION_ID, current_url(), $serializedSortControls);
                if (!empty($sortControls) OR !empty($filterControls)) {
                    echo '<div class="generic_wrap">'.$filterControls.'</div>';
                }
                ?>
            </div>


            <div id="searchresult2"><span class="sub-title no-float"><?php echo 'You\'ve searched for \''.(!empty($_GET['search_keyword']) ? $_GET['search_keyword'] : '').'\''; ?></span>            
            <span class="sub-title-found hidden-xs"><?php echo 'Found '.(!empty($searchResultTotal) ? $searchResultTotal : 0).' item(s)';?></span><div class="clearfix hidden-lg"></div></div>
            
            
            <?php echo $sortControls;?>
            
            <div class="clearfix hidden-lg"></div>
            <?php //if (!empty($_GET['searched'])){?>
                <?php //echo populateSortControl('', current_url())?>
                <!--<span class="sub-title-found"><?php /*echo 'Found '.(!empty($searchResultTotal) ? $searchResultTotal : 0).' items';*/?></span>-->
            <?php //}?>

            <?php //echo $paginator;?>
            <?php
            if (!empty($searchResult) && is_array($searchResult)) { ?>
				<div class="generic-search-result">
                <?php foreach ($searchResult as $classified) {
                    $detailsUrl = ROOT_URL.FOOD_AND_DRINKS_URL.'/details/'.$classified->classified_slug;
                    if ($classified->section_id == EVENTS_ID) {
                        $detailsUrl = ROOT_URL.'events/details/'.$classified->id;
                    } else if ($classified->section_id == TOP_PAEDS_ID) {
                        $detailsUrl = ROOT_URL.TOP_PAEDS_URL.'/details/'.$classified->id;
                    } else if ($classified->section_id == RECIPES_ID) {
                        $detailsUrl = ROOT_URL.RECIPES_URL.'/details/'.$classified->id;
                    } else if ($classified->section_id == BRESTFEEDING_ID) {
                        $detailsUrl = ROOT_URL.BRESTFEEDING_URL.'/details/'.$classified->id;
                    } else if ($classified->section_id == FUNWITHMOM_ID) {
                        $detailsUrl = ROOT_URL.FUNWITHMOM_URL.'/details/'.$classified->id;
                    }?>
                    <div class="listcontainer generic-search">
                        <div class="head-list-contner">
                            <span class="list-title">
                                <a class="classified-title-link" href="<?php echo $detailsUrl;?>">
                                    <?php
                                    echo strtoupper($classified->title);?><?php ?>
                                </a>
                            </span>
                        </div>

                        <div class="event-search-right-wrap pull-right">
                            <div class="generic-request-now pull-right">
                                <label>Date: </label><?php echo date('d F, Y', strtotime($classified->target_date));?>
                            </div>
                            <div class="clearfix"></div>
                            <div class="generic-request-now pull-right">
                                <label>Location: </label><?php echo $classified->classified_locality;?>
                            </div>
                            <div class="clearfix"></div>

                            <div class="generic-request-now pull-right">
                                <img class="hidden loaderImage" src="<?php echo ROOT_URL_BASE.'images/pink-ajax-loader.gif';?>" />
                                <input title="Add to favourites"  type="button" value="" class="addToFavoritesButton  <?php echo !empty($this->user_id) ? 'addToFavoritesControl' : 'ajax-2';?> <?php echo !empty($classified->favouriteId) ? 'active' : '';?>" item_id="<?php echo $classified->id;?>" item_url="<?php echo $detailsUrl;?>" user_id="<?php echo !empty($this->user_id) ? $this->user_id : '';?>" section_id="<?php echo defined('CURRENT_SECTION_ID') ? CURRENT_SECTION_ID : '';?>" favouriteId="<?php echo !empty($classified->favouriteId) ? $classified->favouriteId : '';?>">
                            </div>
                        </div>
                    <div class="generic-search-desc events">
                        <?php
                        echo content_truncate(strip_tags($classified->description), 300, ' ', "...", true);
                        ?><?php //print_r($classified);?>
                    </div>


                    <div class="clearfix"></div></div><?php
                }?>
            </div><?php } else if(!empty($searchCriteria)) {?>
            <div class="listcontainer generic-search-not-found">
                <div class="head-list-contner no-items">
                    <span class="list-title no-item-msg"><?php echo (empty($searchCriteria) ? "Please select some criteria" : "No item found");?></span>
                </div>
            </div>
                <?php
            }
            ?>
            <div class="paginator-botom-wrap">
            <?php echo $paginator;?>
            </div>
        </div>
    </div></div>
<script src="<?php echo ROOT_URL_BASE;?>js/ui/jquery.ui.core.js"></script>
<script src="<?php echo ROOT_URL_BASE;?>js/ui/jquery.ui.widget.js"></script>
<script src="<?php echo ROOT_URL_BASE;?>js/ui/jquery.ui.datepicker.js"></script>
<link rel="stylesheet" href="<?php echo ROOT_URL_BASE;?>css/themes/base/jquery.ui.all.css">
