<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 7/27/2015
 * Time: 5:21 PM
 */
?>
<div class="container user-account my-invitations">
    <div class="content-row">
        <h2>Invitations</h2>

        <div class="col-md-12"><?php
            if(isset($errMsg) && $errMsg != ''){ ?>
                <div class="alert alert-danger">
                    <?php echo $errMsg;?>
                </div>
                <?php unset($errMsg);
            }
            if(isset($succMsg) && $succMsg != ''){ ?>
                <div class="alert alert-success">
                    <?php echo $succMsg;?>
                </div>
                <?php unset($succMsg);
            }?>
<?php echo $paginator;?>
<?php
if (empty($dataList) OR !is_array($dataList)) {?>
    <div class="col-xs-12">
    <div class="alert alert-danger">Sorry! No invitations found</div>
    <span class="link-button-wrap" id="invitation_button"><a href="<?php echo ROOT_URL?>events/add">Create an event</a></span>
    </div><?php
} else {
    ?>
    <div class="my-ads-col">
        <div class="bs-example" data-example-id="simple-table">
            <table class="table">
                <thead>
                <tr>
                    <th class="hidden-xs">No</th>
                    <th class="wide-title-column">Title</th>
                    <th class="hidden-xs">Location</th>
                    <th>Date</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php

                $i = 0;
                $paOrder = !empty($recordCountStart) ? $recordCountStart : 1;
                foreach ($dataList as $item) {?>
                    <tr>
                    <th scope="row" class="hidden-xs"><?php echo $paOrder; ?></th>
                    <td class="wide-title-column"><a
                            href="<?php echo ROOT_URL ?>events/details/<?php echo $item->id?>"><?php echo $item->title; ?></a>
                    </td>
                    <td  class="hidden-xs"><?php echo $item->classified_locality; ?></td>
                    <td><span><?php echo date('H:i A', strtotime($item->target_date)); ?><br/>
                    	<?php echo date('d-m-Y', strtotime($item->target_date)); ?></span>
                    </td>
                    <td><?php
                        if (!empty($item->responseStatusId)) {
                            if (!empty($item->responseStatusIcon) && file_exists(ROOT_PATH . 'images/' . $item->responseStatusIcon)) {
                                echo '<img src="' . ROOT_URL . 'images/' . $item->responseStatusIcon . '" />';
                            }
                            echo !empty($item->responseTitle) ? $item->responseTitle : '';
                        } else {
                            echo '<img src="' . ROOT_URL . 'images/event-notdecided.png" />Not decided';
                        }
                        ?>
                    </td>
                    <td><div class="status-btn">
                        <?php
                        $showNotDecided = false;
                        if (!empty($invitationStatusLookup) && is_array($invitationStatusLookup)) {
                            foreach ($invitationStatusLookup as $item1) {
                                if (empty($item->responseStatusId) OR $item->responseStatusId != $item1->id) {
                                    if (($item->event_full_status == 1 && $item1->handle == 'GOING') OR ($item->event_full_status == 1 && $item->allow_maybe_status == 1 && $item1->handle == 'MAYBE')) {
                                    } else {
                                        if (!empty($item1->icon) && file_exists(ROOT_PATH . 'images/' . $item1->icon)) {
                                            echo '<a href="' . ROOT_URL . 'events/change_status/' . $item->id . '/' . $item1->id . '/' . $item->eventResponseId . '" class="clickAction"><img src="' . ROOT_URL . 'images/' . $item1->icon . '" />' . $item1->title . '</a>';
                                        }
                                    }
                                } else {
                                    $showNotDecided = true;
                                }
                            }
                        }
                        if ($showNotDecided == true) {
                            echo '<a href="'.ROOT_URL.'events/change_status/'.$item->id.'/0" class="clickAction"><img src="' . ROOT_URL . 'images/event-notdecided.png" />Not decided</a>';
                        }//print_r($item);?>
                        <div class="clearfix"></div>
                        </div>
                    </td>
                    </tr><?php
                    $paOrder++;
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
    <?php
    echo "<div class='hidden-xs'>".$paginator."</div>";
}?>
</div>
</div>
<?php echo "<div class='paginator-botom-wrap hidden-lg'>".$paginator."</div>"; ?>
</div>
