<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 7/27/2015
 * Time: 5:21 PM
 */
?>
<div class="container user-account my-event">
    <div class="content-row">
        <h2>My Events</h2>

        <div class="col-md-12"><?php
            if(isset($errMsg) && $errMsg != ''){ ?>
                <div class="alert alert-danger">
                    <?php echo $errMsg;?>
                </div>
                <?php unset($errMsg);
            }
            if(isset($succMsg) && $succMsg != ''){ ?>
                <div class="alert alert-success">
                    <?php echo $succMsg;?>
                </div>
                <?php unset($succMsg);
            }?>
<?php echo $paginator;?>
<?php
if (empty($dataList) OR !is_array($dataList)) {?>
    <div class="col-xs-12">
    <div class="alert alert-danger">
        You have not created any events.               </div>
    <!--<p>You have not placed any ads.</p>-->
    <span class="link-button-wrap"><a href="<?php echo ROOT_URL?>events/add">Create an event</a></span>
    </div><?php
} else {
    ?>
    <div class="my-ads-col">
        <div class="bs-example" data-example-id="simple-table">
            <table class="table">
                <thead>
                <tr>
                    <th class="hidden-xs">No</th>
                    <th class="wide-title-column">Title</th>
                    <th>Location</th>
                    <th>Date</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php

                $i = 0;
                $paOrder = !empty($recordCountStart) ? $recordCountStart : 1;
                foreach ($dataList as $item) { ?>
                    <tr>
                    <th scope="row"  class="hidden-xs"><?php echo $paOrder; ?></th>
                    <td class="wide-title-column"><a
                            href="<?php echo ROOT_URL ?>events/details/<?php echo $item->id?>"><?php echo $item->title; ?></a>
                    </td>
                    <td><?php echo $item->classified_locality; ?></td>
                    <td><span class="hidden-xs"><?php echo date('H:i A, d-m-Y', strtotime($item->target_date)); ?></span>
                    <span class="hiddenlg"><?php echo date('d-m-Y', strtotime($item->target_date)); ?></span>
                    </td>
                    <td><span class="status-btn">
                                        <?php if($item->is_active=='1'){$item ?>
                                            <span class="hidden-xs"><img src="<?php echo ROOT_URL_BASE; ?>/images/ico-done.gif" />
                                            <a href="javascript:void(0)" class="label-success label label-default" >Active</a></span>
                                            <span class="hidden-lg"><a href="javascript:void(0)" class="label-success label label-default" ><img src="<?php echo ROOT_URL_BASE; ?>/images/ico-done.gif" /></a></span>
                                        <?php }else{ ?>
                                            <span class="hidden-xs"><img src="<?php echo ROOT_URL_BASE; ?>/images/ico-delete.gif" />
                                            <a href="javascript:void(0)" class="label-default label label-danger active"  >Inactive</a></span>
                                            <span class="hidden-lg"><a href="javascript:void(0)" class="label-default label label-danger active"  ><img src="<?php echo ROOT_URL_BASE; ?>/images/ico-delete.gif" /></a></span>
                                        <?php } ?>
                                    </td>
                    <td>
                                        <span class="archive-btn">
                                            <a title="Edit" href="<?php echo ROOT_URL ?>events/add/<?php echo $item->id ?>"
                                               class="active+ edit"></a>
                                        </span>
                                        <span class="archive-btn">
                                            <a class="trash" onclick="javascript:if(confirm('Are you sure you want to delete this event ? ')){location.href='<?php echo ROOT_URL ?>events/delete_events/<?php echo $item->id ?>'}"
                                               href="#" title="Delete"></a>
                                        </span>
                    </td>
                    </tr><?php
                    $paOrder++;
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
    <?php
    echo $paginator;
}?>
</div>
</div></div>
