<link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_BASE?>css/events-calendar.css" />
<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 7/26/2015
 * Time: 5:54 PM
 */
//print_r($eventsLookUp);
?>
<div class="container calendar">
    <div class="content-row">
    	<div class="city-filter-classifieds">
            <div class="clasfds-title"><h2>Events
                    <a class="hidden-lg search-control expand color-white mobile-search" rel="listing-control-group">Search</a>
                    <a class="search-control expand hidden-xs" rel="listing-control-group"></a>
                </h2></div>
            <div class="clasfds-btn">
                <span class="post-link"><a class="<?php echo (!$this->is_logged_in) ? 'ajax-2' : '' ?>" href="<?php echo ROOT_URL;?>events/add">Create Event</a>
                <div class="clearfix"></div>
                </span>
            </div>
            <div class="clear"></div>
        </div>
        <?php
        $serializedFilterControls = (!empty($this->headerData['navCategories'][CURRENT_SECTION_ID]['main']->filter_controls) ? $this->headerData['navCategories'][CURRENT_SECTION_ID]['main']->filter_controls : array());
        $filterControls = populateFilterControl(CURRENT_SECTION_ID, current_url(), $serializedFilterControls);
        $serializedSortControls = (!empty($this->headerData['navCategories'][CURRENT_SECTION_ID]['main']->sort_controls) ? $this->headerData['navCategories'][CURRENT_SECTION_ID]['main']->sort_controls : array());
        $sortControls = populateSortControl(CURRENT_SECTION_ID, current_url(), $serializedSortControls);

         if (!empty($filterControls)) {?>
        <?php
            echo '<div class="listing-control-group fullw">'.$filterControls.'</div>';
        }?>
        <div class="clearfix"></div>
        <?php //echo draw_calendar($currentDate->format('m'), $currentDate->format('Y'), $eventsLookUp);?>
        <div id='fullCalendarPlaceholder'></div>
    </div>
</div>
<link href='<?php echo ROOT_URL_BASE;?>fullcalendar/fullcalendar.css' rel='stylesheet' />
<link href='<?php echo ROOT_URL_BASE;?>fullcalendar/fullcalendar.print.css' rel='stylesheet' media='print' />
<script src='<?php echo ROOT_URL_BASE;?>fullcalendar/fullcalendar.min.js'></script>


<link rel='stylesheet' type='text/css' href='<?php echo ROOT_URL_BASE;?>fullcalendar/fullcalendar.css' />
<link rel='stylesheet' type='text/css' href='<?php echo ROOT_URL_BASE;?>fullcalendar/fullcalendar.print.css' media='print' />
<script type='text/javascript' src='<?php echo ROOT_URL_BASE;?>fullcalendar/fullcalendar.js'></script>
<script type='text/javascript'>
    $(document).ready(function() {
        var rootUrlLink = $('#rootUrlLink').val();
        if ($('#fullCalendarPlaceholder').length > 0) {
            var dayTitle = 'dddd, d MMM, yyyy';
            console.log($('.nav-bar').css('height'));
            if($('.nav-bar').css('height') != '38px'){
                dayTitle = 'dddd, d <br /> MMM, yyyy';
            }
            $('#fullCalendarPlaceholder').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                editable: false,
                events: rootUrlLink + "events/getEventsAjax",
                loading: function (bool) {
                    if (bool) $('#loading').show();
                    else $('#loading').hide();
                },
                titleFormat: {
                    month: 'MMMM yyyy',
                    week: "d [ MMM] [ yyyy]{ '-'d MMM yyyy}",
                    day: 'dddd, d MMM, yyyy'
                },
                columnFormat: {
                    /*month: 'dddd',*/
                    week: 'ddd d/M',
                    day: 'dddd d/M'
                },
                contentHeight: 500
            });
        }
    });
</script>

<script src="<?php echo ROOT_URL_BASE;?>js/ui/jquery.ui.core.js"></script>
<script src="<?php echo ROOT_URL_BASE;?>js/ui/jquery.ui.widget.js"></script>
<script src="<?php echo ROOT_URL_BASE;?>js/ui/jquery.ui.datepicker.js"></script>
<link rel="stylesheet" href="<?php echo ROOT_URL_BASE;?>css/themes/base/jquery.ui.all.css">