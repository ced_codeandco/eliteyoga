<div class="container">
    <div class="content-row">
        <div class="clasfds-title"><h2 class="menuclick" rel="pageHeaderTopBanner">Categories</h2></div>
        <div class="clasfds-btn hidden-xs">
            <span class="post-link"><a class="<?php echo (!$this->is_logged_in) ? 'ajax-2' : '' ?>" href="<?php echo ROOT_URL;?>forum/create">New Thread</a>
            <div class="clearfix"></div>
            </span>
        </div>
        <div class="col-md-3 hidden-xs">
            <ul class="classifieds-list-left">
                <li><a class="<?php echo (empty($slug) ? 'active' : '');?>" href="<?php echo ROOT_URL.'forum';?>">All</a></li>
                <?php if (!empty($categories) && is_array($categories)) {
                    foreach ($categories as $cat) {
                        if (!empty($slug) && $slug == $cat['slug']){ $currentCateg =  $cat['name']; }?>
                        <li><a class=" <?php echo ((!empty($slug) && $slug == $cat['slug']) ? 'active' : '');?>" href="<?php echo ROOT_URL.'forum/category/'.$cat['slug']; ?>"><?php echo $cat['name'];?></a></li>
                    <?php }
                }?>
            </ul>
        </div>
        <div class="col-md-9">
            <div class="city-filter-classifieds">
                <span class="sub-title classified-keyword-title">Create New Thread</span>
                <div class="clearfix"></div> 
            </div>
            <?php if (isset($error)): ?>
                <div class="alert alert-error">
                    <a class="close" data-dismiss="alert" href="#">&times;</a>
                    <h4 class="alert-heading">Error!</h4>
                    <?php if (isset($error['title'])): ?>
                        <div>- <?php echo $error['title']; ?></div>
                    <?php endif; ?>
                    <?php if (isset($error['slug'])): ?>
                        <div>- <?php echo $error['slug']; ?></div>
                    <?php endif; ?>
                    <?php if (isset($error['category'])): ?>
                        <div>- <?php echo $error['category']; ?></div>
                    <?php endif; ?>
                    <?php if (isset($error['post'])): ?>
                        <div>- <?php echo $error['post']; ?></div>
                    <?php endif; ?>
                </div>
            <?php endif; ?>



        </div>
        <div class="col-md-9 forum-font pull-right">

            <div class="listcontainer forum create-thread-form-wrap">
                <form class="well" action="" method="post" style="margin: 5px 10px;">

                    <label>Title</label>
                    <input type="text" id="title" name="row[title]" class="span12" placeholder="">
                    <!--<label>Slug (url friendly)</label>-->
                    <input type="hidden" id="slug" name="row[slug]" class="span12" placeholder="">

                    <label>Category</label>
                    <select class="span12" name="row[category_id]">
                        <option value="0">-- none --</option>
                        <?php foreach ($categories as $cat): ?>
                            <option value="<?php echo $cat['id']; ?>"><?php echo $cat['name']; ?></option>
                        <?php endforeach; ?>
                    </select>
                    <link rel="stylesheet" href="<?php echo ROOT_URL_BASE?>resources/jquery/jwysiwyg/jquery.wysiwyg.css"/>
                    <script src="<?php echo ROOT_URL_BASE?>resources/jquery/jwysiwyg/jquery.wysiwyg.js" charset="utf-8"></script>
                    <script src="<?php echo ROOT_URL_BASE?>resources/jquery/jwysiwyg/controls/wysiwyg.link.js" charset="utf-8"></script>


                    <label>First Post</label>
                    <textarea name="row_post[post]" id="firstpost"  rows="8" class="span12"></textarea>
                    <label>Embed video</label>
                    <textarea class="embed_video" name="row_post[embed_video]" placeholder="Paste your code here"></textarea>
                    <input type="submit" name="btn-create" class="btn btn-primary btn-large" value="Create Thread"/>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    controlValue = {
        justifyLeft: { visible : false },
        justifyCenter: { visible : false },
        justifyRight: { visible : false },
        justifyFull: { visible : false },
        insertHorizontalRule: { visible: false },
        insertTable: { visible: false },
        insertImage: { visible: false },
        h1: { visible: false },
        h2: { visible: false },
        h3: { visible: false }
    };
    cssValue = {
        fontFamily: 'Verdana',
        fontSize: '13px'
    };
    $(document).ready(function(){
        $('#firstpost').wysiwyg({
            initialContent: '', html: '',
            controls: controlValue,
            css: cssValue,
            /*autoGrow: true*/
        });
    });
</script>
<script>
    $(function() {
        $('#title').change(function() {
            var title = $('#title').val().toLowerCase().replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-');
            $('#slug').val(title);
        });
    });
</script>