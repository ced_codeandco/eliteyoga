<div class="container forum-container-sp">
    <div class="content-row">
        <div class="clasfds-title"><h2 class="menuclick" rel="pageHeaderTopBanner">Categories</h2></div>
        <div class="clasfds-btn hidden-xs">
            <span class="post-link"><a class="<?php echo (!$this->is_logged_in) ? 'ajax-2' : '' ?>" href="<?php echo ROOT_URL;?>forum/create">New Thread</a>
            <div class="clearfix"></div>
            </span>
        </div>
        <div class="col-md-3 hidden-xs">
            <ul class="classifieds-list-left">
                <li><a class="<?php echo (empty($slug) ? 'active' : '');?>" href="<?php echo ROOT_URL.'forum';?>">All</a></li>
                <?php if (!empty($categories) && is_array($categories)) {
                    foreach ($categories as $cat) {
                        if (!empty($thread->category_id) && $thread->category_id == $cat['id']){ $currentCateg =  $cat['name']; }?>
                        <li><a class=" <?php echo ((!empty($thread->category_id) && $thread->category_id == $cat['id']) ? 'active' : '');?>" href="<?php echo ROOT_URL.'forum/category/'.$cat['slug']; ?>"><?php echo $cat['name'];?></a></li>
                    <?php }
                }?>
            </ul>
        </div>
        <div class="col-md-9">
            <div class="city-filter-classifieds">
                <span class="sub-title classified-keyword-title"><?php
                    echo !empty($currentCateg) ? $currentCateg : 'All';
                    
                    ?>
                </span>
				<div class="clearfix"></div>                
            </div>
            <?php echo !empty($thread->title) ? '<div class="title-wrap">'.$thread->title.'</div>' : '';?>
            
            <div class="clasfds-btn hidden-lg">
                    <span class="post-link more-marg-bottom"><a class="<?php echo (!$this->is_logged_in) ? 'ajax-2' : '' ?>" href="<?php echo ROOT_URL;?>forum/create">New Thread</a>
                    <div class="clearfix"></div>
                    </span>
                </div>
            <div class="clearfix"></div>
            <?php if (isset($tmp_success)): ?>
                <div class="alert alert-success">
                    <a class="close" data-dismiss="alert" href="#">&times;</a>
                    <h4 class="alert-heading">Reply posted!</h4>
                </div>
            <?php endif; ?>
            <?php if (isset($tmp_success_new)): ?>
                <div class="alert alert-success">
                    <a class="close" data-dismiss="alert" href="#">&times;</a>
                    <h4 class="alert-heading">New thread created!</h4>
                </div>
            <?php endif; ?>
            <?php
            if (!empty($posts) && is_array($posts)) {
                foreach ($posts as $post) {?>
                    <div class="listcontainer forum">
                    	<div class="forum-credit pull-left">
							<span class="user_name"><?php echo $post->username; ?></span>
                            <span class="user_date">Posted <?php echo time_ago($post->date_add); ?></span>
                        </div>					
                        <div class="forum-post-wrap pull-left">
                            <?php echo !empty($post->post) ? $post->post.'<br/><br/>' : ''; ?>
                            <?php echo !empty($post->embed_video) ? $post->embed_video.'<br/><br/>' : ''; ?>
                        </div>
                    <div class="clearfix"></div>
                    <!--<div class="forum-credit">posted by <?php echo $post->username; ?>, <?php echo time_ago($post->date_add); ?></div>-->
                    </div><?php
                }
            } else {?>
                <div class="listcontainer">
                    <div class="head-list-contner no-items">
                        <span class="list-title no-item-msg">No items found</span>
                    </div>
                </div>
            <?php
            }
            ?>
            <div class="paginator-botom-wrap"><?php
                if (!empty($page)) { ?>
                    <ul class="pagination"><?php echo $page; ?></ul><?php
                }?>
            </div>



        </div>
        <div class="col-md-9 forum-font pull-right">
            <?php if (!empty($this->user_id)) {?>
            <div class="reply-thread">
                <p>Reply to this Thread:</strong></p>
            </div>

            <div class="listcontainer forum-reply">
            <form class="well forum-controle-btns" action="" method="post" style="margin: 5px 10px;">
                <input type="hidden" name="row[thread_id]" value="<?php echo $thread->id; ?>"/>
                <input type="hidden" name="row[reply_to_id]" value="0"/>
                <input type="hidden" name="row[author_id]" value="<?php echo $this->session->userdata('forumUserid'); ?>"/>
                <input type="hidden" name="row[reply_to_id]" value="<?php echo !empty($post->id) ? $post->id : 0; ?>"/>
                <input type="hidden" name="row[date_add]" value="<?php echo date('Y-m-d H:i:s'); ?>"/>
                <textarea name="row[post]" class="span12" id="textpost" style="height:150px;"></textarea>
                <div class="embed-label">Embed video</div>
                <textarea class="embed_video" name="row[embed_video]" placeholder="Paste your embed video code here"></textarea>
                <input type="submit" name="btn-post" class="forum-post-reply" value="Post Reply"/>
            </form>
            </div>
            <?php } else {?>
                <span class="request-now-btn post-comment hidden-xs"><a href="javascript:void(0)" class="ajax-2 cboxElement">Post Reply</a></span>
            <?php } ?>
        </div>
    </div>
    <?php if (empty($this->user_id)) {?><span id="post_item" class="post-link post-comment hidden-lg larger"><a href="javascript:void(0)" class="ajax-2 cboxElement">Post Reply</a></span><?php } ?>
</div>

<link rel="stylesheet" href="<?php echo ROOT_URL_BASE;?>resources/jquery/jwysiwyg/jquery.wysiwyg.css"/>
<script src="<?php echo ROOT_URL_BASE;?>resources/jquery/jwysiwyg/jquery.wysiwyg.js" charset="utf-8"></script>
<script src="<?php echo ROOT_URL_BASE;?>resources/jquery/jwysiwyg/controls/wysiwyg.link.js" charset="utf-8"></script>

<script>
    controlValue = {
        justifyLeft: { visible : false },
        justifyCenter: { visible : false },
        justifyRight: { visible : false },
        justifyFull: { visible : false },
        insertHorizontalRule: { visible: false },
        insertTable: { visible: false },
        insertImage: { visible: false },
        h1: { visible: false },
        h2: { visible: false },
        h3: { visible: false }
    };
    cssValue = {
        fontFamily: 'Verdana',
        fontSize: '13px'
    };
    $(document).ready(function(){
        $('#textpost').wysiwyg({
            initialContent: '', html: '',
            controls: controlValue,
            css: cssValue,
            /*autoGrow: true*/
        });

        $('.textpostreply').wysiwyg({
            initialContent: '', html: '',
            controls: controlValue,
            css: cssValue
        });
    });
</script>
