<div class="container forum">
    <div class="content-row">        
        <div class="clasfds-title"><h2 class="menuclick" rel="pageHeaderTopBanner">Categories</h2></div>
        <div class="clasfds-btn hidden-xs">
            <span class="post-link"><a class="<?php echo (!$this->is_logged_in) ? 'ajax-2' : '' ?>" href="<?php echo ROOT_URL;?>forum/create">New Thread</a>
            <div class="clearfix"></div>
            </span>
        </div>
        <div class="col-md-3 hidden-xs">
            <ul class="classifieds-list-left">
                <li><a class="<?php echo (empty($slug) ? 'active' : '');?>" href="<?php echo ROOT_URL.'forum';?>">All</a></li>
                <?php if (!empty($categories) && is_array($categories)) {
                    foreach ($categories as $cat) {
                        if (!empty($slug) && $slug == $cat['slug']){ $currentCateg =  $cat['name']; }?>
                        <li><a class=" <?php echo ((!empty($slug) && $slug == $cat['slug']) ? 'active' : '');?>" href="<?php echo ROOT_URL.'forum/category/'.$cat['slug']; ?>"><?php echo $cat['name'];?></a></li>
                    <?php }
                }?>
            </ul>
        </div>
        <div class="col-md-9">
            <div class="city-filter-classifieds">
                <span class="sub-title classified-keyword-title"><?php
                    echo !empty($currentCateg) ? $currentCateg : 'All';
                    ?>
                </span>
				<div class="clearfix"></div>                
            </div>
            <div class="clasfds-btn hidden-lg">
                    <span class="post-link more-marg-bottom"><a class="<?php echo (!$this->is_logged_in) ? 'ajax-2' : '' ?>" href="<?php echo ROOT_URL;?>forum/create">New Thread</a>
                    <div class="clearfix"></div>
                    </span>
                </div>
            <div class="clearfix"></div>
            <?php
            if (!empty($threads) && is_array($threads)) {
                foreach ($threads as $thread) {?>
                    <div class="listcontainer forum forum-font">
                        <div class="thread-list-left-wrap">
                            <span class="list-title"><a class="classified-title-link forum-font" href="<?php echo ROOT_URL.'forum/talk/' . $thread->slug; ?>"><?php echo ucwords(strtolower($thread->title)); ?></a></span>
                            <br />
                            <span class="cat" style="display: block">
                                Category: <a href="<?php echo ROOT_URL.'forum/category/' . $thread->category_slug; ?>" class="cat"><?php echo $thread->category_name; ?></a>
                        </span>
                        </div>
                        <div class="forum-time-view">
                            <span class="post-date"><?php echo time_ago($thread->date_add); ?></span>
                            <span class="views"><?php echo $thread->viewcount.' Views';?></span>
                            <span class="replies"><?php echo $thread->replyCount.' Replies';?></span>
                        </div>
                        <!--<div class="forum-lisheader-wrap">
                            <div class="head-list-contner forum">
                                <span class="list-title"><a class="classified-title-link forum-font" href="<?php /*echo ROOT_URL.'forum/talk/' . $thread->slug; */?>"><?php /*echo ucwords(strtolower($thread->title)); */?></a></span>
                            </div>
                        </div>
                        <div class="pull-right forum-font" style="font-size:12px;color:#999;vertical-align: middle;">
                            <?php /*echo time_ago($thread->date_add); */?>
                            <br />
                            <?php /*echo $thread->viewcount.' Views';*/?>
                            <?php /*echo $thread->replyCount.' Replies';*/?>
                        </div>

                        <span class="cat" style="display: block">
                                Category: <a href="<?php /*echo ROOT_URL.'forum/category/' . $thread->category_slug; */?>" class="cat"><?php /*echo $thread->category_name; */?></a>
                        </span>-->

                    </div><?php
                }
            } else {?>
                <div class="listcontainer">
                    <div class="head-list-contner no-items">
                        <span class="list-title no-item-msg">No items found</span>
                    </div>
                </div>
            <?php
            }
            ?>
            <div class="paginator-botom-wrap hidden-xs"><?php
                if (!empty($page)) { ?>
                    <ul class="pagination"><?php echo $page; ?></ul><?php
                }?>
            </div>
        </div>
    </div>
    <div class="paginator-botom-wrap hidden-lg">
            	<?php if (!empty($page)) { ?>
                    <ul class="pagination"><?php echo $page; ?></ul><?php
                }?>
            	<div class="clearfix"></div>
            </div>            
            <span id="post_item" class="post-link hidden-lg larger"><a class="<?php echo (!$this->is_logged_in) ? 'ajax-2' : '' ?>" href="<?php echo ROOT_URL;?>create_classified">Post An Item</a></span>
</div>
