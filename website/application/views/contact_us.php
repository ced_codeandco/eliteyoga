<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 5/7/2015
 * Time: 9:55 AM
 */?>

<div class="left_area left-are-full contact_us_div">
  <h1 class="heading_h1"> <span><?php echo !empty($cmsData->title) ? $cmsData->title : '';?><?php //echo showEditCmsPage($cmsData, 'top0px');?></span> </h1>
  <div class="product_category_text contact-bg">
    <div class="contact-col-1"><?php echo showEditCmsPage($cmsData, 'top0px');?> <?php echo !empty($cmsData->description) ? $cmsData->description : '';?> </div>
    <div class="contact-col-1 left-margin-45px">
      <h2>Get in Touch</h2>
      <div class="contact-form">
        <div id="abhi">
          <?php
    if(isset($errMsg) && $errMsg != ''){
        echo '<div class="alert alert-danger">' . $errMsg. '</div>';
        unset($errMsg);
    }
    if(isset($succMsg) && $succMsg != ''){
        echo '<div class="alert alert-success">' . $succMsg . '</div>';
        unset($succMsg);
    }
    echo validation_errors();?>
         
          <form id="enquiryForm" name="formID" method="post" action="">
            <input type="text" required name="name" placeholder="Name" id="Name" class="validate[required] name-icon" />
            <input type="email" required name="email" placeholder="Email" id="Email" class="validate[required,custom[email]] email-icon" />
            <input type="text" required name="contact_no" placeholder="Phone" id="Phone" class="validate[required,custom[phone]] phone-icon" />
            <textarea name="comments" placeholder="Message" id="comments" class="massage-icon"></textarea>
            <div class="col-md-12  pull-left g-recaptcha-contact"><div class="g-recaptcha" id="rcaptcha" data-sitekey="<?php echo RECAPTCHA_SITE_KEY;?>" style="transform:scale(0.83);transform-origin:0;-webkit-transform:scale(0.83);transform:scale(0.83);-webkit-transform-origin:0 0;transform-origin:0 0; 0"></div>
            
            <span id="captcha" class="alert alert-danger hidden captcha-alert"></span></div>
            <div class="clearfix"></div>
            <input  value="Submit" type="submit"  class="submit contact-form">
          </form>
        </div>
      </div>
    </div>
    <div class="certifietsbox"><img src="<?php echo !empty($cmsData->cms_banner_image) ? DIR_UPLOAD_BANNER_SHOW.''.$cmsData->cms_banner_image : '';?>"></div>
  </div>
</div>
<script src='https://www.google.com/recaptcha/api.js?hl=en'></script> 
<script>
    $(document).ready(function(){
        $('#showPhoneNumber').click(function(){
            console.log($('#showPhoneNumber').parent().find('span').length);
            $(this).find('span').toggle();
        })
        $('#enquiryForm').submit(function(){
            var v = grecaptcha.getResponse();
            if(v.length == 0)
            {
                $('#captcha').html("Please verify you are not a robot").show();
                return false;
            } else {
                $('#captcha').hide();
            }
        })
    })
</script>