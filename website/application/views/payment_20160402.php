<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>Elite Yoga</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="<?php echo ROOT_URL_BASE.'../';?>images/favicon.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="<?php echo ROOT_URL_BASE.'../';?>fonts/fonts.css" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo ROOT_URL_BASE.'../';?>css/animate.css">
    <link rel="stylesheet" href="<?php echo ROOT_URL_BASE.'../';?>css/remodal.css">
    <link rel="stylesheet" href="<?php echo ROOT_URL_BASE.'../';?>css/remodal-default-theme.css">
    <link rel="stylesheet" href="<?php echo ROOT_URL_BASE.'../';?>css/slider-pro.min.css">
    <link href="<?php echo ROOT_URL_BASE.'../';?>css/style.css" type="text/css" rel="stylesheet" />
    <link href="<?php echo ROOT_URL_BASE.'../';?>css/hover.css" type="text/css" rel="stylesheet" />
    <link href="<?php echo ROOT_URL_BASE.'../';?>slider/jquery.bxslider.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo ROOT_URL_BASE.'../';?>css/responsive.css" type="text/css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo ROOT_URL_BASE.'../';?>js/wow.min.js"></script>
    <script src="<?php echo ROOT_URL_BASE.'../';?>js/parallax.js"></script>
    <script src="<?php echo ROOT_URL_BASE.'../';?>js/remodal.min.js"></script>
    <script src="<?php echo ROOT_URL_BASE.'../';?>js/jquery.sliderPro.min.js"></script>
    <script src="<?php echo ROOT_URL_BASE.'../';?>js/custom-packages.js"></script>
    <script>
        wow = new WOW();
        wow.init();
    </script>
    <script type="text/javascript" src="<?php echo ROOT_URL_BASE.'../';?>slider/jquery.bxslider.min.js"></script>
    <script src="<?php echo ROOT_URL_BASE.'../';?>js/common.js"></script>
    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-56c87bb000d5c2f2"></script>

</head>

<body>
<input type="hidden" id="rootUrlLink" value="<?php echo ROOT_URL;?>"/>
<?php include_once(ROOT_DIRECTORY.'../header.php');?>
<section class="main-section ">
    <header>
        <h2>Payment</h2>
        <?php if (!empty($payment_status) && $payment_status =='failed'){?>
            <p>Please retry to make the payment</p>
        <?php } else if (!empty($booking_details->payment_processed) && $booking_details->payment_processed == 1){?>
            <p>Your payment processed successfully.</p>
        <?php } else {?>
            <p>Please make the payment to confirm your booking.</p>
        <?php }?>
    </header>
    <div class="container resort-packages package-confirmation<?php echo !empty($err_msg) ? ' trans-failed' : ''; echo (!empty($booking_details->payment_processed) && $booking_details->payment_processed == 1) ? ' trans-success' : '';?>">
        <div class="row no-marg-bottom">
            <?php echo !empty($err_msg) ? '<div class="alert alert-danger">'.$err_msg.' <span class="close"><i class="fa fa-times"></i></span></div>' : '';?>
            <?php if (!empty($booking_details->payment_processed) && $booking_details->payment_processed == 1){?>
                    <div class="alert alert-success">We will get back to you once the payment details are confirmed. <span class="close"><i class="fa fa-times"></i></span></div></p></div>
             <?php }
				if (!empty($booking_details)) {?>

				<!--Original
                <div class="Package">
                    <p>Package: <span><?php echo $package->title;?></span></p>
                    <p>Retreat Date: <span><?php echo date('d M Y ', strtotime($booking_details->start_date));?> to <?php echo date('d M Y ', strtotime($booking_details->end_date));?></span></p>
                    <p>Price Approved: <span><?php
					echo !empty($booking_details->price_quoted) ? '$' . $booking_details->price_quoted : '';
					?></span></p>
                </div>
                End Original!-->
                
                <h3>Package Details</h3>
                <div class="Package new-package-list">
                    <p><span class="detail-title"><i class="fa fa-cube"></i> Package Name:</span> <span><?php echo $package->title;?></span></p>
                    <p><span class="detail-title"><i class="fa fa-calendar-check-o"></i> Retreat Date:</span> <span><?php echo date('d M Y ', strtotime($booking_details->start_date));?> to <?php echo date('d M Y ', strtotime($booking_details->end_date));?></span></p>
                    <p><span class="detail-title"><i class="fa fa-tag"></i> Price Approved:</span> <span><?php
					echo !empty($booking_details->price_quoted) ? '$' . $booking_details->price_quoted : '';
					?></span></p>
                </div>
                
                
                <?php
                if (empty($booking_details->is_accepted) OR $booking_details->is_accepted != '1'){?>
                    <div class="book_now_wrap">
                        <div class="alert alert-danger">Sorry! Your booking is not approved yet.</div>
                    </div>
                <?php } else if (empty($booking_details->payment_processed) OR $booking_details->payment_processed != '1'){?>
                <div class="book_now_wrap new-package-button-wrap">
                    <?php if (!empty($payment_status) && $payment_status =='failed'){?>
                    <a href="javascript:void(0);" id="pay_now_btn" class="btn btn-booknow book_package_btn new-package-button" package_id="2">RETRY NOW</a>
                    <?php } else {?>
                    <a href="javascript:void(0);" id="pay_now_btn" class="btn btn-booknow book_package_btn new-package-button" package_id="2">PAY NOW</a>
                    <?php }?>
                    <img class="payment_processing" style="display: none;" src="<?php echo FRONTEND_URL;?>images/ajax-loader.gif" alt=""/>
                </div>
                <?php } ?>
                    <form id="paypal_form" action="<?php echo (defined('PAYPAL_IS_LIVE') && PAYPAL_IS_LIVE == 1) ? PAYPAL_PAYMENT_URL_LIVE : PAYPAL_PAYMENT_URL_SANDBOX;?>" method="post">
                    
					<input type="hidden" name="action" value="capture" />
					<input type="hidden" name="gatewayId" value="<?php echo NEXXUS_GATEWAY_ID; ?>" /> 
					<input type="hidden" name="secretKey" value="<?php echo NEXXUS_SECRET_KEY; ?>" /> 
					<input type="hidden" name="referenceId" value="<?php echo ($booking_details->id); ?>" />
					<input type="hidden" name="amount" value="<?php echo !empty($booking_details->price_quoted) ? $booking_details->price_quoted : $booking_details->price_quoted;?>" /> 
					<input type="hidden" name="mode" value="<?php echo (defined('PAYPAL_IS_LIVE') && PAYPAL_IS_LIVE == 1) ? LIVE : TEST;?>" />
					<input type="hidden" style="width:500px" name="description" value="Description" /> 
					<input type="hidden" style="width:500px" name="returnUrl" value="<?php echo ROOT_URL.'success/'.$booking_details->id;?>" />
					<input type="hidden" name="name" value="<?php echo $booking_details->name; ?>" />
					<input type="hidden" style="width:500px" name="address" value="CUSTOMER ADDRESS" /> 
					<input type="hidden" name="city" value="CUSTOMER CITY" />
					<input type="hidden" name="state" value="CUSTOMER STATE/PROVINCE" />
					<input type="hidden" name="country" value="US" />
					<input type="hidden" name="phone" value="<?php echo (!empty($booking_details->contact_no) ? $booking_details->contact_no : '0000000000'); ?>" />
					<input type="hidden" name="email" value="<?php echo (!empty($booking_details->email) ? $booking_details->email : ERROR_REPORTING_EMAIL); ?>" />
					<input type="hidden" name="currency" value="USD" />
	
                    <!--input type="hidden" name="business" value="<!--?php echo PAYPAL_PAYMENT_EMAIL;?>"-->

                    <!--input style="display:none" type="image" src="http://www.paypal.com/en_US/i/btn/x-click-but01.gif" name="submit" alt="Make payments with PayPal - it's fast, free and secure!"-->
                </form>
            <?php } else {?>
                <div class="booking_not_found">
                    <h3>Sorry!</h3>
                    <p>The booking details are not found</p>
                    <a href="<?php echo ROOT_URL.'packages/';?>" id="pay_now_btn" class="btn btn-booknow book_package_btn" package_id="2">BACK TO PACKAGES</a>
                </div>
            <?php } ?>

        </div>

    </div>

</section>
<?php include_once(ROOT_DIRECTORY.'../our_partners.php');?>
<?php include_once(ROOT_DIRECTORY.'../footer.php');?>
<script type="text/javascript">
    $(document).ready(function(){
        $('#pay_now_btn').click(function(){
            $('#payment_processing').show();
            $('#paypal_form').submit();
        })
    })
</script>
</body>

</html>
