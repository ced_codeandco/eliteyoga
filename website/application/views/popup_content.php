<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 11/12/2015
 * Time: 1:21 PM
 */?>
<div class="pop_up_content_main">
<h1><?php echo $dataContent->title;?></h1>
<?php //print_r($dataContent);?>
<div class="video_div">
    <input type="hidden" class="hidden-video-fields" value="<?php echo prep_url($dataContent->video_link);?>" />
</div>
<?php echo stripslashes($dataContent->description);?>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $(".pop_up_content_main").mCustomScrollbar({

        live:true,

        theme:"inset-dark"

    });
})
</script>