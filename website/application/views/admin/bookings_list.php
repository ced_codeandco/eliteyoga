<div id="content" class="col-lg-10 col-sm-10">
    <div>
        <ul class="breadcrumb">
            <li> <a href="<?php echo ADMIN_ROOT_URL?>">Home</a> </li>
            <li> <a href="#">Booking Requests List</a> </li>
        </ul>
    </div>
    <div class="row">
        <div class="box-content">
            <div class="box col-md-12">
                <div class="box-inner">
                    <div class="box-header well" data-original-title="">
                        <h2><i class="glyphicon glyphicon-user"></i> Booking Requests</h2>
                        <div class="box-icon"> <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a> <a href="#" class="btn btn-minimize btn-round btn-default"><i
                                    class="glyphicon glyphicon-chevron-up"></i></a> <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a> </div>
                    </div>
                    <div class="box-content">
                        <?php if(isset($successMsg) && $successMsg != ''){?>
                            <div class="alert alert-success">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $successMsg; unset($successMsg);?></div>
                        <?php } ?>
                        <?php if(isset($errMsg) && $errMsg != ''){?>
                            <div class="alert alert-danger">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $errMsg; unset($errMsg);?></div>
                        <?php } ?>
                        <table class="table table-striped table-bordered bootstrap-datatable datatable responsive" id="datatable_list">
                            <thead>
                            <tr>

                                <th width="5%">Order</th>
                                <th width="10%">Name</th>
                                <th width="10%">Email</th>
                                <th width="10%">Telephone</th>
                                <!--<th width="30%">Message</th>-->
                                <th width="10%">Time</th>
                                <th width="10%">Status</th>
                                <th width="15%" style="text-align:center">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i = 0;
                            if($contactsList && count($contactsList) > 0 ){
                                $paOrder =1;

                                foreach ($contactsList as $bookings){  ?>
                                    <tr>

                                        <td><?php echo $paOrder; ?> </td>
                                        <td><?php echo $bookings->name;?></td>
                                        <td><?php echo $bookings->email;?></td>
                                        <td><?php echo $bookings->telephone;?></td>
                                        <td><?php echo $bookings->created_date_time;?></td>
                                        <!--<td><?php /*echo $bookings->created_ip;*/?></td>-->
                                        <td><?php
                                            if($bookings->payment_processed == 1) {?>
                                                <span id="status_label_<?php echo $bookings->id?>" class="label-success label label-default">Payment Processed</span>
                                            <?php }else if($bookings->is_accepted == 1) {?>
                                                <span id="status_label_<?php echo $bookings->id?>" class="label-success label label-default">Approved</span>
                                            <?php }else{?>
                                                <span id="status_label_<?php echo $bookings->id?>" class="label-default label label-danger" >Not Approved</span>
                                            <?php }	?>
                                        </td>
                                        <td class="t-center">
                                            <textarea id="message_content_<?php echo $bookings->id?>"  style="display: none;"><?php echo ($bookings->comments); ?></textarea>
                                            <textarea id="reply_content_<?php echo $bookings->id?>"  style="display: none;"><?php echo ($bookings->reply_message); ?></textarea>
                                            <a href="#" onclick="addInquiryReply('<?php echo $bookings->id?>', '<?php echo $bookings->name; ?>',
                                                '<?php echo $bookings->email; ?>',
                                                '<?php echo $bookings->telephone; ?>',
                                                '<?php echo $bookings->start_date; ?>',
                                                '<?php echo $bookings->end_date; ?>',
                                                '<?php echo $bookings->price_dollar_selected; ?>',
                                                '<?php echo $bookings->price_euro_selected; ?>',
                                                '<?php echo $bookings->package_id; ?>',
                                                '<?php echo $bookings->retreat_id; ?>',
                                                '<?php echo $bookings->is_accepted; ?>',
                                                '<?php echo $bookings->price_quoted; ?>',
                                                '<?php echo date('d M Y ', strtotime($bookings->start_date));?> to <?php echo date('d M Y ', strtotime($bookings->end_date));?>',
                                                '<?php echo $bookings->package_details->title;?>',
                                                '<?php echo $bookings->payment_processed;?>')" class="btn btn-info"> <i class="glyphicon glyphicon-edit icon-white"></i> Details</a>

                                            <a class="btn btn-danger" href="#" onclick="javascript:if(confirm('Are you sure to delete ? ')){location.href='<?php echo ADMIN_ROOT_URL?>bookings/delete/<?php echo $bookings->id?>'}"> <i class="glyphicon glyphicon-trash icon-white"></i> Delete </a>
                                        </td>
                                    </tr>

                                    <?php $paOrder++; }

                            } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">

    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h3>Booking Details<span id="inquirerName"></span></h3>
            </div>
            <div class="box-content">
                <div class="modal-body">
                    <div id="booking_details_wrap">

                    </div>
                </div>
            </div>

            <form name="inquiryReply" method="post" id="inquiryReply">
                <div class="box-content">
                    <h3>Approve Booking</h3>
                    <div class="modal-body">
                        <input type="hidden" id="booking_id" name="booking_id" value="" />
                        <input type="hidden" id="package_id" name="package_id" value="" />
                        <input type="hidden" id="retreat_id" name="retreat_id" value="" />
                        <input type="hidden" id="is_accepted" name="is_accepted" value="1" />
                        <input type="hidden" id="replied_member_id" name="replied_member_id" value="<?php echo '1';?>" />

                        <div class="form-group input-group" id="small_description_msg_error">
                            <textarea class="form-control" rows="7" cols="79"  name="reply_message"  id="reply_message" placeholder="Please enter reply message here..."></textarea>
                            <div class="clearfix"></div><br />
                            <div class="price_quoted">Final amount to pay($): <input type="text" name="price_quoted" id="price_quoted"></div>
                            <div class="payment_link">
                                <br />
                                Please click on the link below to make payment.
                                <br /><a href="" target="_blank"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="#"  class="btn btn-default close_model_box" data-dismiss="modal">Close</a>
                    <a href="#" id="send_approve_btn" class="btn btn-primary" onclick="return sendInquiryReply();">Approve</a>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">

    function addInquiryReply(booking_id, name, email, telephone, start_date, end_date, price_dollar_selected, price_euro_selected, package_id, retreat_id, is_accepted, price_quoted, date_text, package_name, payment_processed){
        var default_message = '';
        default_message += 'We accept your booking for the retreat from '+date_text+' under the package "'+package_name+'"' +"\n";

        var url = '<?php echo ROOT_URL;?>payment/'+booking_id+'/'+package_id+'/'+retreat_id;
        $('#booking_id').val(booking_id);
        $('#package_id').val(package_id);
        $('#retreat_id').val(retreat_id);
        $('#booking_details_wrap').html($('#message_content_'+booking_id).val());
        var reply_message = $('#reply_content_'+booking_id).val();
        //$('#is_accepted').val(is_accepted);



        $('#reply_message').val(default_message);
        $('.payment_link').find('a').attr('href', url);
        $('.payment_link').find('a').html(url);
        is_accepted = Number(is_accepted);
        payment_processed = Number(payment_processed);
        if (is_accepted == 1) {
            $('#price_quoted').val(price_quoted);
            $('#reply_message').val(reply_message);
        } else {
            $('#price_quoted').val(price_dollar_selected);
            $('#reply_message').val(default_message);
        }
        if (payment_processed == 1) {
            $('#send_approve_btn').hide();
        } else if (is_accepted == 1) {
            $('#send_approve_btn').html('Update').show();
        } else {
            $('#send_approve_btn').html('Approve').show();
        }
        $('#myModal').modal('show');
    }
    function sendInquiryReply(){

        var formdata = $( "#inquiryReply" ).serialize();
        //formdata = formdata + '&message_body='+encodeURIComponent($('#message_body').html());
        var booking_id = $('#booking_id').val();
        $('#status_label_' + booking_id).removeClass('label-danger').addClass('label-success').html('Approved');
        $('#reply_content_'+booking_id).val($('#reply_message').val());
        $.ajax({
            url: '<?php echo ADMIN_ROOT_URL?>bookings/booking_reply',  //server script to process data
            type: 'POST',
            //Ajax events
            success: function(data){

                if(data == 'error'){
                    alert('Email Not sent!! Please try again later...');
                    return false;
                }else{
                    var id = $('#inquiryId').val();
                    $('#emailText').html('Reply email sent successfully');
                    $('#td_replied_'+id).html('Yes');
                    $('#emailSuccess').show();
                    $('.close_model_box').click();
                }
            },
            error: function(data){$('#abhi').html(data);},
            // Form data
            data: formdata
        });

        return;

    }
</script>