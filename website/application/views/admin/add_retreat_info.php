<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>assets/ckfinder/ckfinder.js"></script>
<div id="content" class="col-lg-10 col-sm-10">
    <!-- content starts -->
    <div>
        <ul class="breadcrumb">
            <li> <a href="<?php echo ADMIN_ROOT_URL?>">Home</a> </li>
            <li> <a href="#"><?php echo $title;?></a> </li>
        </ul>
    </div>
    <div class="row">
        <div class="box-content">
            <div class="box col-md-12">
                <div class="box-inner">
                    <div class="box-header well" data-original-title="">
                        <h2><i class="glyphicon glyphicon-list-alt"></i> <?php echo $title;?></h2>
                        <div class="box-icon"> <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a> <a href="#" class="btn btn-minimize btn-round btn-default"><i
                                    class="glyphicon glyphicon-chevron-up"></i></a> <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a> </div>
                    </div>
                    <div class="box-content"> <?php echo validation_errors(); ?>
                        <?php
                        $editUrl = '';
                        if($action == 'Edit'){
                            $editUrl = '/'.$categoryDetails->id;
                        }

                        $attributes = array('name' => 'categoryForm', 'id' => 'categoryForm', 'enctype' => 'multipart/form-data', 'role'=>'form', 'onsubmit'=>'return validate_category();');
                        echo form_open('',$attributes); ?>
                        <input type="hidden" name="id" id="id" value="<?php echo (isset($categoryDetails->id)) ? $categoryDetails->id : 0;?>" />
                        <input type="hidden" name="current_parent_id" id="current_parent_id" value="<?php if(isset($_SESSION['parent_id'])) { echo $_SESSION['parent_id']; unset($_SESSION['parent_id']); }else { echo (isset($categoryDetails->parent_id)) ? $categoryDetails->parent_id : 0; }?>" />
                        <input type="hidden" name="action" id="action" value="<?php echo $action?>" />
                        <div class="control-group">
                            <label class="control-label" for="selectError">Packages</label>
                            <div class="controls">
                                <select id="parent_id" name="parent_id" data-rel="chosen" required>
                                    <option value="0" selected="selected">Select Package</option>
                                    <?php
                                    if (!empty($packagesList) && is_array($packagesList)) {
                                        foreach ($packagesList as $package) { ?>
                                            <option
                                            value="<?php echo $package->id?>" <?php echo (!empty($categoryDetails->parent_id) && $categoryDetails->parent_id == $package->id) ? "selected='selected'" : "" ?> ><?php echo $package->title?></option><?php
                                        }
                                    }?>
                                </select>
                                <div id="parent_id_msg_error">
                                    <label class="control-label" id="parent_id_msg"></label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group input-group col-md-4" id="title_msg_error">
                            <label class="control-label" for="title">Title<span class="required">*</span></label>
                            <input type="text" class="form-control" maxlength="255" name="title" value="<?php if(isset($_SESSION['title']) && $_SESSION['title'] != '') { echo $_SESSION['title']; unset($_SESSION['title']);}else { echo (isset($categoryDetails->title)) ? $categoryDetails->title : ''; }?>" id="title" placeholder="Enter Title">
                            <br />
                            <label class="control-label" id="title_msg"></label>
                        </div>
                        <div class="form-group input-group col-md-4" id="start_date_msg_error">
                            <label class="control-label" for="title">Date</label>
                            <input type="text" class="form-control" maxlength="255" name="start_date" value="<?php if(isset($_SESSION['start_date']) && $_SESSION['start_date'] != '') { echo $_SESSION['start_date']; unset($_SESSION['start_date']);}else { echo (isset($categoryDetails->start_date)) ? $categoryDetails->start_date : ''; }?>" id="start_date" placeholder="Start Date">
                            <input type="text" class="form-control" maxlength="255" name="end_date" value="<?php if(isset($_SESSION['end_date']) && $_SESSION['end_date'] != '') { echo $_SESSION['end_date']; unset($_SESSION['end_date']);}else { echo (isset($categoryDetails->end_date)) ? $categoryDetails->end_date : ''; }?>" id="end_date" placeholder="End Date">
                            <br />
                            <label class="control-label" id="start_date_msg"></label>
                        </div>

                        <div class="option-tile-wrap">
                            <div class="form-group input-group col-md-4" id="bullet_description_msg_error">
                                <label class="control-label" for="bullet_description">Bullet points on listing page<span class="required">*</span></label>
                                <?php if(isset($_SESSION['bullet_description']) && $_SESSION['bullet_description'] != '') {
                                    $bullet_description = $_SESSION['bullet_description']; unset($_SESSION['bullet_description']);
                                } else {
                                    $bullet_description = (isset($productDetails->bullet_description)) ? $productDetails->bullet_description : '';
                                }
                                $bullet_description = !empty($bullet_description) ? unserialize($bullet_description) : array();
                                ?>
                                <?php if (!empty($bullet_description) && is_array($bullet_description)) {
                                    foreach ($bullet_description as $bullet) {
                                        if (!empty($bullet)) { ?>
                                            <input type="text" class="form-control" maxlength="255"
                                                   name="bullet_description[]" value="<?php echo $bullet; ?>"
                                                   id="bullet_description" placeholder="Enter Points">
                                        <?php }
                                    }
                                } else {?>
                                    <input type="text" class="form-control" maxlength="255"
                                           name="bullet_description[]" value=""
                                           id="bullet_description" placeholder="Enter Points">
                                <?php
                                }?>
                                <input type="button" value="+" onclick="add_more_bullets(this);" title="Add more points">
                                <br />
                                <label class="control-label" id="bullet_description_msg"></label>
                            </div>
                        </div>


                        <div class="control-group">
                            <label class="control-label" for="selectError">Is Active</label>
                            <div class="controls">
                                <select id="is_active" name="is_active" data-rel="chosen">
                                    <option value="1" <?php if(isset($_SESSION['is_active']) && $_SESSION['is_active'] == 1) { echo 'selected="selected"'; unset($_SESSION['is_active']); }else { echo (isset($categoryDetails->is_active) && $categoryDetails->is_active == 1) ? 'selected="selected"' : ''; }?> >Active</option>
                                    <option value="0" <?php if(isset($_SESSION['is_active']) && $_SESSION['is_active'] == 0) { echo 'selected="selected"'; unset($_SESSION['is_active']); }else { echo (isset($categoryDetails->is_active) && $categoryDetails->is_active == 0) ? 'selected="selected"' : ''; }?> >Inactive</option>
                                </select>
                            </div>
                        </div>


                        <br />
                        <button type="submit" class="btn btn-success btn-sm">Submit</button>
                        <?php echo form_close(); ?> </div>
                </div>
            </div>
        </div>
    </div>
    <script language="javascript" type="text/javascript">
        $(document).ready(function() {
            manageFilterControl();
            manageSortControl();
            /*$.ajax({
             type: "POST",
             url: "<?php echo ADMIN_ROOT_URL?>category/get_parent?id="+$("#id").val()+"&current_parent_id="+$("#current_parent_id").val(),
             success: function(result){
             $('#parent_id').val(result);
             },
             complete: function(){

             },
             error: function(){
             $('#parent_id').val('Error occured. Please try later');
             }

             });*/
        });
        function validate_category(){
            if($("#title").val()==''){
                $("#title_msg").html('Please enter Category title');
                $("#title_msg_error").addClass('has-error');
                $("#title").focus();
                return false;
            }else{
                $("#title_msg").html('');
                $("#title_msg_error").removeClass('has-error');
            }
        }
        function manageFilterControl(){
            if ($('#nofiltercontrol').is(':checked')) {
                $('.filtercontrolChecks').removeAttr('checked');
                $('.filtercontrolChecks').attr('disabled', 'disabled');
            } else {
                $('.filtercontrolChecks').removeAttr('disabled');
            }
        }
        function manageSortControl(){
            if ($('#nosortcontrol').is(':checked')) {
                $('.sortcontrolChecks').removeAttr('checked');
                $('.sortcontrolChecks').attr('disabled', 'disabled');
            } else {
                $('.sortcontrolChecks').removeAttr('disabled');
            }
        }

    </script>
