<div id="content" class="col-lg-10 col-sm-10">
<div>
  <ul class="breadcrumb">
    <li> <a href="<?php echo ADMIN_ROOT_URL?>">Home</a> </li>
    <li> <a href="#">Banner List</a> </li>
  </ul>
</div>
<div class="row">
  <div class="box-content">
    <div class="box col-md-12">
      <div class="box-inner">
        <div class="box-header well" data-original-title="">
          <h2><i class=" glyphicon glyphicon-picture"></i> Banner List</h2>
            <a style="float:right" href="<?php echo ADMIN_ROOT_URL;?>banner/add?package=<?php echo !empty($_GET['package']) ? $_GET['package'] : 0;?>"><i class="glyphicon glyphicon-cog"></i> Add Banner</a>
        </div>
        <div class="box-content">
          <?php if(isset($successMsg) && $successMsg != ''){?>
          <div class="alert alert-success">
            <button data-dismiss="alert" class="close" type="button">×</button>
            <?php echo $successMsg; unset($successMsg);?></div>
          <?php } ?>
          <?php if(isset($errMsg) && $errMsg != ''){?>
          <div class="alert alert-danger">
            <button data-dismiss="alert" class="close" type="button">×</button>
            <?php echo $errMsg; unset($errMsg);?></div>
          <?php } ?>

            <div class="control-group">
                <label class="control-label" for="selectError">Retreat</label>
                <div class="controls">
                    <select data-rel="chosen" onchange="window.location='<?php echo ADMIN_ROOT_URL;?>banner?package='+this.value;">
                        <option value="0" selected="selected">Select Retreat</option>
                        <?php
                        if (!empty($packagesList) && is_array($packagesList)) {
                            foreach ($packagesList as $package) { ?>
                                <option
                                value="<?php echo $package->id?>" <?php echo (isset($_GET['package']) && $_GET['package'] == $package->id) ? "selected='selected'" : "" ?> ><?php echo $package->title?></option><?php
                            }
                        }?>
                    </select>
                </div>
            </div>
            <br />
            <div class="clearfix"></div>
          <table class="table table-striped table-bordered bootstrap-datatable datatable responsive" id="datatable_list">
            <thead>
              <tr>
          
          <th width="10%">Order</th>
          <th width="22%">Title</th>
           <th width="22%">Image</th>
          <th width="12%" style="text-align:center">Status</th>
          <th width="20%" style="text-align:center">Action</th>
        </tr>
            </thead>
            <tbody>
              <?php 
		$i = 0;
		if($imageList && count($imageList) > 0 ){
			$paOrder =1; 

		foreach ($imageList as $images){  ?>
        <tr>
          
          <td>
              <?php echo $paOrder; ?>
              <?php if($paOrder>1){?>
                  <a href="javascript:changeOrderCMS('<?php echo $images->id ?>','<?php echo $images->sort_order ?>','Up');">
                      <img src="<?php echo ROOT_URL_BASE?>images/admin/uparrow.png" alt="Up" width="16" height="16" border="0" /></a>
              <?php }?>
              <?php if($paOrder!=count($imageList)){?>
                  <a href="javascript:changeOrderCMS('<?php echo $images->id ?>','<?php echo $images->sort_order ?>','Dn');">
                      <img src="<?php echo ROOT_URL_BASE?>images/admin/downarrow.png" alt="Down" width="16" height="16" border="0" /></a>
              <?php }?>
          </td>
          <td>
            <a href="<?php echo ADMIN_ROOT_URL?>banner/add/<?php echo $images->id?>" ><?php echo $images->title;  ?></a>
          </td>
           <td>
                <?php if(isset($images->image_path) && $images->image_path!='' && file_exists(DIR_UPLOAD_BANNER.$images->image_path)) {?>
     
      <img src="<?php echo ROOT_URL_BASE?>assets/timthumb.php?src=<?php echo DIR_UPLOAD_BANNER_SHOW.$images->image_path ?>&q=100&w=150"/>
     
      <?php } ?>
          </td>
          <td style="text-align:center" id="td_status_<?php echo $images->id ?>">
            
            
            <?php if($images->is_active=='1'){?>
            <a href="<?php echo ADMIN_ROOT_URL?>banner/status_inactive/<?php echo $images->id?>?package=<?php echo !empty($_GET['package']) ? $_GET['package'] : '';?>" class="label-success label label-default" >Active</a>
            <?php }else{?>
            <a href="<?php echo ADMIN_ROOT_URL?>banner/status_active/<?php echo $images->id?>?package=<?php echo !empty($_GET['package']) ? $_GET['package'] : '';?>" class="label-default label label-danger"  >Inactive</a>
            <?php }?>
            
          </td>
          <td class="t-center">
            <a href="<?php echo ADMIN_ROOT_URL?>banner/add/<?php echo $images->id?>?package=<?php echo !empty($_GET['package']) ? $_GET['package'] : '';?>" class="btn btn-info"> <i class="glyphicon glyphicon-edit icon-white"></i> Edit</a>
         
            <a class="btn btn-danger" href="#" onclick="javascript:if(confirm('Are you sure to delete ? ')){location.href='<?php echo ADMIN_ROOT_URL?>banner/delete/<?php echo $images->id?>?package=<?php echo !empty($_GET['package']) ? $_GET['package'] : '';?>'}"> <i class="glyphicon glyphicon-trash icon-white"></i> Delete </a>
            
                   </td>
        </tr>
       
        <?php $paOrder++; }
		
		} ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
function changeOrderCMS(id,sort_order,position)
{
	location.href ="<?php echo ADMIN_ROOT_URL?>banner/order?id="+id+"&sort_order="+sort_order+"&position="+position;
}

</script>