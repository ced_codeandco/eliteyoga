<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>assets/ckfinder/ckfinder.js"></script>
<div id="content" class="col-lg-10 col-sm-10">
<!-- content starts -->
<div>
  <ul class="breadcrumb">
    <li> <a href="<?php echo ADMIN_ROOT_URL?>">Home</a> </li>
    <li> <a href="#"><?php echo $action;?> Banner</a> </li>
  </ul>
</div>
<div class="row">
  <div class="box-content">
    <div class="box col-md-12">
      <div class="box-inner">
        <div class="box-header well" data-original-title="">
          <h2><i class=" glyphicon glyphicon-picture"></i> <?php echo $action;?> Banner</h2>
          
        </div>
        <div class="box-content"> <?php echo validation_errors(); ?>
          <?php 
	$editUrl = '';
	if($action == 'Edit'){
		$editUrl = '/'.$imageDetails->id;
	}
	
	$attributes = array('name' => 'cmsForm', 'id' => 'cmsForm', 'enctype' => 'multipart/form-data', 'role'=>'form', 'onsubmit'=>'return validate_cms();');
				echo form_open(ADMIN_ROOT_URL.'banner/add'.$editUrl,$attributes); ?>
          <input type="hidden" name="id" id="id" value="<?php echo (isset($imageDetails->id)) ? $imageDetails->id : 0;?>" />
          <input type="hidden" name="action" id="action" value="<?php echo $action?>" />
            <div class="control-group">
                <label class="control-label" for="selectError">Packages</label>
                <div class="controls choosen_min_width">
                    <select id="parent_id" name="parent_id" data-rel="chosen" required  onchange="window.location='<?php echo ADMIN_ROOT_URL;?>banner/add?package='+this.value;">
                        <option value="0" >Select Package</option>
                        <?php
                        if (!empty($packagesList) && is_array($packagesList)) {
                            foreach ($packagesList as $package) { ?>
                                <option
                                value="<?php echo $package->id?>" <?php if (!empty($package_id) && $package_id == $package->id){ echo "selected='selected'";}?> ><?php echo $package->title?></option><?php
                            }
                        }?>
                    </select>
                    <div id="parent_id_msg_error">
                        <label class="control-label" id="parent_id_msg"></label>
                    </div>
                </div>
            </div>
          
          <div class="form-group input-group col-md-4" id="title_msg_error">
            <label class="control-label" for="title">Title<span class="required">*</span></label>
            <input type="text" class="form-control" maxlength="255" name="title" value="<?php if(isset($_SESSION['title']) && $_SESSION['title'] != '') { echo $_SESSION['title']; unset($_SESSION['title']);}else { echo (isset($imageDetails->title)) ? $imageDetails->title : ''; }?>" id="title" placeholder="Enter Title">
            <br />
            <label class="control-label" id="title_msg"></label>
          </div>
            <input type="hidden" id="uploaded_image_1" name="uploaded_file" required>
            <div class="cropic_wrap" style="width: <?php echo IMG_BANNER_WIDTH;?>px; height: <?php echo IMG_BANNER_HEIGHT;?>px;" id="cropic_uploader_1">
                <?php if(isset($imageDetails->image_path) && $imageDetails->image_path!='' && file_exists(DIR_UPLOAD_BANNER.$imageDetails->image_path)) {?>
                    <img src="<?php echo DIR_UPLOAD_BANNER_SHOW.$imageDetails->image_path ?>"/>
                <?php } ?>
            </div>
            <div class="clearfix"></div>
            <?php /*
          <div class="form-group input-group col-md-4" id="image_path_msg_error">
            <label for="image_path">Banner Image</label><br />
            <input type="file" name="image_path" id="image_path" class="input-text-02"   />
              <br />
              <span>Optimum size: 3000 X 703 px</span>
              <br />
      <?php if(isset($imageDetails->image_path) && $imageDetails->image_path!='' && file_exists(DIR_UPLOAD_BANNER.$imageDetails->image_path)) {?>
     
      <img src="<?php echo ROOT_URL_BASE?>assets/timthumb.php?src=<?php echo DIR_UPLOAD_BANNER_SHOW.$imageDetails->image_path ?>&q=100&w=900"/>
      <input type="hidden" id="uploaded_file" name="uploaded_file" value="<?php echo $imageDetails->image_path;  ?>" />
      <?php } ?>
        
          </div>*/?>
            <div class="clearfix"></div>
          <div class="control-group">
            <label class="control-label" for="selectError">Is Active</label>
            <div class="controls">
              <select id="is_active" name="is_active" data-rel="chosen">
                <option value="1" <?php if(isset($_SESSION['is_active']) && $_SESSION['is_active'] == 1) { echo 'selected="selected"'; unset($_SESSION['is_active']); }else { echo (isset($imageDetails->is_active) && $imageDetails->is_active == 1) ? 'selected="selected"' : ''; }?> >Active&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
                  <option value="0" <?php if(isset($_SESSION['is_active']) && $_SESSION['is_active'] == 0) { echo 'selected="selected"'; unset($_SESSION['is_active']); }else { echo (isset($imageDetails->is_active) && $imageDetails->is_active == 0) ? 'selected="selected"' : ''; }?> >Inactive&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
              </select>
            </div>
          </div>
         
          
          <br />
          <button type="submit" class="btn btn-success btn-sm">Submit</button>
          <?php echo form_close(); ?> </div>
      </div>
    </div>
  </div>
</div>
<script language="javascript" type="text/javascript">
function validate_(){
	if($("#title").val()==''){
		$("#title_msg").html('Please enter Home Page Banner title');
		$("#title_msg_error").addClass('has-error');
		$("#title").focus();
		return false;
	}else{
		$("#title_msg").html('');
		$("#title_msg_error").removeClass('has-error');
	}
}
</script>
    <script src="<?php echo ROOT_URL_BASE;?>assets/croppic/croppic.min.js"></script>

    <link rel="stylesheet" href="<?php echo ROOT_URL_BASE;?>assets/croppic/assets/css/croppic.css">
    <link rel="stylesheet" href="<?php echo ROOT_URL_BASE;?>css/admin/croppic_custom_css.css">
    <script type="text/javascript">
        $(document).ready(function(){
            var cropperOptions = {
                uploadUrl: '<?php echo ROOT_URL_BASE;?>administrator/banner/cropic_save_image',
                cropUrl:'<?php echo ROOT_URL_BASE;?>administrator/banner/cropic_crop_image',
                modal:true,
                doubleZoomControls:true,
                rotateControls: false,
                outputUrlId:'main_image',
                loaderHtml:'<div class="cropic_loader_wrap"><img src="<?php echo ROOT_URL_BASE;?>images/admin/upload_loader.gif" /> </div> ',
                onBeforeImgUpload: function(){ console.log('onBeforeImgUpload') },
                onAfterImgUpload: function(){ console.log('onAfterImgUpload') },
                onImgDrag: function(){ console.log('onImgDrag') },
                onImgZoom: function(){ console.log('onImgZoom') },
                onBeforeImgCrop: function(){ console.log('onBeforeImgCrop') },
                onAfterImgCrop:function(data){ $('#uploaded_image_1').val(data.file_name);  },
                onReset:function(){ console.log('onReset') },
                onError:function(errormessage){ console.log('onError:'+errormessage) }
            }
            //cropperOptions.outputUrlId = 'uploaded_image_1';
            var cropperHeader = new Croppic('cropic_uploader_1', cropperOptions);
        })
    </script>