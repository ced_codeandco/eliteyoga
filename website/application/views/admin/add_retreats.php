<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>assets/ckfinder/ckfinder.js"></script>
<div id="content" class="col-lg-10 col-sm-10">
    <!-- content starts -->
    <div>
        <ul class="breadcrumb">
            <li> <a href="<?php echo ADMIN_ROOT_URL?>">Home</a> </li>
            <li> <a href="<?php echo ADMIN_ROOT_URL?>cms">Package List</a> </li>
            <li> <a href="<?php echo ADMIN_ROOT_URL?>retreats/?package=<?php echo !empty($package_id) ? $package_id : 0;?>"> Retreats List</a> </li>
            <li> <a href="#"><?php echo $title;?></a> </li>
        </ul>
    </div>
    <div class="row">
        <div class="box-content">
            <div class="box col-md-12">
                <div class="box-inner">
                    <div class="box-header well" data-original-title="">
                        <h2><i class="glyphicon glyphicon-list-alt"></i> <?php echo $title;?></h2>
                        <div class="box-icon"> <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a> <a href="#" class="btn btn-minimize btn-round btn-default"><i
                                    class="glyphicon glyphicon-chevron-up"></i></a> <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a> </div>
                    </div>
                    <div class="box-content"> <?php echo validation_errors(); ?>
                        <?php
                        $editUrl = '';
                        if($action == 'Edit'){
                            $editUrl = '/'.$retreatDetails->id;
                        }

                        $attributes = array('name' => 'categoryForm', 'id' => 'categoryForm', 'enctype' => 'multipart/form-data', 'role'=>'form', 'onsubmit'=>'return validate_category();');
                        echo form_open('',$attributes); ?>
                        <input type="hidden" name="id" id="id" value="<?php echo (isset($retreatDetails->id)) ? $retreatDetails->id : 0;?>" />
                        <input type="hidden" name="current_parent_id" id="current_parent_id" value="<?php if(isset($_SESSION['parent_id'])) { echo $_SESSION['parent_id']; unset($_SESSION['parent_id']); }else { echo (isset($retreatDetails->parent_id)) ? $retreatDetails->parent_id : 0; }?>" />
                        <input type="hidden" name="action" id="action" value="<?php echo $action?>" />
                        <div class="control-group">
                            <label class="control-label" for="selectError">Packages</label>
                            <div class="controls choosen_min_width">
                                <select id="parent_id" name="parent_id" data-rel="chosen" required  onchange="window.location='<?php echo ADMIN_ROOT_URL;?>retreats/add?package='+this.value;">
                                    <option value="0" >Select Package</option>
                                    <?php
                                    if (!empty($packagesList) && is_array($packagesList)) {
                                        foreach ($packagesList as $package) { ?>
                                            <option
                                            value="<?php echo $package->id?>" <?php if (!empty($package_id) && $package_id == $package->id){ echo "selected='selected'";}?> ><?php echo $package->title?></option><?php
                                        }
                                    }?>
                                </select>
                                <div id="parent_id_msg_error">
                                    <label class="control-label" id="parent_id_msg"></label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group input-group col-md-4" id="title_msg_error">
                            <label class="control-label" for="title">Title<span class="required">*</span></label>
                            <input type="text" class="form-control" maxlength="255" name="title" value="<?php if(isset($_SESSION['title']) && $_SESSION['title'] != '') { echo $_SESSION['title']; unset($_SESSION['title']);}else { echo (isset($retreatDetails->title)) ? $retreatDetails->title : ''; }?>" id="title" placeholder="Enter Title">
                            <br />
                            <label class="control-label" id="title_msg"></label>
                        </div>

                        <div class="option-tile-wrap">
                            <div class="form-group input-group col-md-4" id="start_date_msg_error">
                                <label class="control-label" for="start_date">Start Date<span class="required">*</span></label>
                                <input type="text" class="form-control" maxlength="255" name="start_date" value="<?php if(isset($_SESSION['start_date']) && $_SESSION['start_date'] != '') { echo $_SESSION['start_date']; unset($_SESSION['start_date']);}else { echo (isset($retreatDetails->start_date)) ? $retreatDetails->start_date : ''; }?>" id="start_date" placeholder="Start Date">
                                <br />
                                <label class="control-label" id="start_date_msg"></label>
                            </div>
                            <div class="form-group input-group col-md-4" id="end_date_msg_error">
                                <label class="control-label" for="end_date">End Date<span class="required">*</span></label>
                                <input type="text" class="form-control" maxlength="255" name="end_date" value="<?php if(isset($_SESSION['end_date']) && $_SESSION['end_date'] != '') { echo $_SESSION['end_date']; unset($_SESSION['end_date']);}else { echo (isset($retreatDetails->end_date)) ? $retreatDetails->end_date : ''; }?>" id="end_date" placeholder="End Date">
                                <br />
                                <label class="control-label" id="end_date_msg"></label>
                            </div>
                            <div class="clearfix"></div>
                        </div>
						
						<div class="form-group input-group col-md-4" id="limit_msg_error">
                            <label class="control-label" for="title">Availability<span class="required">*</span></label>
							<br />
								<select id="limit_id" name="limit_id" data-rel="chosen" required >
                                    <!--option value="0" >Select Limit</option-->
                                    <?php
                                    if (!empty($limitList) && is_array($limitList)) {
                                        foreach ($limitList as $limit) { ?>
                                            <option
                                            value="<?php echo $limit->id?>" <?php if (!empty($limit_id) && $limit_id == $limit->id){ echo "selected='selected'";}?> ><?php echo $limit->description?></option><?php
                                        }
                                    }?>
                                </select>
                            <br />
                            <label class="control-label" id="title_msg"></label>
                        </div>
						
						<div class="option-tile-wrap">
                            <div class="form-group input-group col-md-4" id="yogi_master_name_msg_error">
                                <label class="control-label" for="yogi_master_name">Yogi Master Name</label>
                                <input type="text" class="form-control" maxlength="255" name="yogi_master_name"
                                       value="<?php echo $retreatDetails->yogi_master_name; ?>" id="yogi_master_name" placeholder="Enter Yogi Master Name">
                                <br/>
                                <label class="control-label" id="yogi_master_name_msg"></label>
                            </div>
                            <div class="form-group input-group col-md-4" id="partner_site_msg_error">
                                <label class="control-label" for="partner_site">Yogi Master Link</label>
                                <input type="text" class="form-control" maxlength="255" name="partner_site"
                                       value="<?php echo $retreatDetails->partner_site;?>" id="partner_site" placeholder="Enter Yogi Master Link">
                                <br/>
                                <label class="control-label" id="partner_site_msg"></label>
                            </div>

							<div class="clearfix"></div>
                        </div>

                            <?php if (!empty($retreatOptionList) && is_array($retreatOptionList)) {
                                $retreat_details = !empty($retreatDetails->retreat_details) ? unserialize($retreatDetails->retreat_details) : '';
                                $retreat_price = !empty($retreat_details['retreat_price']) ? ($retreat_details['retreat_price']) : '';
                                $retreat_value = !empty($retreat_details['retreat_value']) ? ($retreat_details['retreat_value']) : '';
                                //print_r(unserialize('a:2:{s:13:"retreat_price";a:2:{s:6:"dollar";a:2:{i:5;s:2:"33";i:6;s:2:"55";}s:4:"euro";a:2:{i:5;s:2:"44";i:6;s:2:"66";}}s:13:"retreat_value";a:0:{}}'));
                                foreach ($retreatOptionList as $option) {
                                    if (!empty($option->is_price) && $option->is_price == 1 ) {
                                        $input_name_1 = "retreat_price[dollar][$option->id]";
                                        $input_name_2 = "retreat_price[euro][$option->id]";
                                        $field_value_1 = !empty($retreat_price['dollar'][$option->id]) ? $retreat_price['dollar'][$option->id] : '';
                                        $field_value_2 = !empty($retreat_price['euro'][$option->id]) ? $retreat_price['euro'][$option->id] : '';
                                        ?>
                                        <div class="option-tile-wrap">
                                            <div class="form-group input-group col-md-4" id="retreat_value_<?php echo $option->id;?>_msg_error">
                                                <label class="control-label" for="retreat_value_<?php echo $option->id;?>"><?php echo $option->title;?> (&dollar;)<span
                                                        class="required">*</span></label>
                                                <input type="text" class="form-control" maxlength="255" name="<?php echo $input_name_1;?>"
                                                       value="<?php echo $field_value_1;?>" id="retreat_value_<?php echo $option->id;?>" placeholder="Enter Price For <?php echo $option->title;?> (&dollar;)">
                                                <br/>
                                                <label class="control-label" id="retreat_value_<?php echo $option->id;?>_msg"></label>
                                            </div>
                                            <div class="form-group input-group col-md-4" id="retreat_value_<?php echo $option->id;?>_2_msg_error">
                                                <label class="control-label" for="retreat_value_<?php echo $option->id;?>_2"><?php echo $option->title;?> (&euro;)<span
                                                        class="required">*</span></label>
                                                <input type="text" class="form-control" maxlength="255" name="<?php echo $input_name_2;?>"
                                                       value="<?php echo $field_value_2;?>" id="retreat_value_<?php echo $option->id;?>_2" placeholder="Enter Price For <?php echo $option->title;?> (&euro;)">
                                                <br/>
                                                <label class="control-label" id="retreat_value_<?php echo $option->id;?>_2_msg"></label>
                                            </div>

                                        <div class="clearfix"></div>
                                        </div><?php
                                    } else {
                                        $input_name_1 = "retreat_value[value][$option->id]";
                                        $field_value_1 = !empty($retreat_value['value'][$option->id]) ? $retreat_value['value'][$option->id] : '';?>
                                        <div class="option-tile-wrap">
                                        <div class="form-group input-group col-md-4" id="retreat_value_<?php echo $option->id;?>_1_msg_error">
                                            <label class="control-label" for="retreat_value_<?php echo $option->id;?>_1"><?php echo $option->title;?><span
                                                    class="required">*</span></label>
                                            <input type="text" class="form-control" maxlength="255" name="<?php echo $input_name_1;?>"
                                                   value="<?php echo $field_value_1;?>" id="retreat_value_<?php echo $option->id;?>_1" placeholder="Enter <?php echo $option->title;?>">
                                            <br/>
                                            <label class="control-label" id="retreat_value_<?php echo $option->id;?>_1_msg"></label>
                                        </div>
                                        </div><?php
                                    }
                                }
                            }?>


                        <div class="control-group">
                            <label class="control-label" for="selectError">Is Active</label>
                            <div class="controls choosen_min_width">
                                <select id="is_active" name="is_active" data-rel="chosen">
                                    <option value="1" <?php if(isset($_SESSION['is_active']) && $_SESSION['is_active'] == 1) { echo 'selected="selected"'; unset($_SESSION['is_active']); }else { echo (isset($retreatDetails->is_active) && $retreatDetails->is_active == 1) ? 'selected="selected"' : ''; }?> >Active</option>
                                    <option value="0" <?php if(isset($_SESSION['is_active']) && $_SESSION['is_active'] == 0) { echo 'selected="selected"'; unset($_SESSION['is_active']); }else { echo (isset($retreatDetails->is_active) && $retreatDetails->is_active == 0) ? 'selected="selected"' : ''; }?> >Inactive</option>
                                </select>
                            </div>
                        </div>

                        <br />
                        <button type="submit" class="btn btn-success btn-sm">Submit</button>
                        <?php echo form_close(); ?> </div>
                </div>
            </div>
        </div>
    </div>
    <script language="javascript" type="text/javascript">
        $(document).ready(function() {
            manageFilterControl();
            manageSortControl();
            /*$.ajax({
             type: "POST",
             url: "<?php echo ADMIN_ROOT_URL?>category/get_parent?id="+$("#id").val()+"&current_parent_id="+$("#current_parent_id").val(),
             success: function(result){
             $('#parent_id').val(result);
             },
             complete: function(){

             },
             error: function(){
             $('#parent_id').val('Error occured. Please try later');
             }

             });*/
        });
        function set_is_common_value() {
            if (parseInt($('#is_multiple').val()) == 1) {
                $('#description_msg_error').show();
            } else {
                $('#description_msg_error').val('').hide();
            }
        }
        function validate_category(){
            if($("#parent_id").val()=='0'){
                $("#parent_id_msg").html('Please select a package');
                $("#parent_id_msg_error").addClass('has-error');
                $("#parent_id").focus();
                return false;
            }else{
                $("#parent_id_msg").html('');
                $("#parent_id_msg_error").removeClass('has-error');
            }
            if($("#title").val()==''){
                $("#title_msg").html('Please enter Category title');
                $("#title_msg_error").addClass('has-error');
                $("#title").focus();
                return false;
            }else{
                $("#title_msg").html('');
                $("#title_msg_error").removeClass('has-error');
            }
        }
        function manageFilterControl(){
            if ($('#nofiltercontrol').is(':checked')) {
                $('.filtercontrolChecks').removeAttr('checked');
                $('.filtercontrolChecks').attr('disabled', 'disabled');
            } else {
                $('.filtercontrolChecks').removeAttr('disabled');
            }
        }
        function manageSortControl(){
            if ($('#nosortcontrol').is(':checked')) {
                $('.sortcontrolChecks').removeAttr('checked');
                $('.sortcontrolChecks').attr('disabled', 'disabled');
            } else {
                $('.sortcontrolChecks').removeAttr('disabled');
            }
        }

    </script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    <script>
        $(function() {
            $( "#start_date" ).datepicker({
                changeMonth: true,
                changeYear: true,
                onClose: function( selectedDate ) {
                    $( "#end_date" ).datepicker( "option", "minDate", selectedDate );
                },
                dateFormat: "yy-mm-dd"
            });
            $( "#end_date" ).datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                changeYear: true,
                onClose: function( selectedDate ) {
                    $( "#start_date" ).datepicker( "option", "maxDate", selectedDate );
                },
                dateFormat: "yy-mm-dd"
            });
        });
    </script>