<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>assets/ckfinder/ckfinder.js"></script>
<div id="content" class="col-lg-10 col-sm-10">
    <!-- content starts -->
    <div>
        <ul class="breadcrumb">
            <li> <a href="<?php echo ADMIN_ROOT_URL?>">Home</a> </li>
            <li> <a href="<?php echo ADMIN_ROOT_URL?>cms">Package List</a> </li>
            <li> <a href="<?php echo ADMIN_ROOT_URL?>generic_category/?package=<?php echo !empty($package_id) ? $package_id : 0;?>">Retreats Options List</a> </li>

            <li> <a href="#"><?php echo $title;?></a> </li>
        </ul>
    </div>
    <div class="row">
        <div class="box-content">
            <div class="box col-md-12">
                <div class="box-inner">
                    <div class="box-header well" data-original-title="">
                        <h2><i class="glyphicon glyphicon-list-alt"></i> <?php echo $title;?></h2>
                        <div class="box-icon"> <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a> <a href="#" class="btn btn-minimize btn-round btn-default"><i
                                    class="glyphicon glyphicon-chevron-up"></i></a> <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a> </div>
                    </div>
                    <div class="box-content"> <?php echo validation_errors(); ?>
                        <?php
                        $editUrl = '';
                        if($action == 'Edit'){
                            $editUrl = '/'.$categoryDetails->id;
                        }

                        $attributes = array('name' => 'categoryForm', 'id' => 'categoryForm', 'enctype' => 'multipart/form-data', 'role'=>'form', 'onsubmit'=>'return validate_category();');
                        echo form_open('',$attributes); ?>
                        <input type="hidden" name="id" id="id" value="<?php echo (isset($categoryDetails->id)) ? $categoryDetails->id : 0;?>" />
                        <input type="hidden" name="current_parent_id" id="current_parent_id" value="<?php if(isset($_SESSION['parent_id'])) { echo $_SESSION['parent_id']; unset($_SESSION['parent_id']); }else { echo (isset($categoryDetails->parent_id)) ? $categoryDetails->parent_id : 0; }?>" />
                        <input type="hidden" name="action" id="action" value="<?php echo $action?>" />
                        <div class="control-group">
                            <label class="control-label" for="selectError">Packages</label>
                            <div class="controls choosen_min_width">
                                <select id="parent_id" name="parent_id" data-rel="chosen" required>
                                    <option value="0" selected="selected">Select Package</option>
                                    <?php
                                    if (!empty($packagesList) && is_array($packagesList)) {
                                        foreach ($packagesList as $package) { ?>
                                            <option
                                            value="<?php echo $package->id?>" <?php echo (!empty($package_id) && $package_id == $package->id) ? "selected='selected'" : "" ?> ><?php echo $package->title?></option><?php
                                        }
                                    }?>
                                </select>
                                <div id="parent_id_msg_error">
                                    <label class="control-label" id="parent_id_msg"></label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group input-group col-md-4" id="title_msg_error">
                            <label class="control-label" for="title">Title<span class="required">*</span></label>
                            <input type="text" class="form-control" maxlength="255" name="title" value="<?php if(isset($_SESSION['title']) && $_SESSION['title'] != '') { echo $_SESSION['title']; unset($_SESSION['title']);}else { echo (isset($categoryDetails->title)) ? $categoryDetails->title : ''; }?>" id="title" placeholder="Enter Title">
                            <br />
                            <label class="control-label" id="title_msg"></label>
                        </div>

                        <?php /*<div class="form-group input-group col-md-4" id="date_title_custom_msg_error">
                            <label class="control-label" for="date_title_custom">Customize Date Title<span class="required">*</span></label>
                            <input type="text" class="form-control" maxlength="255" name="date_title_custom" value="<?php if(isset($_SESSION['date_title_custom']) && $_SESSION['date_title_custom'] != '') { echo $_SESSION['date_title_custom']; unset($_SESSION['date_title_custom']);}else { echo (isset($retreatDetails->date_title_custom)) ? $retreatDetails->date_title_custom : ''; }?>" id="date_title_custom" placeholder="Enter Custom Date Title">
                            <br />
                            <label class="control-label" id="date_title_custom_msg"></label>
                        </div>*/?>

                        <div class="select-list-wrap">
                            <div class="control-group">
                                <label class="control-label" for="selectError">Is Retreat Option</label>
                                <div class="controls choosen_min_width">
                                    <select id="is_price" name="is_price" data-rel="chosen">
                                        <option value="1" <?php if(isset($_SESSION['is_price']) && $_SESSION['is_price'] == 1) { echo 'selected="selected"'; unset($_SESSION['is_price']); }else { echo (isset($categoryDetails->is_price) && $categoryDetails->is_price == 1) ? 'selected="selected"' : ''; }?> >Retreat Option</option>
                                        <option value="0" <?php if(isset($_SESSION['is_price']) && $_SESSION['is_price'] == 0) { echo 'selected="selected"'; unset($_SESSION['is_price']); }else { echo (isset($categoryDetails->is_price) && $categoryDetails->is_price == 0) ? 'selected="selected"' : ''; }?> >Retreat Info</option>
                                    </select>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="selectError">Is Common Value</label>
                                <div class="controls choosen_min_width">
                                    <select id="is_common" name="is_common" data-rel="chosen" onchange="set_is_common_value(this)">
                                        <option value="0" <?php if(isset($_SESSION['is_common']) && $_SESSION['is_common'] == 0) { echo 'selected="selected"'; unset($_SESSION['is_common']); }else { echo (isset($categoryDetails->is_common) && $categoryDetails->is_common == 0) ? 'selected="selected"' : ''; }?> >No</option>
                                        <option value="1" <?php if(isset($_SESSION['is_common']) && $_SESSION['is_common'] == 1) { echo 'selected="selected"'; unset($_SESSION['is_common']); }else { echo (isset($categoryDetails->is_common) && $categoryDetails->is_common == 1) ? 'selected="selected"' : ''; }?> >Yes</option>
                                    </select>
                                </div>
                            </div>



                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group input-group col-md-4" id="description_msg_error" <?php echo (empty($categoryDetails->is_common) OR $categoryDetails->is_common == 0) ? 'style="display:none;"' : '';?>>
                            <label for="description" class="control-label">Common Description</label>
                            <textarea class="form-control"  maxlength="255" name="description"  id="description" placeholder="Common Description"><?php if(isset($_SESSION['description']) && $_SESSION['description'] != '') { echo $_SESSION['description']; unset($_SESSION['description']);}else { echo (isset($categoryDetails->description)) ? stripslashes($categoryDetails->description) : ''; }?></textarea>

                        </div>

                        <div class="control-group">
                            <label class="control-label" for="selectError">Is Active</label>
                            <div class="controls choosen_min_width">
                                <select id="is_active" name="is_active" data-rel="chosen">
                                    <option value="1" <?php if(isset($_SESSION['is_active']) && $_SESSION['is_active'] == 1) { echo 'selected="selected"'; unset($_SESSION['is_active']); }else { echo (isset($categoryDetails->is_active) && $categoryDetails->is_active == 1) ? 'selected="selected"' : ''; }?> >Active</option>
                                    <option value="0" <?php if(isset($_SESSION['is_active']) && $_SESSION['is_active'] == 0) { echo 'selected="selected"'; unset($_SESSION['is_active']); }else { echo (isset($categoryDetails->is_active) && $categoryDetails->is_active == 0) ? 'selected="selected"' : ''; }?> >Inactive</option>
                                </select>
                            </div>
                        </div>

                        <br />
                        <button type="submit" class="btn btn-success btn-sm">Submit</button>
                        <?php echo form_close(); ?> </div>
                </div>
            </div>
        </div>
    </div>
    <script language="javascript" type="text/javascript">
        $(document).ready(function() {
            manageFilterControl();
            manageSortControl();
            /*$.ajax({
             type: "POST",
             url: "<?php echo ADMIN_ROOT_URL?>category/get_parent?id="+$("#id").val()+"&current_parent_id="+$("#current_parent_id").val(),
             success: function(result){
             $('#parent_id').val(result);
             },
             complete: function(){

             },
             error: function(){
             $('#parent_id').val('Error occured. Please try later');
             }

             });*/
        });
        function set_is_common_value() {
            if (parseInt($('#is_common').val()) == 1) {
                $('#description_msg_error').show();
            } else {
                $('#description_msg_error').val('').hide();
            }
        }
        function validate_category(){
            if($("#parent_id").val()=='0'){
                $("#parent_id_msg").html('Please select a package');
                $("#parent_id_msg_error").addClass('has-error');
                $("#parent_id").focus();
                return false;
            }else{
                $("#parent_id_msg").html('');
                $("#parent_id_msg_error").removeClass('has-error');
            }
            if($("#title").val()==''){
                $("#title_msg").html('Please enter Category title');
                $("#title_msg_error").addClass('has-error');
                $("#title").focus();
                return false;
            }else{
                $("#title_msg").html('');
                $("#title_msg_error").removeClass('has-error');
            }
        }
        function manageFilterControl(){
            if ($('#nofiltercontrol').is(':checked')) {
                $('.filtercontrolChecks').removeAttr('checked');
                $('.filtercontrolChecks').attr('disabled', 'disabled');
            } else {
                $('.filtercontrolChecks').removeAttr('disabled');
            }
        }
        function manageSortControl(){
            if ($('#nosortcontrol').is(':checked')) {
                $('.sortcontrolChecks').removeAttr('checked');
                $('.sortcontrolChecks').attr('disabled', 'disabled');
            } else {
                $('.sortcontrolChecks').removeAttr('disabled');
            }
        }

    </script>
