		<nav class=footer-links>
			<a href="/"><?=lang('text.home')?></a>
			<a href="/<?php echo $this->uri->segment(1);?>/contact-us"><?=lang('text.contactus')?></a>
		
			<a href="/<?php echo $this->uri->segment(1);?>/page/info"><?=lang('text.terms')?></a>
			<a href="/<?php echo $this->uri->segment(1);?>/page/privacy-policy"><?=lang('text.privacy')?></a>
		</nav>
		<footer>
			<section class=toolbar-bottom>
				<nav class=left-wrapper>
					<a class="istap btn black no-text" href="<?php echo ($this->uri->segment(1)=="en" ? '/' : '/'.$this->uri->segment(1).'/home');?>"><i class=icon-home></i></a>
                  
				</nav>
				<nav class=right-wrapper>
					<button class="istap btn black" id=submit><span><?=lang('text.submit')?></span></button>
				</nav>
			</section>
		</footer>
		
		<!-- js library here -->
        <!--<script src="/js/vendor/zepto.min.js"></script>
        <script src="/js/helper.js"></script>
		<script src="http://code.jquery.com/jquery-latest.min.js"></script>-->
		<script src="/js/main.js"></script>
        <!-- Google Analytics below -->
		<?php
  $googleAnalyticsImageUrl = googleAnalyticsGetImageUrl();
?>
<img src="<?= $googleAnalyticsImageUrl ?>" />
    </body>
</html>