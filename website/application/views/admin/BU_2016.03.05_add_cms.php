<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>assets/ckfinder/ckfinder.js"></script>
<div id="content" class="col-lg-10 col-sm-10">
    <!-- content starts -->
    <div>
        <ul class="breadcrumb">
            <li> <a href="<?php echo ADMIN_ROOT_URL?>">Home</a> </li>
            <li> <a href="#"><?php echo $action;?> Package Page</a> </li>
        </ul>
    </div>
    <div class="row">
        <div class="box-content">
            <div class="box col-md-12">
                <div class="box-inner">
                    <div class="box-header well" data-original-title="">
                        <h2><i class="glyphicon glyphicon-list-alt"></i> <?php echo $action;?> Package Page</h2>
                        <div class="box-icon"> <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a> <a href="#" class="btn btn-minimize btn-round btn-default"><i
                                    class="glyphicon glyphicon-chevron-up"></i></a> <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a> </div>
                    </div>
                    <div class="box-content"> <?php echo validation_errors(); ?>
                        <?php
                        $editUrl = '';
                        if($action == 'Edit'){
                            $editUrl = '/'.$cmsDetails->id;
                        }

                        $attributes = array('name' => 'cmsForm', 'id' => 'cmsForm', 'enctype' => 'multipart/form-data', 'role'=>'form', 'onsubmit'=>'return validate_cms();');
                        echo form_open('', $attributes); ?>
                        <input type="hidden" name="id" id="id" value="<?php echo (isset($cmsDetails->id)) ? $cmsDetails->id : 0;?>" />
                        <input type="hidden" name="action" id="action" value="<?php echo $action?>" />
                        <input type="hidden" id="parent_id" name="parent_id" value="0">

                        <div class="form-group input-group col-md-4" id="title_msg_error">
                            <label class="control-label" for="title">Title<span class="required">*</span></label>
                            <input type="text" class="form-control" maxlength="255" name="title" value="<?php if(isset($_SESSION['title']) && $_SESSION['title'] != '') { echo $_SESSION['title']; unset($_SESSION['title']);}else { echo (isset($cmsDetails->title)) ? $cmsDetails->title : ''; }?>" id="title" placeholder="Enter Title">
                            <br />
                            <label class="control-label" id="title_msg"></label>
                        </div>

                        <div class="form-group input-group col-md-4" id="package_location_msg_error">
                            <label class="control-label" for="package_location">Location<span class="required">*</span></label>
                            <input type="text" class="form-control"  name="package_location" value="<?php if(isset($_SESSION['package_location']) && $_SESSION['package_location'] != '') { echo $_SESSION['package_location']; unset($_SESSION['package_location']);}else { echo (isset($cmsDetails->package_location)) ? $cmsDetails->package_location : ''; }?>" id="package_location" placeholder="Location">
                            <br />
                            <label class="control-label" id="package_location_msg"></label>
                        </div>

                        <div class="form-group input-group col-md-4" id="sub_title_msg_error">
                            <label class="control-label" for="sub_title">Sub Title<span class="required">*</span></label>
                            <input type="text" class="form-control"  name="sub_title" value="<?php if(isset($_SESSION['sub_title']) && $_SESSION['sub_title'] != '') { echo $_SESSION['sub_title']; unset($_SESSION['sub_title']);}else { echo (isset($cmsDetails->sub_title)) ? $cmsDetails->sub_title : ''; }?>" id="sub_title" placeholder="Enter Sub Title">
                            <br />
                            <label class="control-label" id="sub_title_msg"></label>
                        </div>

                        <div class="form-group input-group col-md-4" id="small_description_msg_error">
                            <label for="small_description" class="control-label">Short Description</label>
                            <textarea class="form-control"   name="small_description"  id="small_description" placeholder="Short Description"><?php if(isset($_SESSION['small_description']) && $_SESSION['small_description'] != '') { echo $_SESSION['small_description']; unset($_SESSION['small_description']);}else { echo (isset($cmsDetails->small_description)) ? $cmsDetails->small_description : ''; }?></textarea>

                        </div>
                        <div class="form-group input-group col-md-4" id="description_msg_error">
                            <label for="description">Description</label><br />
                            <?php if(isset($_SESSION['description']) && $_SESSION['description'] != '') { $description =  $_SESSION['description']; unset($_SESSION['description']);}else { $description =  (isset($cmsDetails->description)) ? $cmsDetails->description : ''; }?>
                            <?php echo $this->ckeditor->editor("description",stripslashes($description));?>
                        </div>
                        <div class="form-group input-group col-md-4" id="extra_description_msg_error">
                            <label for="extra_description">More Info</label><br />
                            <?php if(isset($_SESSION['extra_description']) && $_SESSION['extra_description'] != '') { $extra_description =  $_SESSION['extra_description']; unset($_SESSION['extra_description']);}else { $extra_description =  (isset($cmsDetails->extra_description)) ? $cmsDetails->extra_description : ''; }?>
                            <?php echo $this->ckeditor->editor("extra_description",stripslashes($extra_description));?>
                        </div>
                        <div class="form-group input-group col-md-4" id="date_title_msg_error">
                            <label class="control-label" for="date_title">Custom Date Title<span class="required">*</span></label>
                            <input type="text" class="form-control"  name="date_title" value="<?php if(isset($_SESSION['date_title']) && $_SESSION['date_title'] != '') { echo $_SESSION['date_title']; unset($_SESSION['date_title']);}else { echo (isset($cmsDetails->date_title)) ? $cmsDetails->date_title : ''; }?>" id="date_title" placeholder="Custom Date Title">
                            <br />
                            <label class="control-label" id="date_title_msg"></label>
                        </div>


                        <div class="form-group input-group col-md-4" id="cms_banner_image_msg_error">
                            <label for="cms_banner_image">Banner Image</label><br />

                            <input type="file" name="cms_banner_image" id="cms_banner_image" class="input-text-02"   />
                            <?php if(isset($cmsDetails->cms_banner_image) && $cmsDetails->cms_banner_image!='' && file_exists(DIR_UPLOAD_BANNER.$cmsDetails->cms_banner_image)) {?>

                                <img src="<?php echo ROOT_URL_BASE?>assets/timthumb.php?src=<?php echo DIR_UPLOAD_BANNER_SHOW.$cmsDetails->cms_banner_image ?>&q=100&w=600"/>
                                <input type="hidden" id="uploaded_file" name="uploaded_file" value="<?php echo $cmsDetails->cms_banner_image;  ?>" />
                            <?php } ?>
                        </div>
                        <div class="clearfix"></div>

                        <div class="control-group">
                            <label class="control-label" for="selectError">Is Active</label>
                            <div class="controls">
                                <select id="is_active" name="is_active" data-rel="chosen">
                                    <option value="0" selected="selected">In Active</option>
                                    <option value="1" <?php if(isset($_SESSION['is_active']) && $_SESSION['is_active'] == 1) { echo 'selected="selected"'; unset($_SESSION['is_active']); }else { echo (isset($cmsDetails->is_active) && $cmsDetails->is_active == 1) ? 'selected="selected"' : ''; }?> >Active</option>
                                </select>
                            </div>
                        </div>


                        <br />
                        <button type="submit" class="btn btn-success btn-sm">Submit</button>
                        <?php echo form_close(); ?> </div>
                </div>
            </div>
        </div>
    </div>
    <script language="javascript" type="text/javascript">
        function validate_cms(){
            if($("#title").val()==''){
                $("#title_msg").html('Please enter CMS title');
                $("#title_msg_error").addClass('has-error');
                $("#title").focus();
                return false;
            }else{
                $("#title_msg").html('');
                $("#title_msg_error").removeClass('has-error');
            }
        }
    </script>
