<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>Elite Yoga</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="<?php echo ROOT_URL_BASE.'../';?>images/favicon.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="<?php echo ROOT_URL_BASE.'../';?>fonts/fonts.css" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo ROOT_URL_BASE.'../';?>css/animate.css">
    <link rel="stylesheet" href="<?php echo ROOT_URL_BASE.'../';?>css/remodal.css">
    <link rel="stylesheet" href="<?php echo ROOT_URL_BASE.'../';?>css/remodal-default-theme.css">
    <link rel="stylesheet" href="<?php echo ROOT_URL_BASE.'../';?>css/slider-pro.min.css">
    <link href="<?php echo ROOT_URL_BASE.'../';?>css/style.css" type="text/css" rel="stylesheet" />
    <link href="<?php echo ROOT_URL_BASE.'../';?>css/hover.css" type="text/css" rel="stylesheet" />
    <link href="<?php echo ROOT_URL_BASE.'../';?>slider/jquery.bxslider.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo ROOT_URL_BASE.'../';?>css/responsive.css" type="text/css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo ROOT_URL_BASE.'../';?>js/wow.min.js"></script>
    <script src="<?php echo ROOT_URL_BASE.'../';?>js/parallax.js"></script>
    <script src="<?php echo ROOT_URL_BASE.'../';?>js/remodal.min.js"></script>
    <script src="<?php echo ROOT_URL_BASE.'../';?>js/jquery.sliderPro.min.js"></script>
    <script src="<?php echo ROOT_URL_BASE.'../';?>js/custom-packages.js"></script>
    <script>
        wow = new WOW();
        wow.init();
    </script>
    <script type="text/javascript" src="<?php echo ROOT_URL_BASE.'../';?>slider/jquery.bxslider.min.js"></script>
    <script src="<?php echo ROOT_URL_BASE.'../';?>js/common.js"></script>
    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-56c87bb000d5c2f2"></script>

</head>

<body>
<input type="hidden" id="rootUrlLink" value="<?php echo ROOT_URL;?>"/>
<?php include_once(ROOT_DIRECTORY.'../header.php');?>
<section class="main-section ">
    <header>
        <h2>Packages</h2>
        <p>Yoga is no longer confined to a yoga studio, Elite yoga is revolutionizing the way yoga is performed<br/>by allowing you to experience the world in 5 star settings.</p>
    </header>
    <?php if (!empty($packageList) && is_array($packageList)) {
        foreach ($packageList as $package) { //print_r($package);?>
            <div class="container resort-packages">
                <div class="row">
                    <h2><?php echo $package->title;?>, <span><?php echo $package->package_location;?></span></h2>
                    <div class="col-md-9">
                        <!-- property images slider HTML  -->
                        <?php
                        $imageList = !empty($package->imageList) ? $package->imageList : array();
                        if (!empty($imageList) && is_array($imageList)) {?>
                            <div class="property-images slider-pro">
                                <div class="sp-slides">
                                    <?php
                                    foreach ($imageList as $image) {
                                        if (!empty($image->image_path) && file_exists(DIR_UPLOAD_BANNER.$image->image_path)) { ?>
                                            <div class="sp-slide">
                                                <img src="<?php echo DIR_UPLOAD_BANNER_SHOW.$image->image_path; ?>" alt="property image">
                                            </div>
                                        <?php
                                        }
                                    }?>
                                </div>
                                <div class="sp-thumbnails">
                                    <?php
                                    foreach ($imageList as $image) {
                                        if (!empty($image->image_path) && file_exists(DIR_UPLOAD_BANNER . $image->image_path)) { ?>
                                            <img class="sp-thumbnail"
                                                 src="<?php echo DIR_UPLOAD_BANNER_SHOW.$image->image_path; ?>"
                                                 alt="property image thumb">
                                        <?php
                                        }
                                    }?>
                                </div>
                            </div>
                        <?php
                        }?>
                        <!-- End of property slider -->

                    </div>
                    <div class="col-md-3">
                        <div class="package-writeup">
                            <h4><?php echo $package->sub_title;?></h4>
                            <p><?php echo $package->small_description;?> <em>(package excludes flights)</em>
                        </div>
                        <div class="clearfix"></p>

                            <a class="more_details" data-content-id="description_content_<?php echo $package->id;?>" href="#model_content_id_<?php echo $package->id;?>">More Details</a>
                        </div>
                    </div>
                </div>
                <?php if (!empty($package->extra_description)){?>
                <div class="row">
                    <div class="col-md-12 logo_packages">
                        <?php echo stripslashes($package->extra_description);?>
                    </div>
                </div>
                <?php }
                /*print_r($package->retreatOptions);
                print_r($package->retreatList);*/
                $retreatOptions = array();?>
                <!-- End row -->
                <div class="row">
                    <div class="col-md-12">  
                        <!-- Pricing table -->
                        <table class="pricing-table">
                            <thead>
                            <tr>
                                <th><?php echo $package->date_title;?></th>
                                <th class="text-center">Availability</th>
                                <?php if (!empty($package->retreatOptions) && is_array($package->retreatOptions)) {
                                    foreach ($package->retreatOptions as $options) {?>
                                        <th><?php echo $options->title; ?></th>
                                    <?php }
                                }?>
                            </tr>
                            </thead>
                            <?php if (!empty($package->retreatList) && is_array($package->retreatList)){?>
                            <tbody><?php
                            $retreat_counter =1;
                            $number_of_retreats = count($package->retreatList);
							
                                foreach ($package->retreatList as $retreat) {
                                    $retreat_details = @unserialize($retreat->retreat_details);
                                    $retreat_price = prepare_retreat_details($retreat_details['retreat_price']);
                                    //print_r($retreat_price);?>
                                    <tr <?php echo ($retreat_counter > 2) ? 'class="show-more"' : '';?>>
                                        <td>
											<?php echo date('d M Y ', strtotime($retreat->start_date));?> to <?php echo date('d M Y ', strtotime($retreat->end_date));?>
											<?php 
												if($retreat->yogi_master_name !== NULL && trim($retreat->yogi_master_name) !== ""){
													echo "<br />";
													echo "by: ";
													echo "<a href='" . $retreat->partner_site . "' target='_blank'>" . $retreat->yogi_master_name . "</a>";
												}
											?>
										</td>
                               			<td class="availability-col"><span class="<?php if($retreat->limit_id==3){echo 'not-available';}elseif($retreat->limit_id==2){echo 'limited-space';}else {echo "available";}?>"></span> 
											<?php 
												foreach ($limitList as $limit){
													if($limit->id === $retreat->limit_id){
														echo ($limit->description);
													}
												}
											?> 
                                        </td>
                                    <?php if (!empty($package->retreatOptions) && is_array($package->retreatOptions)) {
                                        $option_count = 1;
                                        foreach ($package->retreatOptions as $options) {
                                            if ($options->is_price == 1) { ?>
                                                <td>
                                                <!--<input type="checkbox" id="16oct_person"/>-->
                                                <input type="hidden" name="package_name_<?php echo $package->id; ?>_<?php echo $retreat->id; ?>_<?php echo $options->id; ?>"
                                                       value="<?php echo $package->title; ?>"/>
                                                <input type="hidden" name="date_text_<?php echo $package->id; ?>_<?php echo $retreat->id; ?>_<?php echo $options->id; ?>"
                                                       value="<?php echo date('d M Y ', strtotime($retreat->start_date));?> to <?php echo date('d M Y ', strtotime($retreat->end_date));?>"/>
                                                <input type="hidden" name="price_text_<?php echo $package->id; ?>_<?php echo $retreat->id; ?>_<?php echo $options->id; ?>"
                                                       value="<?php
                                                       echo !empty($retreat_price[$options->id]['dollar']) ? '$' . $retreat_price[$options->id]['dollar'] : '';
                                                       /*echo (!empty($retreat_price[$options->id]['dollar']) && !empty($retreat_price[$options->id]['euro'])) ? '/' : '';
                                                       echo !empty($retreat_price[$options->id]['euro']) ? '£' . $retreat_price[$options->id]['euro'] : '';*/
                                                       ?>"/>
                                                <input type="hidden" name="start_date_<?php echo $package->id; ?>_<?php echo $retreat->id; ?>_<?php echo $options->id; ?>"
                                                       value="<?php echo $retreat->start_date; ?>"/>
                                                <input type="hidden" name="end_date_<?php echo $package->id; ?>_<?php echo $retreat->id; ?>_<?php echo $options->id; ?>"
                                                       value="<?php echo $retreat->end_date; ?>"/>
                                                <input type="hidden" name="retreat_id_<?php echo $package->id; ?>_<?php echo $retreat->id; ?>_<?php echo $options->id; ?>"
                                                       value="<?php echo $retreat->id; ?>"/>
                                                <input type="hidden" name="price_dollar_selected_<?php echo $package->id; ?>_<?php echo $retreat->id; ?>_<?php echo $options->id; ?>"
                                                       value="<?php echo !empty($retreat_price[$options->id]['dollar']) ? $retreat_price[$options->id]['dollar'] : '';; ?>"/>
                                                <input type="hidden" name="price_euro_selected_<?php echo $package->id; ?>_<?php echo $retreat->id; ?>_<?php echo $options->id; ?>"
                                                       value="<?php echo !empty($retreat_price[$options->id]['euro']) ? $retreat_price[$options->id]['euro'] : '';; ?>"/>
                                                <input type="checkbox" class="retreat_check_box"
                                                       package_id="<?php echo $package->id; ?>" retreat_id="<?php echo $retreat->id; ?>" option_id="<?php echo $options->id; ?>" id="retreat_check_box_<?php echo $retreat->id; ?>_<?php echo $options->id; ?>"/>
                                                <label for="retreat_check_box_<?php echo $retreat->id; ?>_<?php echo $options->id; ?>">
                                                    <?php
                                                    echo !empty($retreat_price[$options->id]['dollar']) ? '$' . $retreat_price[$options->id]['dollar'] : '';

                                                    /*echo (!empty($retreat_price[$options->id]['dollar']) && !empty($retreat_price[$options->id]['euro'])) ? '/' : '';

                                                    echo !empty($retreat_price[$options->id]['euro']) ? '£' . $retreat_price[$options->id]['euro'] : '';*/
                                                    ?></label>
                                                </td><?php
                                            } else if ($options->is_common == 1) {
                                                if ($retreat_counter == 1) { ?>
                                                    <td <?php echo ($options->is_common == 1) ? 'rowspan="'.$number_of_retreats.'"' : ''; ?>>
                                                        <?php echo nl2br(stripslashes($options->description)); ?>
                                                    </td> <?php
                                                }
                                            } else {?>
                                                <td><?php
                                                echo !empty($retreat_details['retreat_value']['value'][$options->id]) ? $retreat_details['retreat_value']['value'][$options->id] : '';?>
                                                </td><?php
                                            }
                                            $option_count++;
                                        }
                                    }?>
                                    </tr><?php
                                    $retreat_counter++;
                                }?>
                            </tbody>
                            <?php }?>

                        </table>
                        <!-- Pricing table -->
                    </div>
                </div>
                <div class="arrow-down-wrapper">
                    <?php if ($number_of_retreats > 2) {?>
                    <a href="" class="btn-arrow-down">
                        <i class="fa fa-chevron-down"></i>
                    </a>
                    <?php }?>
                </div>
                <div class="terms-conditions">
                    <small>I agree to the <a href="#modal">Terms &amp; Conditions</a>.</small>
                </div>
                <a href="" class="btn btn-booknow book_package_btn" package_id="<?php echo $package->id; ?>">BOOK NOW</a>
            </div>
            <div class="remodal list_style tandc-PremierYoga" data-remodal-id="model_content_id_<?php echo $package->id;?>">
                <button data-remodal-action="close" class="remodal-close"></button>

                <h2><?php echo $package->sub_title;?></h2>

                <?php echo stripslashes($package->description);?>
                <br>
                <!-- <button data-remodal-action="cancel" class="remodal-cancel btn btn-shareit">Cancel</button> -->
            </div>
        <?php }
    } else {?>
    <?php } ?>
</section>
<?php include_once(ROOT_DIRECTORY.'../our_partners.php');?>
<?php include_once(ROOT_DIRECTORY.'../footer.php');?>


<!-- Modal HTML below -->
<div class="remodal tandc-modal" id="booking_popup_wrap">
    <button data-remodal-action="close" class="remodal-close"></button>

    <h2>Book Package</h2>
    <form class="contact_form" id="booking_form">
        <div>
            <input type="hidden" name="package_id">
            <input type="hidden" name="retreat_id">
            <input type="hidden" name="start_date">
            <input type="hidden" name="end_date">
            <input type="hidden" name="price_dollar_selected">
            <input type="hidden" name="price_euro_selected">

            <p class="booking_details_wrap">
                Package:
                <span id="package_name"></span>
            </p>
            <p class="booking_details_wrap">
                Date:
                <span id="package_date"></span>
            </p>
            <p class="booking_details_wrap">
                Price:
                <span id="package_price"></span>
            </p>
            <input type="text" placeholder="Full Name" class="input_text full_name" name="full_name" required="required">
            <input type="text" placeholder="Telephone" class="input_text telephone" name="telephone" required="required">
            <input type="text" placeholder="Street" class="input_text full_name" name="street" required="required">
            <input type="text" placeholder="City" class="input_text telephone" name="city" required="required">
            <input type="text" placeholder="State" class="input_text full_name" name="state" required="required">
            <select name="country" class="input_text telephone" required="required">
                <option value="AF">Afghanistan</option>
                <option value="AX">Åland Islands</option>
                <option value="AL">Albania</option>
                <option value="DZ">Algeria</option>
                <option value="AS">American Samoa</option>
                <option value="AD">Andorra</option>
                <option value="AO">Angola</option>
                <option value="AI">Anguilla</option>
                <option value="AQ">Antarctica</option>
                <option value="AG">Antigua and Barbuda</option>
                <option value="AR">Argentina</option>
                <option value="AM">Armenia</option>
                <option value="AW">Aruba</option>
                <option value="AU">Australia</option>
                <option value="AT">Austria</option>
                <option value="AZ">Azerbaijan</option>
                <option value="BS">Bahamas</option>
                <option value="BH">Bahrain</option>
                <option value="BD">Bangladesh</option>
                <option value="BB">Barbados</option>
                <option value="BY">Belarus</option>
                <option value="BE">Belgium</option>
                <option value="BZ">Belize</option>
                <option value="BJ">Benin</option>
                <option value="BM">Bermuda</option>
                <option value="BT">Bhutan</option>
                <option value="BO">Bolivia, Plurinational State of</option>
                <option value="BQ">Bonaire, Sint Eustatius and Saba</option>
                <option value="BA">Bosnia and Herzegovina</option>
                <option value="BW">Botswana</option>
                <option value="BV">Bouvet Island</option>
                <option value="BR">Brazil</option>
                <option value="IO">British Indian Ocean Territory</option>
                <option value="BN">Brunei Darussalam</option>
                <option value="BG">Bulgaria</option>
                <option value="BF">Burkina Faso</option>
                <option value="BI">Burundi</option>
                <option value="KH">Cambodia</option>
                <option value="CM">Cameroon</option>
                <option value="CA">Canada</option>
                <option value="CV">Cape Verde</option>
                <option value="KY">Cayman Islands</option>
                <option value="CF">Central African Republic</option>
                <option value="TD">Chad</option>
                <option value="CL">Chile</option>
                <option value="CN">China</option>
                <option value="CX">Christmas Island</option>
                <option value="CC">Cocos (Keeling) Islands</option>
                <option value="CO">Colombia</option>
                <option value="KM">Comoros</option>
                <option value="CG">Congo</option>
                <option value="CD">Congo, the Democratic Republic of the</option>
                <option value="CK">Cook Islands</option>
                <option value="CR">Costa Rica</option>
                <option value="CI">Côte d'Ivoire</option>
                <option value="HR">Croatia</option>
                <option value="CU">Cuba</option>
                <option value="CW">Curaçao</option>
                <option value="CY">Cyprus</option>
                <option value="CZ">Czech Republic</option>
                <option value="DK">Denmark</option>
                <option value="DJ">Djibouti</option>
                <option value="DM">Dominica</option>
                <option value="DO">Dominican Republic</option>
                <option value="EC">Ecuador</option>
                <option value="EG">Egypt</option>
                <option value="SV">El Salvador</option>
                <option value="GQ">Equatorial Guinea</option>
                <option value="ER">Eritrea</option>
                <option value="EE">Estonia</option>
                <option value="ET">Ethiopia</option>
                <option value="FK">Falkland Islands (Malvinas)</option>
                <option value="FO">Faroe Islands</option>
                <option value="FJ">Fiji</option>
                <option value="FI">Finland</option>
                <option value="FR">France</option>
                <option value="GF">French Guiana</option>
                <option value="PF">French Polynesia</option>
                <option value="TF">French Southern Territories</option>
                <option value="GA">Gabon</option>
                <option value="GM">Gambia</option>
                <option value="GE">Georgia</option>
                <option value="DE">Germany</option>
                <option value="GH">Ghana</option>
                <option value="GI">Gibraltar</option>
                <option value="GR">Greece</option>
                <option value="GL">Greenland</option>
                <option value="GD">Grenada</option>
                <option value="GP">Guadeloupe</option>
                <option value="GU">Guam</option>
                <option value="GT">Guatemala</option>
                <option value="GG">Guernsey</option>
                <option value="GN">Guinea</option>
                <option value="GW">Guinea-Bissau</option>
                <option value="GY">Guyana</option>
                <option value="HT">Haiti</option>
                <option value="HM">Heard Island and McDonald Islands</option>
                <option value="VA">Holy See (Vatican City State)</option>
                <option value="HN">Honduras</option>
                <option value="HK">Hong Kong</option>
                <option value="HU">Hungary</option>
                <option value="IS">Iceland</option>
                <option value="IN">India</option>
                <option value="ID">Indonesia</option>
                <option value="IR">Iran, Islamic Republic of</option>
                <option value="IQ">Iraq</option>
                <option value="IE">Ireland</option>
                <option value="IM">Isle of Man</option>
                <option value="IL">Israel</option>
                <option value="IT">Italy</option>
                <option value="JM">Jamaica</option>
                <option value="JP">Japan</option>
                <option value="JE">Jersey</option>
                <option value="JO">Jordan</option>
                <option value="KZ">Kazakhstan</option>
                <option value="KE">Kenya</option>
                <option value="KI">Kiribati</option>
                <option value="KP">Korea, Democratic People's Republic of</option>
                <option value="KR">Korea, Republic of</option>
                <option value="KW">Kuwait</option>
                <option value="KG">Kyrgyzstan</option>
                <option value="LA">Lao People's Democratic Republic</option>
                <option value="LV">Latvia</option>
                <option value="LB">Lebanon</option>
                <option value="LS">Lesotho</option>
                <option value="LR">Liberia</option>
                <option value="LY">Libya</option>
                <option value="LI">Liechtenstein</option>
                <option value="LT">Lithuania</option>
                <option value="LU">Luxembourg</option>
                <option value="MO">Macao</option>
                <option value="MK">Macedonia, the former Yugoslav Republic of</option>
                <option value="MG">Madagascar</option>
                <option value="MW">Malawi</option>
                <option value="MY">Malaysia</option>
                <option value="MV">Maldives</option>
                <option value="ML">Mali</option>
                <option value="MT">Malta</option>
                <option value="MH">Marshall Islands</option>
                <option value="MQ">Martinique</option>
                <option value="MR">Mauritania</option>
                <option value="MU">Mauritius</option>
                <option value="YT">Mayotte</option>
                <option value="MX">Mexico</option>
                <option value="FM">Micronesia, Federated States of</option>
                <option value="MD">Moldova, Republic of</option>
                <option value="MC">Monaco</option>
                <option value="MN">Mongolia</option>
                <option value="ME">Montenegro</option>
                <option value="MS">Montserrat</option>
                <option value="MA">Morocco</option>
                <option value="MZ">Mozambique</option>
                <option value="MM">Myanmar</option>
                <option value="NA">Namibia</option>
                <option value="NR">Nauru</option>
                <option value="NP">Nepal</option>
                <option value="NL">Netherlands</option>
                <option value="NC">New Caledonia</option>
                <option value="NZ">New Zealand</option>
                <option value="NI">Nicaragua</option>
                <option value="NE">Niger</option>
                <option value="NG">Nigeria</option>
                <option value="NU">Niue</option>
                <option value="NF">Norfolk Island</option>
                <option value="MP">Northern Mariana Islands</option>
                <option value="NO">Norway</option>
                <option value="OM">Oman</option>
                <option value="PK">Pakistan</option>
                <option value="PW">Palau</option>
                <option value="PS">Palestinian Territory, Occupied</option>
                <option value="PA">Panama</option>
                <option value="PG">Papua New Guinea</option>
                <option value="PY">Paraguay</option>
                <option value="PE">Peru</option>
                <option value="PH">Philippines</option>
                <option value="PN">Pitcairn</option>
                <option value="PL">Poland</option>
                <option value="PT">Portugal</option>
                <option value="PR">Puerto Rico</option>
                <option value="QA">Qatar</option>
                <option value="RE">Réunion</option>
                <option value="RO">Romania</option>
                <option value="RU">Russian Federation</option>
                <option value="RW">Rwanda</option>
                <option value="BL">Saint Barthélemy</option>
                <option value="SH">Saint Helena, Ascension and Tristan da Cunha</option>
                <option value="KN">Saint Kitts and Nevis</option>
                <option value="LC">Saint Lucia</option>
                <option value="MF">Saint Martin (French part)</option>
                <option value="PM">Saint Pierre and Miquelon</option>
                <option value="VC">Saint Vincent and the Grenadines</option>
                <option value="WS">Samoa</option>
                <option value="SM">San Marino</option>
                <option value="ST">Sao Tome and Principe</option>
                <option value="SA">Saudi Arabia</option>
                <option value="SN">Senegal</option>
                <option value="RS">Serbia</option>
                <option value="SC">Seychelles</option>
                <option value="SL">Sierra Leone</option>
                <option value="SG">Singapore</option>
                <option value="SX">Sint Maarten (Dutch part)</option>
                <option value="SK">Slovakia</option>
                <option value="SI">Slovenia</option>
                <option value="SB">Solomon Islands</option>
                <option value="SO">Somalia</option>
                <option value="ZA">South Africa</option>
                <option value="GS">South Georgia and the South Sandwich Islands</option>
                <option value="SS">South Sudan</option>
                <option value="ES">Spain</option>
                <option value="LK">Sri Lanka</option>
                <option value="SD">Sudan</option>
                <option value="SR">Suriname</option>
                <option value="SJ">Svalbard and Jan Mayen</option>
                <option value="SZ">Swaziland</option>
                <option value="SE">Sweden</option>
                <option value="CH">Switzerland</option>
                <option value="SY">Syrian Arab Republic</option>
                <option value="TW">Taiwan, Province of China</option>
                <option value="TJ">Tajikistan</option>
                <option value="TZ">Tanzania, United Republic of</option>
                <option value="TH">Thailand</option>
                <option value="TL">Timor-Leste</option>
                <option value="TG">Togo</option>
                <option value="TK">Tokelau</option>
                <option value="TO">Tonga</option>
                <option value="TT">Trinidad and Tobago</option>
                <option value="TN">Tunisia</option>
                <option value="TR">Turkey</option>
                <option value="TM">Turkmenistan</option>
                <option value="TC">Turks and Caicos Islands</option>
                <option value="TV">Tuvalu</option>
                <option value="UG">Uganda</option>
                <option value="UA">Ukraine</option>
                <option value="AE" selected="selected">United Arab Emirates</option>
                <option value="GB">United Kingdom</option>
                <option value="US">United States</option>
                <option value="UM">United States Minor Outlying Islands</option>
                <option value="UY">Uruguay</option>
                <option value="UZ">Uzbekistan</option>
                <option value="VU">Vanuatu</option>
                <option value="VE">Venezuela, Bolivarian Republic of</option>
                <option value="VN">Viet Nam</option>
                <option value="VG">Virgin Islands, British</option>
                <option value="VI">Virgin Islands, U.S.</option>
                <option value="WF">Wallis and Futuna</option>
                <option value="EH">Western Sahara</option>
                <option value="YE">Yemen</option>
                <option value="ZM">Zambia</option>
                <option value="ZW">Zimbabwe</option>
            </select>
            <input type="email" placeholder="Email Address" class="input_text" name="email" required="required">
            <!--<input type="text" placeholder="Subject" class="input_text">-->
        </div>
        <textarea name="comments" style="display: none;"></textarea>
        <!--<div >
            <textarea placeholder="Message"></textarea>
        </div>-->
        <div class="submit_div">
            <input type="submit" class="submit_contact" value="SEND" id="booking_form_submit">
        </div>
        <div class="submit_div" style="display: none;" id="submit_successfully">
            <p><span>Booking request has been successfully sent! Our team will email your booking confirmation within 24 hrs.</span></p>
        </div>
    </form>
    <div id="loader-wrap" style="display: none;"><img src="<?php echo ROOT_URL_BASE;?>../images/ajax-loader.gif" /></div>
</div>
<div class="remodal tandc-modal" data-remodal-id="modal">
    <button data-remodal-action="close" class="remodal-close"></button>

    <h2>TERMS & CONDITIONS<br/>RETREAT BOOKING</h2>

    <p>These terms and conditions (the "Terms and Conditions") apply to the provision of retreat bookings in the United Arab Emirates, Bali or any other place specified with hotel stays, spa access, airport transfers in Dubai Airports or Ngurah Rai International Airport in Denpasar, Bali  (Or place of Retreat) only (not in place of origin) and yoga classes  (the "Services" or "Retreats") that we will be providing to you ("You", "Your", "Yours", "Guest").</p>
    <p>These Services will be provided through our company, Elite Yoga retreats Middle East Ltd or via our website ("We", "Us", "Our", "Elite Yoga", "Elite Yoga Domains").  </p>
    <p>These Terms and Conditions form the entire binding agreement ("Agreement") between the parties relating to the Services being offered. </p>
    <p>This Agreement will be deemed to supersede and/or replace any previous written or oral contracts, communications or correspondence. </p>
    <p>Execution of the Agreement and/or use of the Services indicates your acknowledgment and unconditional acceptance of all Terms and Conditions set out herein. </p>
    <p>For the avoidance of doubt, the term "Retreat" or "Retreats" in this Agreement can refer to any kind of combination including varied and different hotel stays, transfers, yoga classes, activities.</p>

	<h4>1. BACKGROUND</h4>	
	 <p>1.1 Elite Yoga provides the Retreats for You to avail in any combination required or offered. </p>
     <p>1.2 You can book the total Retreat package with hotel stays, transfers, yoga classes and any other combination of Services via Our website. </p>
	
	<h4>2. EXCLUSIVITY</h4>	
     <p>2.1 This Agreement is non exclusive, whereby Elite Yoga retains the right to sell its Services and Retreats to other Guests. In a similar fashion, You are not bound to only use Our Services. </p>
     <p>2.2 All Retreats, itineraries and content related communication provided by Elite Yoga to the Guest through documents, e-mails, website content and other means of communication remain the Intellectual Property of Elite Yoga and may be only be used by the Guest within the Terms and Conditions of this Agreement only. We hold You to high levels of Confidentiality, and any breach of confidentiality shall be deemed a material breach of this Agreement.</p>
     <p>2.3 The Guest shall not use any of the products, content, Retreat designs and details or any Intellectual Property, created by Elite Yoga as per Clause 3.2, outside of this Agreement or with any other third parties in any capacity whatsoever.</p>

 	<h4>3. RENEWAL AND TERMINATION </h4>
     <p>3.1 This Agreement shall be renewed automatically at the end of the Term unless terminated by either party. </p>
     <p>3.2 Elite Yoga and/or the Guest shall have the right to terminate this Agreement at any time by giving the other party thirty (30) days' notice in writing.</p>
     <p>3.3 Elite Yoga reserves the right to terminate this Agreement immediately and without notice if there is a major breach in the terms of this Agreement by The Guest. </p>

 	<h4>4. PRIVACY POLICY</h4>
     <p>Elite Yoga must collect personal information from Clients in order to give effect to any booking, to deliver the Tours and any collateral services, to assist in evaluating such Tours. Elite Yoga takes care to safeguard all Client information and protect the privacy of all of our Clients. Elite Yoga collects, uses and discloses only that information reasonably required to enable us to provide the particular Product or service that you have requested as described in the Our Privacy Policy. In particular, Elite Yoga may share your information, as necessary, with our third-party suppliers and operators who deliver services or component parts of Product to Clients, in order to enable them to provide such products and services. All such third parties are bound by the terms of the Elite Yoga Privacy Policy. By submitting any personal information to Elite Yoga, Clients indicate their acceptance of the terms of Elite Yoga' Privacy Policy.</p>

 	<h4>5. RETREATS</h4>
     <p>5.1 The Retreats to be provided to the Guest will be outlined in each individual on the website. The details of which shall be specified for each Guest or combination of Services and for specific dates of your Retreat.</p> 
     <p>5.2 Dates of future Retreats or combination of Services shall be provided on the website. </p>
     <p>5.3 Elite Yoga is able to operate its Retreats according to the prices mentioned on the website and are subject to change.</p>

	<h4>6. LIMITS OF RESPONSIBILITY</h4>
     <p>6.1 The Retreat will begin after the Clients have passed Customs at the airport and collected their luggage and when contact is made between Elite Yoga and the Client at the arrivals lounge in the airport of arrival. Elite Yoga does not take any responsibility for the client for any Customs, Passport or Luggage related issues. Should any Client not be able to pass through Customs, their Retreat will be deemed cancelled and no refund shall be provided. Any costs of delay shall be borne by the Client. 
     <p>6.2 The Retreat will end at the moment of drop off of the Client at the airport of departure. Elite Yoga does not take any responsibility for the Client for any Customs, Passport or Luggage related issues at the airport of departure.</p>
     <p>6.3 Elite Yoga does not cover any refunds for missed flights or late arrivals and subsequently late start/no start of the Services or Tours. All additional costs have to be settled personally on the spot by the Client. </p>
     <p>6.4 Any disability requiring special attention must be reported to Elite Yoga at the time of booking the Services. Elite Yoga will not take responsibility for such disabilities causing issues with availing if the Services, and will make reasonable efforts to accommodate the special needs of disabled Clients, but is not responsible for any denial of services by carriers, hotels, lodges, restaurants, or other independent suppliers, nor any additional expenses incurred, which the Client shall be personally liable for. Motor coaches and minibuses are not equipped with wheelchair ramps. We regret we cannot provide individual assistance to Clients for walking, dining, getting on/off motor coaches and other transportation vehicles, or other personal needs. Clients who need assistance must be accompanied by a qualified and physically able companion. In addition, We do not take and responsibility of any injury caused by such disability.</p>
	<p><span style="text-decoration: underline; color: #777 !important;">6.5. Clients are responsible for assessing their own suitability and capability to participate in the Retreat such Client has booked. All Clients should consult their physician regarding their fitness for travel and activities such as Yoga. Elite Yoga encourages all Clients to seek their physician's advice regarding necessary or advisable vaccinations, medical precautions, or other medical concerns regarding the entirety of the Client's travel and the Retreat with Elite Yoga. Elite Yoga does not provide medical advice.</span> Certain types of yoga or positions may not be suitable for all people due to restrictions posed by limitations in mobility, physical or cognitive disability, pregnancy or various other physical or mental conditions. It is the Client's responsibility to assess the risks and requirements of each activity in light of such Client's limitations, physical and mental fitness and condition, and any medical requirements or issues of such Client.</p>
	<p>6.6 Elite Yoga, nor any person acting through or on their behalf, shall be liable for any loss or damage whatsoever in respect of person or property arising from any cause. We shall not be responsible for loss or damage caused by delays, sickness, accident, injury or death, nor for any extra expense incurred as a result thereof. </p> 
    
 	<h4>7. LOCATIONS OF SERVICES</h4>
     <p>7.1 Due to the nature of business and developments in the United Arab Emirates, Elite Yoga cannot guarantee the future availability of locations for the provision of Retreats. Therefore this Agreement does not guarantee the availability of locations specified on the website and Elite Yoga cannot be held responsible or liable if access to a location is denied. Elite Yoga will work with The Guest to keep them informed of location availability and alternatives or changes as soon as practical to do so.</p>

	<h4>7. BOOKINGS, FEES, PRICES AND PAYMENT</h4>
	<p>8.1 The Guest shall buy Elite Yoga Retreats at the prices mentioned on the website whereby any bank charges are to be paid by The Guest.</p>
	<p>8.2 The price of the Retreat will be as quoted on the website from time to time, except in cases of obvious error. Prices are liable to change at any time, but changes will not affect bookings already accepted.  Despite Our best efforts, some of the Retreats listed on the website may be incorrectly priced. Elite Yoga EXPRESSLY RESERVES THE RIGHT TO CORRECT ANY PRICING ERRORS ON OUR WEBSITE AND/OR ON PENDING RESERVATIONS MADE UNDER AN INCORRECT PRICE. IN SUCH EVENT, IF AVAILABLE, WE WILL OFFER YOU THE OPPORTUNITY TO KEEP YOUR PENDING RESERVATION AT THE CORRECT PRICE OR WE WILL CANCEL YOUR RESERVATION WITHOUT PENALTY. Elite Yoga is under no obligation to provide Retreats to you at an incorrect (lower) price, even after you have been sent confirmation of your booking.</p>
	<p>8.3 The Guest shall ensure bookings are communicated to Elite Yoga as soon as possible and no later than forty five (45) days prior to the date of arrival in order for appropriate arrangements to be made. In addition, flight details must be provided in advance for transfers to be arranged. </p>
	<p>8.4 Bookings shall be made online. </p>
	<p>8.5 Payment of all amounts will be in the currency specified on the booking website. The fees will be charged in United Arab Emirates Dirhams (AED) and will be reflected in the Client's bank account in the currency of choice, or the currency of the account being used. </p>
	<h4>9. CANCELLATION POLICY, NON-REFUNDABLE</h4> 
	<p>9.1	Elite Yoga Retreats are sometimes, by their very nature, weather dependent. If The Guest has booked a Retreat and Elite Yoga has to cancel it due to adverse weather conditions or other unexpected events or closures, there are two steps: 
	<p style="margin-left: 15px;">9.1.1 Elite Yoga will endeavour to reschedule the activity to another day or another activity within the same budget; or </p>
	<p style="margin-left: 15px;">9.1.2 If The Client cannot reschedule to another day or activity, then Eite Yoga will refund if Elite Yoga is refunded by the third party</p>
	<p>9.2 Elite Yoga will keep The Guest appraised of any weather conditions or other events and causes likely to alter a booking made by The Guest. This will be done, where possible, the day before the tour, or as soon as possible after it is brought to Our attention.</p>
	<p>9.3 Bookings and alterations are subject to written (email) confirmation and shall be subject to additional charges and availability, at Elite Yoga's discretion.</p>
	<p>9.4. In the case of "Force Majeure" Elite Yoga reserves the right to withdraw from any commitment. No valid claim for damages can be made in this case.</p>
	<p>9.5 You can cancel your Retreat however bookings are non-refundable.</p>

	<h4>10. INDEMNITY</h4> 
	<p>10.1 Clients using Elite Yoga Services will be required to sign waivers prior to undertaking a number of activities, specifically yoga classes, with third parties booked by Elite Yoga. </p>
	<p>10.2 Elite Yoga does not take any responsibility for the Clients in case the Client does not respect the local Laws and Customs.</p>
	<p>10.3 Elite Yoga does not take any responsibility for the Client for any Customs, Passport or Luggage immigration or visa related issues in any capacity.  It is the responsibility of the Guest to ensure they have checked whether they require a visa to enter either the UAE or Bali, or anywhere else that Elite Yoga offers its Retreats</p>
	<p>10.4 It is recommended that Clients have adequate insurance to cover personal accidents, medical expenses, loss of luggage etc. </p>
	<p>10.5 Some medications are not permitted to be brought into the UAE. Clients should check with their respective consulates, embassies and local health boards for the latest health requirements.</p> 
	<p>10.6 THE CARRIERS, HOTELS AND OTHER SUPLIERS PROVIDING TRAVEL OR OTHER SERVICES FOR ELITE YOGA ARE INDEPENDENT CONTRACTORS AND NOT AGENTS OR EMPLOYEES OF ELITE YOGA OR ITS AFFILIATES. ELITE YOGA AND ITS AFFILIATES ARE NOT LIABLE FOR THE ACTS, ERRORS, OMISSIONS, REPRESENTATIONS, WARRANTIES, BREACHES OR NEGLIGENCE OF ANY SUCH SUPPLIERS OR FOR ANY PERSONAL INJURIES, DEATH, PROPERTY DAMAGE, OR OTHER DAMAGES OR EXPENSES RESULTING THEREFROM. HOTELS.COM AND ITS AFFILIATES HAVE NO LIABILITY AND WILL MAKE NO REFUND IN THE EVENT OF ANY DELAY, CANCELLATION, OVERBOOKING, STRIKE, FORCE MAJEURE OR OTHER CAUSES BEYOND THEIR DIRECT CONTROL, AND THEY HAVE NO RESPONSIBILITY FOR ANY ADDITIONAL EXPENSE, OMISSIONS, DELAYS, RE-ROUTING OR ACTS OF ANY GOVERNMENT OR AUTHORITY. IN NO EVENT SHALL HOTELS.COM, ITS AFFILIATES, AND/OR THEIR RESPECTIVE SUPPLIERS BE LIABLE FOR ANY DIRECT, INDIRECT, PUNITIVE, INCIDENTAL, SPECIAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF, OR IN ANY WAY CONNECTED WITH, THE USE OF THIS WEBSITE OR WITH THE DELAY OR INABILITY TO USE THIS WEBSITE, OR FOR ANY INFORMATION, SOFTWARE, PRODUCTS, AND SERVICES OBTAINED THROUGH THIS WEBSITE, OR OTHERWISE ARISING OUT OF THE USE OF THIS WEBSITE, WHETHER BASED ON CONTRACT, TORT, STRICT LIABILITY, OR OTHERWISE, EVEN IF ELITE YOGA, ITS AFFILIATES, AND/OR THEIR RESPECTIVE SUPPLIERS HAVE BEEN ADVISED OF THE POSSIBILITY OF DAMAGES. BECAUSE SOME STATES/JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF LIABILITY FOR CONSEQUENTIAL OR INCIDENTAL DAMAGES, THE ABOVE LIMITATION MAY NOT APPLY TO YOU.</p>
    
	<h4>11. INSURANCE </h4> 
	<p>11.1	Elite Yoga has a Public Liability Insurance and the policy will be shown to The Guest upon request.</p> 

	<h4>12.	GOVERNING LAW & JURISDICTION</h4>
	<p>12.1	It is agreed that both Elite Yoga and The Guest are acting in utmost good faith and for mutual benefit. Both the parties undertake to try and settle disputes mutually and amicably.</p>  
	<p>12.2	Unresolved disputes, if any, shall be settled initially through negotiation and mediation. A mediator shall be appointed by Elite Yoga, to be agreed to by the Tour Operator or individual client.</p>  
	<p>12.3	Should any dispute not be settled within thirty (30) days, then it shall be settled by the courts of Dubai and in accordance with the laws of Dubai, United Arab Emirates.</p> 

	<h4>13. NOTICES </h4> 
	<p>Any and all notices, requests, demands and/or other communications required or desired to be given under this Agreement shall be in writing and shall be valid given or made to the other party if personally served, sent by electronic mail (e-mail), fax or registered post. If such notice or demand is served personally or by fax or e-mail, notice shall be deemed constructively made at the time of receipt of the document as noted personally or electronically. If such notice, demand or other communication is given by registered post; such notice shall be conclusively deemed given ten business days after mailing.</p>

	<h4>14. OTHER</h4> 

	<p>14.1	ENTIRE AGREEMENT: This Agreement (including the Annex, if any) is the only and whole Agreement between you and us relating to the Services and supersedes and replaces any prior written or oral agreements, representations or understandings between them. This clause is without prejudice to the provisions relating to changes or the parties' rights to conclude addenda or amendments to this Agreement.</p>

	<p>14.2	SEVERABILITY: In case any provision of this Agreement becomes invalid or unenforceable, the parties shall remain bound by the remainder of the Agreement and replace the invalid or unenforceable provisions with new provisions having a similar effect to the maximum extent possible.</p>


	<p>14.3	WAIVER: Neither failure nor delay by a party to enforce at any time any one or more of the terms or conditions of this Agreement shall operate as a waiver thereof, or of the right to subsequently enforce all terms and conditions of this Agreement.</p>

	<p>14.4	OPERATING HOURS: Elite Yoga office hours are from 9am to 6pm (UAE time) from Sunday to Thursday. Telephone bookings may only be made during this time. Email bookings will be answered during office hours.</p>
 
	<p>14.5	LANGUAGE: This Agreement is drawn up in the English language. Any translation into another language is for convenience and information purposes only. In case of conflict between the English language version and such translation, the English language version shall prevail. Headings in this Agreement are inserted for convenience only and shall not affect the interpretation or construction of this Agreement.</p>

	<p>14.6	CONFIDENTIALITY: You acknowledge that the Services contain confidential and highly sensitive material. You therefore agree to maintain in strict confidence any such and other business sensitive and confidential information (including, for the avoidance of doubt, the terms of this Agreement) and apply security measures no less stringent than the measures which you apply to protect your own like information, but not less than a reasonable degree of care, to prevent unauthorized disclosure and use of the confidential information. Disclosure is only allowed with explicit prior permission or as may be required by law.</p>

	<p>14.7	FORCE MAJEURE: Neither party shall be liable for any delay or default in performing hereunder if such delay or default is caused by conditions beyond its control including, but not limited to natural disasters, government restrictions, wars, terrorism, insurrections, nuclear incidents and/or any other cause beyond the control of the party whose performance is affected.</p>

<p>By making a Retreat booking through Elite Yoga, the Guest acknowledges that they have read this agreement and its schedules, understand them and agree to be bound by their terms and conditions. Further, they agree that this agreement and its schedules are the complete and exclusive statement of the agreement between them, superseding all proposals or other prior agreements, oral or written, and all other communications relating to the subject. In the event of any inconsistency or discrepancy between the provisions of this agreement and its schedules annexed hereto, the provisions of this agreement shall prevail over the attachments. </p>
    
    <br>
    <!-- <button data-remodal-action="cancel" class="remodal-cancel btn btn-shareit">Cancel</button> -->
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('.retreat_check_box').click(function(){
            if ($(this).is(':checked')) {
                var package_id = $(this).attr('package_id');
                $('.retreat_check_box[package_id="' + package_id + '"]').prop('checked', false);
                $(this).prop('checked', 'checked');
            }
        })
        $('.book_package_btn').click(function () {
            var package_id = $(this).attr('package_id');

            if ($('.retreat_check_box[package_id="'+package_id+'"]:checked').length < 1) {
                alert('Please select a package');
                return false;
            } else {
                var check_box_checked = $('.retreat_check_box[package_id="'+package_id+'"]:checked');
                var retreat_id = $(check_box_checked).attr('retreat_id');
                var option_id = $(check_box_checked).attr('option_id');
                //alert(package_id + '' + retreat_id + '' + option_id);

                //$('.retreat_check_box[package_id="'+package_id+'"]:checked')
                console.log('start_date_'+package_id+'_'+retreat_id+'_'+option_id);
                var start_date = $('[name="start_date_'+package_id+'_'+retreat_id+'_'+option_id+'"').val();
                var end_date = $('[name="end_date_'+package_id+'_'+retreat_id+'_'+option_id+'"').val();
                var price_dollar_selected = $('[name="price_dollar_selected_'+package_id+'_'+retreat_id+'_'+option_id+'"').val();
                var price_euro_selected = $('[name="price_euro_selected_'+package_id+'_'+retreat_id+'_'+option_id+'"').val();
                var retreat_id = $('[name="retreat_id_'+package_id+'_'+retreat_id+'_'+option_id+'"').val();

                var package_name = $('[name="package_name_'+package_id+'_'+retreat_id+'_'+option_id+'"').val();
                var date_text = $('[name="date_text_'+package_id+'_'+retreat_id+'_'+option_id+'"').val();
                var price_text = $('[name="price_text_'+package_id+'_'+retreat_id+'_'+option_id+'"').val();
                var booking_details = '<p class="booking_details">Package:<span>'+package_name+'</span> </p>';
                booking_details += '<p class="booking_details">Date:<span>'+date_text+'</span> </p>';
                booking_details += '<p class="booking_details">Price:<span>'+price_text+'</span> </p>';

                $('#booking_popup_wrap').find('[name="package_id"]').val(package_id);
                $('#booking_popup_wrap').find('[name="start_date"]').val(start_date);
                $('#booking_popup_wrap').find('[name="end_date"]').val(end_date);
                $('#booking_popup_wrap').find('[name="retreat_id"]').val(retreat_id);
                $('#booking_popup_wrap').find('[name="price_dollar_selected"]').val(price_dollar_selected);
                $('#booking_popup_wrap').find('[name="price_euro_selected"]').val(price_euro_selected);
                $('#booking_popup_wrap').find('[name="comments"]').val(booking_details);

                $('#booking_popup_wrap').find('#package_name').html(package_name);
                $('#booking_popup_wrap').find('#package_date').html(date_text);
                $('#booking_popup_wrap').find('#package_price').html(price_text);

                $('#submit_successfully').hide();
                var options = {};
                //var inst = $('[data-remodal-id=popup_booking]').remodal();
                var inst = $('#booking_popup_wrap').remodal();

                /**
                 * Opens the modal window
                 */
                inst.open();


            }


            return false;
        })
        $('#booking_form').submit(function(){
            var booking_for_data = $('#booking_form').serialize();
            //booking_for_data
            $('#booking_form').find('input[type="text"],input[type="email"]').prop('disabled', true);
            $('#loader-wrap').show();

            jQuery.ajax({
                type: "POST",
                url: $('#rootUrlLink').val() + 'subscribe',
                dataType: 'json',
                data: booking_for_data,
                cache: false,
                success: function(data) {
                    $('#loader-wrap').hide();
                    var status = data.status;
                    var msg = data.message;
                    if(status*1 == 1)
                    {
                        $('#booking_form').find('input[type="text"],input[type="email"]').val('').prop('disabled', false);
                        $('#submit_successfully').show();
                    }
                    else
                    {
                        alert('Something went wrong!!\n\nPlease try again later');
                        $('#booking_form').find('input[type="text"],input[type="email"]').prop('disabled', false);
                    }
                },
                error: function(){
                    $('#loader-wrap').hide();
                    alert('Something went wrong!!\n\nPlease try again later');
                    $('#booking_form').find('input[type="text"],input[type="email"]').prop('disabled', false);
                }
            });
            return false;
        })
    })
</script>
<!-- End modal HTML -->
</body>

</html>
