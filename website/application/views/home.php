<div class="left_area home_page"><?php
    $featuredPosts = $this->contentData['featuredPosts'];
    if (!empty($featuredPosts) && !empty($featuredPosts)){?>
    <h1 class="heading_h1"><span>Featured Posts</span></h1>
    <ul class="slider_home"><?php
        foreach ($featuredPosts as $post) {
            ?>
            <li>
            <?php
			if (!empty($post->featured_banner) && file_exists(DIR_UPLOAD_BANNER.$post->featured_banner)){?>
                <a href="<?php echo ROOT_URL.'read-post/'.$post->classified_slug;?>"><img class="featured-post-img" src="<?php echo ROOT_URL_BASE;?>assets/timthumb.php?src=<?php echo DIR_UPLOAD_BANNER_SHOW.$post->featured_banner;?>&q=100&w=830"/></a>
            <?php } else if (!empty($post->banner_image) && file_exists(DIR_UPLOAD_BANNER.$post->banner_image)){?>
            <a href="<?php echo ROOT_URL.'read-post/'.$post->classified_slug;?>"><img class="featured-post-img" src="<?php echo ROOT_URL_BASE;?>assets/timthumb.php?src=<?php echo DIR_UPLOAD_BANNER_SHOW.$post->banner_image;?>&q=100&w=830"/></a>
            <?php }?>

            <div class="slider_text featured-posts">
                <div class="border_div">
                    <div class="border_div">
                        <div class="text_div2">
                            <h4><?php if (!empty($post->section_id)) {
                                $sectionId = $post->section_id;
                                if (isset($this->headerData['navCategories'][$sectionId]['main']->title) && !empty($this->headerData['navCategories'][$sectionId]['main']->title)) {?>
                                    <a href="<?php echo ROOT_URL.'posts/'.$this->headerData['navCategories'][$sectionId]['main']->url_slug;?>"><?php echo $this->headerData['navCategories'][$sectionId]['main']->title;?></a>
                                <?php
                                }
                            }
                                ?></h4>

                            <h2><a href="<?php echo ROOT_URL.'read-post/'.$post->classified_slug;?>"><?php echo !empty($post->title) ? content_truncate($post->title, 60, ' ', '...') : '';?></a></h2>

                            <p class="AGaramondPro"><?php echo !empty($post->description) ? content_truncate($post->description, 75, ' ', '...') : '';?></p>
                            <a href="<?php echo ROOT_URL.'read-post/'.$post->classified_slug;?>" class="see_more">See More</a></div>
                    </div>
                </div>
            </div>
            <?php echo showEditPost($post);?>
            </li><?php
        }
        ?>
    </ul><?php
    }?>
    <?php
    $homePagePosts = $this->contentData['homePagePosts'];
    if (!empty($homePagePosts) && is_array($homePagePosts)) {
        $i = 0;
        ?><div class="home-post-wrap"><?php
        foreach ($homePagePosts as $post) {
            if (!empty($post) && is_array($post)) {
                $i++;?>
                <div class="home_category_list <?php echo ($i % 2 == 0 ) ? 'div_right' : '';?>">
                <h1 class="heading_h1"><span><a href="<?php echo ROOT_URL.'posts/'.$post[0]->category->url_slug;?>"><?php echo (!empty($post[0]) && !empty($post[0]->category) && !empty($post[0]->category->title)) ? $post[0]->category->title : '';?></a></span></h1>
                <?php 
				if (!empty($post[0]->banner_image) && file_exists(DIR_UPLOAD_BANNER.$post[0]->banner_image)){?>
                    <a href="<?php echo ROOT_URL.'read-post/'.$post[0]->classified_slug;?>"><img class="home-post-img" src="<?php echo DIR_UPLOAD_BANNER_SHOW.$post[0]->banner_image;?>"/></a>
                <?php }?>
                <?php echo showEditPost($post[0], 'right25px bottom25px');?>
                <div class="home_category_text">
                    <div class="border_div">
                        <div class="border_div">
                            <div class="text_div">
                                <?php if (!empty($post[0]->description)) {?>
                                <h2><a href="<?php echo ROOT_URL.'read-post/'.$post[0]->classified_slug;?>"><?php echo !empty($post[0]->title) ? content_truncate($post[0]->title, 60, ' ', '...') : ''; ?></a></h2>

                                <p class="AGaramondPro home-categ-border"><?php echo !empty($post[0]->description) ? content_truncate($post[0]->description, 30, ' ', '...') : '';?>
                                    <a href="<?php echo ROOT_URL.'read-post/'.$post[0]->classified_slug;?>" class="read_more">Read More</a></p>
                                <?php }?>

                                <?php if (!empty($post[1]->description)) {?>
                                <h2><a href="<?php echo ROOT_URL.'read-post/'.$post[1]->classified_slug;?>"><?php echo !empty($post[1]->title) ? content_truncate($post[1]->title, 60, ' ', '...') : ''; ?></a></h2>

                                <p class="AGaramondPro home-categ-border"><?php echo !empty($post[1]->description) ? content_truncate($post[1]->description, 30, ' ', '...') : '';?>
                                    <a href="<?php echo ROOT_URL.'read-post/'.$post[1]->classified_slug;?>" class="read_more">Read More</a></p>
                                <?php }?>

                                <?php if (!empty($post[2]->description)) {?>
                                <h2><a href="<?php echo ROOT_URL.'read-post/'.$post[2]->classified_slug;?>"><?php echo !empty($post[2]->title) ? content_truncate($post[2]->title, 60, ' ', '...') : ''; ?></a></h2>

                                <p class="AGaramondPro home-categ-border"><?php echo !empty($post[2]->description) ? content_truncate($post[2]->description, 30, ' ', '...') : '';?>
                                    <a href="<?php echo ROOT_URL.'read-post/'.$post[2]->classified_slug;?>" class="read_more">Read More</a></p>
                                <?php }?>
                                <a href="<?php echo ROOT_URL.'posts/'.((!empty($post[0]->category) && !empty($post[0]->category->url_slug)) ? $post[0]->category->url_slug : '');?>" class="see_more">View All</a>

                            </div>
                        </div>
                    </div>
                </div>
                </div><?php
                echo ($i %2 == 0) ? '<div class="clearfix"></div> </div><div class="home-post-wrap">' : '';
            }
        }
        ?></div><?php
    }
    ?>
</div>
