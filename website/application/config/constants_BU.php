<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);
define('MAX_BANNER_IMAGE_SIZE',200);
define('MAX_FUN_IMAGE_SIZE',1000);
define('MAX_EVENTS_IMAGE_SIZE',3072);

define('IMAGE_ALLOWED_TYPES','gif|jpg|png|jpeg');
define('RECORD_PER_PAGE',10);
define('TIPS_PER_PAGE',10); 
define('EVENTS_PER_PAGE',10);
define('ATTENDEE_PER_PAGE',10);
define('PROJECT_NAME', 'MomsUnited');
define('SITE_NAME', 'MomsUnited');
define('DEFAULT_EMAIL', 'info@momsunited.com');
//define('DEFAULT_EMAIL', 'anas.muhammed@gmail.com');
define('RECAPTCHA_SECRET', '6LdxVwYTAAAAAE2-GhtXSeqExdvMtqZcyMShbvNO');
define('RECAPTCHA_SITE_KEY', '6LdxVwYTAAAAAAtgFyz02zMwdtHBS26rIu0hRJcl');
define('DEFAULT_COUNTRY_ID', 225);

define('ROOT_PATH', ROOT_DIRECTORY);
if (empty($_SERVER['HTTP_HOST'])) {
    define('ROOT_URL', 'http://192.168.1.120:9091/');
    define('ROOT_URL_BASE', 'http://192.168.1.120:9091/');
} else {
    define('ROOT_URL', 'http://'.$_SERVER['HTTP_HOST'].'/');
    define('ROOT_URL_BASE', 'http://'.$_SERVER['HTTP_HOST']. '/');
}
define('LOG_ENTRY', TRUE);
//define('ROOT_URL_BASE', 'http://'.$_SERVER['SERVER_NAME'].'/');

define('CSS_PATH', ROOT_URL_BASE.'css/' );
define('JS_PATH', ROOT_URL_BASE.'js/' );

define('ADMIN_ROOT_URL', ROOT_URL.'administrator/' );
define('ADMIN_ROOT_PATH', ROOT_URL_BASE.'administrator/' );
define('ADMIN_CSS_PATH', CSS_PATH.'admin/' );
define('ADMIN_JS_PATH', JS_PATH.'admin/' );

define('MEMBER_ROOT_URL', ROOT_URL.'member/' );
define('MEMBER_ROOT_PATH', ROOT_PATH.'member/' );
define('MEMBER_CSS_PATH', CSS_PATH.'member/' );
define('MEMBER_JS_PATH', JS_PATH.'member/' );

define('DIR_UPLOAD_BANNER', ROOT_PATH.'uploads/banners/');
define('DIR_UPLOAD_BANNER_SHOW', ROOT_URL_BASE.'uploads/banners/');
define('DIR_UPLOAD_CLASSIFIED', ROOT_PATH.'uploads/classifieds/');
define('DIR_UPLOAD_CLASSIFIED_SHOW', ROOT_URL_BASE.'uploads/classifieds/');
define('DIR_UPLOAD_FUN', ROOT_PATH.'uploads/funWithMum/');
define('DIR_UPLOAD_FUN_SHOW', ROOT_URL_BASE.'uploads/funWithMum/');
define('DIR_UPLOAD_ADVERTIZE', ROOT_PATH.'uploads/advertize/');
define('DIR_UPLOAD_ADVERTIZE_SHOW', ROOT_URL_BASE.'uploads/advertize/');
define('DIR_UPLOAD_EVENTS', ROOT_PATH.'uploads/events/');
define('DIR_UPLOAD_EVENTS_SHOW', ROOT_URL_BASE.'uploads/events/');
/*
|--------------------------------------------------------------------------
| Forum table names
|--------------------------------------------------------------------------
|
| These modes are table names used in forum
|
*/
define('TBL_CATEGORIES', 'cibb_categories');
define('TBL_POSTS',      'cibb_posts');
define('TBL_THREADS',    'cibb_threads');
define('TBL_USERS',      'cibb_users');
define('TBL_ROLES',      'cibb_roles');
define('CIBB_TITLE',     ' &mdash; Moms United Forum');

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');
/*
|--------------------------------------------------------------------------
| SMTP configurations
|--------------------------------------------------------------------------
|
| These values are used for esnding emails
|
*/
define('DEFAULT_FROM_EMAIL', 'info@momsunited.com');
define('DEFAULT_FROM_NAME', 'MomsUnited');
define('SMTP_PASSWORD', 'Q56pek5%u6');

/*
|--------------------------------------------------------------------------
| Tips and tricks section id
|--------------------------------------------------------------------------
|
| These values are used to identify Tips and tricks section
|
*/
define('RECIPES_ID', 14);
define('TOP_PAEDS_ID', 15);
define('BRESTFEEDING_ID', 2);

/*
|--------------------------------------------------------------------------
| Recipes sub-section id
|--------------------------------------------------------------------------
|
| These values are used to identify recipes section
|
*/
define('POSITIONS_ID', 8);

/*
|--------------------------------------------------------------------------
| Tips and tricks url slug section
|--------------------------------------------------------------------------
|
| These values are used to identify Tips and tricks section
|
*/
define('TOP_PAEDS_URL', 'top-paeds');
define('RECIPES_URL', 'recipes');
define('BREAST_FEEDING_URL', 'breast-feeding');
/* End of file constants.php */
/* Location: ./application/config/constants.php */