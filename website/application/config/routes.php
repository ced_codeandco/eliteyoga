<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/




$route['default_controller'] = 'administrator/cms';
$route['posts'] = 'home/index/';
$route['posts/(:any)'] = 'home/category/$1';
$route['read-post/(:any)'] = 'home/read_post/$1';
$route['contact-us'] = 'home/contact_us';
$route['contact-us/(:any)'] = 'home/contact_us/$1';
$route['subscribe'] = 'home/subscribe';
$route['search'] = 'home/search';
$route['packages'] = 'home/packages';
$route['packages2'] = 'home/packages2';
$route['payment'] = 'home/payment';
$route['payment/(:any)'] = 'home/payment/$1';
$route['success/(:any)'] = 'home/success/$1';
$route['thankyou/(:any)'] = 'home/thankyou/$1';
$route['payment_failed/(:any)'] = 'home/payment_failed/$1';

# For demo only
$route['demo/elite_yoga/packages'] = 'home/packages';
$route['demo/elite_yoga/payment'] = 'home/payment';
$route['demo/elite_yoga/payment/(:any)'] = 'home/payment/$1';
$route['demo/elite_yoga/success/(:any)'] = 'home/success/$1';
$route['demo/elite_yoga/thankyou/(:any)'] = 'home/thankyou/$1';
$route['demo/elite_yoga/payment_failed/(:any)'] = 'home/payment_failed/$1';

#$route['cms/(:any)'] = 'cms/index/$1';



/*$route['search/(:any)'] = 'search/index/$1';*/
/*$route['cms/(:any)'] = 'cms/index/$1';*/

/*$route['create_classified'] = 'home/create_classified';
$route['create_classified'] = 'home/create_classified';
$route['create_classified'] = 'home/create_classified';
$route['create_classified'] = 'home/create_classified';*/
$route['404_override'] = '';

//$route['login'] = 'home/login/';

$route['administrator'] = 'administrator/dashboard';
$route['administrator/login'] = 'administrator/dashboard/login';
$route['administrator/admin_profile'] = 'administrator/dashboard/admin_profile';
$route['administrator/forgot_password'] = 'administrator/dashboard/forgot_password';
$route['administrator/reset_password/(:any)'] = 'administrator/dashboard/reset_password/$1';
$route['administrator/no_access'] = 'administrator/dashboard/no_access';
$route['administrator/logout'] = 'administrator/dashboard/logout';
$route['administrator/(:any)'] = 'administrator/$1';

$route['contact_us'] = 'home/contact_us';
$route['(:any)'] = 'cms/index/$1';


/* End of file routes.php */
/* Location: ./application/config/routes.php */