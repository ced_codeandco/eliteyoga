<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');



/*

|--------------------------------------------------------------------------

| File and Directory Modes

|--------------------------------------------------------------------------

|

| These prefs are used when checking and setting modes when working

| with the file system.  The defaults are fine on servers with proper

| security, but you may wish (or even need) to change the values in

| certain environments (Apache running a separate process for each

| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should

| always be used to set the mode correctly.

|

*/

define('FILE_READ_MODE', 0644);

define('FILE_WRITE_MODE', 0666);

define('DIR_READ_MODE', 0755);

define('DIR_WRITE_MODE', 0777);

define('MAX_BANNER_IMAGE_SIZE',2560);

define('MAX_FUN_IMAGE_SIZE',1000);

define('MAX_EVENTS_IMAGE_SIZE',3072);



define('IMAGE_ALLOWED_TYPES','gif|jpg|png|jpeg');

define('HOME_CATEGORY_POST_COUNT', 3);

define('SEARCH_RESULTS_PER_PAGE',6);

define('RECORD_PER_PAGE',7);

define('RELATED_ARTICLE_COUNT', 5);

define('TIPS_PER_PAGE',10);

define('EVENTS_PER_PAGE',10);

define('ATTENDEE_PER_PAGE',10);

define('PROJECT_NAME', 'EliteYoga');

define('SITE_NAME', 'EliteYoga');

//define('DEFAULT_EMAIL', 'admin@eliteyoga.ae');

define('DEFAULT_EMAIL', 'elite.yoga.me@gmail.com');

define('RECAPTCHA_SECRET', '6LfrxgwTAAAAALJKZHar74xX4uaSWrpB05Ntn_eU');

define('RECAPTCHA_SITE_KEY', '6LfrxgwTAAAAAGX6EohqQj9OUrvqKjZxo0J_1zj_');

define('DEFAULT_COUNTRY_ID', 225);


define('PAYPAL_IS_LIVE', '0');
define('PAYPAL_PAYMENT_URL_LIVE', 'https://www.paypal.com/cgi-bin/webscr');
define('PAYPAL_PAYMENT_URL_SANDBOX', 'https://www.sandbox.paypal.com/cgi-bin/webscr');
define('PAYPAL_PAYMENT_EMAIL', 'jassi-facilitator@codeandco.ae');


define('ROOT_PATH', ROOT_DIRECTORY);

if (empty($_SERVER['HTTP_HOST'])) {

    define('ROOT_URL', 'http://codeandcode.co/website/');

    define('ROOT_URL_BASE', 'http://codeandcode.co/website/');

} else {

    define('ROOT_URL', 'http://'.$_SERVER['HTTP_HOST'].'/demo/elite_yoga/website/');

    define('ROOT_URL_BASE', 'http://'.$_SERVER['HTTP_HOST']. '/demo/elite_yoga/website/');
    define('FRONTEND_URL', 'http://'.$_SERVER['HTTP_HOST']. '/demo/elite_yoga/');

}

define('LOG_ENTRY', TRUE);

//define('ROOT_URL_BASE', 'http://'.$_SERVER['SERVER_NAME'].'/');



define('CSS_PATH', ROOT_URL_BASE.'css/' );

define('JS_PATH', ROOT_URL_BASE.'js/' );



define('ADMIN_ROOT_URL', ROOT_URL.'administrator/' );

define('ADMIN_ROOT_PATH', ROOT_URL_BASE.'administrator/' );

define('ADMIN_CSS_PATH', CSS_PATH.'admin/' );

define('ADMIN_JS_PATH', JS_PATH.'admin/' );



define('MEMBER_ROOT_URL', ROOT_URL.'member/' );

define('MEMBER_ROOT_PATH', ROOT_PATH.'member/' );

define('MEMBER_CSS_PATH', CSS_PATH.'member/' );

define('MEMBER_JS_PATH', JS_PATH.'member/' );



define('DIR_UPLOAD_BANNER', ROOT_PATH.'uploads/banners/');

define('DIR_UPLOAD_BANNER_SHOW', ROOT_URL_BASE.'uploads/banners/');

define('DIR_UPLOAD_CLASSIFIED', ROOT_PATH.'uploads/classifieds/');

define('DIR_UPLOAD_CLASSIFIED_SHOW', ROOT_URL_BASE.'uploads/classifieds/');

define('DIR_UPLOAD_FUN', ROOT_PATH.'uploads/funWithMum/');

define('DIR_UPLOAD_FUN_SHOW', ROOT_URL_BASE.'uploads/funWithMum/');

define('DIR_UPLOAD_ADVERTIZE', ROOT_PATH.'uploads/advertize/');

define('DIR_UPLOAD_ADVERTIZE_SHOW', ROOT_URL_BASE.'uploads/advertize/');

define('DIR_UPLOAD_EVENTS', ROOT_PATH.'uploads/events/');

define('DIR_UPLOAD_EVENTS_SHOW', ROOT_URL_BASE.'uploads/events/');





/*

|--------------------------------------------------------------------------

| File Stream Modes

|--------------------------------------------------------------------------

|

| These modes are used when working with fopen()/popen()

|

*/



define('FOPEN_READ',							'rb');

define('FOPEN_READ_WRITE',						'r+b');

define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care

define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care

define('FOPEN_WRITE_CREATE',					'ab');

define('FOPEN_READ_WRITE_CREATE',				'a+b');

define('FOPEN_WRITE_CREATE_STRICT',				'xb');

define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

/*

|--------------------------------------------------------------------------

| SMTP configurations

|--------------------------------------------------------------------------

|

| These values are used for esnding emails

|

*/

define('ERROR_REPORTING_EMAIL', 'elite.yoga.me@gmail.com');
define('DEFAULT_FROM_EMAIL', 'elite.yoga.me@gmail.com');

define('DEFAULT_FROM_NAME', 'EliteYoga');

define('SMTP_PASSWORD', 'eliteyoga2016');



/*

|--------------------------------------------------------------------------

| CMS home page id

|--------------------------------------------------------------------------

|

| These values are used to identify home page content in cms

|

*/

define('CMS_HOME_PAGE_ID', 1);
define('CMS_CONTENT_PAGE_ID', 2);
define('CMS_NETWORK_PAGE_ID', 3);
define('CMS_STUDIOS_PAGE_ID', 4);
define('CMS_CONTACT_PAGE_ID', 5);




/*

|--------------------------------------------------------------------------

| Tips and tricks section id

|--------------------------------------------------------------------------

|

| These values are used to identify Tips and tricks section

|

*/

define('CLASSIFIEDS_ID', 1);

define('EVENTS_ID', 2);

define('TOP_PAEDS_ID', 3);

define('RECIPES_ID', 4);

define('BRESTFEEDING_ID', 5);

define('FUNWITHMOM_ID', 6);

define('NURSERIES_ID', 30);

/*

|--------------------------------------------------------------------------

| COMPETITONS CATEGORY ID

|--------------------------------------------------------------------------

|

| These are category id of competitions

|

*/

define('CATEG_COMPETION_ID', 32);
define('CATEG_TOP_10_ID', 52);

/*

|--------------------------------------------------------------------------

| Banner image sizes

|--------------------------------------------------------------------------

|

|

*/
define('IMG_BANNER_WIDTH', 820);
define('IMG_BANNER_HEIGHT', 500);


/* End of file constants.php */

/* Location: ./application/config/constants.php */