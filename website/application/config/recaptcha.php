<?php

// To use reCAPTCHA, you need to sign up for an API key pair for your site.
// link: http://www.google.com/recaptcha/admin
$config['recaptcha_site_key'] = '6LdxVwYTAAAAAAtgFyz02zMwdtHBS26rIu0hRJcl';
$config['recaptcha_secret_key'] = '6LdxVwYTAAAAAE2-GhtXSeqExdvMtqZcyMShbvNO';

// reCAPTCHA supported 40+ languages listed here:
// https://developers.google.com/recaptcha/docs/language
$config['recaptcha_lang'] = 'en';

/* End of file recaptcha.php */
/* Location: ./application/config/recaptcha.php */
