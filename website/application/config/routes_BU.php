<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/




$route['default_controller'] = 'search';
$route['search/(:any)'] = 'search/index/$1';
$route['cms/(:any)'] = 'cms/index/$1';

/*$route['create_classified'] = 'home/create_classified';
$route['create_classified'] = 'home/create_classified';
$route['create_classified'] = 'home/create_classified';
$route['create_classified'] = 'home/create_classified';*/
$route['404_override'] = '';

//$route['login'] = 'home/login/';

$route['administrator'] = 'administrator/dashboard';
$route['administrator/login'] = 'administrator/dashboard/login';
$route['administrator/admin_profile'] = 'administrator/dashboard/admin_profile';
$route['administrator/forgot_password'] = 'administrator/dashboard/forgot_password';
$route['administrator/reset_password/(:any)'] = 'administrator/dashboard/reset_password/$1';
$route['administrator/no_access'] = 'administrator/dashboard/no_access';
$route['administrator/logout'] = 'administrator/dashboard/logout';

$route['member'] = 'member/dashboard';
$route['login/ajax'] = 'member/dashboard/login';
$route['login/ajax'] = 'member/dashboard/login';
$route['register/ajax'] = 'register';
$route['home/login'] = 'member/dashboard/login';
$route['contact_us'] = 'home/contact_us';
$route['member/profile'] = 'member/dashboard/profile';
$route['member/forgot_password'] = 'member/dashboard/forgot_password';
$route['member/reset_password/(:any)'] = 'member/dashboard/reset_password/$1';
$route['member/verify_email/(:any)'] = 'member/dashboard/verify_email/$1';
$route['my_adds'] = 'member/dashboard/my_adds';
$route['member/my_searches'] = 'member/dashboard/my_searches';
$route['member/my_profile'] = 'member/dashboard/my_profile';
$route['member/account_settings'] = 'member/dashboard/account_settings';
$route['create_classified'] = 'member/dashboard/create_classified';
$route['create_classified/(:any)'] = 'member/dashboard/create_classified/$1';
$route['details/(:any)'] = 'search/details/$1';
$route['details'] = 'member/dashboard/details';
$route['facebookLogin'] = 'member/dashboard/facebookLogin';
$route['tips_and_tricks/details/(:any)'] = 'tips_and_tricks/tips_details/$1';

$route['my-mission'] = 'cms/index/my-mission';
$route['forum'] = 'thread/index/0';
$route['forum/(:num)'] = 'thread/index/$1';
$route['forum/(:any)'] = 'thread/$1';

$route['tips_and_tricks/(:any)'] = 'tips_and_tricks/index/$1';
$route['tips_and_tricks/(:any)/(:any)'] = 'tips_and_tricks/index/$1/$2';
$route['tips_and_tricks/(:any)/(:any)/(:any)'] = 'tips_and_tricks/index/$1/$2/$3';
$route['tips_and_tricks/(:any)/(:any)/(:any)/(:any)'] = 'tips_and_tricks/index/$1/$2/$3/$4';

$route[TOP_PAEDS_URL.'/details/(:any)'] = 'tips_and_tricks/tips_details/$1';
$route[TOP_PAEDS_URL.'/(:any)'] = 'tips_and_tricks/index/$1';
$route[TOP_PAEDS_URL.'/(:any)/(:any)'] = 'tips_and_tricks/index/$1/$2';
$route[TOP_PAEDS_URL.'/(:any)/(:any)/(:any)'] = 'tips_and_tricks/index/$1/$2/$3';
$route[TOP_PAEDS_URL.'/(:any)/(:any)/(:any)/(:any)'] = 'tips_and_tricks/index/$1/$2/$3/$4';

$route[RECIPES_URL.'/details/(:any)'] = 'tips_and_tricks/tips_details/$1';
$route[RECIPES_URL.'/(:any)'] = 'tips_and_tricks/index/$1';
$route[RECIPES_URL.'/(:any)/(:any)'] = 'tips_and_tricks/index/$1/$2';
$route[RECIPES_URL.'/(:any)/(:any)/(:any)'] = 'tips_and_tricks/index/$1/$2/$3';
$route[RECIPES_URL.'/(:any)/(:any)/(:any)/(:any)'] = 'tips_and_tricks/index/$1/$2/$3/$4';

$route[BREAST_FEEDING_URL.'/details/(:any)'] = 'tips_and_tricks/tips_details/$1'; 
$route[BREAST_FEEDING_URL.'/(:any)'] = 'tips_and_tricks/index/$1';
$route[BREAST_FEEDING_URL.'/(:any)/(:any)'] = 'tips_and_tricks/index/$1/$2';
$route[BREAST_FEEDING_URL.'/(:any)/(:any)/(:any)'] = 'tips_and_tricks/index/$1/$2/$3';
$route[BREAST_FEEDING_URL.'/(:any)/(:any)/(:any)/(:any)'] = 'tips_and_tricks/index/$1/$2/$3/$4';

$route['fun_with_mum/(:any)'] = 'fun_with_mum/index/$1';  
$route['events/list'] = 'events/index';
$route['events/list/(:any)'] = 'events/index/$1';

$route['rate_tip'] = 'tips_and_tricks/rate_tip';
$route['fun_details/(:any)'] = 'fun_with_mum/fun_details/$1';
$route['rate_fun'] = 'fun_with_mum/rate_fun';

$route['member/no_access'] = 'member/dashboard/no_access';
$route['member/logout'] = 'member/dashboard/logout';



/*
$route['administrator/admin'] = 'administrator/admin';
$route['administrator/admin/add'] = 'administrator/admin/add';
$route['administrator/admin/add/(:any)'] = 'administrator/admin/add/$1';
$route['administrator/admin/change_password/(:any)'] = 'administrator/admin/change_password/$1';
$route['administrator/admin/delete/(:any)'] = 'administrator/admin/delete/$1';
$route['administrator/admin/status/active/(:any)'] = 'administrator/admin/status_active/$1';
$route['administrator/admin/status/deactive/(:any)'] = 'administrator/admin/status_inactive/$1';


$route['administrator/members'] = 'administrator/members';
$route['administrator/members/add'] = 'administrator/members/add';
$route['administrator/members/edit/(:any)'] = 'administrator/members/edit/$1';
$route['administrator/members/delete/(:any)'] = 'administrator/members/delete/$1';
$route['administrator/members/status/active/(:any)'] = 'administrator/members/status_active/$1';
$route['administrator/members/status/deactive/(:any)'] = 'administrator/members/status_deactive/$1';

$route['administrator/cms/index/(:any)'] = 'administrator/cms/index/$1';
$route['administrator/cms/add'] = 'administrator/cms/add';
$route['administrator/cms/order'] = 'administrator/cms/order';
$route['administrator/cms/add/(:any)'] = 'administrator/cms/add/$1';
$route['administrator/cms/change_password/(:any)'] = 'administrator/cms/change_password/$1';
$route['administrator/cms/delete/(:any)'] = 'administrator/cms/delete/$1';
$route['administrator/cms/status/active/(:any)'] = 'administrator/cms/status_active/$1';
$route['administrator/cms/status/deactive/(:any)'] = 'administrator/cms/status_inactive/$1';


$route['administrator/setting'] = 'administrator/setting';
$route['administrator/setting/add'] = 'administrator/setting/add';
$route['administrator/setting/add/(:any)'] = 'administrator/setting/add/$1';
$route['administrator/setting/status/active/(:any)'] = 'administrator/setting/status_active/$1';
$route['administrator/setting/status/deactive/(:any)'] = 'administrator/setting/status_inactive/$1';


$route['administrator/category/index/(:any)'] = 'administrator/category/index/$1';
$route['administrator/category/add'] = 'administrator/category/add';
$route['administrator/category/get_parent'] = 'administrator/category/get_parent';
$route['administrator/category/order'] = 'administrator/category/order';
$route['administrator/category/add/(:any)'] = 'administrator/category/add/$1';
$route['administrator/category/change_password/(:any)'] = 'administrator/category/change_password/$1';
$route['administrator/category/delete/(:any)'] = 'administrator/category/delete/$1';
$route['administrator/category/status/active/(:any)'] = 'administrator/category/status_active/$1';
$route['administrator/category/status/deactive/(:any)'] = 'administrator/category/status_inactive/$1'; */





/* End of file routes.php */
/* Location: ./application/config/routes.php */