<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/




$route['default_controller'] = 'home';
$route['posts'] = 'home/index/';
$route['posts/(:any)'] = 'home/category/$1';
$route['read-post/(:any)'] = 'home/read_post/$1';
$route['contact-us'] = 'home/contact_us';
$route['contact-us/(:any)'] = 'home/contact_us/$1';
$route['subscribe'] = 'home/subscribe';
$route['search'] = 'home/search';
$route['cms/(:any)'] = 'cms/index/$1';



/*$route['search/(:any)'] = 'search/index/$1';*/
/*$route['cms/(:any)'] = 'cms/index/$1';*/

/*$route['create_classified'] = 'home/create_classified';
$route['create_classified'] = 'home/create_classified';
$route['create_classified'] = 'home/create_classified';
$route['create_classified'] = 'home/create_classified';*/
$route['404_override'] = '';

//$route['login'] = 'home/login/';

$route['administrator'] = 'administrator/dashboard';
$route['administrator/login'] = 'administrator/dashboard/login';
$route['administrator/admin_profile'] = 'administrator/dashboard/admin_profile';
$route['administrator/forgot_password'] = 'administrator/dashboard/forgot_password';
$route['administrator/reset_password/(:any)'] = 'administrator/dashboard/reset_password/$1';
$route['administrator/no_access'] = 'administrator/dashboard/no_access';
$route['administrator/logout'] = 'administrator/dashboard/logout';

$route['directory'] = 'home/maindirectory/';
$route['directory/(:any)'] = 'home/directory/$1';
$route['read_directory/(:any)'] = 'home/read_directory/$1';
$route['member'] = 'member/dashboard';
$route['login/ajax'] = 'member/dashboard/login';
$route['login/ajax'] = 'member/dashboard/login';
$route['register/ajax'] = 'register';
$route['home/login'] = 'member/dashboard/login';
$route['contact_us'] = 'home/contact_us';
$route['about_us'] = 'home/about_us';
$route['member/profile'] = 'member/dashboard/profile';
$route['member/forgot_password'] = 'member/dashboard/forgot_password';
$route['member/reset_password/(:any)'] = 'member/dashboard/reset_password/$1';
$route['member/verify_email/(:any)'] = 'member/dashboard/verify_email/$1';
$route['my_adds'] = 'member/dashboard/my_adds';
$route['my_favourites'] = 'member/dashboard/my_favourites';
$route['member/my_searches'] = 'member/dashboard/my_searches';
$route['member/my_profile'] = 'member/dashboard/my_profile';
$route['member/account_settings'] = 'member/dashboard/account_settings';
$route['create_classified'] = 'member/dashboard/create_classified';
$route['create_classified/(:any)'] = 'member/dashboard/create_classified/$1';

$route['facebookLogin'] = 'member/dashboard/facebookLogin';


/* End of file routes.php */
/* Location: ./application/config/routes.php */