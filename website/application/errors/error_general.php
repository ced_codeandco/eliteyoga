<!DOCTYPE html>
<html lang="en">
<head>
    <title>Error</title>
    <style type="text/css">

        ::selection{ background-color: #E13300; color: white; }
        ::moz-selection{ background-color: #E13300; color: white; }
        ::webkit-selection{ background-color: #E13300; color: white; }

        body {
            background-color: #fff;
            margin: 40px;
            font: 13px/20px normal Helvetica, Arial, sans-serif;
            color: #4F5155;
        }

        a {
            color: #003399;
            background-color: transparent;
            font-weight: normal;
        }

        h1 {
            color: #444;
            background-color: transparent;
            border-bottom: 1px solid #D0D0D0;
            font-size: 19px;
            font-weight: normal;
            margin: 0 0 14px 0;
            padding: 14px 15px 10px 15px;
        }

        code {
            font-family: Consolas, Monaco, Courier New, Courier, monospace;
            font-size: 12px;
            background-color: #f9f9f9;
            border: 1px solid #D0D0D0;
            color: #002166;
            display: block;
            margin: 14px 0 14px 0;
            padding: 12px 10px 12px 10px;
        }

        #container {
            margin: 10px;
            border: 1px solid #D0D0D0;
            -webkit-box-shadow: 0 0 8px #D0D0D0;
        }

        p {
            margin: 12px 15px 12px 15px;
        }
    </style>
</head>
<body>
<div id="container">
    <h1><?php echo $heading; ?> </h1>
    <?php echo $message; ?>
</div>
</body>
</html>
<?php return; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <link rel="icon" href="<?php echo ROOT_URL_BASE;?>images/favicon.png">

    <meta name="format-detection" content="telephone=no">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>eMotion </title>
    <meta name="keywords" content="eMotion" />
    <meta name="Description" content="eMotion "/>
    <meta property="og:title" content="eMotion " />
    <meta property="og:description" content="eMotion " />
    <meta name="og:image" content="<?php echo ROOT_URL_BASE;?>images/logo.jpg"/>
    <link href="<?php echo ROOT_URL_BASE;?>fonts/fonts.css" type="text/css" rel="stylesheet" />

    <link href="<?php echo ROOT_URL_BASE;?>css/style.css" type="text/css" rel="stylesheet" />

    <link href="<?php echo ROOT_URL_BASE;?>css/dev.css" type="text/css" rel="stylesheet" />

    <link href="<?php echo ROOT_URL_BASE;?>assets/slider/jquery.bxslider.css" rel="stylesheet" type="text/css" />

    <link href="<?php echo ROOT_URL_BASE;?>css/responsive.css" type="text/css" rel="stylesheet" title="eMotion" />

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

    <script type="text/javascript" src="<?php echo ROOT_URL_BASE;?>assets/slider/jquery.bxslider.min.js"></script>

    <script type="text/javascript" src="<?php echo ROOT_URL_BASE;?>js/scripts.js"></script>

    <script src="<?php echo ROOT_URL_BASE;?>js/jquery.mCustomScrollbar.concat.min.js"></script>

    <link href="<?php echo ROOT_URL_BASE;?>css/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="<?php echo ROOT_URL_BASE;?>js/helper.js"></script>

    <script type="text/javascript">

        jQuery(document).ready(function(){



            jQuery('.bxslider').bxSlider({

                controls: true,

                mode: 'fade', auto: true,speed:1000,

                onSliderLoad: function () {

                    $('.bx-controls-direction').hide();

                    $('.bx-wrapper').hover(

                        function () {  $(this).find('.bx-controls-direction').fadeIn(300); },

                        function () {  $(this).find('.bx-controls-direction').fadeOut(300); }

                    );

                }

            });



        });
    </script>

</head>



<body id="" >
<input type="hidden" name="ROOT_URL" id="rootUrlLink" value="<?php echo ROOT_URL_BASE;?>" />
<div class="header_main">

    <div class="main_outer">

        <div class="logo_div"><a href="<?php echo ROOT_URL_BASE;?>"><img src="images/logo.png" /></a></div>

        <span class="pull-left" id="menuclick"><div id="nav-icon3"> <span></span> <span></span> <span></span> <span></span> </div></span>

        <div class="menu_nav_div">

            <ul class="nav">

                <li><a href="<?php echo ROOT_URL_BASE;?>" class="active">Home</a></li>
                <li>
                    <a href="<?php echo ROOT_URL_BASE;?>content"   >Content</a>
                </li>

                <li>
                    <a href="<?php echo ROOT_URL_BASE;?>network"   >Network</a>
                </li>

                <li>
                    <a href="<?php echo ROOT_URL_BASE;?>studios"   >Studios</a>
                </li>

                <li><a href="#contact-us" >Contact</a></li>
                <!--<li><a href="#contact_us">Contact</a></li>-->

            </ul>

        </div>

        <div class="clearfix"></div>

    </div>

</div><div class="content_main">
    <div class="main_outer wrap_404">

        <h2>500</h2>

        <label>Sorry! Something went wrong.</label>
        <label><a href="<?php echo ROOT_URL_BASE;?>" class="">Please try again later</a>.</label>
    </div>

</div>
<div id="contact-us" class="contact_main">

    <div id="map"></div>

    <div class="main_outer">

        <div class="contact_form">

            <p class="color_emotion">CONTACT</p>
            <p>Dubai Studio City<br />
                Building Tower – Office 306<br />
                PO Box 502798, Dubai – UAE</p>

            <p><a href="https://www.google.com/maps/place//@25.044909,55.251111,16z/" target="_blank">25.044909,55.251111</a></p>

            <p><label>t.</label>  +971 4 375 6859<br />

                <label>f.</label> +971 4 375 6859<br />

                <label>e.</label> admin@emotion.ae</p>

            <form id="contactform" name="contactform" >

                <input type="text" name="name" placeholder="Your Name"  id="Name" class="contact_input subscr-control" required  />
                <input type="text" name="email" placeholder="Email Address" id="Email" class="contact_input subscr-control" required />
                <textarea name="comments" placeholder="Inquiry" maxlength="150" class="contact_input subscr-control" id="Message" required></textarea>
                <input type="number" placeholder="How much is 8 + 6" class="captcha entrybox captcha_contact subscr-control" name="captcha" id="captcha-form" autocomplete="off" required/>
                <input  name="submit" value="SEND" type="submit"  class="submit bg_emotion subscr-control">
                <div class="contact-response hidden" id="subsc-response"></div>
            </form>
            <div class="clearfix"></div>
            <div class="loader-wrap hidden">
            </div>
        </div>

    </div>
    <input type="hidden" id="map_latitude" value="25.044909">
    <input type="hidden" id="map_longitude" value="55.251111">
</div>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=&sensor=false&extension=.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#contactform').submit(function(){
            $('.subscr-control').hide();$('.loader-wrap').show();
            var email = $('#subscription-email').val();
            jQuery.ajax({
                type: "POST",
                url: $('#rootUrlLink').val() + 'contact_us',
                dataType: 'json',
                data: $('#contactform').serialize(),
                cache: false,
                success: function(data) {
                    $('.subscr-control').show();$('.loader-wrap').hide();
                    var status = data.status;
                    var msg = data.message;
                    var new_captcha = data.new_captcha;
                    if(status*1 == 1)
                    {
                        $('#subsc-response').html(msg).removeClass('alert-danger').addClass('alert-success').show();
                        $('#contactform').find('input[type="text"],input[type="number"],textarea').val('');

                    }
                    else
                    {
                        $('#subsc-response').html(msg).removeClass('alert-success').addClass('alert-danger').show();
                    }
                    $('#captcha-form').val('');
                    $('#captcha-form').attr('placeholder', new_captcha);
                },
                complete: function(){
                    $('.subscr-control').show();$('.loader-wrap').hide();
                },
                error: function(){
                    $('#subCategoryContainer').html('Something went wrong!! Please try later');
                    $('.subscr-control').show();$('.loader-wrap').hide();
                }
            });
            return false;
        })
    })
    jQuery(document).ready(function(){
        var latitude = $('#map_latitude').val();
        var longitude = $('#map_longitude').val();
        // When the window has finished loading create our google map below
        google.maps.event.addDomListener(window, 'load', init);
        function init() {
            // Basic options for a simple Google Map
            // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
            var mapOptions = {
                // How zoomed in you want the map to start at (always required)
                zoom: 14,
                // The latitude and longitude to center the map (always required)
                //center: new google.maps.LatLng(25.21108,55.281955),
                center: new google.maps.LatLng(latitude,longitude),
                // How you would like to style the map.
                // This is where you would paste any style found on Snazzy Maps.
                styles: [{featureType:"landscape",stylers:[{color:"#e3e3e3"},{lightness:45},{visibility:"on"}]},{featureType:"poi",stylers:[{saturation:-100},{lightness:51},{visibility:"simplified"}]},{featureType:"road.highway",stylers:[{saturation:-100},{visibility:"simplified"}]},{featureType:"road.arterial",stylers:[{saturation:-100},{lightness:30},{visibility:"on"}]},{featureType:"road.local",stylers:[{saturation:-100},{lightness:40},{visibility:"on"}]},{featureType:"transit",stylers:[{saturation:-100},{visibility:"simplified"}]},{featureType:"administrative.province",stylers:[{visibility:"off"}]/**/},{featureType:"administrative.locality",stylers:[{visibility:"off"}]},{featureType:"administrative.neighborhood",stylers:[{visibility:"on"}]/**/},{featureType:"water",elementType:"labels",stylers:[{visibility:"on"},{lightness:-25},{saturation:-100}]},{featureType:"water",elementType:"geometry",stylers:[{color:"#000"}]}]
            };
            // Get the HTML DOM element that will contain your map
            // We are using a div with id="map" seen below in the <body>
            var mapElement = document.getElementById('map');
            // Create the Google Map using out element and options defined above
            var map = new google.maps.Map(mapElement, mapOptions);
            var marker = new google.maps.Marker({

                map: map,

                position: map.getCenter(),

                icon: 'images/google-map-cion.png'

            });
            var infowindow = new google.maps.InfoWindow();
            //  infowindow.setContent('<b>RTC Facilities Management</b><br/>Blue Bay Tower<br/>PO Box 487044<br />Dubai, UAE');
            // infowindow.open(map, marker);
            google.maps.event.addListener(marker, 'click', function() {

                //infowindow.open(map, marker);

            });

        }
    })
</script><div class="footer_copy">

    <div class="main_outer">

        <p>© 2015 <a>emotion</a>. Designed & Developed by <a href="http://codeandco.ae" target="_blank">Code&Co</a>.</p>

    </div>

</div>

</body>

</html>
<?php return; ?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Error</title>
<style type="text/css">

::selection{ background-color: #E13300; color: white; }
::moz-selection{ background-color: #E13300; color: white; }
::webkit-selection{ background-color: #E13300; color: white; }

body {
	background-color: #fff;
	margin: 40px;
	font: 13px/20px normal Helvetica, Arial, sans-serif;
	color: #4F5155;
}

a {
	color: #003399;
	background-color: transparent;
	font-weight: normal;
}

h1 {
	color: #444;
	background-color: transparent;
	border-bottom: 1px solid #D0D0D0;
	font-size: 19px;
	font-weight: normal;
	margin: 0 0 14px 0;
	padding: 14px 15px 10px 15px;
}

code {
	font-family: Consolas, Monaco, Courier New, Courier, monospace;
	font-size: 12px;
	background-color: #f9f9f9;
	border: 1px solid #D0D0D0;
	color: #002166;
	display: block;
	margin: 14px 0 14px 0;
	padding: 12px 10px 12px 10px;
}

#container {
	margin: 10px;
	border: 1px solid #D0D0D0;
	-webkit-box-shadow: 0 0 8px #D0D0D0;
}

p {
	margin: 12px 15px 12px 15px;
}
</style>
</head>
<body>
	<div id="container">
		<h1><?php echo $heading; ?> </h1>
		<?php echo $message; ?>
	</div>
</body>
</html>