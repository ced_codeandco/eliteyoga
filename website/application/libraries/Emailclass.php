<?php
/**
 * Created by PhpStorm.
 * Author: Anas
 * Date: 4/19/2015
 * Time: 1:42 PM
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Emailclass extends CI_Email
{
    function send_mail($recipient_email, $subject = '', $template = '', $from_email_name = '', $email_from_id = '')
    {
        $CI =& get_instance();
        $CI->load->library('email');
        $CI->load->library('config');

        $config['protocol']    = 'smtp';
        $config['smtp_host']    = 'ssl://smtp.gmail.com';
        $config['smtp_port']    = '465';
        $config['smtp_timeout'] = '7';
        $config['smtp_user']    = DEFAULT_FROM_EMAIL;
        $config['smtp_pass']    = SMTP_PASSWORD;
        $config['charset']    = 'utf-8';
        $config['newline']    = "\r\n";
        $config['mailtype'] = 'html'; // or html
        $config['validation'] = TRUE; // bool whether to validate email or not
        $config['wordwrap'] = TRUE;

        $CI->email->initialize($config);

        $from_email_id = ($email_from_id != '') ? $email_from_id : DEFAULT_EMAIL;
        $from_email_name = ($from_email_name != '') ? $from_email_name : DEFAULT_FROM_NAME;

        $CI->email->from($from_email_id, $from_email_name);
        $CI->email->to($recipient_email);

        $CI->email->subject($subject);
        $CI->email->message($template);

        $CI->email->print_debugger();

        /*if(!$response = $CI->email->send()) {
            var_dump($response);
            log_message('error', 'Mail Sending failure.Please contact administrator');
        } else {
            return true;
        }*/

        try {
            if(!$CI->email->send()) {
                log_message('error', 'Mail Sending failure.Please contact administrator');
            } else {
                return true;
            }
        } catch (Exception $e) {
            $log = "\n\nError: ".$e->getMessage()."\n";
            log_message('', $log);
        }
    }

    public function emailFooter(){
        $footerText = '';
        $footerText .= "<br><br><br>Thanks & Regards,<br>
		Team ".SITE_NAME;
        return $footerText;
    }
    public function emailHeader(){
        return '<br><br><br>';
    }

}