<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member_controller extends CI_Controller {

    /**
     * Holder for session values
     */
    public $user_id = false;
    public $is_admin = false;
    public $is_member = false;
    public $is_logged_in = false;
    public $headerData;
    public $contentData;
    public $footerData;

    /**
     * Class constructor.
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('url','form'));
        $this->load->model('member_model');
        $this->load->library(array('form_validation', 'session'));
        $this->load->model(array('classified_model', 'inquiry_model', 'cms_model', 'setting_model', 'admin_model', 'generic_category_model', 'cms_model'));//, 'tips_and_tricks_category_model', 'tips_and_tricks_sub_category_model', 'fun_category_model', 'category_model'));
        $this->headerData['navCategories'] = $this->generic_category_model->getCategoriesLookup();
        $this->headerData['categories']    = $this->admin_model->category_get_all();

        $this->footerData['footerCMSList'] = $this->cms_model->getCMSPageList('id,title,cms_slug'," id != 1 AND is_active = '1' AND on_footer='1' AND parent_id= 0",'ORDER BY cms_order ASC');
        $this->headerData['headerCMSList'] = $this->cms_model->getCMSPageList('id,title,cms_slug'," id != 1 AND is_active = '1' AND on_header='1' AND parent_id= 0",'ORDER BY cms_order ASC');
        /*$this->headerData['classifiedsCategs'] = $this->category_model->getAllRecords('*'," is_active = '1' ",'ORDER BY category_order ASC');
        $this->headerData['tipsCategories'] = $this->tips_and_tricks_category_model->getAllRecords('id,title'," is_active = '1' ",'ORDER BY category_order ASC');
        $this->headerData['tipsSubCategsLookup'] = $this->tips_and_tricks_sub_category_model->getTipsSubCategsLookup();
        $this->headerData['funCategories'] = $this->fun_category_model->getAllRecords('id,title'," is_active = '1' ",'ORDER BY category_order ASC');*/

        $this->setting_model->defineAllSettingVariables();
        $this->user_id = $this->session->userdata('id');
        $this->is_admin = $this->session->userdata('is_admin');
        $this->is_member = $this->session->userdata('is_member');
        if (!empty($this->user_id) && $this->is_member == true) {
            $this->is_logged_in = true;
            $this->headerData['isLoggedIn'] = $this->is_logged_in;
            $this->headerData['isMemberLogin'] = $this->member_model->checkMemberLogin();
            $this->headerData['activeMemberDetails'] = $this->member_model->activeMemberDetails();
            $this->headerData['myItemsCount'] = $this->classified_model->getMyItemsCount($this->user_id);
        }
        $current_method = $this->router->fetch_method();
        $controller = $this->router->fetch_class();
        if(!empty($this->user_id) && $this->is_admin == true && $this->is_forum()) {
            //DO NOTHING - ADMIN IS ACCESSING FORUM
        } else if(!empty($this->user_id) && $this->is_admin == true && !$this->is_forum()) {
            //$this->session->sess_destroy();
            //redirect(ADMIN_ROOT_URL);
        } else if($this->user_id == null || $this->user_id == '' || $this->is_member != true) {
            $safe_methods = array('login', 'logout', 'forgot_password', 'reset_password', 'register', 'verify_email', 'facebookLogin');
            $event_safe_methods = array('index', 'details', 'getEventsAjax');
            if ($controller == 'events' &&  in_array($current_method, $event_safe_methods)) {

            } else if (!in_array($current_method, $safe_methods)) {

                //$this->session->sess_destroy();
                redirect(ROOT_URL.'#login');
                exit();
            }
        }
        $this->headerData['current_method'] = $current_method;
        $this->headerData['current_controller'] = $controller;
    }

    public function is_forum()
    {
        $current_controle = $this->router->fetch_class();

        return ($current_controle == 'thread');
    }

    public function recaptcha($str='')
    {
        echo "Here";
        $google_url="https://www.google.com/recaptcha/api/siteverify";
        $secret= RECAPTCHA_SECRET;
        $ip=$_SERVER['REMOTE_ADDR'];
        $url=$google_url."?secret=".$secret."&response=".$str."&remoteip=".$ip;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 10);
        curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.16) Gecko/20110319 Firefox/3.6.16");
        $res = curl_exec($curl);
        curl_close($curl);
        $res= json_decode($res, true);
        //reCaptcha success check
        if($res['success'])
        {
            return TRUE;
        }
        else
        {
            $this->form_validation->set_message('recaptcha', 'reCAPTCHA is not validated(It says that you are a robot). Please try again');
            return FALSE;
        }
    }
}