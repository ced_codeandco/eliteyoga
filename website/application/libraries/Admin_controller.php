<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_controller extends CI_Controller {

    public $user_id = false;
    public $is_admin = false;
    public $is_logged_in = false;
    public $groupEdit = false;
    public $frontendUrl = false;
    public $groupEditUrl = false;

    /**
     * Class constructor.
     *
     */
    public function __construct()
    {
        parent::__construct();
        $user_id = $this->session->userdata('admin_id');
        $is_admin = $this->session->userdata('is_admin');

        /*if (!empty($user_id) && $is_admin != true && $is_member == true) {
            redirect(ROOT_URL.'login');
        } else*/
        if($user_id == null || $user_id == '') {
            $safe_methods = array('login', 'logout', 'forgot_password', 'reset_password');
            $current_method = $this->router->fetch_method();

            if (!in_array($current_method, $safe_methods)) {

                //$this->session->sess_destroy();
                redirect(ROOT_URL.'administrator/login');
                exit;
            }
            $this->is_logged_in = true;
        }
        $this->user_id = $user_id;
        $this->manageInlineEdit();
        $this->manageEditorField();
    }

    private function manageEditorField() {
        if (isset($_POST['ckeditor_description'])) {
            $_POST['description'] = $_POST['ckeditor_description'];
            unset($_POST['ckeditor_description']);
        }
    }

    function manageInlineEdit() {
        $frontendUrl = $this->input->get('frontendUrl');
        $groupEdit = $this->input->get('groupEdit');

        if (!empty($groupEdit)) {
            $this->groupEdit = $groupEdit;
            $this->groupEditUrl = $frontendUrl;
        } else {
            $this->groupEdit = $this->session->flashdata('groupEdit');
            $this->groupEditUrl = $this->session->flashdata('groupEditUrl');
        }

        if (!empty($frontendUrl) && empty($this->groupEdit)) {
            $this->frontendUrl = $frontendUrl;
        } else if (!empty($frontendUrl) && !empty($this->groupEdit)) {
            $this->groupEdit = $groupEdit;
            $this->frontendUrl = '';
            $this->groupEditUrl = $frontendUrl;
        } else {
            $this->frontendUrl = $this->session->flashdata('frontendUrl');
        }
        if (!empty($this->frontendUrl)) {
            $this->session->set_flashdata('frontendUrl', $this->frontendUrl);
        } else if (!empty($this->groupEdit)) {
            $this->session->set_flashdata('groupEdit', $this->groupEdit);
            $this->session->set_flashdata('groupEditUrl', $this->groupEditUrl);
        }

        /*echo "<br>groupEditUrl: ";
        var_dump($this->groupEditUrl);
        echo "<br>frontendUrl: ";
        var_dump($this->frontendUrl);*/
    }
}