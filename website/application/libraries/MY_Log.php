<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 4.3.2 or newer
 *
 * @package        CodeIgniter
 * @author        ExpressionEngine Dev Team
 * @copyright    Copyright (c) 2006, EllisLab, Inc.
 * @license        http://codeigniter.com/user_guide/license.html
 * @link        http://codeigniter.com
 * @since        Version 1.0
 * @filesource
 */
// ------------------------------------------------------------------------
/**
 * MY_Logging Class
 *
 * This library assumes that you have a config item called
 * $config['show_in_log'] = array();
 * you can then create any error level you would like, using the following format
 * $config['show_in_log']= array('DEBUG','ERROR','INFO','SPECIAL','MY_ERROR_GROUP','ETC_GROUP');
 * Setting the array to empty will log all error messages.
 * Deleting this config item entirely will default to the standard
 * error loggin threshold config item.
 *
 * @package        CodeIgniter
 * @subpackage    Libraries
 * @category    Logging
 * @author        ExpressionEngine Dev Team. Mod by Chris Newton
 */
class MY_Log extends CI_Log {
    private $emailSent = 0;
    /**
     * Constructor
     */
    public function __construct()
    {
        $config =& get_config();

        $this->_log_path = ($config['log_path'] != '') ? $config['log_path'] : APPPATH.'logs/';

        if ( ! is_dir($this->_log_path) OR ! is_really_writable($this->_log_path))
        {
            $this->_enabled = FALSE;
        }

        if (is_numeric($config['log_threshold']))
        {
            $this->_threshold = $config['log_threshold'];
        }

        if ($config['log_date_format'] != '')
        {
            $this->_date_fmt = $config['log_date_format'];
        }
    }

    private function isCommandLineInterface()
    {
        return (php_sapi_name() === 'cli');
    }

    // --------------------------------------------------------------------
    /**
     * Write Log File
     *
     * Generally this function will be called using the global log_message() function
     *
     * @access    public
     * @param    string    the error level
     * @param    string    the error message
     * @param    bool    whether the error is a native PHP error
     * @return    bool
     */
    public function write_log($level = 'error', $msg, $php_error = FALSE)
    {
        $result = parent::write_log($level, $msg, $php_error);

        if ($result == TRUE && strtoupper($level) == 'ERROR') {
            $messageChunks = explode(' ', $msg);
            $errorType = !empty($messageChunks[1]) ? trim($messageChunks[1]) : '';

            if (strtolower($errorType)  != 'notice' && strtolower($errorType) != 'warning') {
                //print_r(array($level, $msg, $php_error)); //die();

                $ci =& get_instance();
                $ci->config->load('email_php_errors');
                if ($this->_enabled === FALSE) {
                    return FALSE;
                }

                $level = strtoupper($level);

                if (!isset($this->_levels[$level]) OR ($this->_levels[$level] > $this->_threshold)) {
                    return FALSE;
                }


                $message = '';

                if ($this->isCommandLineInterface()) {
                    $message .= 'CMD ';
                }

                $message .= $level . ' ' . (($level == 'INFO') ? ' -' : '-') . ' ' . date($this->_date_fmt) . ' --> ' . $msg . "\n";

                $content = "Domain: " . ROOT_URL . "\r\n";
                $content .= current_url() . "\r\n";

                if (config_item('email_php_errors')) {
                    // set up email with config values
                    $ci->load->library('email');
                    $ci->email->from(config_item('php_error_from'));
                    $ci->email->to(config_item('php_error_to'));
                    // set up subject
                    $subject = 'An Error Logged on ' . SITE_NAME;
                    $ci->email->subject($subject);
                    // set message and send
                    $ci->email->message($content . $message);
                    //print_r($content . $message);die("Hhhh");
                    $ci->email->send();//die('Here');
                    //$this->emailSent++;
                }
            }
            //exit;

            return TRUE;
        }
    }

    /**
     * replace short tags with values.
     *
     * @access private
     * @param string $content
     * @param string $severity
     * @param string $message
     * @param string $filepath
     * @param int $line
     * @return string
     */
    private function _replace_short_tags($content, $severity, $message, $filepath, $line)
    {
        $content = str_replace('{{severity}}', $severity, $content);
        $content = str_replace('{{message}}', $message, $content);
        $content = str_replace('{{filepath}}', $filepath, $content);
        $content = str_replace('{{line}}', $line, $content);
        return $content;
    }

}