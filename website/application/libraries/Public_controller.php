<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Public_controller extends CI_Controller {

    public $user_id = false;
    public $is_admin = false;
    public $is_member = false;
    public $is_logged_in = false;
    public $headerData;
    public $contentData;
    public $footerData;
    public $isAdminLoggedIn = false;


    /**
     * Class constructor.
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->user_id = $this->session->userdata('id');
        $this->is_admin = $this->session->userdata('is_admin');
        $this->is_member = $this->session->userdata('is_member');

        //$this->load->model(array('cms_model', 'setting_model', 'member_model', 'cms_model', 'classified_model', 'generic_category_model', 'admin_model', 'advertize_model','directory_category_model','classified_directory_model'));
        $this->load->model(array('cms_model', 'setting_model', 'generic_category_model', 'admin_model'));

        //$this->headerData['headerCMSList'] = $this->cms_model->getCMSPageList('id,title,cms_slug,home_page_description,home_logo_border,home_logo,theme_color,on_home'," is_active = '1' AND on_header='1' AND parent_id= 0",'ORDER BY cms_order ASC');
        $this->headerData['homePageData'] = $this->cms_model->getDetails(CMS_HOME_PAGE_ID);

        $this->footerData['contactUs'] = $this->cms_model->getDetails(CMS_CONTACT_PAGE_ID);

        //$this->contentData['verify_robot'] = $this->session->userdata('verify_robot');


        $this->setting_model->defineAllSettingVariables();
        $this->headerData['isMemberLogin'] = array();
        if (!empty($this->user_id) && $this->is_member == true) {
            $this->is_logged_in = true;
        }
        $this->headerData['current_method'] = $current_method = $this->router->fetch_method();;
        $this->headerData['current_controller'] = $controller = $this->router->fetch_class();
        $this->checkAdmin();
    }

    public function generateRobotText() {
        $verify_robot = array(
            mt_rand(1,9), mt_rand(1,9)
        );

        if ($this->input->post('verify_robot')) {
            $this->footerData['verify_robot_old'] = $this->session->userdata('verify_robot');
        }
        $this->footerData['verify_robot'] = $verify_robot;
        $data = array('verify_robot' => $verify_robot);
        $this->session->set_userdata($data);
    }

    public function validate_captcha($answer) {
        $verify_robot = $this->session->userdata('verify_robot');
        //var_dump($answer);
        //print_r($verify_robot);
        return ($answer == ($verify_robot[0] + $verify_robot[1]));
    }

    private function checkAdmin(){
        $admin_id = $this->session->userdata('admin_id');
        $is_admin = $this->session->userdata('is_admin');

        if($is_admin && !empty($admin_id)) {
            $this->isAdminLoggedIn = true;
        }
    }

    public function getSubCategoryListBySlug()
    {
        //print_r($this->headerData['navCategories']);
        if (defined('CURRENT_SECTION_ID') && !empty($this->headerData['navCategories'][CURRENT_SECTION_ID]['sub'])) {

            return $this->headerData['navCategories'][CURRENT_SECTION_ID]['sub'];
        }
    }

    public function getCategoryBySlug($categorySlug)
    {
        return $this->generic_category_model->getCategoryBySlug($categorySlug);
    }

    public function getCategoryById($categoryId)
    {
        return $this->generic_category_model->getDetails($categoryId);
    }
	
	public function getDirectoryBySlug($categorySlug)
    {
        return $this->directory_category_model->getCategoryBySlug($categorySlug);
    }

    public function getDirectoryById($categoryId)
    {
        return $this->directory_category_model->getDetails($categoryId);
    }
	 public function getHeaderbannerById($categoryId)
    {
        return $this->directory_category_model->getDetails($categoryId);
    }
}