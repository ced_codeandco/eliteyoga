<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**

 * Classifie Utility Helpers

 *

 * @package		Classifie

 * @subpackage	Helpers

 * @category	Helpers

 * @author		Anas

 */



// ------------------------------------------------------------------------

/**

 * Random String Generation

 *

 * @access	public

 * @param	void

 * @return	string

 */

function gen_random_string($length = 10) {



    $characters = '0123456789abcdefghijklmnopqrstuvwxyz';

    $string = "";

    for ($p = 0; $p < $length; $p++)

    {

        $string .= $characters[mt_rand(0, strlen($characters)-1)];

    }

    return $string;



}



/**

 * Function to return proper error messages while uploading files.

 *

 * @param $code

 * @author Anas

 */

function decode_upload_error( $code ) {

    switch ($code) {

        case UPLOAD_ERR_INI_SIZE:

            $message =  'The uploaded file exceeds the upload_max_filesize ('. ( (int)(ini_get('upload_max_filesize')) ."MB" ).') directive in php.ini';

            break;

        case UPLOAD_ERR_FORM_SIZE:

            $message = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';

            break;

        case UPLOAD_ERR_PARTIAL:

            $message = 'The uploaded file was only partially uploaded';

            break;

        case UPLOAD_ERR_NO_FILE:

            $message = 'No file was uploaded';

            break;

        case UPLOAD_ERR_NO_TMP_DIR:

            $message = 'Missing a temporary folder';

            break;

        case UPLOAD_ERR_CANT_WRITE:

            $message = 'Failed to write file to disk';

            break;

        case UPLOAD_ERR_EXTENSION:

            $message = 'File upload stopped by extension';

            break;

        default:

            $message = 'Unknown upload error';

            break;

    }

    return $message;

}

/**

 * Validate URL

 *

 * @access public

 * @param  string

 * @return string

 */

function valid_url($url)

{

    return ( ! preg_match('/^(http|https|ftp):\/\/([A-Z0-9][A-Z0-9_-]*(?:\.[A-Z0-9][A-Z0-9_-]*)+):?(\d+)?\/?/i', $url)) ? FALSE : TRUE;

}



/**

 * Method is used to validate strings to allow alpha numeric spaces underscores and dashes ONLY.

 *@param $str    String    The item to be validated.

 *@return BOOLEAN   True if passed validation false if otherwise.

 */

function alpha_numeric_dash_space($str = '')

{

    if (preg_match('!^[\w .-]*$!', $str)) {

        return TRUE;

    }

    return FALSE;

}

/**

 * Method is used to validate strings to allow alphabets and spaces ONLY.

 *@param $str    String    The item to be validated.

 *@return BOOLEAN   True if passed validation false if otherwise.

 */

function alpha_space($str_in = '')

{



    if (preg_match('/[^ A-Za-z]/', $str_in))

    {

        return FALSE;

    }

    else

    {

        return TRUE;

    }

}

/**

 * Method is used to validate strings to allow alpha spaces underscores and dashes ONLY.

 *@param $str    String    The item to be validated.

 *@return BOOLEAN   True if passed validation false if otherwise.

 */

function alpha_dash_space($str_in = '')

{

    if ( ! preg_match('/[^ A-Za-z]/', $str_in))

    {

        $this->form_validation->set_message('alpha_dash_space', 'The %s field may only contain alphabets, spaces, underscores, and dashes.');

        return FALSE;

    }

    else

    {

        return TRUE;

    }

}

/**

 * Truncate the input content with sentence breaks.

 *

 */

function content_truncate($string, $limit = 150 , $break = ".", $pad = "", $strict = FALSE)

{

    $string = strip_tags(stripslashes($string));

    if(strlen($string) <= $limit) return $string;

    if($strict === TRUE)

    {

        $string = substr($string, 0, $limit) . $pad;

    }

    else if(false !== ($breakpoint = strpos($string, $break, $limit)))

    {

        if($breakpoint < strlen($string) - 1)

        {

            $string = substr($string, 0, $breakpoint) . $pad;

        }

    }

    return $string;

}

/**

 * Escapes content for insertion into the database using addslashes(), for security.

 *

 * Works on arrays.

 *

 * @param string|array $data to escape

 * @return string|array escaped as query safe string

 */

function escape( $data ) {

    if ( is_array( $data ) ) {

        foreach ( (array) $data as $k => $v ) {

            if ( is_array( $v ) )

                $data[$k] = escape( $v );

            else

                $data[$k] = _weak_escape( $v );

        }

    } else {

        $data = _weak_escape( $data );

    }



    return $data;

}



/**

 * Weak escape, using addslashes()

 *

 * @param string $string

 * @return string

 */

function _weak_escape( $string ) {

    return addslashes( $string );

}



/**

 *

 *

 *

 */

function process_search($search_keyword = '', $fieldname = '', $operation_mode = '')

{



    $s = stripslashes($search_keyword);

    preg_match_all('/".*?("|$)|((?<=[\\s",+])|^)[^\\s",+]+/', $s, $matches);



    $search_terms = $matches[0];



    $searchand = $search = '';

    foreach ( (array) $search_terms as $term ) {

        $term = addslashes($term);

        $search .= "{$searchand} {$fieldname} LIKE '%{$term}%'";

        $searchand = ' OR ';

    }



    $term =  escape ( str_replace(array("%", "_"), array("\\%", "\\_"), $s) );

    if ( count($search_terms) > 1 && $search_terms[0] != $s )

        $search .= " OR {$fieldname} LIKE '%{$term}%'";



    $search_condition = (!empty($operation_mode)) ? $search.' '.$operation_mode : $search.' ';

    return $search_condition;

}

/**

 * Function to format csv strings to insert a space after comma.

 *

 * @param string $str.

 * @access public.

 * @return string.

 */

function format_csv_string($str)

{

    if($str == '')

    {

        return '--';

    }

    else

    {

        return str_replace('|', ', ', $str);

    }

}

/**

 * Function to re-arrange an array of items to be displayed in topdown columns.

 * Used to display price range, geographic area in top - down columns.

 *

 * @access public.

 * @param Array $input_array.

 * @param integer $column_count.

 * @return Array.

 *

 */

function make_topdown_ordered_array($input_array, $column_count)

{

    $input_array = array_values($input_array);

    $return_array = FALSE;

    if(is_array($input_array) && count($input_array) > 0)

    {

        $total_count = count($input_array);

        $col_reminder[0] = 0;

        for($i = 1; $i <= $column_count; $i++)

        {

            $col_reminder[$i] = 0;

            if($total_count % $column_count >= $i){

                $col_reminder[$i] = 1;

            }

        }

        $count_rows = floor($total_count / $column_count);

        $ret_indx = 0;

        for($i = 0; $i <= $count_rows; $i++)

        {

            $array_index = $i;

            for($j = 0; $j < $column_count; $j++)

            {

                if(isset($input_array[$array_index])){

                    $return_array[$ret_indx] = $input_array[$array_index];

                    if(is_object($return_array[$ret_indx]))

                    {

                        $return_array[$ret_indx]->index = $array_index + 1;

                    }

                    else

                    {

                        $return_array[$ret_indx]['index'] = $array_index + 1;

                    }

                    unset($input_array[$array_index]);

                }

                $ret_indx ++;

                $array_index = $array_index + $count_rows + $col_reminder[$j+1];

            }

        }

    }

    return $return_array;

}

/**

 * Function to get local time based upon timezone

 *

 * @param integer $client_timezone client timezone

 * @param date/time $current_date date/time value from DB

 * @param string $format date format

 *

 * @return timezone based local time

 */

function get_local_time($client_timezone, $current_date, $format)

{

    $datetime = new DateTime($current_date);

    $la_time = new DateTimeZone($client_timezone);

    $datetime->setTimezone($la_time);



    return $datetime->format($format);

}



/*function make_difference_string($date)

{

    $dStart = new DateTime($date);

    $dEnd  = new DateTime();

    $dDiff = $dStart->diff($dEnd);

    return $dDiff->format('%R%d') . ' days'; // use for point out relation: smaller/greater

    return $dDiff->days;

    return '20 minutes ago '.$date;

}*/

function pluralize( $count, $text )

{

    return $count . ( ( $count == 1 ) ? ( " $text" ) : ( " ${text}s" ) );

}



function make_difference_string( $datetime )

{

    if ($datetime instanceof DateTime) {

        /*$interval = date_create('now')->diff($datetime);

        $suffix = ($interval->invert ? ' ago' : '');

        if ($v = $interval->y >= 1) return pluralize($interval->y, 'year') . $suffix;

        if ($v = $interval->m >= 1) return pluralize($interval->m, 'month') . $suffix;

        if ($v = $interval->d >= 1) return pluralize($interval->d, 'day') . $suffix;

        if ($v = $interval->h >= 1) return pluralize($interval->h, 'hour') . $suffix;

        if ($v = $interval->i >= 1) return pluralize($interval->i, 'minute') . $suffix;

        return pluralize($interval->s, 'second') . $suffix;*/

        return '';

    }

}



/**

 * Function to crop a string and append a suffix

 *

 * @param $string

 * @param int $length

 * @param string $suffix

 *

 * @return string

 */

function cropString($string, $length = 10, $suffix = '...')

{

    return (strlen($string) > $length) ? substr($string, 0, $length) . $suffix : $string;

}



function parseJavascriptSafe($string){

    return str_replace("\n", '\n', str_replace('"', '\"', addcslashes(str_replace("\r", '', (string)$string), "\0..\37'\\")));

}



function build_filldata_javascript($fieldData) {



    $javascriptCode = '';

    foreach ( (array)$fieldData as $key => $val ) {

        $val = parseJavascriptSafe($val);

        $javascriptCode .= "\n";

        $javascriptCode .= "\t\tif($('input[name=\"$key\"]').length >0) {\n";

        $javascriptCode .= "\t\t\t$('input[name=\"$key\"]').val(\"$val\");";

        $javascriptCode .= "\n\t\t} else if($('select[name=\"$key\"]').length >0) {\n";

        $javascriptCode .= "\t\t\t$('select[name=\"$key\"]').val(\"$val\");";

        $javascriptCode .= "\n\t\t} else if($('textarea[name=\"$key\"]').length >0) {\n";

        $javascriptCode .= "\t\t\t$('textarea[name=\"$key\"]').val(\"$val\");";

        $javascriptCode .= "\n\t\t} \n";



    }



    return $javascriptCode;

}



function prepareAdvertiseBanner($advertizeObj, $class = 'top_banner') {

    $advertizeBanner = '';

    if (!empty($advertizeObj) && !empty($advertizeObj->image_path) && file_exists(DIR_UPLOAD_ADVERTIZE.$advertizeObj->image_path)) {

        if (!empty($advertizeObj) && !empty($advertizeObj->advertize_url)) {

            $advertizeBanner = '<a href="' . prep_url($advertizeObj->advertize_url) . '" target="_blank">';

        }

        $advertizeBanner .= '<img alt="'.$advertizeObj->title.'" src="'.ROOT_URL_BASE.'assets/timthumb.php?src='.DIR_UPLOAD_ADVERTIZE_SHOW . $advertizeObj->image_path.'&q=100&w=300" class="'.$class.'" />';

        if (!empty($advertizeObj) && !empty($advertizeObj->advertize_url)) {

            $advertizeBanner .= '</a>';

        }

    }



    return auto_link($advertizeBanner);

}



function searchQueryToTags($searchUrl)

{

    $searchUrl = !empty($searchUrl) ? urldecode($searchUrl) : '';

    $parsed_url = parse_url($searchUrl);

    $query = html_entity_decode($parsed_url['query']);

    if (!empty($query)) {

        parse_str($query, $queryParts);

    }

    $searchKeys = '';



    if (!empty($queryParts) && is_array($queryParts)) {

        $searchKeys .= '';

        if (isset($queryParts['country']) && $queryParts['country'] != '') {

            $countryName = getLookUpValues('tbl_countries', 'name', 'id', $queryParts['country']);

            $searchKeys .= ' <a href="javascript:void(0)">Country: ' . $countryName .'</a>';

        }

        if (isset($queryParts['category']) && $queryParts['category'] != '' && is_numeric($queryParts['category'])) {

            $categoryName = getLookUpValues('tbl_category', 'title', 'id', $queryParts['category']);

            $searchKeys .= ' <a href="javascript:void(0)">Category: ' . $categoryName . '</a>';

        }

        if (isset($queryParts['brand']) && $queryParts['brand'] != '')

            $searchKeys .= ' <a href="javascript:void(0)">Brand: '. $queryParts['brand'] . '</a>';



        if (isset($queryParts['model']) && $queryParts['model'] != '')

            $searchKeys .= ' <a href="javascript:void(0)">Model: '. $queryParts['brand'] . '</a>';



        if (isset($queryParts['manufacture_year']) && $queryParts['manufacture_year'] != '')

            $searchKeys .= ' <a href="javascript:void(0)">Manufacture year: '. $queryParts['manufacture_year'].'</a>';



        if (isset($queryParts['min_amount']) && $queryParts['min_amount'] != '')

            $searchKeys .= ' <a href="javascript:void(0)">Minimum price: '. $queryParts['min_amount'] .'</a>';



        if (isset($queryParts['max_amount']) && $queryParts['max_amount'] != '')

            $searchKeys .= ' <a href="javascript:void(0)">Maximum price: '.  $queryParts['max_amount'] .'</a>';



        if (isset($queryParts['search_city']) && $queryParts['search_city'] != '') {

            $searchKeys .= ' <a href="javascript:void(0)">City: '. $queryParts['search_city'] . '</a>';

        }



        if (isset($queryParts['search_city']) && $queryParts['search_city'] != '' &&

            isset($queryParts['search_city_range']) && $queryParts['search_city_range'] != ''

        ) {

            $searchKeys .= ' <a href="javascript:void(0)">Within the range of : '. $queryParts['search_city_range'] .' miles </a>';

        }



        if (isset($queryParts['search_keyword']) && $queryParts['search_keyword'] != '') {

            $searchKeys .= ' <a href="javascript:void(0)">Having keywords : '. $queryParts['search_keyword'] . '</a>';

        }

    }



    return  $searchKeys ;

}



function getLookUpValues($table, $selectField, $whereField, $whereValue)

{

    $CI =& get_instance();

    $CI->load->database();

    $query = $CI->db->select($selectField)->from($table)->where($whereField, $whereValue)->limit(1)->get();



    $result = $query->row();



    return $result->$selectField;

}



function getCurrentUrl()

{

//        echo '';

//        echo getcwd(); die();



    $path = ROOT_URL;

    $path .= (!empty($_SERVER['PATH_INFO']) ? ltrim($_SERVER['PATH_INFO'], "/") : '');

    $path .= (!empty($_SERVER['QUERY_STRING']) ? '?'.$_SERVER['QUERY_STRING'] : '');



    return $path;

}



function roundAverage($rate)

{

    $fraction = $rate - floor($rate);

    if ( $fraction > 0.75) {

        $rate = floor($rate) + 1;

    } else if ( $fraction > 0.25) {

        $rate = floor($rate) + 0.5;

    } else {

        $rate = floor($rate);

    }



    return $rate;

}





/**

 * Created by PhpStorm.

 * User: Anas <anas@codeandco.ae>

 * Date: 7/26/2015

 * Time: 5:01 PM

 */

function draw_calendar($month, $year, $events = array())

{



    $dateObj   = DateTime::createFromFormat('m-Y', $month.'-'.$year);

//    $d->modify('first day of previous month');

    $monthName = $dateObj->format('F');

    $previousMonth = date_create($dateObj->format('Y-m-d'))->modify('-1 month');

    $nextMonth = date_create($dateObj->format('Y-m-d'))->modify('+1 month');

    //print_r($previousMonth);

    //print_r($nextMonth);

    $calendar = '<div class="cal-controles">';



    $calendar .= '<span class="navigator"><a href="'.ROOT_URL.'events/list/'.$previousMonth->format('F-Y').'">Previous</a></span>';

//    $calendar .= '<div class="control"><select><option value="">Year</option></select></div>';



    $calendar .= '<span class="navigator pull-right"><a href="'.ROOT_URL.'events/list/'.$nextMonth->format('F-Y').'">Next</a></span>';



    $calendar .= '</div>';

    $calendar .= '<div class="cal-title">'.$monthName .', '.$year.'</div>';

    $calendar .= '<div class="cal-wrap">';

    /* draw table */

    $calendar .= '<table cellpadding="0" cellspacing="0" class="calendar">';



    /* table headings */

    $headings = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');

    $calendar .= '<tr class="calendar-row"><td class="calendar-day-head">' . implode('</td><td class="calendar-day-head">', $headings) . '</td></tr>';



    /* days and weeks vars now ... */

    $running_day = date('w', mktime(0, 0, 0, $month, 1, $year));

    $days_in_month = date('t', mktime(0, 0, 0, $month, 1, $year));

    $days_in_this_week = 1;

    $day_counter = 0;

    $dates_array = array();



    /* row for week one */

    $calendar .= '<tr class="calendar-row">';



    /* print "blank" days until the first of the current week */

    for ($x = 0; $x < $running_day; $x++):

        $calendar .= '<td class="calendar-day-np"> </td>';

        $days_in_this_week++;

    endfor;



    /* keep going with days.... */

    for ($list_day = 1; $list_day <= $days_in_month; $list_day++) {

        $calendar .= '<td class="calendar-day">';

        /* add a wrapper for daily events*/

        $calendar .= '<div class="calendar-day-wrap">';

        /* add in the day number */

        $calendar .= '<div class="day-number">' . $list_day . '</div>';

        $today_timestamp = strtotime($year.'-'.$month.'-'.$list_day);

        /** QUERY THE DATABASE FOR AN ENTRY FOR THIS DAY !!  IF MATCHES FOUND, PRINT THEM !! **/

        if (!empty($events[$today_timestamp])) {

            $counter = 0;

            //print_r($events[$today_timestamp]);

            foreach ($events[$today_timestamp] as $todayEvents) {

                if (!empty($todayEvents['title']) && !empty($todayEvents['id'])) {

                    $calendar .= '<p class="event-cal-title"> <a class="tooltip" href="'.ROOT_URL.'events/details/'.$todayEvents['id'].'">' . content_truncate($todayEvents['title'], 30, ' ') . '

                    <span>

                        <img class="callout" src="'.ROOT_URL_BASE.'images/callout.gif" />

                        <strong class="event-details-link" url="'.ROOT_URL.'events/details/'.$todayEvents['id'].'">' . $todayEvents['title'] . ' </strong><br />

                        ' . $todayEvents['title'] . '

                    </span>

                    </a> </p>';

                    $counter++;

                }

            }

            //$calendar .= ($counter < 2) ? '<p class="event-cal-title-empty"> &nbsp; </p>' : '';

        } else {

            //$calendar .= str_repeat('<p>'.$today_timestamp .'</p>', 2);

            $calendar .= str_repeat('<p class="event-cal-title-empty">&nbsp;</p>', 1);

        }



        $calendar .= '</div>';

        $calendar .= '</td>';

        if ($running_day == 6) {

            $calendar .= '</tr>';

            if (($day_counter + 1) != $days_in_month) {

                $calendar .= '<tr class="calendar-row">';

            }

            $running_day = -1;

            $days_in_this_week = 0;

        }

        $days_in_this_week++;

        $running_day++;

        $day_counter++;

    }



    /* finish the rest of the days in the week */

    if($days_in_this_week < 8):

        for($x = 1; $x <= (8 - $days_in_this_week); $x++):

            $calendar.= '<td class="calendar-day-np"> </td>';

        endfor;

    endif;



    /* final row */

    $calendar.= '</tr>';



    /* end the table */

    $calendar.= '</table>';

    $calendar .= '</div>';

    /* all done, return result */

    return $calendar;

}





function time_ago($date) {



    if(empty($date)) {

        return "No date provided";

    }



    $periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");

    $lengths = array("60","60","24","7","4.35","12","10");

    $now = time();

    $unix_date = strtotime($date);



    // check validity of date



    if(empty($unix_date)) {

        return "Bad date";

    }



    // is it future date or past date

    if($now > $unix_date) {

        $difference = $now - $unix_date;

        $tense = "ago";

    } else {

        $difference = $unix_date - $now;

        $tense = "from now";

    }

    for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {

        $difference /= $lengths[$j];

    }

    $difference = round($difference);

    if($difference != 1) {

        $periods[$j].= "s";

    }



    return "$difference $periods[$j] {$tense}";

}



function getUrlByCategId($categoryId = '')

{

    $mainCategUrl = '';

    if ($categoryId == CLASSIFIEDS_ID) {

        $mainCategUrl = FOOD_AND_DRINKS_URL;

    } if ($categoryId == EVENTS_ID) {

    $mainCategUrl = FAMILY_FUN_URL;

} else if ($categoryId == TOP_PAEDS_ID) {

    $mainCategUrl = TOP_PAEDS_URL;

} else if ($categoryId == RECIPES_ID) {

    $mainCategUrl = RECIPES_URL;

} else if ($categoryId == BRESTFEEDING_ID) {

    $mainCategUrl = BRESTFEEDING_URL;

} else if ($categoryId == FUNWITHMOM_ID) {

    $mainCategUrl = FUNWITHMOM_URL;

} else if ($categoryId == NURSERIES_ID) {

    $mainCategUrl = NURSERIES_URL;

}



    return $mainCategUrl;

}



function getIdByCategUrl($categoryUrl = '')

{

    $categoryId = '';

    if ($categoryUrl == FOOD_AND_DRINKS_URL) {

        $categoryId = CLASSIFIEDS_ID;

    } if ($categoryUrl == FAMILY_FUN_URL) {

    $categoryId = EVENTS_ID;

} else if ($categoryUrl == TOP_PAEDS_URL) {

    $categoryId = TOP_PAEDS_ID;

} else if ($categoryUrl == RECIPES_URL) {

    $categoryId = RECIPES_ID;

} else if ($categoryUrl == BRESTFEEDING_URL) {

    $categoryId = BRESTFEEDING_ID;

} else if ($categoryUrl == FUNWITHMOM_URL) {

    $categoryId = FUNWITHMOM_ID;

}



    return $categoryId;

}

function populateSortControl($sectionId, $currentUrl, $serializedSortControls = '')

{

    $baseUrl = current_url();

    $currentOrder = !empty($_GET['sort']) ? $_GET['sort'] : '';

    $query = parse_str($_SERVER['QUERY_STRING'], $params);

    if (isset($params['sort'])) { unset($params['sort']); }

    if (isset($params['per_page'])) { unset($params['per_page']); }

    http_build_query($params);

    if ($serializedSortControls === false) {

        $sortIndexes = '';

    } else if (!empty($serializedSortControls)) {

        $sortIndexes = unserialize($serializedSortControls);

    } else {

        $sortIndexes = getSortControls($sectionId);

    }

    if (in_array('-1', $sortIndexes)) return false;

    $sortControl = '<div class="sort-control-wrap select_style">';



    if (!empty($sortIndexes) && is_array($sortIndexes)) {

        //$sortControl .= '<label>Sort By:</label>';

        $sortControl .= '<select class="sort-select" id="sort-control" onchange="sortReload(this)">';

        $queryString = http_build_query($params);

        $url = $baseUrl.(!empty($queryString) ? '?'.$queryString : '');

        $sortControl .= '<option value="'.$url.'">Sort By</option>';

        foreach ($sortIndexes as $index) {

            $component = sortControlByHandle($index);

            if (!empty($component)) {

                $params['sort'] = $index;

                $queryString = http_build_query($params);

                $url = $baseUrl . (!empty($queryString) ? '?' . $queryString : '');

                $selected = (!empty($currentOrder) && $currentOrder == $index) ? 'selected="selected"' : '';

                $sortControl .= '<option ' . $selected . ' value="' . $url . '">' . $component['title'] . '</option>';

            }

        }

        $sortControl .= '</select>';

    }





    $sortControl .= '</div>';



    return $sortControl;

}



function getSortControls($sectionId = '')

{



    if ($sectionId == CLASSIFIEDS_ID) {



        return array('created_desc', 'created_asc', 'price_asc', 'price_desc');



    } else if ($sectionId == EVENTS_ID) {



        return array('event_date_desc', 'alpha_asc', 'alpha_desc');



    } else if ($sectionId == TOP_PAEDS_ID) {



        return array('rating_desc', 'rating_asc', 'alpha_asc', 'alpha_desc');



    } else if ($sectionId == RECIPES_ID) {



        return array('rating_desc', 'rating_asc', 'alpha_asc', 'alpha_desc');



    } else if ($sectionId == BRESTFEEDING_ID) {



        return array('alpha_asc', 'alpha_desc');



    } else if ($sectionId == FUNWITHMOM_ID) {



        return array('rating_desc', 'rating_asc', 'alpha_asc', 'alpha_desc');



    }  else if (empty($sectionId)) {



        return array('created_desc', 'created_asc', 'price_asc', 'price_desc' , 'alpha_asc', 'alpha_desc', 'event_date_desc');



    } else {



        return array('created_desc', 'rating_asc', 'alpha_asc', 'alpha_desc');



    }

}





function sortControlByHandle($handle, $all = false)

{

    $sortControls = array(

        'created_desc' => array('id' => 1, 'title' => 'Newest to Oldest', 'query' => 'created_date_time DESC', 'extra_desc' => 'Created date'),

        'created_asc' => array('id' => 2, 'title' => 'Oldest to Newest', 'query' => 'created_date_time ASC', 'extra_desc' => 'Created date'),

        'price_asc' => array('id' => 3, 'title' => 'Price Lowest to Highest', 'query' => 'amount ASC'),

        'price_desc' => array('id' => 4, 'title' => 'Price Highest to Lowest', 'query' => 'amount DESC'),

        'rating_desc' => array('id' => 5, 'title' => 'Rating Highest to Lowest', 'query' => 'averageRate DESC'),

        'rating_asc' => array('id' => 6, 'title' => 'Rating Lowest to Highest', 'query' => 'averageRate ASC'),

        'alpha_asc' => array('id' => 7, 'title' => 'Title A to Z', 'query' => 'title ASC'),

        'alpha_desc' => array('id' => 8, 'title' => 'Title Z to A', 'query' => 'title  DESC'),

        'relevance' => array('id' => 9, 'title' => 'Relevance', 'query' => 'sort_order  ASC'),

        //'event_date_desc' => array('id' => 9, 'title' => 'Most Recent', 'query' => 'target_date DESC', 'extra_desc' => 'For event date field'),

    );



    if ($all == true) {

        return $sortControls;

    }



    return !empty($sortControls[$handle]) ? $sortControls[$handle] : false;

}



function getTimeIntervalInMinutes($minutes = 5)

{

    $begin = new DateTime( '2012-08-01 00:00:00' );

    $end = new DateTime( '2012-08-01 24:00:00' );

    $interval = new DateInterval("PT".$minutes."M");

    $daterange = new DatePeriod($begin, $interval ,$end);

    $dateIntervalArray = '';

    foreach($daterange as $date){

        $dateIntervalArray[] = $date->format("h:i A");

    }



    return $dateIntervalArray;

}



function populateSelectCity($cityList, $fieldName = 'city')

{

    $lookupSelect = '<select class="tiny-select" name="'.$fieldName.'"><option value="">Select City</option>';

    if(!empty($cityList) && is_array($cityList)) {

        foreach ($cityList as $city) {

            $selected = '';

            if (!empty($_GET[$fieldName]) && $_GET[$fieldName] == $city->id) {

                $selected = 'selected ';

            }

            $lookupSelect .= '<option ' . $selected . 'value="' . $city->id . '">' . ($city->combined) . '</option>';

        }

    }

    $lookupSelect .= '</select>';



    return $lookupSelect;

}



function populateSelectCategory($itemsList, $fieldName = 'category')

{

    $lookupSelect = '<select class="tiny-select" name="'.$fieldName.'"><option value="">Select Category</option>';

    if(!empty($itemsList) && is_array($itemsList)) {

        foreach ($itemsList as $item) {

            $selected = '';

            if (!empty($_GET[$fieldName]) && $_GET[$fieldName] == $item->id) {

                $selected = 'selected ';

            }

            $lookupSelect .= '<option ' . $selected . 'value="' . $item->id . '">' . ($item->title) . '</option>';

        }

    }

    $lookupSelect .= '</select>';



    return $lookupSelect;

}







function populateSelectFromLookup($itemsList, $fieldName, $paceholder)

{

    $lookupSelect = '<select class="tiny-select" name="'.$fieldName.'"><option value="">'.$paceholder.'</option>';

    if(!empty($itemsList) && is_array($itemsList)) {

        foreach ($itemsList as $item) {

            $selected = '';

            if (!empty($_GET[$fieldName]) && $_GET[$fieldName] == $item->id) {

                $selected = 'selected ';

            }

            $lookupSelect .= '<option ' . $selected . 'value="' . $item->id . '">' . ($item->title) . '</option>';

        }

    }

    $lookupSelect .= '</select>';



    return $lookupSelect;

}



function populateFilterControl($sectionId, $currentUrl, $serializedFilterControls = '')

{

    $filterControls = ((!empty($serializedFilterControls) && $serializedFilterControls != '-1') ? unserialize($serializedFilterControls) : array());

    if(empty($filterControls) OR in_array('-1', $filterControls)) {

        return false;

    }

    $CI =& get_instance();

    $CI->load->database();



    $CI->db->from('tbl_filter_options');

    $CI->db->where('is_active', '1');

    $CI->db->where('is_deleted', '0');

    $CI->db->where_in('id', $filterControls);



    $query = $CI->db->get();

    $queryData = $query->result() ;

//print_r($queryData);

    $filterControls = '';



    if (!empty($queryData) && is_array($queryData)) {

        $filterControls .= '<div class="filter-group-wrap">';

        $filterControls .= '<form method="get" action="">';

        $filterControls .= '<input type="hidden" name="filter" value="1">';

        $ctr = 1;

        foreach ($queryData as $result) {

            if($ctr%3 == 0){$no_marg = 'no-marg-right'; }else {$no_marg = '';}

            $filterControls .= '<div class="filter-item-wrap '.$no_marg.'">';

            if ($result->isLookUp == 1) {

                //Get lookup lists

                //lookupTable

                $lookupDataFunction = !empty($result->lookupDataFunction) ? $result->lookupDataFunction : '';

                $lookupDataFunction = (empty($lookupDataFunction) OR !function_exists($lookupDataFunction)) ? 'getLookupFromTable' : $lookupDataFunction;

                $lookUpPopulateFunction = !empty($result->lookUpPopulateFunction) ? $result->lookUpPopulateFunction : '';

                $lookUpPopulateFunction = (empty($lookUpPopulateFunction) OR !function_exists($lookUpPopulateFunction)) ? 'populateSelectFromLookup' : $lookUpPopulateFunction;

                $paceholder = !empty($result->display_head) ? $result->display_head : $result->title;

                //lookupDataFunction

                $lookUpDataList = $lookupDataFunction($result->lookupTable);

                //lookUpPopulateFunction

                $filterControls .= $lookUpPopulateFunction($lookUpDataList, $result->handle, $paceholder);

                //echo $filterControls;

            } else if ($result->field_type == 'text') {

                $paceholder = !empty($result->display_head) ? $result->display_head : $result->title;

                $selected = !empty($_GET[$result->handle]) ? $_GET[$result->handle] : '';

                $filterControls .= '<input type="text" name="'.$result->handle.'" placeholder="'.$paceholder.'" value="'.$selected.'">';

            } else if ($result->field_type == 'date') {

                $paceholder = !empty($result->display_head) ? $result->display_head : $result->title;

                $selected = !empty($_GET[$result->handle]) ? $_GET[$result->handle] : '';

                $filterControls .= '<input class="filter-date-picker" type="text" name="'.$result->handle.'" placeholder="'.$paceholder.'" value="'.$selected.'">';

            }

            //print_r($result);

            $filterControls .= '</div>';

            $ctr++;

        }



        $filterControls .= '<div class="filter-submit-wrap no-marg-right">';

        $filterControls     .= '<input type="submit" value="SEARCH">';

        $filterControls .= '</div>';

        $filterControls .= '</form>';

        $filterControls .= '</div>';

    }



    return $filterControls;

    /*$this->db->select($field);

    $this->db->where('user_id',$user_id);



    $query = $this->db->get('tbl_member');

    if($query->num_rows()==1)return $query->row();



    return NULL;*/

}



function prepareLookUpItems($table)

{

    $CI =& get_instance();

    $CI->load->database();



    $CI->db->from($table);

    //$CI->db->where('is_active', '1');

    //$CI->db->where('is_deleted', '0');



    $query = $CI->db->get();

    return $query->result();

}



function prepareCategoryItems($table)

{

    $CI =& get_instance();

    $CI->load->database();



    $CI->db->from($table);

    $CI->db->where('parent_id', CURRENT_SECTION_ID);

    $CI->db->where('is_active', '1');

    $CI->db->where('is_deleted', '0');



    $query = $CI->db->get();

    return $query->result();

}

function getLookupFromTable($table)

{

    $CI =& get_instance();

    $CI->load->database();



    $CI->db->from($table);

    $CI->db->where('is_active', '1');

    $CI->db->where('is_deleted', '0');



    $query = $CI->db->get();

    return $query->result();

}



function getSettingsByName($settingsName)

{

    $CI =& get_instance();

    $CI->load->database();



    $CI->db->where('default_title', $settingsName);

    $CI->db->from('tbl_setting');

    $query = $CI->db->get();

    //echo $CI->db->last_query();



    return $query->row();

}



function showEditSettings($settingsName = '', $class = '')

{

    $CI =& get_instance();

    if ($CI->isAdminLoggedIn) {

        if (!empty($settingsName)) {

            $dataObj = getSettingsByName($settingsName);

            $url = ADMIN_ROOT_URL . 'setting/add/' . $dataObj->id . '?frontendUrl=' . base64_encode(current_url());

        } else {

            $url = ADMIN_ROOT_URL . 'setting?groupEdit=1&frontendUrl=' . base64_encode(current_url());

        }

        return '<a class="edit_div admin_a_link settings '.$class.'" href="' . $url . '">Edit</a>';

    } else {

        return false;

    }

}



function showEditBanner($dataObj, $class = '')

{

    $CI =& get_instance();

    if ($CI->isAdminLoggedIn) {

        if (!empty($dataObj->id)) {

            $url = ADMIN_ROOT_URL . 'advertize/add/' . $dataObj->id . '?frontendUrl=' . base64_encode(current_url());

        } else {

            $url = ADMIN_ROOT_URL . 'advertize?frontendUrl=' . base64_encode(current_url());

        }

        return '<a class="edit_div admin_a_link settings '.$class.'" href="' . $url . '">Edit</a>';

    } else {

        return false;

    }

}



function showEditCmsPage($dataObj = '', $class = '', $wrap_pre = '', $wrap_post = '')

{

    $CI =& get_instance();

    if ($CI->isAdminLoggedIn) {

        if (!empty($dataObj->id)) {

            $url = ADMIN_ROOT_URL . 'cms/add/' . $dataObj->id . '?frontendUrl=' . base64_encode(current_url());

        } else {

            $url = ADMIN_ROOT_URL . 'cms?groupEdit=1&frontendUrl=' . base64_encode(current_url());

        }

        return $wrap_pre.'<a class="edit_div admin_a_link settings '.$class.'" href="' . $url . '">Edit</a>'.$wrap_post;

    } else {

        return false;

    }

}



function showEditmeettheteamPage($dataObj = '', $class = '', $wrap_pre = '', $wrap_post = '')

{

    $CI =& get_instance();

    if ($CI->isAdminLoggedIn) {

        if (!empty($dataObj->id)) {

            $url = ADMIN_ROOT_URL . 'meettheteam/add/' . $dataObj->id . '?frontendUrl=' . base64_encode(current_url());

        } else {

            $url = ADMIN_ROOT_URL . 'meettheteam?groupEdit=1&frontendUrl=' . base64_encode(current_url());

        }

        return $wrap_pre.'<a class="edit_div admin_a_link settings '.$class.'" href="' . $url . '">Edit</a>'.$wrap_post;

    } else {

        return false;

    }

}



function showEditCategory($dataObj = '', $class = '', $wrap_pre = '', $wrap_post = '')

{

    $CI =& get_instance();

    if ($CI->isAdminLoggedIn) {

        if (!empty($dataObj->id)) {

            $url = ADMIN_ROOT_URL . 'generic_category/add/' . $dataObj->id . '?frontendUrl=' . base64_encode(current_url());

        } else {

            $url = ADMIN_ROOT_URL . 'generic_category?groupEdit=1&frontendUrl=' . base64_encode(current_url());

        }

        return $wrap_pre.'<a class="edit_div admin_a_link settings '.$class.'" href="' . $url . '">Edit</a>'.$wrap_post;

    } else {

        return false;

    }

}


function showEditDirectoryCategory($dataObj = '', $class = '', $wrap_pre = '', $wrap_post = '')

{

    $CI =& get_instance();

    if ($CI->isAdminLoggedIn) {

        if (!empty($dataObj->id)) {

            $url = ADMIN_ROOT_URL . 'directory_category/add/' . $dataObj->id . '?frontendUrl=' . base64_encode(current_url());

        } else {

            $url = ADMIN_ROOT_URL . 'directory_category?groupEdit=1&frontendUrl=' . base64_encode(current_url());

        }

        return $wrap_pre.'<a class="edit_div admin_a_link settings '.$class.'" href="' . $url . '">Edit</a>'.$wrap_post;

    } else {

        return false;

    }

}


function showEditPost($dataObj = '', $class = '')

{

    $CI =& get_instance();

    if ($CI->isAdminLoggedIn) {

        if (!empty($dataObj->id)) {

            $url = ADMIN_ROOT_URL . 'data_entry/add/' . $dataObj->id . '?'.(!empty($dataObj->section_id) ? 'section_id='.$dataObj->section_id.'&' : '').'frontendUrl=' . base64_encode(current_url());

        } else {

            $url = ADMIN_ROOT_URL . 'data_entry?frontendUrl=' . base64_encode(current_url());

        }

        return '<a class="edit_div admin_a_link settings '.$class.'" href="' . $url . '">Edit</a>';

    } else {

        return false;

    }

}



function showEditBannerpost($dataObj = '', $class = '')

{

    $CI =& get_instance();

    if ($CI->isAdminLoggedIn) {

        if (!empty($dataObj->id)) {

            $url = ADMIN_ROOT_URL . 'advertize/add/' . $dataObj->id .'?'.'frontendUrl=' . base64_encode(current_url());

        } else {

            $url = ADMIN_ROOT_URL . 'advertize?frontendUrl=' . base64_encode(current_url());

        }

        return '<a class="edit_div admin_a_link settings '.$class.'" href="' . $url . '">Edit</a>';

    } else {

        return false;

    }

}







function showEditDirectory($dataObj = '', $class = '', $wrap_pre = '', $wrap_post = '')

{

    $CI =& get_instance();

    if ($CI->isAdminLoggedIn) {

        if (!empty($dataObj->id)) {

            $url = ADMIN_ROOT_URL . 'directory_category/add/' . $dataObj->id . '?frontendUrl=' . base64_encode(current_url());

        } else {

            $url = ADMIN_ROOT_URL . 'directory_category?groupEdit=1&frontendUrl=' . base64_encode(current_url());

        }

        return $wrap_pre.'<a class="edit_div admin_a_link settings '.$class.'" href="' . $url . '">Edit</a>'.$wrap_post;

    } else {

        return false;

    }

}



function showEditDirectorypost($dataObj = '', $class = '')

{

    $CI =& get_instance();

    if ($CI->isAdminLoggedIn) {

        if (!empty($dataObj->id)) {

            $url = ADMIN_ROOT_URL . 'directory_entry/add/' . $dataObj->id . '?'.(!empty($dataObj->section_id) ? 'section_id='.$dataObj->section_id.'&' : '').'frontendUrl=' . base64_encode(current_url());

        } else {

            $url = ADMIN_ROOT_URL . 'directory_entry?frontendUrl=' . base64_encode(current_url());

        }

        return '<a class="edit_div admin_a_link settings '.$class.'" href="' . $url . '">Edit</a>';

    } else {

        return false;

    }

}

function getPaginationConfig() {
    $config['full_tag_open'] = '<ul class="pagination_ul" id="pagination">';
    $config['full_tag_close'] = '</ul>';

    /*$config['first_link'] = '<span aria-hidden="true">First</span>';;
    $config['first_tag_open'] = '<li>';
    $config['first_tag_close'] = '</li>';*/

    $config['prev_link'] = 'Prev';
    $config['prev_tag_open'] = '<li class="prev_pagination">';
    $config['prev_tag_close'] = '</li>';

    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';

    $config['next_link'] = 'Next';
    $config['next_tag_open'] = '<li class="next_pagination">';
    $config['next_tag_close'] = '</li>';

    $config['cur_tag_open'] = '<li class="active_pagination"><a class="active">';
    $config['cur_tag_close'] = '</a></li>';

    /*$config['last_link'] = '<span aria-hidden="true">Last</span>';;
    $config['last_tag_open'] = '<li>';
    $config['last_tag_close'] = '</li>';*/

    return $config;
}

function prepare_retreat_details($retreat_details) {
//print_r($retreat_details);
    $return = array();
    if (!empty($retreat_details['dollar']) && is_array($retreat_details['dollar'])) {
        foreach ($retreat_details['dollar'] as $key => $value) {
            $return[$key]['dollar'] = $value;
        }
    }
    if (!empty($retreat_details['euro']) && is_array($retreat_details['euro'])) {
        foreach ($retreat_details['euro'] as $key => $value) {
            $return[$key]['euro'] = $value;
        }
    }
    //print_r($return);
    return $return;
}

function generate_clean_file_name($string) {
    $string_parts = pathinfo($string);
    $string = !empty($string_parts['filename']) ? $string_parts['filename'] : 'upload_file';

    setlocale(LC_ALL, "en_US.UTF8");
    //Remove everything except alphabets and digits
    $new_string = preg_replace("/[^a-z0-9]/i"," ", ltrim(rtrim($string)));
    //Remove multiple spaces
    $new_string = preg_replace("/\s+/", " ", $new_string);
    //Replace space with dashes
    $new_string = str_replace(" ","-", $new_string);

    return $new_string;
}
/*End of file utility_helper.php. Location application/helper*/