<div class="footer_main">
  <div class="site">
    <div class="newsletter_div">
      <h4>Subscribe to Our Newsletter</h4>
      <p>Sign up for special offers and discounts of your next retreat and to <br/>
        keep informed of new exciting locations that we will be launching <br/>
        in the near future.</p>
      <form>
        <input type="text" placeholder="Enter Your Email Address" />
        <input class="hvr-bounce-to-right" type="submit" value="Sign Up" />
      </form>
      <h4>Stay Connected With Us</h4>
      <ul class="socail_ul">
        <li> <a class="hvr-float" href=""><img src="<?php echo (defined('ROOT_URL_BASE')) ? ROOT_URL_BASE.'../' : '';?>images/icon/icon2.jpg" /></a> </li>
        <li> <a class="hvr-float" href=""><img src="<?php echo (defined('ROOT_URL_BASE')) ? ROOT_URL_BASE.'../' : '';?>images/icon/icon3.jpg" /></a> </li>
        <li> <a class="hvr-float" href=""><img src="<?php echo (defined('ROOT_URL_BASE')) ? ROOT_URL_BASE.'../' : '';?>images/icon/icon4.jpg" /></a> </li>
        <li> <a class="hvr-float" href=""><img src="<?php echo (defined('ROOT_URL_BASE')) ? ROOT_URL_BASE.'../' : '';?>images/icon/icon8.jpg" /></a> </li>
      </ul>
    </div>
    <div class="testmonial_footer">
      <h4>Testimonials</h4>
      <p>Don't just take it from us, let our customers do the talking!</p>
      <ul class="testmonial_ul">
        <li><span class="left_qu01 mobile_view"></span>Elite Yoga Retreats are definitely a class above the rest. I have been on many yoga retreats but this was on another level. The hotel where we stayed was absolutely magnificent and the food was amazing. I felt a million dollars afterwards and can't wait to book another retreat in 2016. <span class="right_qu01"></span><span class="testmonial_name">
          <label>Zoe Ball</label>
          / A Yoga Retreat Enthusiast</span></li>
        <li><span class="left_qu01 mobile_view"></span>I had an absolutely amazing time. It was the perfect combination and the whole experience was 5 star and nothing less. The waldorf was breathtaking, after the yoga I would soak in some much needed vitamin D by the pool and enjoy the skyline of Dubai with it's iconic structures in the background from the Burj Al arab to the Burj Khalifa.<span class="right_qu01"></span><span class="testmonial_name">
          <label>Tom Davidson</label>
          / A rejuvenated City Slicker</span></li>
        <li><span class="left_qu01 mobile_view"></span>It was magnificent; the yoga sessions undertaken by Derya not only improved my physical fitness but also helped with my mind. The Palm Dubai was the perfect venue; it offered breath-taking views of Dubai and doing yoga on the Arabian Sea with the dusty purple sunset in the background was truly magical<span class="right_qu01"></span><span class="testmonial_name">
          <label>Angela Smith</label>
          / A more positive Inspiring Yogi</span></li>
      </ul>
    </div>
    <div class="border_div"></div>
    <ul class="footer_menu">
    	<li <?php if($activepage=='index' || $activepage=='V5' || $activepage=='V4' || $activepage=='V6' || $activepage==''){ echo 'class="active"'; } ?>><a href="<?php echo (defined('ROOT_URL_BASE')) ? ROOT_URL_BASE.'../' : '';?>index">Home</a></li>
        <li <?php if($activepage=='packages'){ echo 'class="active"'; } ?>><a href="<?php echo (defined('ROOT_URL_BASE')) ? ROOT_URL_BASE.'../' : '';?>packages">Packages</a></li>
        <li <?php if($activepage=='tailorpackages'){ echo 'class="active"'; } ?>><a href="<?php echo (defined('ROOT_URL_BASE')) ? ROOT_URL_BASE.'../' : '';?>tailorpackages">Tailor made Packages</a></li>
        <li <?php if($activepage=='partners'){ echo 'class="active"'; } ?>><a href="<?php echo (defined('ROOT_URL_BASE')) ? ROOT_URL_BASE.'../' : '';?>partners">Partners</a></li>
        <li <?php if($activepage=='csr'){ echo 'class="active"'; } ?>><a href="<?php echo (defined('ROOT_URL_BASE')) ? ROOT_URL_BASE.'../' : '';?>csr">Csr</a></li>
        <li <?php if($activepage=='testimonials'){ echo 'class="active"'; } ?>><a href="<?php echo (defined('ROOT_URL_BASE')) ? ROOT_URL_BASE.'../' : '';?>testimonials">Testimonials</a></li>
        <li <?php if($activepage=='aboutus'){ echo 'class="active"'; } ?>><a href="<?php echo (defined('ROOT_URL_BASE')) ? ROOT_URL_BASE.'../' : '';?>aboutus">About Us</a></li>
        <li <?php if($activepage=='contactus'){ echo 'class="active"'; } ?>><a href="<?php echo (defined('ROOT_URL_BASE')) ? ROOT_URL_BASE.'../' : '';?>contactus">Contact Us</a></li>
    </ul>
    <p class="copy_p">&copy; 2016 <a href="<?php echo (defined('ROOT_URL_BASE')) ? ROOT_URL_BASE.'../' : '';?>index.php">Elite Yoga</a>. All Rights Reserved. Designed & Developed by <a href="http://codeandco.ae" target="_blank">Code&Co </a></p>
  </div>
</div>