<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Elite Yoga</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="icon"  href="images/favicon.png">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<link href="fonts/fonts.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" href="css/animate.css">
<link href="css/style.css" type="text/css" rel="stylesheet" />
<link href="css/hover.css" type="text/css" rel="stylesheet" />
<link href="slider/jquery.bxslider.css" rel="stylesheet" type="text/css" />
<link href="css/responsive.css" type="text/css" rel="stylesheet" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="js/parallax.js"></script>
<script type="text/javascript" src="js/wow.min.js"></script>
<script>
    wow = new WOW();
    wow.init();   
  </script>
<script type="text/javascript" src="slider/jquery.bxslider.min.js"></script>
<script src="js/common.js"></script>

</head>

<body>
<?php include_once('header.php');?>
<div class="middle_page_part">
	<div class="site">
        <h2>Tailor made Packages</h2>
        <p>5 star luxurious retreats with only the best yoga, what more do you want for a hen or bachelorette party,<br/>birthday celebration or a relaxing break with exclusive friends.</p>
        <div class="tailor_package_images  slideInLeft wow">
        	<img src="images/tailor_01.jpg" />
            <img src="images/tailor_02.jpg" />
            <img src="images/tailor_03.jpg" />
        </div>
    	
        <div class="tailor_package_text">
        	<p>Are you getting married and would like to arrange an exclusive private yoga retreat for your friends so that you look fabulous on your wedding day?</p>
<p>Or</p>
<p>maybe a group of you are feeling stressed and would like a relaxing break?</p>

<label>Maybe it's your birthday and you want to do a girly yoga break. 
Either way Elite Yoga Retreats can custom design a retreat for you.</label>

<span class=" slideInRight wow"><a href="contactus">Please contact us with your specifications<br />
and we can do a tailor made break for you and your friends. </a>
</span>
        </div>
 
    </div> 
      
</div>

<?php include_once('our_partners.php');?>
<?php include_once('footer.php');?>

</body>
</html>
