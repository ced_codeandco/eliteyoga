<?php $activepage =  basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']); 
?>
<div id="header">
        <div class="site">
            <div class="logo">
                <h1><a href="index.php"></a></h1></div>
            <div class="header_right">
                <div class="email_header">
                    For all your inquiries email us at:
                    <a href="mailto:info@eliteyoga.co">info@eliteyoga.co</a>
                </div>
                <span class="pull-left mobile_view" id="menuclick"><div id="nav-icon3"> <span></span> <span></span> <span></span> <span></span> </div>
            </span>
            <ul class="nav">
                <li <?php if($activepage=='index' || $activepage=='V5' || $activepage=='V4' || $activepage=='V6' || $activepage=='' || $activepage=='elite_yoga'){ echo 'class="active"'; } ?>><a href="index">Home</a></li>
                <li <?php if($activepage=='packages'){ echo 'class="active"'; } ?>><a href="<?php echo (defined('ROOT_URL_BASE')) ? ROOT_URL_BASE.'../' : '';?>packages">Packages</a></li>
                <li <?php if($activepage=='tailorpackages'){ echo 'class="active"'; } ?>><a href="<?php echo (defined('ROOT_URL_BASE')) ? ROOT_URL_BASE.'../' : '';?>tailorpackages">Tailor made Packages</a></li>
                <li <?php if($activepage=='partners'){ echo 'class="active"'; } ?>><a href="<?php echo (defined('ROOT_URL_BASE')) ? ROOT_URL_BASE.'../' : '';?>partners">Partners</a></li>
                <li <?php if($activepage=='csr'){ echo 'class="active"'; } ?>><a href="<?php echo (defined('ROOT_URL_BASE')) ? ROOT_URL_BASE.'../' : '';?>csr">Csr</a></li>
                <li <?php if($activepage=='aboutus'){ echo 'class="active"'; } ?>><a href="<?php echo (defined('ROOT_URL_BASE')) ? ROOT_URL_BASE.'../' : '';?>aboutus">About Us</a></li>
                <li <?php if($activepage=='contactus'){ echo 'class="active"'; } ?>><a href="<?php echo (defined('ROOT_URL_BASE')) ? ROOT_URL_BASE.'../' : '';?>contactus">Contact Us</a></li>
            </ul>
        </div>
    </div>
    </div>