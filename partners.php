<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>Elite Yoga</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="images/favicon.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link href="fonts/fonts.css" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/remodal.css">
    <link rel="stylesheet" href="css/remodal-default-theme.css">
    <link href="css/style.css" type="text/css" rel="stylesheet" />
    <link href="css/hover.css" type="text/css" rel="stylesheet" />
    <link href="slider/jquery.bxslider.css" rel="stylesheet" type="text/css" />
    <link href="css/responsive.css" type="text/css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="js/parallax.js"></script>
    <script src="js/jquery.waypoints.min.js"></script>
    <script type="text/javascript" src="js/wow.min.js"></script>
    <script type="text/javascript" src="js/custom-partners.js"></script>
    <script src="js/remodal.min.js"></script>
    <script>
    wow = new WOW();
    wow.init();
    </script>
    <script type="text/javascript" src="slider/jquery.bxslider.min.js"></script>
    <script src="js/common.js"></script>
</head>

<body>
    <?php include_once('header.php');?>
    <section class="main-section">
        <header>
            <h2>Partners</h2>
            <!-- Keep the below line break -->
            <p>Elite Yoga are honored to be partnered with some of the best yoga instructors in the world that not only work<br/>on your physical being but help you reconnect with your spiritual side.</p>
        </header>
        <div class="container partners-testimonials">
            <div class="row">
                <div class="col-md-3">
                    <article class="instructor-article animated fadeInLeft">
                        <figure>
                            <a href="#derya"><span class="partner-lines">
                                <img src="images/partner-derya.jpg" alt="yoga instructor" />
                            </span></a>
                        </figure>
                        <div class="instructor-writeup">
                            <h4><a href="#derya">Mrs Derya Varol Pasinler</a></h4>
                            <p>Yoga Instructor</p>
                            <p>A certified Sivananda Yoga instructor and is accredited by the Sivananda Yoga Vedanta Center in Kerala, India &amp; 
                            Sivananda ashram Yoga Farm in Grass Valley, CA, USA. </p>
                        </div>
                    </article>
                </div>
                <div class="col-md-3">
                    <article class="instructor-article animated fadeInLeft">
                        <figure>
                            <a href="#damlasuer"><span class="partner-lines">
                                <img src="images/partner-damla.jpg" alt="yoga instructor" />
                            </span></a>
                        </figure>
                        <div class="instructor-writeup">
                            <h4><a href="#damlasuer">Damla Suer</a></h4>
                            <p>Yoga Instructor</p>
                            <p>(Yoga Alliance ID: 142843) started her yoga practice as a student of a Yoga Works certified teacher in 2009, when she was 35 years old.</p>
                        </div>
                    </article>
                </div>
                
            	<div class="col-md-3">
                    <article class="instructor-article animated fadeInRight">
                        <figure>
                            <a href="#keiko"><span class="partner-lines">
                                <img src="images/partner-keiko.jpg" alt="yoga instructor" />
                            </span></a>
                        </figure>
                        <div class="instructor-writeup">
                            <h4><a href="#keiko">Keiko Onishi</a></h4>
                            <p>Resident Yogi Master exclusive for Bali</p>
                            <p>Began practising Iyengar Yoga in 1979; that helped to maintain balance and good health.</p> 
                        </div>
                    </article>
                </div>
                <div class="col-md-3">
                    <article class="instructor-article animated fadeInRight">
                        <figure>
                            <a href="#steve"><span class="partner-lines">
                                <img src="images/partner-steve.jpg" alt="yoga instructor" />
                            </span></a>
                        </figure>
                        <div class="instructor-writeup">
                            <h4><a href="#steve">Steve Bracken</a></h4>
                            <p>Resident Yogi Master exclusive for Bali</p>
                            <p>Scaravelli Inspired Yoga class (suitable for everyone) in the first morning session and in the next session teach a themed class 
                            for common painful conditions.</p>
                        </div>
                    </article>
                </div>
                
                <div class="clearfix"></div>
                
                <div class="col-md-3">
                    <article class="instructor-article animated fadeInRight">
                        <figure>
                        	<a href="#niki"><span class="partner-lines">
                                <img src="images/partner-niki.jpg" alt="yoga instructor" />
                            </span></a>
                        </figure>
                        <div class="instructor-writeup">
                            <h4><a href="#niki">Niki Dalhammer</a></h4>
                            <p>Resident Yogi Master exclusive for Bali</p>
                            <p>Kundalini Yoga Teacher, IKYTA licensed (trained by Gurmukh Kaur Khalsa & Gurushabd Los Angeles, California.</p> 
                        </div>
                    </article>
                </div>
                <div class="col-md-3">
                    <article class="instructor-article animated fadeInRight">
                        <figure>
                            <a href="#matt"><span class="partner-lines">
                                <img src="images/partner-matt.jpg" alt="yoga instructor" />
                            </span></a>
                        </figure>
                        <div class="instructor-writeup">
                            <h4><a href="#matt">Matt Julian</a></h4>
                            <p>Resident Yogi Master exclusive for Bali</p>
                            <p>Matt is for a dynamic vinyasa practice, exploring the feeling of being grounded and connected through an energetic 
                            yet playful flow, with a constant focus on the breath.</p>
                        </div>
                    </article>
                </div>
                <div class="col-md-3">
                    <article class="instructor-article animated fadeInLeft">
                        <figure>
                            <a href="#lisa"><span class="partner-lines">
                                <img src="images/partner-lisa.jpg" alt="yoga instructor" />
                            </span></a>
                        </figure>
                        <div class="instructor-writeup">
                            <h4><a href="#lisa">Lisa Tonelli</a></h4>
                            <p>Resident Yogi Master exclusive for Bali</p>
                            <p>Having spent the last 15 years traveling the globe teaching and training in Iyengar Yoga, Lisa is highly qualified and well regarded.</p>
                        </div>
                    </article>
                </div>                
            </div>
        </div>
    </section>
    <?php include_once('footer.php');?>
    
    <!-- Popup -->
    <div class="remodal partners" data-remodal-id="derya">
        <button data-remodal-action="close" class="remodal-close"></button>
        <img src="images/partner-derya.jpg" />
        <h2>Mrs Derya Varol Pasinler</h2>  
        <p>Derya Pasinler is a certified Sivananda Yoga instructor and is accredited by the Sivananda Yoga Vedanta Center in Kerala, India & Sivananda ashram Yoga Farm in Grass Valley, CA, USA.</p>
        <p>She holds 500 hours teacher training certificate and teaches the principle of Hatha Yoga and Sivananda Yoga. She's a specialist in leading group classes and 
        works with private clients building personalized yoga programs.</p>
        <p>Derya started practicing yoga in 2000 and then decided to share her gift with others. As well as this, she is a member of REP's in the UAE. REP's is an independent professional body, which recognizes the qualifications and expertise of fitness professionals in the UAE. By being a member, she meet's the health and fitness industry's agreed UAE Fitness Occupational Standards.</p>
        <p>During her yoga practice she uses a combination of Hatha Yoga, Sivananda Yoga, Jivamukti Yoga and Anusara Yoga as well as specific breathing 
        techniques which include ujayi breath, kapalabathi, alternate nostril breathing and bandhas.</p>
        <p>Derya incorporates the words of Swami Vishnudevananda into her teaching philosophy.</p>        
        <div class="testmonial_text">
        <span class="left_qu"></span>
        Health is wealth, peace of mind is happiness, Yoga shows the way.
        <span class="right_qu"></span>
        </div>
        
        <h3>Inspiring Messages for Derya</h3>
       	<blockquote>
            <p>I have started practicing yoga with Derya, about 3 years ago and from the moment I met her, her energy enlightened me. Derya is an inspirational and a very attentive yoga instructor with an innate ability to read her students minds. She makes adjustments kindly and accurately and communicates.</p>
            <p>She is always learning and expanding her knowledge and horizons on the spiritual and physical aspects of yoga and what a difference this enthusiasm for learning and sharing makes on her classes.</p>
            <p>
            After every lesson with Derya, I feel lighter, brighter and stronger both physically and spiritually (inside and out).<span class="close-quote"><img src="images/close-quote.png"></span>
            </p>
            <cite>Balkizsumerler</cite>
        </blockquote>
        <hr/ data-animation-name="fadeInUp" data-animation-delay="0.3">
        <blockquote data-animation-name="fadeInUp">
            <p>Yoga in the mornings with Derya is such a peaceful yet energizing way to start the day; once it's done I feel stronger, more relaxed, and more upright! I felt very comfortable from the first session, as she makes sure that all abilities are catered for. She has a lovely and supportive attitude – talk to her about any aches and pains you have, and she'll have some helpful advice.<span class="close-quote"><img src="images/close-quote.png"></span></p>
            <cite>Hannah</cite>
        </blockquote>
        <hr/ data-animation-name="fadeInUp" data-animation-delay="0.3">
        <blockquote data-animation-name="fadeInUp">
            <p>I have been a student of Derya at her Yoga class for the past 6 months. This was my first introduction to yoga. Derya is an excellent instructor, she has made me aware of muscles which I did not know existed. She rapidly gasped my limits and helped me push myself beyond that. She has guided me one-on-one through difficult moves, breathing practices and even when stretchıng. Through her guidance I came to love yoga and cannot wait each week to get to her sessions. Without any doubt, I would recommend Derya as a yoga instructor to anyone, regardless of their level. <span class="close-quote"><img src="images/close-quote.png"></span></p>
            <cite>Erhan</cite>
        </blockquote>
    </div>
    
    <div class="remodal partners" data-remodal-id="damlasuer">
        <button data-remodal-action="close" class="remodal-close"></button>
        <img src="images/partner-damla.jpg" />
        <h2>Damla Suer</h2>  
        <p>(Yoga Alliance ID: 142843) started her yoga practice as a student of a Yoga Works certified teacher in 2009, when she was 35 years old. Regarding this as 'a better late than never' type of an encounter; she kept on practicing and studying about Yoga since then.</p>
        <p>She completed her RYT 200hrs training in Dubai in February 2014 and RYT 300hrs in February 2015 with Dr. Sanjay Sharma from Tattva Yoga Shala in Rishikesh. Dr. Sanjay is a doctorate graduate of Kaivalyadhama in Pune; which was founded by Swami Kuvalayananda in 1924 as one of the oldest Yoga Institute in the world.</p>
        <p>She also holds a 100hrs Yin Yoga teacher training certificate by Jade Wood and Emily Baxter; a 90hrs Yoga Anatomy Principles certificate by Leslie Kaminoff; and a Groovy Kids Yoga Teacher training certificate by Greville Henwood.</p>
        <p>She is currently studying Kaivalyadhama's Online 100hrs Yoga Teacher's Training Course (kdham.org); as she believes in life long learning.</p>
        <p>Damla regards 'Yoga as a science that one proves in her own laboratory i.e. her own body; and a holistic comprehensive theory, which addresses all aspects of a human life (social, personal, physical, physiological, mental, psychological, intellectual and spiritual) as laid out in Patanjali's 8 limb Yoga system'.</p>
    </div>
    
    <div class="remodal partners" data-remodal-id="keiko">
        <button data-remodal-action="close" class="remodal-close"></button>
        <img src="images/partner-keiko.jpg" />
        <h2>Keiko Onishi</h2>  
        <p>Began practising Iyengar Yoga in 1979; that helped to maintain balance and good health.</p> 
		<p>After many years of practice, Keiko wanted to share the numerous benefits of yoga and it was a natural progression to train to be a teacher. Now happily doing special sessions catered for corporate clients bearing in mind their specialist needs. Her background of being brought up in the culture and society of Japan largely based on Buddhist philosophy is supporting her to find meaningful links between Patanjali's yoga philosophy and Buddha's teaching.</p>
    </div>
    
    <div class="remodal partners" data-remodal-id="steve">
        <button data-remodal-action="close" class="remodal-close"></button>
        <img src="images/partner-steve.jpg" />
        <h2>Steve Bracken</h2>  
        <p>Scaravelli Inspired Yoga class (suitable for everyone) in the first morning session and in the next session teach a themed class for common painful conditions.</p>
        <p>Steve is a yoga teacher with an extensive background in other movement disciplines (martial arts, personal training, abilitation). He is also an experienced body worker using soft tissue manipulation, sports therapy, structural bodywork, energy healing, and nutrition to treat injuries and painful conditions. As well as this, Steve teaches atomy on yoga teacher trainings and has published a book taking a holistic view at health called, 'Health for the Whole of You' on amazon kindle. Steve brings all of his knowledge to his yoga classes in a lively and fun manner.  
website: <a href="http://www.mandalahealth.net" target="_blank">www.mandalahealth.net</a></p>
    </div>
    
    <div class="remodal partners" data-remodal-id="niki">
        <button data-remodal-action="close" class="remodal-close"></button>
        <img src="images/partner-niki.jpg" />
        <h2>Niki Dalhammer</h2>  
        <p>Kundalini Yoga Teacher, IKYTA licensed (trained by Gurmukh Kaur Khalsa & Gurushabd Los Angeles, California. Licensed clinical psychodynamic Psychotherapist and Life Coach. Niki holds a masters degree in Social Work, graduated from the University of Nevada (Fulbright Scholar).</p> 
<p>Specializing in treating all psychological imbalances and energy blockages, such as Depression, Anxiety, Mood disorders, ADD, ODD and all symptoms that may occur as a result of divorce, abandonment, break-ups, or other difficulties in relationships. Couples counselling, transitional issues through moving and cultural shock.</p>
    </div>
    
    <div class="remodal partners" data-remodal-id="matt">
        <button data-remodal-action="close" class="remodal-close"></button>
        <img src="images/partner-matt.jpg" />
        <h2>Matt Julian</h2>  
        <p>Matt is for a dynamic vinyasa practice, exploring the feeling of being grounded and connected through an energetic yet playful flow, with a constant focus on the breath.</p>
        <p>By keeping the flow simple and fun, the awareness can drop out of the thinking mind, and tap in to the innate wisdom within our bodies. Matt shares his yoga teaching base between London and Adelaide, and teaches retreats and workshops as he travels. Matt also has a background in health and fitness, with a degree in Human Movement, he is a qualified Health Coach through the Institute of Integrative Nutrition, and was a personal trainer for many years, including being the Head of Fitness for premier health clubs across London, Sydney and Adelaide. All these things contribute to and influence his teaching style today. <a href="http://www.matt.yoga" target="_blank">www.matt.yoga.</a></p>
    </div>
    
    <div class="remodal partners" data-remodal-id="lisa">
        <button data-remodal-action="close" class="remodal-close"></button>
        <img src="images/partner-lisa.jpg" />
        <h2>Lisa Tonelli</h2>  
        <p>Having spent the last 15 years travelling the globe teaching and training in Iyengar Yoga, Lisa is highly qualified and well regarded.</p> 
        <p>She has taught and trained international athletes, company CEOs and corporate executives as well as people from all walks of life. Her philosophy is simple - balance is the key to health and happiness.  Lisa has run a studio in London since 2005  and where she worked with sports teams such as London Irish RFC. She has worked closely with GB Olympic and Chelsea Football Chiropractor (Tom Greenaway) at the Waldegrave Clinic in South West London. Having now returned to Sydney, Lisa has advised corporations on stress management and wellbeing. 
A number of large companies and schools are now providing yoga as way of managing stress and the pressures of modern life.</p>
    </div>
</body>

</html>
